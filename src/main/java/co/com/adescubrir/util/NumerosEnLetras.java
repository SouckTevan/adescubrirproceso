package co.com.adescubrir.util;

public class NumerosEnLetras {
    public static final int MAYUSCULAS = 1;
    public static final int MINUSCULAS = 2;
    public static final int PROPER_CASE = 3;

    public static String Centena(int uni, int dec, int cen)
    {
        String cTexto = "";
        switch (cen)
        {
            case 1:
                if ((dec + uni) == 0)
                    cTexto = "cien ";
                else
                    cTexto = "ciento ";
                break;
            case 2:
                cTexto = "doscientos ";
                break;
            case 3:
                cTexto = "trescientos ";
                break;
            case 4:
                cTexto = "cuatrocientos ";
                break;
            case 5:
                cTexto = "quinientos ";
                break;
            case 6:
                cTexto = "seiscientos ";
                break;
            case 7:
                cTexto = "setecientos ";
                break;
            case 8:
                cTexto = "ochocientos ";
                break;
            case 9:
                cTexto = "novecientos ";
                break;
            default:
                cTexto = "";
                break;
        }
        return cTexto;
    }

    public static String Decena(int uni, int dec)
    {
        String cTexto = "";
        switch (dec)
        {
            case 1:
                switch (uni)
                {
                    case 0:
                        cTexto = "diez ";
                        break;
                    case 1:
                        cTexto = "once ";
                        break;
                    case 2:
                        cTexto = "doce ";
                        break;
                    case 3:
                        cTexto = "trece ";
                        break;
                    case 4:
                        cTexto = "catorce ";
                        break;
                    case 5:
                        cTexto = "quince ";
                        break;
                    default:
                        cTexto = "dieci";
                        break;
                }
                break;
            case 2:
                if (uni == 0)
                    cTexto = "veinte ";
                else
                    cTexto = "veinti";
                break;
            case 3:
                cTexto = "treinta ";
                break;
            case 4:
                cTexto = "cuarenta ";
                break;
            case 5:
                cTexto = "cincuenta ";
                break;
            case 6:
                cTexto = "sesenta ";
                break;
            case 7:
                cTexto = "setenta ";
                break;
            case 8:
                cTexto = "ochenta ";
                break;
            case 9:
                cTexto = "noventa ";
                break;
            default:
                cTexto = "";
                break;
        }
        if ((uni > 0) && (dec > 2))
            cTexto = cTexto + "y ";
        return cTexto;
    }

    public static String Unidad(int uni, int dec)
    {
        String cTexto = "";
        if (dec != 1)
        {
            switch (uni)
            {
                case 1:
                    cTexto = "un ";
                    break;
                case 2:
                    cTexto = "dos ";
                    break;
                case 3:
                    cTexto = "tres ";
                    break;
                case 4:
                    cTexto = "cuatro ";
                    break;
                case 5:
                    cTexto = "cinco ";
                    break;
            }
        }
        switch (uni)
        {
            case 6:
                cTexto = "seis ";
                break;
            case 7:
                cTexto = "siete ";
                break;
            case 8:
                cTexto = "ocho ";
                break;
            case 9:
                cTexto = "nueve ";
                break;
        }
        return cTexto;
    }

    public static String Plural(String Palabra)
    {
        String strPal = "";
        int pos = 0;
        if (Palabra.trim().length() > 0)
        {
            pos = Palabra.compareTo(Palabra.substring(Palabra.length() - 2, Palabra.length() - 1));
            if (pos > 0)
                strPal = Palabra + "s ";
            else
                strPal = Palabra + "es ";
        }
        return strPal;
    }

    public static String NumLet(long Numero)
    {
        String TFNumero = "", NumTmp = "";
        int co1, co2, pos, dig = 0, cen = 0, dec = 0, uni = 0;
        String letra1, letra2, letra3, Leyenda = "";
        NumTmp = completar(Numero);
        co1 = 1;
        pos = 1;
        while (co1 <= 5)
        {
            co2 = 1;
            while (co2 <= 3)
            {
                dig = Integer.parseInt(NumTmp.substring(pos - 1, pos));
                switch (co2)
                {
                    case 1:
                        cen = dig;
                        break;
                    case 2:
                        dec = dig;
                        break;
                    case 3:
                        uni = dig;
                        break;
                }
                co2++;
                pos++;
            }
            letra3 = Centena(uni, dec, cen);
            letra2 = Decena(uni, dec);
            letra1 = Unidad(uni, dec);
            switch (co1)
            {
                case 1:
                    if ((cen + dec + uni) == 1)
                        Leyenda = "billon ";
                    else if ((cen + dec + uni) > 1)
                        Leyenda = "billones ";
                    break;
                case 2:
                    if (((cen + dec + uni) >= 1) && (Integer.parseInt(NumTmp.substring(6, 9)) == 0))
                        Leyenda = "mil millones ";
                    else if ((cen + dec + uni) >= 1)
                        Leyenda = "mil ";
                    break;
                case 3:
                    if ((cen + dec == 0) && (uni == 1))
                        Leyenda = "millon ";
                    else if ((cen > 0) || (dec > 0) || (uni > 1))
                        Leyenda = "millones ";
                    break;
                case 4:
                    if ((cen + dec + uni) >= 1)
                        Leyenda = "mil ";
                    break;
                case 5:
                    if ((cen + dec + uni) >= 1)
                        Leyenda = "";
                    break;
            }
            co1++;
            TFNumero = TFNumero + letra3 + letra2 + letra1 + Leyenda;
            Leyenda = "";
            letra1 = "";
            letra2 = "";
            letra3 = "";
        }
        return TFNumero;
    }

    public static String numerosEnLetras(long Numero, int Estilo)
    {
        String strLetras = "";
        String NumTmp = "";
        NumTmp = completar(Numero);
        if (Numero < 1)
        {
            strLetras = strLetras + "cero " + "Pesos" + " ";
        }
        else
        {
            strLetras = strLetras + NumLet(Numero);
            if ((Integer.parseInt(NumTmp) == 1) || (Integer.parseInt(NumTmp) < 2))
            {
                strLetras = strLetras + "Peso";
            }
            else if ((Integer.parseInt(NumTmp.substring(3, 14)) == 0) || (Integer.parseInt(NumTmp.substring(9, 15)) == 0))
            {
                if (Numero < 10)
                    strLetras = strLetras + " Pesos" + " ";
                else
                    strLetras = strLetras + " de Pesos" + " ";
            }
            else
            {
                strLetras = strLetras + "Pesos" + " ";
            }
        }
        switch (Estilo)
        {
            case MAYUSCULAS:
                strLetras = strLetras.toUpperCase();
                break;
            case MINUSCULAS:
                strLetras = strLetras.toLowerCase();
                break;
            case PROPER_CASE:
                strLetras = properCase(strLetras);
        }
        if (strLetras.startsWith("un mil "))
        {
            strLetras = strLetras.substring(3, strLetras.length() - 1);
        }
        return strLetras;
    }

    public static String completar(long numero)
    {
        String ret = "";
        String temp = "" + numero;
        int tam = temp.length();
        if (15 - tam > 0)
        {
            temp = "";
            for (int i = 0; i < 15 - tam; i++)
            {
                temp = temp + "0";
            }
            ret = temp + "" + numero;
        }
        return ret;
    }

    /**
     *
     * @param cadena
     *            String que se va a convertir en formato de tipo Titulo
     * @return String en formato Titulo
     */
    public static String properCase(String cadena)
    {
        String u = cadena.toUpperCase();
        String fixedProperCase = "";
        String twoPast = " ";
        String lastChar = " ";
        String specialChars = " ~`@#$%^&*()_-+={}[]|\\:;\"'<>,.?/1234567890";
        for (int i = 0; i < cadena.length(); i++)
        {
            String char1 = "" + u.charAt(i);
            if (specialChars.indexOf(lastChar) != -1)
            {
                if ((lastChar == "\'") && (twoPast != "D"))
                {
                    char1 = char1.toLowerCase();
                }
                else if (char1.equals("Y"))
                    char1 = char1.toLowerCase();
            }
            else
            {
                char1 = char1.toLowerCase();
            }
            fixedProperCase = fixedProperCase + "" + char1;
            twoPast = lastChar;
            lastChar = char1;
        }
        fixedProperCase = fixedProperCase.trim();
        return fixedProperCase;
    }

    public NumerosEnLetras()
    {
    }

    public static void main(String[] args)
    {
        System.out.println(NumerosEnLetras.numerosEnLetras(12454575, 2));
    }
}
