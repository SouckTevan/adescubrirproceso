package co.com.adescubrir.util;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Constants implements Serializable
{
    public static final String EMAIL_REGEX = "([a-zA-Z0-9_\\-\\.]+)@((\\[a-z]{1,3}\\.[a-z]{1,3}\\.[a-z]{1,3}\\.)|(([a-zA-Z\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)";
    public static final String DNI_REGEX = "[a-zA-Z0-9]+";
    public static final String DATETIME_FORMAT = "yyyy-MM-dd hh:mm:ss a";
    public static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final String NUMBER_FORMAT = "##0";
    public static final String CURRENCY_FORMAT = "#,##0";
    public static final String ACTIVO = "A";
    public static final String TILDE_A = "\u00C1";
    public static final String TILDE_E = "\u00C9";
    public static final String TILDE_I = "\u00CD";
    public static final String TILDE_O = "\u00D3";
    public static final String TILDE_U = "\u00DA";
    public static final String TILDE_a = "\u00E1";
    public static final String TILDE_e = "\u00E9";
    public static final String TILDE_i = "\u00ED";
    public static final String TILDE_o = "\u00F3";
    public static final String TILDE_u = "\u00FA";
    public static final String HTML_TILDE_A = "&#193";
    public static final String HTML_TILDE_E = "&#201";
    public static final String HTML_TILDE_I = "&#205";
    public static final String HTML_TILDE_O = "&#211";
    public static final String HTML_TILDE_U = "&#218";
    public static final String HTML_TILDE_a = "&#225";
    public static final String HTML_TILDE_e = "&#233";
    public static final String HTML_TILDE_i = "&#237";
    public static final String HTML_TILDE_o = "&#243";
    public static final String HTML_TILDE_u = "&#250";
}
