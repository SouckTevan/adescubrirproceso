package co.com.adescubrir.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import co.com.adescubrir.model.bo.Proveedor;
import co.com.adescubrir.model.bo.Sucursal;
import co.com.adescubrir.model.bo.eventos.CotizacionEvento;
import co.com.adescubrir.model.bo.eventos.TiqueteEvento;
import co.com.adescubrir.service.ApplicationService;
import co.com.mycommons.util.CorreoElectronico;

@RequestMapping("/emailServices/")
@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST})
public class EmailServicesRest {

    private AdministracionServiceRest admin = new AdministracionServiceRest();
    private PDFServicesRest pdfServices = new PDFServicesRest();

    @PostMapping("/enviarMailCotizacion")
    public String enviarMailCotizacion(@RequestBody Map<String, Object> json, HttpServletResponse httpResponse) throws IOException, JSONException {
        ObjectMapper mapper = new ObjectMapper();
        CotizacionEvento cotizacion = mapper.convertValue(json.get("cotizacion"), CotizacionEvento.class);
        String email = (String) json.get("email");

        String retorno = "";
        if (email != null && !"".equals(email))
        {
            String emailPassword = ApplicationService.getParametro("mail." + email + ".password");
            System.out.println(emailPassword);
            if (!"".equals(emailPassword))
            {
                Proveedor proveedor = admin.getProveedor(cotizacion.getProveedor().getId());
                Sucursal sucursal = proveedor.getSucursal(cotizacion.getSucursal());
                System.out.println(cotizacion.getProveedor().getEmail());
                String mailTo = "";
                if (sucursal != null)
                    mailTo = sucursal.getEmail();
                if (mailTo == null || "".equals(mailTo))
                    mailTo = cotizacion.getProveedor().getEmail();
                if (mailTo != null && !"".equals(mailTo))
                {
                    System.out.println(mailTo);
                    pdfServices.generarPDFCotizacion(json, httpResponse);
                    if (CotizacionEvento.ORDEN_SERVICIO.equals(cotizacion.getEstado()))
                    {
                        try
                        {
//                            CorreoElectronico.enviarCorreoPlano(email, ApplicationService.getParametro("mail." + email + ".password"), mailTo, "", "",
//                                    "Orden de Servicio de Evento", getTextoMailOrdenServicio(), "Name", "application/pdf",
//                                    (byte[]) request.getSession().getAttribute("bytesMEDIA"), false);
                            retorno = "Correo electronico enviado a " + mailTo;
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                            retorno = "Enviando correo electronico - " + e.getMessage();
                        }
//                        finally
//                        {
//                            request.getSession().removeAttribute("nombreArchivo");
//                            request.getSession().removeAttribute("bytesMEDIA");
//                            proveedor = null;
//                            sucursal = null;
//                        }
                    }
                    else if (CotizacionEvento.PRELIQUIDACION.equals(cotizacion.getEstado()))
                    {
                        try
                        {
//                            CorreoElectronico.enviarCorreoPlano(email, ApplicationService.getParametro("mail." + email + ".password"), mailTo, "", "",
//                                    "Pre-liquidación de Evento", getTextoMailPreliquidacion(), (String) request.getSession().getAttribute("nombreArchivo"), "application/pdf",
//                                    (byte[]) request.getSession().getAttribute("bytesMEDIA"), false);
                            retorno = "Correo electronico enviado a " + mailTo;
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                            retorno = "ERROR enviando correo electronico - " + e.getMessage();
                        }
//                        finally
//                        {
//                            request.getSession().removeAttribute("nombreArchivo");
//                            request.getSession().removeAttribute("bytesMEDIA");
//                            proveedor = null;
//                            sucursal = null;
//                        }
                    }
                }
                else
                    retorno = "No se tiene email configurado para el proveedor";
            }
            else
                retorno = "No se tiene configurada la clave para el email del usuario";
        }
        else
            retorno = "No se tiene configurado el correo para el usuario";
        return retorno;
    }

    private String getTextoMailOrdenServicio() {
        StringBuffer sb = new StringBuffer();
        sb.append("\n");
        sb.append("Cordial Saludo,\n\n");
        sb.append("Con el presente correo  solicito la realización del Evento descrito en la \"Orden de Servicio\" que se encuentra como archivo adjunto en este mensaje.\n\n");
        sb.append("Alguna inquietud o aclaración será atendida con gusto.\n\n");
        sb.append("Agradecemos su confirmación por este mismo medio.\n\n");
        sb.append("NOTA:\n");
        sb.append("Para realizar el pago de la factura se requiere Factura Individual por evento y  especificar en las observaciones el \"ID Evento\".  Agradecemos su colaboración para así lograr gestiones efectivas.\n\n\n");
        sb.append("--\n");
        sb.append("Coordinación Logística Eventos\n");
        sb.append("Adescubrir Travel & Adventure\n");
        sb.append("Telfax: (6) 8811565\n");
        sb.append("Cel: 312-2861096\n");
        sb.append("\n");
        return sb.toString();
    }

    private String getTextoMailPreliquidacion() {
        StringBuffer sb = new StringBuffer();
        sb.append("\n");
        sb.append("Cordial Saludo,\n\n");
        sb.append("Con el presente correo solicito amablemente la realización de la Pre-liquidación del Evento descrito en el documento \"Orden de Pre-liquidación\" que se encuentra como archivo adjunto en este mensaje.\n\n");
        sb.append("Requiero me informen el valor unitario de los servicios y la disponibilidad para realizar el evento en las fechas relacionadas.\n\n");
        sb.append("Alguna inquietud o aclaración será atendida con gusto.\n\n");
        sb.append("Agradecemos su confirmación por este mismo medio.\n\n");
        sb.append("--\n");
        sb.append("Coordinación Logística Eventos\n");
        sb.append("Adescubrir Travel & Adventure\n");
        sb.append("Telfax: (6) 8811565\n");
        sb.append("Cel: 312-2861096\n");
        sb.append("\n");
        return sb.toString();
    }

    public TiqueteEvento emailTiquete(Map<String, Object> json) {
        int estado = (int) json.get("estado");
        String nombre = (String) json.get("nombre");
        String fechaInicio = (String) json.get("fechaInicio");

        ObjectMapper mapper = new ObjectMapper();
        TiqueteEvento tiquete = mapper.convertValue(json.get("tiquete"), TiqueteEvento.class);

        if (tiquete.getAerolinea() == null)
        {
            tiquete.setAerolinea(admin.getAerolinea(tiquete.getIdAerolinea()));
        }

        if (estado == 7)
        {
            try
            {
                CorreoElectronico.enviarCorreoPlano(ApplicationService.getParametro("mail.notificaciones.eventos.from"), ApplicationService.getParametro("mail.notificaciones.eventos.from.password"),
                        ApplicationService.getParametro("mail.notificaciones.eventos.tiquetes.facturado"), "", "", "Se ha creado un tiquete en evento ya FACTURADO - ID: "
                                + tiquete.getIdEvento(),
                        "Se ha creado un tiquete  en el evento No " + tiquete.getIdEvento() + " y ya se encuentra facturado.\n\nNombre: " + nombre
                                + "\nFecha Inicial: " + DateFormat.getDateInstance().format(fechaInicio)
                                + "\n\n\nDatos ingresados en el tiquete.\n\nPasajero: " + tiquete.getNombrePasajero() + "\nRuta: " + tiquete.getRuta() + "\nFecha Salida: "
                                + DateFormat.getDateInstance().format(tiquete.getFechaSalida()) + "\nAerolinea: "
                                + (tiquete.getAerolinea() == null ? "" : tiquete.getAerolinea().getNombre())
                                + "\n\nPOr favor validar ya que esto representa un cambio en los totales de facturacion", true);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        return tiquete;
    }
}
