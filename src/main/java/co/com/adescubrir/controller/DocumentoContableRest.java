package co.com.adescubrir.controller;

import co.com.adescubrir.business.IBaseService;
import co.com.adescubrir.business.ServiceLocator;
import co.com.adescubrir.model.bo.CuentaBancaria;
import co.com.adescubrir.model.bo.Persona;
import co.com.adescubrir.model.bo.Proveedor;
import co.com.adescubrir.model.bo.contable.*;
import co.com.adescubrir.model.bo.eventos.Contrato;
import co.com.adescubrir.model.bo.eventos.Evento;
import co.com.adescubrir.model.bo.eventos.LegalizacionGasto;
import co.com.adescubrir.model.bo.security.Usuario;
import co.com.adescubrir.service.ApplicationService;
import co.com.adescubrir.util.Constants;
import co.com.mycommons.util.CorreoElectronico;
import co.com.mycommons.util.Formato;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.*;

@SuppressWarnings("unchecked")
@RequestMapping("/documentoContable/")
@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
public class DocumentoContableRest {

    private AdministracionServiceRest adminRest = new AdministracionServiceRest();
    private EventoServiceRest eventoRest = new EventoServiceRest();
    private final Path root = Paths.get("/backups/files/adescubrirWeb/");

    private IBaseService getBaseService() {
        return ServiceLocator.getInstance().getBaseService();
    }

    @PostMapping("/getDocumentosContables")
    public Map<String, Object> consultarDocumentos(@RequestBody Map<String, Object> json) {

        String where = "";
        String whereAnticipo = "";
        if (((int) json.get("id")) > 0) {
            where = "WHERE documentocontable_id = "+((int) json.get("id"));
            whereAnticipo = "WHERE id = "+((int) json.get("id"));
        }else {
            if (((int) json.get("tipo")) > 0) {
                where = "WHERE tipodocumento_id = "+((int) json.get("tipo"));
            }
            if (((int) json.get("idEvento")) > 0) {
                if (where.isEmpty()) {
                    where = "WHERE evento_id = " + ((int) json.get("idEvento"));
                }else {
                    where += " AND evento_id = " + ((int) json.get("idEvento"));
                }
                if (whereAnticipo.isEmpty()) {
                    whereAnticipo = "WHERE evento_id = " + ((int) json.get("idEvento"));
                }else {
                    whereAnticipo += " AND evento_id = " + ((int) json.get("idEvento"));
                }
            }
            if (!json.get("estado").equals("")) {
                if (where.isEmpty()) {
                    where = "WHERE estado = '"+json.get("estado")+"'";
                }else {
                    where += " AND estado = '"+json.get("estado")+"'";
                }
                if (whereAnticipo.isEmpty()) {
                    whereAnticipo = "WHERE estado = '"+json.get("estado")+"'";
                }else {
                    whereAnticipo += " AND estado = '"+json.get("estado")+"'";
                }
            }
            // if (json.get("contrato") != "") {
            //  if (where.isEmpty()) {
                    //  where = "WHERE contrato_id = '"+json.get("contrato")+"'";
                    // }else {
                    // where += " AND contrato_id = '"+json.get("contrato")+"'";
                    // }
            // if (whereAnticipo.isEmpty()) {
                    // whereAnticipo = "WHERE contrato_id = '"+json.get("contrato")+"'";
                    // }else {
                    // whereAnticipo += " AND contrato_id = '"+json.get("contrato")+"'";
                    // }
            //  }
            if (where.isEmpty()) {
                where = "WHERE fecha BETWEEN '" + json.get("fechaInicial") + "' AND '" + json.get("fechaFin") + "'";
            }else {
                where += " AND fecha BETWEEN '" + json.get("fechaInicial") + "' AND '" + json.get("fechaFin") + "'";
            }
            if (whereAnticipo.isEmpty()) {
                whereAnticipo = "WHERE fecha BETWEEN '" + json.get("fechaInicial") + "' AND '" + json.get("fechaFin") + "'";
            }else {
                whereAnticipo += " AND fecha BETWEEN '" + json.get("fechaInicial") + "' AND '" + json.get("fechaFin") + "'";
            }
        }

        String query = "SELECT documentocontable_id, evento_id, persona_id, total, creador_id, (SELECT usuario FROM persona WHERE persona_id = dc.creador_id) as usuario, estado, (SELECT fecha_inicio_evento FROM evento WHERE evento_id = dc.evento_id) AS fechaInicio, (SELECT SUM(total) FROM documentocontable_pago WHERE documentocontable_id = dc.documentocontable_id) AS totalSaldo, (SELECT contrato.nombre FROM evento INNER JOIN contrato ON evento.contrato_id = contrato.contrato_id WHERE evento.evento_id = dc.evento_id LIMIT 1) AS nombreContrato, (SELECT contrato.numero FROM evento INNER JOIN contrato ON evento.contrato_id = contrato.contrato_id WHERE evento.evento_id = dc.evento_id LIMIT 1) AS numeroContrato, (SELECT contrato_id FROM evento WHERE evento.evento_id = dc.evento_id LIMIT 1) AS contrato_id, (SELECT (SELECT (SELECT nombre FROM estado WHERE estado_id = ev.estado_id) FROM evento AS ev WHERE evento_id = dc.evento_id) AS eventoEstado), conceptopago FROM documentocontable AS dc "+where;
        String queryAnticipo = "SELECT id, evento_id, beneficiario_id, valor, creador_id, (SELECT usuario FROM persona WHERE persona_id = an.creador_id) as usuario, estado, (SELECT fecha_inicio_evento FROM evento WHERE evento_id = an.evento_id) AS fechaInicio, (SELECT SUM(total) FROM anticipo_pagos WHERE id_anticipo = an.id) AS totalSaldo, (SELECT contrato.nombre FROM evento INNER JOIN contrato ON evento.contrato_id = contrato.contrato_id WHERE evento.evento_id = an.evento_id LIMIT 1) AS nombreContrato, (SELECT contrato.numero FROM evento INNER JOIN contrato ON evento.contrato_id = contrato.contrato_id WHERE evento.evento_id = an.evento_id LIMIT 1) AS numeroContrato, (SELECT contrato_id FROM evento WHERE evento.evento_id = an.evento_id LIMIT 1) AS contrato_id, (SELECT (SELECT (SELECT nombre FROM estado WHERE estado_id = ev.estado_id) FROM evento AS ev WHERE evento_id = an.evento_id) AS eventoEstado), concepto FROM anticipo AS an "+whereAnticipo;
        System.out.println(query);
        System.out.println(queryAnticipo);

        ArrayList<Object[]> cont = (ArrayList<Object[]>) ServiceLocator.getInstance().getBaseService().executeSQLQuery(query);
        ArrayList<Object[]> contAnticipo = (ArrayList<Object[]>) ServiceLocator.getInstance().getBaseService().executeSQLQuery(queryAnticipo);

        Map<String, Object> dataGroup = new HashMap<>();
        int total = 0;
        int cantidad = 0;

        if (!contAnticipo.isEmpty()) {

            for (Object[] objects : contAnticipo) {
                HomeDocumentoContable data = new HomeDocumentoContable();
                data.setId((int) objects[0]);
                data.setIdEvento((int) objects[1]);
                data.setIdPersona((int) objects[2]);
                data.setTotal((int) objects[3]);
                if (((objects[8]) != null)) {
                    data.setValor(((BigInteger) objects[8]).intValue());
                } else {
                    data.setValor(0);
                }
                data.setSaldo(data.getTotal()-data.getValor());
                data.setPersona(adminRest.getPersona((int) objects[2], false));
                data.setIdCreador((int) objects[4]);
                data.setUsuario((String) objects[5]);
                data.setEstado((String) objects[6]);
                data.setFechaInicio((Date) objects[7]);
                data.setNombreContrato((String) objects[9]);
                data.setNumeroContrato((String) objects[10]);
                data.setEstadoEvento((String) objects[12]);
                data.setNombreConcepto((String) objects[13]);
                data.setAnticipoNuevo(true);
                total += ((int) objects[3]);
                cantidad += 1;
                if (dataGroup.get(((String) objects[9])) == null) {
                    ArrayList<HomeDocumentoContable> dataArray = new ArrayList<>();
                    Map<String, Object> key = new HashMap<>();
                    dataArray.add(data);
                    key.put("total", objects[3]);
                    key.put("key", objects[10] + " - " + objects[9]);
                    key.put("data", dataArray);
                    dataGroup.put(((String) objects[9]), key);
                }else {
                    ((ArrayList<HomeDocumentoContable>) ((Map<String, Object>) dataGroup.get(((String) objects[9]))).get("data")).add(data);
                    int currentTotal = (((int) ((Map<String, Object>) dataGroup.get(((String) objects[9]))).get("total")) + ((int) objects[3]));
                    ((Map<String, Object>) dataGroup.get(((String) objects[9]))).put("total", currentTotal);
                }
            }
        }

        if (!cont.isEmpty()) {
            for (Object[] objects : cont) {
                HomeDocumentoContable data = new HomeDocumentoContable();
                data.setId(((BigInteger) objects[0]).intValue());
                data.setIdEvento((int) objects[1]);
                data.setIdPersona((int) objects[2]);
                data.setTotal(((BigDecimal) objects[3]).intValue());
                if (((objects[8]) != null)) {
                    data.setValor(((BigDecimal) objects[8]).intValue());
                } else {
                    data.setValor(0);
                }
                data.setSaldo(data.getTotal()-data.getValor());
                data.setPersona(adminRest.getPersona((int) objects[2], false));
                data.setIdCreador((int) objects[4]);
                data.setUsuario((String) objects[5]);
                data.setEstado((String) objects[6]);
                data.setFechaInicio((Date) objects[7]);
                data.setNombreContrato((String) objects[9]);
                data.setNumeroContrato((String) objects[10]);
                data.setEstadoEvento((String) objects[12]);
                data.setNombreConcepto((String) objects[13]);
                data.setAnticipoNuevo(false);
                total += (((BigDecimal) objects[3]).intValue());
                cantidad += 1;
                if (dataGroup.get(((String) objects[9])) == null) {
                    ArrayList<HomeDocumentoContable> dataArray = new ArrayList<>();
                    Map<String, Object> key = new HashMap<>();
                    dataArray.add(data);
                    key.put("total", ((BigDecimal) objects[3]).intValue());
                    key.put("key", objects[10] + " - " + objects[9]);
                    key.put("data", dataArray);
                    dataGroup.put(((String) objects[9]), key);
                }else {
                    ((ArrayList<HomeDocumentoContable>) ((Map<String, Object>) dataGroup.get(((String) objects[9]))).get("data")).add(data);
                    int currentTotal = (((int) ((Map<String, Object>) dataGroup.get(((String) objects[9]))).get("total")) + (((BigDecimal) objects[3]).intValue()));
                    ((Map<String, Object>) dataGroup.get(((String) objects[9]))).put("total", currentTotal);
                }
            }
        }
        dataGroup.put("total", total);
        dataGroup.put("cantidad", cantidad);
        return dataGroup;
    }

    @GetMapping("/getDocumentoContable/{idDocumento}/{personaCompleto}")
    public DocumentoContable getDocumentoContable(@PathVariable int idDocumento, @PathVariable boolean personaCompleto) {
        ArrayList<String> properties = new ArrayList<>();
        Map<String, Object> propertyValues = new HashMap<>();
        properties.add("documentocontable_id = :id");
        propertyValues.put("id", idDocumento);

        ArrayList<DocumentoContable> datos = (ArrayList<DocumentoContable>) getBaseService().getByPropertiesNamed(DocumentoContable.class.getName(), properties, propertyValues, "");

        if (!datos.isEmpty()) {
            DocumentoContable retorno = datos.get(0);
            retorno.setPersona(adminRest.getPersona(retorno.getPersona_id(),personaCompleto));
            retorno.setCreador((Usuario) adminRest.getPersona(retorno.getCreador_id(),false));
            retorno.setPagos(getDocumentoPagos((int) retorno.getId()));
            retorno.setTipoDocumento(adminRest.getTipoDocumentoContable(retorno.getTipodocumento_id()));
            return retorno;
        }
        return null;
    }

    @GetMapping("/autorizar/{idDocumento}")
    public DocumentoContable autorizar(@PathVariable int idDocumento) throws Exception {
        DocumentoContable documentoContable = getDocumentoContable(idDocumento, false);
        if (documentoContable != null) {
            documentoContable.setEstado(DocumentoContable.AUTORIZADO);
            documentoContable = saveDocumentoContable(documentoContable);
            Persona persona = adminRest.getPersona(documentoContable.getPersona_id(),true);
            try
            {
                DecimalFormat df = new DecimalFormat("#,##0");
                StringBuffer sb = new StringBuffer();
                sb.append("Se ha autorizado el pago : " + documentoContable.getId() + ".<br><br>\n\n");
                sb.append("A nombre de : " + persona.getNombre() + "<br>\n");
                sb.append("Identificaci"+Constants.HTML_TILDE_o+"n: " + persona.getIdentificacion() + "<br>\n");
                sb.append("<b>Valor : " + df.format(documentoContable.getTotal()) + "</b><br>\n");
                if (persona.getCuentasBancarias().isEmpty())
                {
                    sb.append("Cuenta bancaria : Sin cuenta bancaria\n");
                }
                else
                {
                    for (CuentaBancaria cuenta : persona.getCuentasBancarias())
                    {
                        sb.append("Cuenta bancaria : " + cuenta.getBanco().getNombre() + " " + cuenta.getCuenta() + " " + cuenta.getTipoCuenta() + "\n");
                        sb.append("<b>Titular :</b> " + cuenta.getNombreTitular() + " <b>Identificaci"+Constants.HTML_TILDE_o+"n:</b> " + cuenta.getTitular() + " " + cuenta.getTipoCuenta() + "\n");
                    }
                }
                sb.append("<br>Tipo de Pago : " + documentoContable.getTipoDocumento().getNombre() + "<br>\n");
                sb.append("Fecha Creaci"+Constants.HTML_TILDE_o+"n : " + DateFormat.getDateInstance().format(new Date()) + "<br>\n");
                sb.append("Usuario que genera : " + documentoContable.getCreador().getNombre() + "<br>\n");
                if (documentoContable.getEventoId() != 0)
                {
                    Evento evento = new Evento();
                    evento.setId(documentoContable.getEventoId());
                    evento = eventoRest.getEvento(documentoContable.getEventoId());
                    LegalizacionGasto gasto = new LegalizacionGasto();
                    gasto.setFecha(documentoContable.getFecha());
                    gasto.setTipo("ANTICIPO");
                    if (documentoContable.getPersona().getNombres() == null)
                        gasto.setNombres(persona.getNombre());
                    else
                        gasto.setNombres(persona.getNombres());
                    if (persona.getApellidos() == null)
                        gasto.setApellidos(persona.getNombre());
                    else
                        gasto.setApellidos(persona.getApellidos());
                    gasto.setFechaRegistro(new Date());
                    gasto.setConcepto(documentoContable.getObservaciones());
                    gasto.setIdentificacion(persona.getIdentificacion());
//                    gasto.setUsuario(documentoContable.getCreador());
                    gasto.setIdUsuario(documentoContable.getCreador_id());
                    gasto.setValor(documentoContable.getTotal());
                    gasto.setValorEvento(documentoContable.getTotal());
                    gasto.setDocumentoId(documentoContable.getId());
                    gasto = eventoRest.addLegalizacion(evento, gasto, "gastos");
                    gasto.setIdEvento(documentoContable.getEventoId());
                    gasto = (LegalizacionGasto) getBaseService().saveUpdate(gasto);
                    evento.getGastos().add(gasto);
                    eventoRest.saveEvento(evento);
                    Contrato contrato = adminRest.getContrato(evento.getIdContrato(),false);
                    // Evento evento = (Evento) ServiceLocator.getInstance().getEventosService().getById(Evento.class.getName(), documentoContable.getEventoId(), new String[] { "contrato", "sede" });
                    sb.append("<br>\n<b>Contrato: " + contrato.getNumero() + " - " + contrato.getNombre() + "</b>");
                    sb.append("<br>\nEvento : " + evento.getId() + " - " + evento.getNombre());
                    sb.append("<br>\n<b>Prioridad : " + documentoContable.getPrioridad() + "</b>");
                    sb.append("<br>\nConcepto : " + documentoContable.getConceptoPago());
                    sb.append("<br>\n<b>Fecha Inicio : " + DateFormat.getDateInstance().format(evento.getFechaInicioEvento()) + "</b>");
                    sb.append("<br>\nLugar :" + evento.getSede().getNombre().substring(0,evento.getSede().getNombre().indexOf(" (")));
                } else {
                    sb.append("");
                }
                sb.append("<br>\nObservaciones: " + documentoContable.getObservaciones() + "<br>\n");
                sb.append("<br><br>\n\nPor favor proceder con el pago.");
                sb.append("<br>Ingrese al sistema desde este link http://www.adescubrir.com:8080/adescubrirWeb/pages/session.jsf");
                sb.append("");
                CorreoElectronico.enviarCorreoHtml(ApplicationService.getParametro("mail.notificaciones.eventos.from"), ApplicationService.getParametro("mail.notificaciones.eventos.from.password"),
                        ApplicationService.getParametro("mail.notificaciones.tesoreria.pagos.to"),
                        "", "",
                        "Se ha autorizado el pago No. " + documentoContable.getId()
                                + (documentoContable.getEventoId() != 0 ? " Evento ID: " + documentoContable.getEventoId() : ""), sb.toString(), true);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return documentoContable;
        }
        return null;
    }

    public DocumentoContable saveDocumentoContable(DocumentoContable documento) throws Exception {
        if (documento.getId() == 0)
        {
            if (TipoDocumentoContable.CXC.equals(documento.getTipoDocumentoTxt()))
            {
                documento.setTipoDocumento(adminRest.getTipoDocumentoContable(1));
            }
        }
        documento = (DocumentoContable) getBaseService().saveUpdate(documento);
        return documento;
    }

    @GetMapping("/generar/{idDocumento}/{changeStatus}")
    public DocumentoContable generar(@PathVariable int idDocumento, @PathVariable boolean changeStatus, HttpServletRequest request) throws Exception {
        DocumentoContable documento = getDocumentoContable(idDocumento, false);
        Evento evento = eventoRest.getEvento(documento.getEventoId());
        if (changeStatus) {
            documento.setEstado(DocumentoContable.AUTORIZACION);
        }
        return emailAnticipo(request, evento,documento,false);
    }

    public DocumentoContable emailAnticipo(HttpServletRequest request, Evento evento, DocumentoContable anticipo, boolean reenviar) throws Exception {
        if (!reenviar) {
            anticipo = saveDocumentoContable(anticipo);
        }
        boolean isSolicitado = anticipo.getEstado().equals(Anticipo.SOLICITADO);
        Contrato contrato = adminRest.getContrato(evento.getIdContrato());
        String body = "";

        if (anticipo.getEstado().equals(Anticipo.AUTORIZACION) || anticipo.getEstado().equals(Anticipo.PAGADO) || anticipo.getEstado().equals(Anticipo.AUTORIZADO)) {
            String semilla = "" + Calendar.getInstance().getTimeInMillis();
            String opcion = "APROBAR_ANTICIPO";
            String clave = ApplicationService.getParametro("security.servlet.password.documentocontable");

            body =
                    "</b><br>\nConcepto del pago: <b>" + anticipo.getConceptoPago() + "</b><br>\nPrioridad: <b>" + anticipo.getPrioridad() + "</b><br>\nCiudad: <b>"
                            + evento.getSede().getNombre().substring(0,evento.getSede().getNombre().indexOf(" (")) + "</b><br>\nObservaciones: <b>" + anticipo.getObservaciones() + "</b><br>\nUsuario Creador: <b>"
                            + anticipo.getCreador().getUsuario() + "</b><br>\nFecha Creaci" + Constants.HTML_TILDE_o + "n: <b>"
                            + DateFormat.getDateInstance().format(new Date())
                            + (!Anticipo.PAGADO.equals(anticipo.getEstado())
                            ? "</b>\n\nPor favor proceder a autorizar el anticipo. Menu -> Tesoreria -> Listado Pagos. Consulte por Tipo Anticipo"
                            + "\n\nPara Aprobar de clic en el siguiente link " + "http://" + request.getServerName() + ":"
                            + request.getServerPort() + "/adescubrirWeb/DocumentoContableServlet?documentoID=" + anticipo.getId() + "&opcion="
                            + opcion + "&semilla=" + semilla + "&security="
                            + ApplicationService.MD5(semilla + clave + ("" + anticipo.getId()) + opcion)
                            : "</b><br><br><b>ANTICIPO YA PAGADO</b>");
        }

        if (isSolicitado) {
            body =
                    "</b><br>\nCiudad: <b>" + evento.getSede().getNombre().substring(0,evento.getSede().getNombre().indexOf(" (")) + "</b><br>\nObservaciones: <b>" + anticipo.getObservaciones()
                            + "</b><br>\nUsuario Creador: <b>" + anticipo.getCreador().getUsuario() + "</b><br>\nFecha Creaci" + Constants.HTML_TILDE_o
                            + "n: <b>" + DateFormat.getDateInstance().format(new Date())
                            + "</b><br><br>\n\nPor favor proceder a generar el anticipo en el evento. Dar clic en el boton Genera del anticipo.";
        }

        try {
            String imageStr = eventoRest.base64File("logoGrande.png");

            CorreoElectronico.enviarCorreoHtml(ApplicationService.getParametro("mail.notificaciones.eventos.from"),
                    ApplicationService.getParametro("mail.notificaciones.eventos.from.password"),
                    ApplicationService.getParametro("mail.notificaciones.pagos.autorizacion.to"), "", "",
                    (isSolicitado ? "Solicitud" : "Generación") + " de Anticipo. No: " + anticipo.getId() + ". Evento: " + evento.getId(),
                    "Buen d" + Constants.HTML_TILDE_i + "a,<br>\nSe ha creado el anticipo No: " + anticipo.getId() + ".<br><br>\n\nA nombre de : <b>"
                            + anticipo.getPersona().getNombre() + "</b><br>\nIdentificaci" + Constants.HTML_TILDE_o + "n: <b>"
                            + anticipo.getPersona().getIdentificacion() + "</b><br>\nValor: <b>"
                            + Formato.convertirDoubleACadena(Constants.CURRENCY_FORMAT, anticipo.getTotal()) + "</b><br>\nContrato : <b>"
                            + contrato.getNumero() + " - " + contrato.getNombre() + "</b><br>\nEmpresa del Contrato: <b>"
                            + contrato.getNitCompania() + " - " + contrato.getNombreCompania() + "</b><br>\nEvento: <b>"
                            + evento.getId() + "</b><br>\nFecha Inicio: <b>" + DateFormat.getDateInstance().format(evento.getFechaInicioEvento()) + body
                            + "<br><br><img src='data:image/png;base64," + imageStr + "'\" width=\"200\" height=\"100\"></img>",
                    true);
            // "logoGrande.png", "image/png", b,
        } catch (Exception e) {
            e.printStackTrace();
        }

        return anticipo;
    }

    @GetMapping("/getDocumentoPagos/{idDocumento}")
    public ArrayList<Pago> getDocumentoPagos(@PathVariable int idDocumento) {
        ArrayList<String> properties = new ArrayList<>();
        Map<String, Object> propertyValues = new HashMap<>();
        properties.add("documentocontable_id = :id");
        propertyValues.put("id", idDocumento);

        ArrayList<Pago> datos = (ArrayList<Pago>) getBaseService().getByPropertiesNamed(Pago.class.getName(), properties, propertyValues, "");

        if (!datos.isEmpty()) {
            for (Pago pago: datos) {
                pago.setMedioPago(adminRest.getMedioPago(pago.getIdMedioPago()));
            }
            return datos;
        }
        return new ArrayList<>();
    }

    @PostMapping("/savePago")
    public Pago savePago(@RequestParam("file") MultipartFile file, @RequestParam("data") String data) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, String> json = new HashMap<>();
        try {
            json = mapper.readValue(data, Map.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        DocumentoContable documento = mapper.convertValue(json.get("documento"), DocumentoContable.class);
        Pago pago = mapper.convertValue(json.get("pago"), Pago.class);
        String uploadFileName = file.getOriginalFilename().substring(file.getOriginalFilename().indexOf("_") + 1);
        String fileName = ApplicationService.getFilesDirectory() + "/documentocontable/pago/" + documento.getId() + "_" + uploadFileName;
        pago.setSoporte(fileName);
        try {
            Files.copy(file.getInputStream(), this.root.resolve(fileName));
        } catch (Exception e) {
            throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
        }
        if (pago.getIdBanco() == 0) {
            pago.setBanco(null);
        }
        documento.setPersona(adminRest.getPersona(documento.getPersona_id(),true));
        pago.setIdDocumentoContable((int) documento.getId());

        String queryPago = "SELECT count(documentocontable_id) FROM documentocontable_pago WHERE documentocontable_id = "+(int) documento.getId()+"";
        ArrayList<Object> resPago = (ArrayList<Object>) ServiceLocator.getInstance().getBaseService().executeSQLQuery(queryPago);
        pago.setId((int) documento.getId()+(((BigInteger)resPago.get(0)).intValue()+1));
        pago = (Pago) getBaseService().saveUpdate(pago);
        documento.getPagos().add(pago);

        if (documento.getSaldo() <= 0) {
            documento.setEstado(DocumentoContable.PAGADO);
        }else {
            documento.setEstado(DocumentoContable.CON_SALDO);
        }
        documento = saveDocumentoContable(documento);
        if (pago.getIdMedioPago() == 3) {
            try {
                Evento event = new Evento();
                if (documento.getEventoId() > 0) {
                    event = eventoRest.getEvento(documento.getEventoId());
                }
                DecimalFormat df = new DecimalFormat("#,##0");

                CorreoElectronico.enviarCorreoPlano(ApplicationService.getParametro("mail.notificaciones.eventos.from"),
                        ApplicationService.getParametro("mail.notificaciones.eventos.from.password"),
                        (documento.getEventoId() > 0 ? event.getResponsable().getEmail() : "")
                                + (documento.getPersona().getEmail() != null && !documento.getPersona().getEmail().equals("") ? "," + documento.getPersona().getEmail()
                                : ""),
                        "",
                        "",
                        "Notificacion de pago realizado",
                        "Se ha realizado un pago." + " \n\nA nombre de : " + documento.getPersona().getNombre() + "\nValor: "
                                + Formato.convertirDoubleACadena("#,##0", pago.getTotal()) + "\nObservaciones : " + pago.getObservaciones() + "\nFecha Pago: "
                                + DateFormat.getDateInstance().format(new Date()) + "\n\nPor favor validar el estado del pago en sus cuentas.", true);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        return pago;
    }

    /* ----------------- Anticipos -------------------- */
    @GetMapping("/autorizarAnticipo/{idDocumento}")
    public Anticipo autorizarAnticipo(@PathVariable int idDocumento) throws Exception {
        Anticipo anticipo = getAnticipo(idDocumento);
        if (anticipo != null) {
            anticipo.setEstado(DocumentoContable.AUTORIZADO);
            anticipo = eventoRest.saveAnticipo(anticipo);
            Persona persona = adminRest.getPersona(anticipo.getIdBeneficiario(),true);
            try
            {
                DecimalFormat df = new DecimalFormat("#,##0");
                StringBuffer sb = new StringBuffer();
                sb.append("Se ha autorizado el pago : " + anticipo.getId() + ".<br><br>\n\n");
                sb.append("A nombre de : " + persona.getNombre() + "<br>\n");
                sb.append("Identificaci"+Constants.HTML_TILDE_o+"n: " + persona.getIdentificacion() + "<br>\n");
                sb.append("<b>Valor : " + df.format(anticipo.getValor()) + "</b><br>\n");
                if (persona.getCuentasBancarias() == null || persona.getCuentasBancarias().isEmpty())
                {
                    sb.append("Cuenta bancaria : Sin cuenta bancaria\n");
                }
                else
                {
                    for (CuentaBancaria cuenta : persona.getCuentasBancarias())
                    {
                        sb.append("Cuenta bancaria : " + cuenta.getBanco().getNombre() + " " + cuenta.getCuenta() + " " + cuenta.getTipoCuenta() + "\n");
                        sb.append("<b>Titular :</b> " + cuenta.getNombreTitular() + " <b>Identificaci"+Constants.HTML_TILDE_o+"n:</b> " + cuenta.getTitular() + " " + cuenta.getTipoCuenta() + "\n");
                    }
                }
                sb.append("<br>Tipo de Pago : ANTICIPO<br>\n");
                sb.append("Fecha Creaci"+Constants.HTML_TILDE_o+"n : " + DateFormat.getDateInstance().format(new Date()) + "<br>\n");
                sb.append("Usuario que genera : " + anticipo.getCreador().getNombre() + "<br>\n");
                if (anticipo.getIdEvento() != 0)
                {
                    Evento evento = new Evento();
                    evento.setId(anticipo.getIdEvento());
                    evento = eventoRest.getEvento(anticipo.getIdEvento());
                    LegalizacionGasto gasto = new LegalizacionGasto();
                    gasto.setFecha(anticipo.getFecha());
                    gasto.setTipo("ANTICIPO");
                    if (anticipo.getBeneficiario().getNombres() == null)
                        gasto.setNombres(persona.getNombre());
                    else
                        gasto.setNombres(persona.getNombres());
                    if (persona.getApellidos() == null)
                        gasto.setApellidos(persona.getNombre());
                    else
                        gasto.setApellidos(persona.getApellidos());
                    gasto.setFechaRegistro(new Date());
                    gasto.setConcepto(anticipo.getObservacion());
                    gasto.setIdentificacion(persona.getIdentificacion());
                    gasto.setIdUsuario(anticipo.getIdCreador());
                    gasto.setValor(anticipo.getValor());
                    gasto.setValorEvento(anticipo.getValor());
                    gasto.setDocumentoId(anticipo.getId());
                    gasto.setIdEvento(anticipo.getIdEvento());
                    gasto = eventoRest.addLegalizacion(evento, gasto, "gastos");
                    gasto = (LegalizacionGasto) getBaseService().saveUpdate(gasto);
                    evento.getGastos().add(gasto);
                    eventoRest.saveEvento(evento);
                    Contrato contrato = adminRest.getContrato(evento.getIdContrato(),false);
                    // Evento evento = (Evento) ServiceLocator.getInstance().getEventosService().getById(Evento.class.getName(), documentoContable.getEventoId(), new String[] { "contrato", "sede" });
                    sb.append("<br>\n<b>Contrato: " + contrato.getNumero() + " - " + contrato.getNombre() + "</b>");
                    sb.append("<br>\nEvento : " + evento.getId() + " - " + evento.getNombre());
                    sb.append("<br>\n<b>Prioridad : " + anticipo.getPrioridad() + "</b>");
                    sb.append("<br>\nConcepto : " + anticipo.getConcepto());
                    sb.append("<br>\n<b>Fecha Inicio : " + DateFormat.getDateInstance().format(evento.getFechaInicioEvento()) + "</b>");
                    sb.append("<br>\nLugar :" + evento.getSede().getNombre().substring(0,evento.getSede().getNombre().indexOf(" (")));
                }
                else
                    sb.append("");
                sb.append("<br>\nObservaciones: " + anticipo.getObservacion() + "<br>\n");
                sb.append("<br><br>\n\nPor favor proceder con el pago.");
                sb.append("<br>Ingrese al sistema desde este link http://www.adescubrir.com:8080/adescubrirWeb/pages/session.jsf");
                sb.append("");
                CorreoElectronico.enviarCorreoHtml(ApplicationService.getParametro("mail.notificaciones.eventos.from"), ApplicationService.getParametro("mail.notificaciones.eventos.from.password"),
                        ApplicationService.getParametro("mail.notificaciones.tesoreria.pagos.to"),
                        "", "",
                        "Se ha autorizado el pago No. " + anticipo.getId()
                                + (anticipo.getIdEvento() != 0 ? " Evento ID: " + anticipo.getIdEvento() : ""), sb.toString(), true);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return anticipo;
        }
        return null;
    }

    @GetMapping("/getAnticipo/{idDocumento}")
    public Anticipo getAnticipo(@PathVariable int idDocumento) {
        ArrayList<String> properties = new ArrayList<>();
        Map<String, Object> propertyValues = new HashMap<>();
        properties.add("id = :id");
        propertyValues.put("id", idDocumento);

        ArrayList<Anticipo> datos = (ArrayList<Anticipo>) getBaseService().getByPropertiesNamed(Anticipo.class.getName(), properties, propertyValues, "");

        if (!datos.isEmpty()) {
            Anticipo retorno = datos.get(0);
            retorno.setBeneficiario(adminRest.getProveedor(retorno.getIdBeneficiario()));
            retorno.setCreador((Usuario) adminRest.getPersona(retorno.getIdCreador(),false));
            retorno.setPagos(getAnticipoPagos(retorno.getId()));
            return retorno;
        }
        return null;
    }

    @GetMapping("/getAnticipoPagos/{idDocumento}")
    public ArrayList<AnticipoPagos> getAnticipoPagos(@PathVariable int idDocumento) {
        ArrayList<String> properties = new ArrayList<>();
        Map<String, Object> propertyValues = new HashMap<>();
        properties.add("id_anticipo = :id");
        propertyValues.put("id", idDocumento);

        ArrayList<AnticipoPagos> datos = (ArrayList<AnticipoPagos>) getBaseService().getByPropertiesNamed(AnticipoPagos.class.getName(), properties, propertyValues, "");

        if (!datos.isEmpty()) {
            for (AnticipoPagos pago: datos) {
                pago.setMedioPago(adminRest.getMedioPago(pago.getIdMedioPago()));
            }
            return datos;
        }
        return new ArrayList<>();
    }

    @GetMapping("/generarAnticipo/{idDocumento}/{changeStatus}")
    public Anticipo generarAnticipo(@PathVariable int idDocumento, @PathVariable boolean changeStatus, HttpServletRequest request) throws Exception {
        Anticipo anticipo = getAnticipo(idDocumento);
        Evento evento = eventoRest.getEvento(anticipo.getIdEvento());
        if (changeStatus) {
            anticipo.setEstado(DocumentoContable.AUTORIZACION);
        }
        return eventoRest.emailAnticipo(request, evento, anticipo,false);
    }

    @PostMapping("/saveAnticipoPago")
    public AnticipoPagos saveAnticipoPago(@RequestParam("file") MultipartFile file, @RequestParam("data") String data) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, String> json = new HashMap<>();
        try {
            json = mapper.readValue(data, Map.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Anticipo anticipo = mapper.convertValue(json.get("documento"), Anticipo.class);
        AnticipoPagos anticipoPago = mapper.convertValue(json.get("pago"), AnticipoPagos.class);
        String uploadFileName = file.getOriginalFilename().substring(file.getOriginalFilename().indexOf("_") + 1);
        String fileName = ApplicationService.getFilesDirectory() + "/documentocontable/anticipos/" + anticipo.getId() + "_" + uploadFileName;
        anticipoPago.setSoporte(fileName);
        try {
            Files.copy(file.getInputStream(), this.root.resolve(fileName));
        } catch (Exception e) {
            throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
        }
        if (anticipoPago.getIdBanco() == 0) {
            anticipoPago.setBanco(null);
        }
        anticipo.setBeneficiario((Proveedor) adminRest.getPersona(anticipo.getIdBeneficiario(),true));
        anticipoPago.setIdAnticipo(anticipo.getId());
        anticipoPago = (AnticipoPagos) getBaseService().saveUpdate(anticipoPago);
        anticipo.getPagos().add(anticipoPago);
        if (anticipo.getSaldo() <= 0) {
            anticipo.setEstado(DocumentoContable.PAGADO);
        }else {
            anticipo.setEstado(DocumentoContable.CON_SALDO);
        }
        anticipo = eventoRest.saveAnticipo(anticipo);
        if (anticipoPago.getIdMedioPago() == 3) {
            try {
                Evento event = new Evento();
                if (anticipo.getIdEvento() > 0) {
                    event = eventoRest.getEvento(anticipo.getIdEvento());
                    event.setResponsable(adminRest.getUsuario(event.getIdResponsable(),false));
                }
                DecimalFormat df = new DecimalFormat("#,##0");
                CorreoElectronico.enviarCorreoPlano(ApplicationService.getParametro("mail.notificaciones.eventos.from"),
                        ApplicationService.getParametro("mail.notificaciones.eventos.from.password"),
                        (anticipo.getIdEvento() > 0 ? event.getResponsable().getEmail() : "")
                                + (anticipo.getBeneficiario().getEmail() != null && !anticipo.getBeneficiario().getEmail().equals("") ? "," + anticipo.getBeneficiario().getEmail()
                                : ""),
                        "",
                        "",
                        "Notificacion de pago realizado",
                        "Se ha realizado un pago." + " \n\nA nombre de : " + anticipo.getBeneficiario().getNombre() + "\nValor: "
                                + Formato.convertirDoubleACadena("#,##0", anticipoPago.getTotal()) + "\nObservaciones : " + anticipoPago.getObservaciones() + "\nFecha Pago: "
                                + DateFormat.getDateInstance().format(new Date()) + "\n\nPor favor validar el estado del pago en sus cuentas.", true);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        return anticipoPago;
    }
}
