package co.com.adescubrir.controller;

import co.com.adescubrir.business.IBaseService;
import co.com.adescubrir.business.ServiceLocator;
import co.com.adescubrir.model.bo.*;
import co.com.adescubrir.model.bo.contable.IndicadorRetencion;
import co.com.adescubrir.model.bo.contable.MedioPago;
import co.com.adescubrir.model.bo.contable.TipoDocumentoContable;
import co.com.adescubrir.model.bo.eventos.*;
import co.com.adescubrir.model.bo.security.*;
import co.com.adescubrir.util.JwtUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyObject;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import org.codehaus.groovy.control.CompilationFailedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.lang.Number;

@SuppressWarnings("unchecked")
@RequestMapping("/administracion/")
@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
public class AdministracionServiceRest {

	private IBaseService getBaseService() {
		return ServiceLocator.getInstance().getBaseService();
	}

	private final Path root = Paths.get("/backups/files/adescubrirWeb/");

	@Autowired
	private JwtUtil jwtUtil;

	@Autowired
	private AuthenticationManager authenticationManager;

	// ----------- Retorna todas las personas ---------
	@GetMapping("/getPersonas")
	public List<Persona> getPersonas() {
		ArrayList<Persona> retorno = new ArrayList<Persona>();
		String query = "SELECT persona_id, nombre, apellidos, razonsocial FROM persona WHERE estado = 'A';";

		ArrayList<Object[]> cont = (ArrayList<Object[]>) ServiceLocator.getInstance().getBaseService().executeSQLQuery(query);

		if (!cont.isEmpty()) {
			for (Object[] objects : cont) {
				Persona people = new Persona();
				people.setId((int) objects[0]);
				people.setNombre((String) objects[1]);
				people.setApellidos((String) objects[2]);
				people.setRazonSocial((String) objects[3]);
				retorno.add(people);
			}
		}

		return retorno;
	}

	// ---------- Retorna una persona buscada por id ------
	@GetMapping("/getPersona/{persona_id}")
	public Persona getPersona(@PathVariable int persona_id, boolean... isCompleto) {
		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();
		properties.add("persona_id = :persona_id");
		propertyValues.put("persona_id", persona_id);

		ArrayList<Persona> datos = (ArrayList<Persona>) getBaseService().getByPropertiesNamed(Persona.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			Persona retorno = datos.get(0);
			if (isCompleto == null || isCompleto[0]) {
				retorno.setMunicipio(getMunicipioById(retorno.getMunicipio_id()));
				retorno.setContactos(getContactosByPersonId(retorno.getId()));
				retorno.setCuentasBancarias(getCuentasBancarias(retorno.getId()));
				retorno.setSucursales(getSucursalesByPersonId(retorno.getId()));
				retorno.setDocumentos(getRegistroDocumentoByPersonId(retorno.getId()));
			}
			return retorno;
		}

		return null;
	}

	// ---------- Retorna una persona buscada por id ------
	public Persona getPersonaByUsername(String username) {
		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();
		properties.add("usuario = :username");
		propertyValues.put("username", username);

		ArrayList<Persona> datos = (ArrayList<Persona>) getBaseService().getByPropertiesNamed(Persona.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			Persona retorno = datos.get(0);
			return retorno;
		}

		return null;
	}

	// // ------- Devuelve todos los proveedores referidos en la tabla persona (Fk)
	// @GetMapping("/getProveedores")
	// public ArrayList<HomeProveedor> getProveedores() {
	// ArrayList<HomeProveedor> retorno = new ArrayList<>();
	//
	// ArrayList<String> properties = new ArrayList<String>();
	// Map<String, Object> propertyValues = new HashMap<String, Object>();
	//
	// ArrayList<Proveedor> datos = (ArrayList<Proveedor>) getBaseService().getByPropertiesNamed(Proveedor.class.getName(),properties, propertyValues, "");
	//
	// for (Proveedor prov: datos) {
	// HomeProveedor homeProv = new HomeProveedor();
	// homeProv.setId(prov.getId());
	// homeProv.setNombre(prov.getNombre());
	// homeProv.setIdentificacion(prov.getIdentificacion());
	// homeProv.setCelular(prov.getCelular());
	// homeProv.setRazonSocial(prov.getRazonSocial());
	// homeProv.setEmail(prov.getEmail());
	// homeProv.setTipoProveedor(getTipoProveedor(prov.getTipoProveedor_id()));
	// homeProv.setMunicipio(getMunicipioById(prov.getMunicipio_id()));
	// if (prov.getEstado().equals("A")) {
	// retorno.add(homeProv);
	// }
	// }
	// if (!retorno.isEmpty()) {
	// return retorno;
	// }
	// return null;
	// }

	// ------- Devuelve todos los proveedores referidos en la tabla persona (Fk)
	@GetMapping("/getProveedores")
	public ArrayList<HomeProveedor> getProveedores() {
		ArrayList<HomeProveedor> retorno = new ArrayList<>();

		String query = "SELECT persona_id, nombre, identificacion, celular, telefono, razonsocial, email,";
		 	   query += "(SELECT nombre FROM municipio WHERE municipio_id = prn.municipio_id) as municipio, ";
			   query += "(SELECT string_agg((SELECT nombre FROM tipoproveedor WHERE tipoproveedor_id = pt.id_tipo), ', ') AS nombre FROM proveedor_tipo AS pt WHERE id_proveedor = prv.id) AS nombreTipo";
		       query += " FROM persona as prn INNER JOIN proveedor as prv ON prn.persona_id = prv.id WHERE prn.estado = 'A';";

		ArrayList<Object[]> cont = (ArrayList<Object[]>) ServiceLocator.getInstance().getBaseService().executeSQLQuery(query);

		if (!cont.isEmpty()) {
			for (Object[] objects : cont) {
				HomeProveedor homeProv = new HomeProveedor();
				homeProv.setId((int) objects[0]);
				homeProv.setNombre((String) objects[1]);
				homeProv.setIdentificacion((String) objects[2]);
				homeProv.setCelular((String) objects[3]);
				homeProv.setTelefono((String) objects[4]);
				homeProv.setRazonSocial((String) objects[5]);
				homeProv.setEmail((String) objects[6]);
				homeProv.setMunicipio((String) objects[7]);
				homeProv.setTipoProveedor((String) objects[8]);
				retorno.add(homeProv);
			}
		}

		return retorno;
	}

	// ------- Devuelve todos los proveedores referidos en la tabla persona (Fk)
	@GetMapping("/getProveedoresByMunicipio/{idMunicipio}")
	public ArrayList<Proveedor> getProveedoresByMunicipio(@PathVariable("idMunicipio") int idMunicipio) {
		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();
		properties.add("municipio_id = :municipio_id");
		propertyValues.put("municipio_id", idMunicipio);

		ArrayList<Proveedor> datos = (ArrayList<Proveedor>) getBaseService().getByPropertiesNamed(Proveedor.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			return datos;
		}
		return null;
	}

	// -------------
	// @param id del proveedor
	// @return un proveedor con toda la informacion
	// -------------
	@GetMapping("/getProveedor/{id}")
	public Proveedor getProveedor(@PathVariable("id") int id) {

		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		properties.add("id = :id");
		propertyValues.put("id", id);

		ArrayList<Proveedor> datos = (ArrayList<Proveedor>) getBaseService().getByPropertiesNamed(Proveedor.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			Proveedor retorno = datos.get(0);
			retorno.setMunicipio(getMunicipioById(retorno.getMunicipio_id()));
			retorno.setContactos(getContactosByPersonId(retorno.getId()));
			retorno.setCuentasBancarias(getCuentasBancarias(retorno.getId()));
			retorno.setSucursales(getSucursalesByPersonId(retorno.getId()));
			retorno.setDocumentos(getRegistroDocumentoByPersonId(retorno.getId()));
			retorno.setTarifas(getProveedorTarifa(retorno.getId()));
			retorno.setTipos(getTiposByProveedor(retorno.getId(),true));
			return retorno;
		}

		return null;
	}

	// -------------
	// @param id del contacto
	// @return Todos los contactos con el tipo de contacto y el municipio
	// -------------
	@GetMapping("/getContacto")
	public List<Contacto> getContactosByPersonId(@PathVariable("id") int id) {
		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();
		properties.add("persona_id = :persona_id");
		propertyValues.put("persona_id", id);

		ArrayList<Contacto> datos = (ArrayList<Contacto>) getBaseService().getByPropertiesNamed(Contacto.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			for (Contacto tpContacto : datos) {
				tpContacto.setTipoContacto(getTipoContacto(tpContacto.getTipoContacto_id()));
				tpContacto.setMunicipio(getMunicipioById(tpContacto.getMunicipio_id()));
			}
		}

		return datos;
	}

	// -------------
	// @param id del tipo de contacto
	// @return Devuelve un tipo de contacto
	// -------------
	@GetMapping("/getTipoContacto/{id}")
	public TipoContacto getTipoContacto(@PathVariable("id") int id) {

		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();
		properties.add("tipocontacto_id = :tipocontacto_id");
		propertyValues.put("tipocontacto_id", id);

		ArrayList<TipoContacto> datos =
				(ArrayList<TipoContacto>) getBaseService().getByPropertiesNamed(TipoContacto.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			TipoContacto retorno = datos.get(0);
			return retorno;
		}

		return null;
	}

	// -------------
	// @return Devuelve todos los tipos de contacto
	// -------------
	@GetMapping("/getTiposContactos")
	public Collection<TipoContacto> getTiposContactos() {

		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();

		ArrayList<TipoContacto> datos =
				(ArrayList<TipoContacto>) getBaseService().getByPropertiesNamed(TipoContacto.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			return datos;
		}

		return null;
	}

	// -------------
	// @param Objeto Tipo Concepto a guardar
	// @return Un Tipo Concepto con todos los datos
	// -------------
	@PostMapping("/saveTipoContacto")
	TipoContacto saveTipoContacto(@RequestBody TipoContacto tipoContacto) throws Exception {

		// ---------- Se guarda en la tabla Estado
		tipoContacto = (TipoContacto) getBaseService().saveUpdate(tipoContacto);

		return tipoContacto;
	}

	// -------------
	// @return Todos las tarifas
	// -------------
	@GetMapping("/getTipoTarifa")
	public Collection<TipoTarifa> getTiposTarifas() {
		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();

		ArrayList<TipoTarifa> datos = (ArrayList<TipoTarifa>) getBaseService().getByPropertiesNamed(TipoTarifa.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			return datos;
		}

		return null;
	}

	// -------------
	// @parama Id de la tarifa
	// @return Un tarifa
	// -------------
	@GetMapping("/getTipoTarifaById/{idTarifa}")
	public TipoTarifa getTipoTarifaById(@PathVariable("idTarifa") int id) {
		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		properties.add("tipotarifa_id = :tipoTarifa_id");
		propertyValues.put("tipoTarifa_id", id);

		ArrayList<TipoTarifa> datos = (ArrayList<TipoTarifa>) getBaseService().getByPropertiesNamed(TipoTarifa.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			return datos.get(0);
		}

		return null;
	}

	// -------------
	// @param Objeto Tipo Evento a guardar
	// @return Un Tipo Evento con todos los datos
	// -------------
	@PostMapping("/saveTipoTarifa")
	TipoTarifa saveTipoTarifa(@RequestBody TipoTarifa tipoTarifa) throws Exception {

		// ---------- Se guarda en la tabla Estado
		tipoTarifa = (TipoTarifa) getBaseService().saveUpdate(tipoTarifa);

		return tipoTarifa;
	}

	// -------------
	// @param id del proveedor
	// @return Las sucursales con su banco y el municipio
	// -------------
	@GetMapping("/getSucursales/{id}")
	public List<Sucursal> getSucursalesByPersonId(@PathVariable("id") int id) {
		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();
		properties.add("persona_id = :persona_id");
		propertyValues.put("persona_id", id);

		ArrayList<Sucursal> datos = (ArrayList<Sucursal>) getBaseService().getByPropertiesNamed(Sucursal.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			for (Sucursal sucursal : datos) {
				sucursal.setMunicipio(getMunicipioById(sucursal.getMunicipio_id()));
				sucursal.setCuentaBancaria(getCuentaBancaria(sucursal.getId()));
			}
		}

		return datos;
	}

	// -------------
	// @param id del proveedor
	// @return Una sucursal con su banco y el municipio
	// -------------
	@GetMapping("/getSucursal/{id}")
	public Sucursal getSucursalByPersonId(@PathVariable("id") int id) {
		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();
		properties.add("persona_id = :persona_id");
		propertyValues.put("persona_id", id);

		ArrayList<Sucursal> datos = (ArrayList<Sucursal>) getBaseService().getByPropertiesNamed(Sucursal.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			Sucursal sucursal = datos.get(0);
			sucursal.setMunicipio(getMunicipioById(sucursal.getMunicipio_id()));
			sucursal.setCuentaBancaria(getCuentaBancaria(sucursal.getId()));

			return sucursal;
		}

		return null;
	}

	// -------------
	// @param
	// @return Todos los municipios
	// -------------
	@GetMapping("/getMunicipios")
	public Collection<Municipio> getMunicipios() {

		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();

		ArrayList<Municipio> datos = (ArrayList<Municipio>) getBaseService().getByPropertiesNamed(Municipio.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			for (Municipio mun : datos) {
				mun.setNombre(mun.getNombre() + " (" + mun.getDane() + ")");
			}
			return datos;
		}

		return null;
	}

	// -------------
	// @param id del municipio
	// @return Un municipio con su departamento
	// -------------
	@GetMapping("/getMunicipioById/{id}")
	public Municipio getMunicipioById(int id) {

		if (id != 0) {
			ArrayList<String> properties = new ArrayList<String>();
			Map<String, Object> propertyValues = new HashMap<String, Object>();
			properties.add("municipio_id = :municipio_id");
			propertyValues.put("municipio_id", id);

			ArrayList<Municipio> datos =
					(ArrayList<Municipio>) getBaseService().getByPropertiesNamed(Municipio.class.getName(), properties, propertyValues, "");

			if (!datos.isEmpty()) {
				Municipio retorno = datos.get(0);
				retorno.setNombre(retorno.getNombre() + " (" + retorno.getDane() + ")");
				retorno.setDepartamento(getDepartamento((retorno.getDepartamento_id())));
				return retorno;
			}
		}

		return null;
	}

	// -------------
	// @param id del municipio
	// @return Un municipio con su departamento
	// -------------
	@GetMapping("/getMunicipiosByEstado/{estado}")
	public Collection<Municipio> getMunicipiosByEstado(@PathVariable("estado") String estado) {

		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();
		properties.add("estado = :estado");
		propertyValues.put("estado", estado);

		ArrayList<Municipio> datos = (ArrayList<Municipio>) getBaseService().getByPropertiesNamed(Municipio.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			for (Municipio mun : datos) {
				mun.setNombre(mun.getNombre() + " (" + mun.getDane() + ")");
			}
		}
		return datos;
	}

	public Municipio getMunicipioByIdSinDep(int id) {

		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();
		properties.add("municipio_id = :municipio_id");
		propertyValues.put("municipio_id", id);

		ArrayList<Municipio> datos = (ArrayList<Municipio>) getBaseService().getByPropertiesNamed(Municipio.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			Municipio retorno = datos.get(0);
			return retorno;
		}

		return null;
	}

	// -------------
	// @param id del departamento
	// @return Todos los municipios de un departamento
	// -------------
	@GetMapping("/getMunicipioByDep/{idDepartamento}")
	public Collection<Municipio> getMunicipioByDepartamento(@PathVariable("idDepartamento") int idDepartamento) {

		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();
		properties.add("departamento_id = :departamento_id");
		propertyValues.put("departamento_id", idDepartamento);

		ArrayList<Municipio> datos = (ArrayList<Municipio>) getBaseService().getByPropertiesNamed(Municipio.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			for (Municipio mun : datos) {
				mun.setNombre(mun.getNombre() + " (" + mun.getDane() + ")");
			}
			return datos;
		}

		return null;
	}

	// -------------
	// @param id del proveedor/persona
	// @return Todas las cuentas bancarias de un proveedor
	// -------------
	public List<CuentaBancaria> getCuentasBancarias(int id) {
		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();
		properties.add("persona_id = :persona_id");
		propertyValues.put("persona_id", id);

		ArrayList<CuentaBancaria> datos =
				(ArrayList<CuentaBancaria>) getBaseService().getByPropertiesNamed(CuentaBancaria.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			for (CuentaBancaria ctaBancaria : datos) {
				ctaBancaria.setBanco(getBanco(ctaBancaria.getBanco_id()));
			}
			return datos;
		}

		return null;
	}

	// -------------
	// @param id del proveedor/persona
	// @return Una cuentas bancarias de un proveedor
	// -------------
	public CuentaBancaria getCuentaBancaria(int id) {
		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();
		properties.add("persona_id = :persona_id");
		propertyValues.put("persona_id", id);

		ArrayList<CuentaBancaria> datos =
				(ArrayList<CuentaBancaria>) getBaseService().getByPropertiesNamed(CuentaBancaria.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			CuentaBancaria retorno = datos.get(0);
			retorno.setBanco(getBanco(retorno.getBanco_id()));

			return retorno;
		}

		return null;
	}

	// -------------
	// @param id del banco
	// @return Un banco
	// -------------
	@GetMapping("/getBanco/{id}")
	public Banco getBanco(@PathVariable("id") int id) {
		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();
		properties.add("banco_id = :banco_id");
		propertyValues.put("banco_id", id);

		ArrayList<Banco> datos = (ArrayList<Banco>) getBaseService().getByPropertiesNamed(Banco.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			Banco retorno = datos.get(0);
			return retorno;
		}

		return null;
	}

	// -------------
	// @return Todos los bancos
	// -------------
	@GetMapping("/getBancos")
	public Collection<Banco> getBancos() {

		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();

		ArrayList<Banco> datos = (ArrayList<Banco>) getBaseService().getByPropertiesNamed(Banco.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			return datos;
		}

		return null;
	}

	// -------------
	// @param Objeto Documentos a guardar
	// @return Un Documentos con todos los datos
	// -------------
	@PostMapping("/saveBanco")
	Banco saveDocumento(@RequestBody Banco banco) throws Exception {

		// ---------- Se guarda en la tabla Estado
		banco = (Banco) getBaseService().saveUpdate(banco);

		return banco;
	}

	// -------------
	// @param id del proveedor/persona
	// @return Todos los registros de documento con su documento
	// -------------
	@GetMapping("/getRegistroDocumento")
	public List<RegistroDocumento> getRegistroDocumentoByPersonId(@PathVariable("id") int id) {
		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();
		properties.add("persona_id = :persona_id");
		propertyValues.put("persona_id", id);

		ArrayList<RegistroDocumento> datos =
				(ArrayList<RegistroDocumento>) getBaseService().getByPropertiesNamed(RegistroDocumento.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			for (RegistroDocumento rgDocumento : datos) {
				rgDocumento.setDocumento(getDocumento(rgDocumento.getDocumento_id()));

				String query = "SELECT persona_id, nombres, nombre, apellidos FROM persona WHERE persona_id =" + rgDocumento.getUsuario_id();

				ArrayList<Object[]> cont = (ArrayList<Object[]>) ServiceLocator.getInstance().getBaseService().executeSQLQuery(query);

				if (!cont.isEmpty()) {
					for (Object[] objects : cont) {
						Usuario user = new Usuario();
						user.setId((int) objects[0]);
						if (((String) objects[1]) == null) {
							if ((String) objects[3] == null) {
								user.setNombres((String) objects[2]);
							} else {
								user.setNombres((String) objects[2] + " " + objects[3]);
							}
						} else {
							user.setNombres((String) objects[1]);
						}
						rgDocumento.setUsuario(user);
					}
				}
			}
			return datos;
		}

		return null;
	}

	// -------------
	// @return Todos los departamentos
	// -------------
	@GetMapping("/getDepartamentos")
	public Collection<Departamento> getDepartamentos() {

		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();

		ArrayList<Departamento> datos =
				(ArrayList<Departamento>) getBaseService().getByPropertiesNamed(Departamento.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			for (Departamento dep : datos) {
				dep.setNombre(dep.getNombre() + " (" + dep.getDane() + ")");
			}
			return datos;
		}

		return null;
	}

	// ------- Devuelve un departamento con su pais
	public Departamento getDepartamento(int id) {

		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();
		properties.add("departamento_id = :departamento_id");
		propertyValues.put("departamento_id", id);

		ArrayList<Departamento> datos =
				(ArrayList<Departamento>) getBaseService().getByPropertiesNamed(Departamento.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			Departamento retorno = datos.get(0);
			retorno.setPais(getPais(retorno.getPais_id()));
			return retorno;
		}

		return null;
	}

	// ------- Devuelve un pais
	public Pais getPais(int id) {
		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();
		properties.add("pais_id = :pais_id");
		propertyValues.put("pais_id", id);

		ArrayList<Pais> datos = (ArrayList<Pais>) getBaseService().getByPropertiesNamed(Pais.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			Pais retorno = datos.get(0);
			return retorno;
		}

		return null;
	}

	// -------------
	// @return Devuelve todos los tipos de contacto
	// -------------
	@GetMapping("/getTiposConceptos")
	public Collection<TipoConcepto> getTiposConceptos() {

		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();

		ArrayList<TipoConcepto> datos =
				(ArrayList<TipoConcepto>) getBaseService().getByPropertiesNamed(TipoConcepto.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			return datos;
		}

		return null;
	}

	// -------------
	// @param id del tipo concepto
	// @return Todos los conceptos que pertenecen al tipo de concepto
	// -------------
	@GetMapping("/getConceptoByTpConcepto/{idTipoConcepto}")
	public List<Concepto> getConceptoByTipoConcepto(@PathVariable("idTipoConcepto") int id) {
		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();
		properties.add("tipoconcepto_id = :tipoconcepto_id");
		propertyValues.put("tipoconcepto_id", id);

		ArrayList<Concepto> datos = (ArrayList<Concepto>) getBaseService().getByPropertiesNamed(Concepto.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			return datos;
		}

		return null;
	}

	// -------------
	// @param id del tipo concepto
	// @return Un concepto
	// -------------
	@GetMapping("/getConceptoById/{idConcepto}")
	public Concepto getConceptoById(@PathVariable("idConcepto") int id) {
		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();
		properties.add("concepto_id = :concepto_id");
		propertyValues.put("concepto_id", id);

		ArrayList<Concepto> datos = (ArrayList<Concepto>) getBaseService().getByPropertiesNamed(Concepto.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			return datos.get(0);
		}

		return null;
	}

	// -------------
	// @param id del Proveedor
	// @return Todas las tarifas de un proveedor
	// -------------
	@GetMapping("/getProveedorTarifa/{idProveedor}")
	public Collection<Tarifa> getProveedorTarifa(@PathVariable("idProveedor") int id) {
		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();
		properties.add("proveedor_id = :proveedor_id");
		propertyValues.put("proveedor_id", id);

		ArrayList<Tarifa> datos = (ArrayList<Tarifa>) getBaseService().getByPropertiesNamed(Tarifa.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			for (Tarifa tarifa : datos) {
				tarifa.setConcepto(getConceptoById(tarifa.getConcepto_id()));
				tarifa.setTipoTarifa(getTipoTarifaById(tarifa.getTipotarifa_id()));
				tarifa.setSucursal(getSucursalByPersonId(id));
			}
			return datos;
		}

		return null;
	}

	// -------------
	// @param Objeto proveedor a guardar
	// @return Un proveedor con todos los datos
	// -------------
	@PostMapping("/saveProveedor")
	Proveedor saveProveedor(@RequestParam("file[]") MultipartFile[] files, @RequestParam("persona") String prov) throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		Proveedor proveedor = objectMapper.readValue(prov, Proveedor.class);

		// ---------- Se guarda en la tabla persona
		proveedor = (Proveedor) getBaseService().saveUpdate(proveedor);

		// ---------- Se consultan los datos actuales
		// List<Contacto> contactosActuales = getContactosByPersonId(proveedor.getId());
		// List<CuentaBancaria> cuentaBanActuales = getCuentasBancarias(proveedor.getId());
		// List<Sucursal> sucursalActuales = getSucursalesByPersonId(proveedor.getId());
		Collection<Tarifa> tarifaActuales = getProveedorTarifa(proveedor.getId());
		// List<RegistroDocumento> documentosActuales = getRegistroDocumentoByPersonId(proveedor.getId());

		// // --------- Se borran los contacto que no esten
		// if (contactosActuales != null) {
		// for (Contacto contactoAc : contactosActuales) {
		// if (!proveedor.getContactos().contains(contactoAc)) {
		// getBaseService().delete(contactoAc);
		// }
		// }
		// }
		//
		// // --------- Se borran las cuentas bancarias que no esten
		// if (cuentaBanActuales != null) {
		// for (CuentaBancaria cuentaBn : cuentaBanActuales) {
		// if (!proveedor.getCuentasBancarias().contains(cuentaBn)) {
		// getBaseService().delete(cuentaBn);
		// }
		// }
		// }
		//
		// // --------- Se borran las sucursales que no esten
		// if (sucursalActuales != null) {
		// for (Sucursal sucursal : sucursalActuales) {
		// if (!proveedor.getSucursales().contains(sucursal)) {
		// getBaseService().delete(sucursal);
		// }
		// }
		// }

		// --------- Se borran las tarifas que no esten
		if (tarifaActuales != null) {
			for (Tarifa tarifa : tarifaActuales) {
				if (!proveedor.getTarifas().contains(tarifa)) {
					getBaseService().delete(tarifa);
				}
			}
		}

		// --------- Se borran los documentos que no esten
		// if (documentosActuales != null) {
		// for (RegistroDocumento doc : documentosActuales) {
		// if (!proveedor.getDocumentos().contains(doc)) {
		// getBaseService().delete(doc);
		// }
		// }
		// }

		// // --------- Se guarda el objeto Persona Contacto
		// for (Contacto contacto: proveedor.getContactos()) {
		// contacto.setId(proveedor.getId());
		// getBaseService().saveUpdate(contacto);
		// }
		//
		// // --------- Se guarda el objeto Persona Cuentas Bancarias
		// for (CuentaBancaria cuentaBancaria: proveedor.getCuentasBancarias()) {
		// cuentaBancaria.setId(proveedor.getId());
		// getBaseService().saveUpdate(cuentaBancaria);
		// }
		//
		// // --------- Se guarda el objeto Persona Sucursal
		// for (Sucursal sucursal: proveedor.getSucursales()) {
		// sucursal.setId(proveedor.getId());
		// getBaseService().saveUpdate(sucursal);
		// }

		// --------- Se guarda el objeto Proveedor Tarifa
		for (Tarifa tarifa : proveedor.getTarifas()) {
			tarifa.setProveedor_id(proveedor.getId());
			getBaseService().saveUpdate(tarifa);
		}

		List<ProveedorTipo> tiposActuales = getTiposByProveedor(proveedor.getId(), false);

		// --------- Se borran los contacto que no estén
		if (tiposActuales != null) {
			for (ProveedorTipo tiposAc : tiposActuales) {
				System.out.println(tiposAc);
				if (!proveedor.getTipos().contains(tiposAc)) {
					getBaseService().delete(tiposAc);
				}
			}
		}

		// --------- Se guarda el objeto Proveedor Tipos
		for (ProveedorTipo tipo : proveedor.getTipos()) {
			tipo.setIdProveedor(proveedor.getId());
			tipo.setIdTipo(tipo.getIdTipo());
			getBaseService().saveUpdate(tipo);
		}

		refreshDataPerson(proveedor, files);
		// // --------- Se guarda el objeto Persona Documentos
		// if (files != null) {
		// for (RegistroDocumento doc : proveedor.getDocumentos()) {
		// doc.setPersona_id(proveedor.getId());
		// for (MultipartFile file : files) {
		// if (file.getOriginalFilename().equals(doc.getNombreArchivo())) {
		// String fileName = proveedor.getId() + "_" + doc.getFechaHora().getTime() + "_" + file.getOriginalFilename();
		// doc.setRutaArchivo("/backups/files/adescubrirWeb/proveedores/" + fileName);
		// try {
		// Files.copy(file.getInputStream(), this.root.resolve("proveedores/" + fileName));
		// } catch (Exception e) {
		// throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
		// }
		// getBaseService().saveUpdate(doc);
		// }
		// }
		// }
		// }

		return proveedor;
	}

	// -------------
	// @param Objeto proveedor a guardar
	// @return Un proveedor con todos los datos
	// -------------
	@PostMapping("/savePersona")
	Persona savePersona(@RequestParam("file[]") MultipartFile[] files, @RequestParam("persona") String person) throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		Persona persona = objectMapper.readValue(person, Persona.class);

		// ---------- Se guarda en la tabla persona
		persona = (Persona) getBaseService().saveUpdate(persona);

		refreshDataPerson(persona, files);

		return persona;
	}

	public void refreshDataPerson(Persona person, MultipartFile[] files) throws Exception {
		List<Contacto> contactosActuales = getContactosByPersonId(person.getId());
		List<CuentaBancaria> cuentaBanActuales = getCuentasBancarias(person.getId());
		List<Sucursal> sucursalActuales = getSucursalesByPersonId(person.getId());
		List<RegistroDocumento> documentosActuales = getRegistroDocumentoByPersonId(person.getId());

		// --------- Se borran los contacto que no esten
		if (contactosActuales != null) {
			for (Contacto contactoAc : contactosActuales) {
				if (!person.getContactos().contains(contactoAc)) {
					getBaseService().delete(contactoAc);
				}
			}
		}

		// --------- Se borran las cuentas bancarias que no esten
		if (cuentaBanActuales != null) {
			for (CuentaBancaria cuentaBn : cuentaBanActuales) {
				if (!person.getCuentasBancarias().contains(cuentaBn)) {
					getBaseService().delete(cuentaBn);
				}
			}
		}

		// --------- Se borran las sucursales que no esten
		if (sucursalActuales != null) {
			for (Sucursal sucursal : sucursalActuales) {
				if (!person.getSucursales().contains(sucursal)) {
					getBaseService().delete(sucursal);
				}
			}
		}

		if (documentosActuales != null) {
			for (RegistroDocumento doc : documentosActuales) {
				if (!person.getDocumentos().contains(doc)) {
					getBaseService().delete(doc);
				}
			}
		}

		// --------- Se guarda el objeto Persona Contacto
		for (Contacto contacto : person.getContactos()) {
			contacto.setId(person.getId());
			getBaseService().saveUpdate(contacto);
		}

		// --------- Se guarda el objeto Persona Cuentas Bancarias
		for (CuentaBancaria cuentaBancaria : person.getCuentasBancarias()) {
			cuentaBancaria.setId(person.getId());
			getBaseService().saveUpdate(cuentaBancaria);
		}

		// --------- Se guarda el objeto Persona Sucursal
		for (Sucursal sucursal : person.getSucursales()) {
			sucursal.setId(person.getId());
			getBaseService().saveUpdate(sucursal);
		}

		// --------- Se guarda el objeto Persona Documentos
		if (files != null) {
			for (RegistroDocumento doc : person.getDocumentos()) {
				doc.setPersona_id(person.getId());
				for (MultipartFile file : files) {
					if (file.getOriginalFilename().equals(doc.getNombreArchivo())) {
						String fileName = person.getId() + "_" + doc.getFechaHora().getTime() + "_" + file.getOriginalFilename();
						doc.setRutaArchivo("/backups/files/adescubrirWeb/proveedores/" + fileName);
						try {
							Files.copy(file.getInputStream(), this.root.resolve("proveedores/" + fileName));
						} catch (Exception e) {
							throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
						}
						getBaseService().saveUpdate(doc);
					}
				}
			}
		}

	}

	// -------------
	// @return Devuelve todos los tipos de Roles
	// -------------
	@GetMapping("/getRoles")
	public Collection<Rol> getRoles() {

		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();

		ArrayList<Rol> datos = (ArrayList<Rol>) getBaseService().getByPropertiesNamed(Rol.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			return datos;
		}
		return null;
	}

	// -------------
	// @return Devuelve un Role
	// -------------
	@GetMapping("/getRol/{id}")
	public Rol getRol(@PathVariable("id") int id) {

		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		properties.add("rol_id = :rol_id");
		propertyValues.put("rol_id", id);

		ArrayList<Rol> datos = (ArrayList<Rol>) getBaseService().getByPropertiesNamed(Rol.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			Rol retorno = datos.get(0);
			retorno.setOpciones((ArrayList<RolOpcion>) getRolOpcion(retorno.getId()));
			return retorno;
		}
		return null;
	}

	// -------------
	// @return Devuelve un Role
	// -------------
	@GetMapping("/getRolOpcion/{id}")
	public Collection<RolOpcion> getRolOpcion(@PathVariable("id") int id) {

		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		properties.add("rol_id = :rol_id");
		propertyValues.put("rol_id", id);

		ArrayList<RolOpcion> datos = (ArrayList<RolOpcion>) getBaseService().getByPropertiesNamed(RolOpcion.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			return datos;
		}
		return null;
	}

	// -------------
	// @param Objeto Rol a guardar
	// @return Un Rol con todos los datos
	// -------------
	@PostMapping("/saveRol")
	Rol saveRol(@RequestBody Rol rol) throws Exception {

		// ---------- Se guarda en la tabla rol
		rol = (Rol) getBaseService().saveUpdate(rol);

		List<RolOpcion> rolOpcionAct = (List<RolOpcion>) getRolOpcion(rol.getId());

		// --------- Se borran las opciones que no esten
		if (rolOpcionAct != null) {
			for (RolOpcion rolOp : rolOpcionAct) {
				if (!rol.getOpciones().contains(rolOp)) {
					getBaseService().delete(rolOp);
				}
			}
		}

		for (RolOpcion rolOpcion : rol.getOpciones()) {
			rolOpcion.setRol_id(rol.getId());
			getBaseService().saveUpdate(rolOpcion);
		}

		return rol;
	}

	// -------------
	// @return Devuelve todos las Opciones
	// -------------
	@GetMapping("/getOpciones")
	public Collection<Opcion> getOpciones() {

		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();

		ArrayList<Opcion> datos = (ArrayList<Opcion>) getBaseService().getByPropertiesNamed(Opcion.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			return datos;
		}
		return null;
	}

	// -------------
	// @return Devuelve una Opcion
	// -------------
	@GetMapping("/getOpcion/{id}")
	public Opcion getOpcion(@PathVariable("id") int id) {

		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		properties.add("opcion_id = :opcion_id");
		propertyValues.put("opcion_id", id);

		ArrayList<Opcion> datos = (ArrayList<Opcion>) getBaseService().getByPropertiesNamed(Opcion.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			return datos.get(0);
		}
		return null;
	}

	// -------------
	// @param Objeto Opción a guardar
	// @return Una Opción con todos los datos
	// -------------
	@PostMapping("/saveOpcion")
	Opcion saveOpcion(@RequestBody Opcion opcion) throws Exception {

		// ---------- Se guarda en la tabla opcion
		opcion = (Opcion) getBaseService().saveUpdate(opcion);

		return opcion;
	}

	// -------------
	// @param Objeto proveedor a guardar
	// @return Un proveedor con todos los datos
	// -------------
	@PostMapping("/deshabilitarProveedor")
	public void deshabilitarProveedor(@RequestBody int idProveedor) throws Exception {
		Proveedor proveedor = getProveedor(idProveedor);
		proveedor.setEstado("I");

		getBaseService().saveUpdate(proveedor);
	}

	// -------------
	// @return Devuelve todos los Usuarios
	// -------------
	@GetMapping("/getUsuarios")
	public Collection<Usuario> getUsuarios() {

		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();

		ArrayList<Usuario> datos = (ArrayList<Usuario>) getBaseService().getByPropertiesNamed(Usuario.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			return datos;
		}
		return null;
	}

	// -------------
	// @param Objeto proveedor a guardar
	// @return Un proveedor con todos los datos
	// -------------
	@PostMapping("/saveUsuario")
	Usuario saveUsuario(@RequestBody Usuario usuario) throws Exception {

		ArrayList<UsuarioRol> rolesFormulario = new ArrayList<>();
		ArrayList<UsuarioOpcion> opcionesFormulario = new ArrayList<>();

		// ---------- Se guarda en la tabla persona
		usuario = (Usuario) getBaseService().saveUpdate(usuario);

		// ---------- Se consultan los datos actuales
		List<UsuarioRol> rolesDB = getUsuarioRolesByPersonId(usuario.getId());
		List<UsuarioOpcion> opcionesDB = getOpcionesByPersonId(usuario.getId());

		for (Rol roles : usuario.getRoles()) {
			UsuarioRol roltemp = new UsuarioRol();
			roltemp.setUsuarioId(usuario.getId());
			roltemp.setRolId(roles.getId());
			rolesFormulario.add(roltemp);
		}
		//
		for (UsuarioOpcion opc : usuario.getOpciones()) {
			UsuarioOpcion opctemp = new UsuarioOpcion();
			opctemp.setUsuarioId(usuario.getId());
			opctemp.setOpcionId(opc.getOpcionId());
			opcionesFormulario.add(opctemp);
		}

		// ------------ Se borran los roles que no esten
		if (rolesDB != null) {
			for (UsuarioRol roldb : rolesDB) {
				if (!rolesFormulario.contains(roldb)) {
					getBaseService().delete(roldb);
				}
			}
		}

		// --------- Se borran las opciones que no esten
		if (opcionesDB != null) {
			for (UsuarioOpcion opcdb : opcionesDB) {
				if (!opcionesFormulario.contains(opcdb)) {
					getBaseService().delete(opcdb);
				}
			}
		}

		// --------- Se guarda el objeto UsuarioRol
		for (UsuarioRol rol : rolesFormulario) {
			getBaseService().saveUpdate(rol);
		}

		// --------- Se guarda el objeto UsuarioOpcion
		for (UsuarioOpcion opc : opcionesFormulario) {
			getBaseService().saveUpdate(opc);
		}

		return usuario;
	}

	// -------------
	// @param id del Usuario
	// @return Todos los id de roles
	// -------------
	@GetMapping("/getUsuarioRolesByPersonId")
	public List<UsuarioRol> getUsuarioRolesByPersonId(@PathVariable("id") int id) {
		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();
		properties.add("usuario_id = :usuario_id");
		propertyValues.put("usuario_id", id);

		ArrayList<UsuarioRol> datos = (ArrayList<UsuarioRol>) getBaseService().getByPropertiesNamed(UsuarioRol.class.getName(), properties, propertyValues, "");
		return datos;
	}

	// -------------
	// @param id del Usuario
	// @return Todos los id de roles
	// -------------
	@GetMapping("/getRolesByUserId")
	public List<Rol> getRolesByUserId(@PathVariable("id") int id) {
		ArrayList<Rol> retorno = new ArrayList<Rol>();
		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();
		properties.add("usuario_id = :usuario_id");
		propertyValues.put("usuario_id", id);

		ArrayList<UsuarioRol> datos = (ArrayList<UsuarioRol>) getBaseService().getByPropertiesNamed(UsuarioRol.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			properties.clear();
			propertyValues.clear();

			for (UsuarioRol rol : datos) {
				properties.add("rol_id = :rol_id");
				propertyValues.put("rol_id", rol.getRolId());
				properties.add("estado = :estado");
				propertyValues.put("estado", "A");

				ArrayList<Rol> datosRol = (ArrayList<Rol>) getBaseService().getByPropertiesNamed(Rol.class.getName(), properties, propertyValues, "");

				if (!datosRol.isEmpty()) {
					retorno.add(datosRol.get(0));
				}
			}
			return retorno;
		}
		return null;
	}

	// -------------
	// @param id del Usuario
	// @return Todos los id de opciones
	// -------------
	@GetMapping("/getOpcionesByPersonId")
	public List<UsuarioOpcion> getOpcionesByPersonId(@PathVariable("id") int id) {
		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();
		properties.add("usuario_id = :usuario_id");
		propertyValues.put("usuario_id", id);

		ArrayList<UsuarioOpcion> datos =
				(ArrayList<UsuarioOpcion>) getBaseService().getByPropertiesNamed(UsuarioOpcion.class.getName(), properties, propertyValues, "");
		return datos;
	}

	// -------------
	// @param id del proveedor
	// @return un proveedor con toda la informacion
	// -------------
	@GetMapping("/getUsuario/{id}/{isCompleto}")
	public Usuario getUsuario(@PathVariable int id, @PathVariable boolean isCompleto) {

		// Collection<Rol> rolesBD = getRoles();
		//// Collection<UsuarioRol> rolesUsuario = new ArrayList<>();
		// Collection<UsuarioOpcion> opcionesBD = getOpciones();
		//// Collection<UsuarioOpcion> opcionesUsuarios = new ArrayList<>();
		ArrayList<Rol> rolesCargar = new ArrayList<>();
		ArrayList<UsuarioOpcion> opcionesCargar = new ArrayList<>();

		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();
		properties.add("id = :id");
		propertyValues.put("id", id);

		ArrayList<Usuario> datos = (ArrayList<Usuario>) getBaseService().getByPropertiesNamed(Usuario.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			Usuario retorno = datos.get(0);
			if (isCompleto) {
				retorno.setMunicipio(getMunicipioById(retorno.getMunicipio_id()));
				retorno.setRoles(getRolesByUserId(retorno.getId()));
				retorno.setOpciones(getOpcionesByPersonId(retorno.getId()));
			}
			return retorno;
		}
		return null;
	}

	// ----------- Retorna todos los contratos ---------
	@GetMapping("/getContratos")
	public List<Contrato> getContratos() {
		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();

		ArrayList<Contrato> datos = (ArrayList<Contrato>) getBaseService().getByPropertiesNamed(Contrato.class.getName(), properties, propertyValues, "");

		return datos;
	}

	// ----------- Retorna todos los contratos ---------
	@GetMapping("/getContratosPorEstado/{estado}")
	public List<Contrato> getContratosPorEstado(@PathVariable("estado") String estado) {
		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();
		properties.add("estado = :estado");
		propertyValues.put("estado", estado);

		ArrayList<Contrato> datos = (ArrayList<Contrato>) getBaseService().getByPropertiesNamed(Contrato.class.getName(), properties, propertyValues, "");

		return datos;
	}

	// ----------- Retorna los nombres contratos ---------
	@GetMapping("/getNombresContratosPorEstado/{estado}")
	public ArrayList<Map<String, String>> getNombresContratosPorEstado(@PathVariable("estado") String estado) {
		ArrayList<Map<String, String>> retorno = new ArrayList<>();

		String query = "SELECT contrato_id, nombre, numero FROM contrato WHERE estado = '" + estado + "'";

		ArrayList<Object[]> cont = (ArrayList<Object[]>) ServiceLocator.getInstance().getBaseService().executeSQLQuery(query);

		if (!cont.isEmpty()) {
			for (Object[] objects : cont) {
				Map<String, String> data = new HashMap<>();
				data.put("contratoId", "" + (int) objects[0]);
				data.put("nombre", (String) objects[1]);
				data.put("numero", (String) objects[2]);
				retorno.add(data);
			}
		}

		return retorno;
	}

	// -------------
	// @param id del contrato
	// @return un contrato con toda la informacion{
	// -------------
	@GetMapping("/getContrato/{id}")
	public Contrato getContrato(@PathVariable("id") int id, boolean... completo) {
		boolean full = completo != null && completo.length > 0 ? completo[0] : true;
		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();
		properties.add("contrato_id = :id");
		propertyValues.put("id", id);

		ArrayList<Contrato> datos = (ArrayList<Contrato>) getBaseService().getByPropertiesNamed(Contrato.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			Contrato retorno = datos.get(0);
			if (full) {
				retorno.setContratante(getPersona(retorno.getContratante_id(), false));
				retorno.setTarifas(getContratoTarifa(retorno.getId()));
				retorno.setObservaciones(getContratoObservacion(retorno.getId()));
				retorno.setMunicipios(getMunicipioContrato(retorno.getId()));
				retorno.setConceptos(getConceptoContrato(retorno.getId()));
			}
			// retorno.setDocumentos(getRegistroDocumentoByPersonId(retorno.getId())); TODO
			return retorno;
		}

		return null;
	}

	// ----------- Retorna todas las tarifas de un contrato ---------
	@GetMapping("/getContratoTarifa/{idContrato}")
	public List<TarifaContrato> getContratoTarifa(@PathVariable("idContrato") int idContrato) {
		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		properties.add("contrato_id = :contrato_id");
		propertyValues.put("contrato_id", idContrato);

		ArrayList<TarifaContrato> datos =
				(ArrayList<TarifaContrato>) getBaseService().getByPropertiesNamed(TarifaContrato.class.getName(), properties, propertyValues, "");

		for (TarifaContrato tCon : datos) {
			tCon.setConcepto(getConceptoById(tCon.getConcepto_id()));
			tCon.setMunicipio(getMunicipioById(tCon.getMunicipio_id()));
			tCon.setTipoTarifa(getTipoTarifaById(tCon.getTipotarifa_id()));
		}
		return datos;
	}

	// ----------- Retorna todas las observaciones de un contrato ---------
	public List<RegistroObservacion> getContratoObservacion(int idContrato) {
		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();
		properties.add("contrato_id = :contrato_id");
		propertyValues.put("contrato_id", idContrato);

		ArrayList<RegistroObservacion> datos =
				(ArrayList<RegistroObservacion>) getBaseService().getByPropertiesNamed(RegistroObservacion.class.getName(), properties, propertyValues, "");

		return datos;
	}

	// ----------- Retorna todos los municipios de un contrato ---------
	@GetMapping("/getMunicipioContrato/{id}")
	public List<Municipio> getMunicipioContrato(@PathVariable("id") int id) {
		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();
		properties.add("contrato_id = :contrato_id");
		propertyValues.put("contrato_id", id);

		ArrayList<ContratoMunicipio> datos =
				(ArrayList<ContratoMunicipio>) getBaseService().getByPropertiesNamed(ContratoMunicipio.class.getName(), properties, propertyValues, "");
		ArrayList<Municipio> retorno = new ArrayList<>();

		if (!datos.isEmpty()) {
			for (ContratoMunicipio muni : datos) {
				retorno.add(getMunicipioById(muni.getMunicipio_id()));
			}

			return retorno;
		}

		return null;
	}

	// ----------- Retorna todos los conceptos de un contrato ---------
	public List<Concepto> getConceptoContrato(int idContrato) {
		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();
		properties.add("contrato_id = :contrato_id");
		propertyValues.put("contrato_id", idContrato);

		ArrayList<ContratoConcepto> datos =
				(ArrayList<ContratoConcepto>) getBaseService().getByPropertiesNamed(ContratoConcepto.class.getName(), properties, propertyValues, "");
		ArrayList<Concepto> retorno = new ArrayList<>();

		if (!datos.isEmpty()) {
			for (ContratoConcepto concepto : datos) {
				retorno.add(getConceptoById(concepto.getConcepto_id()));
			}

			return retorno;
		}

		return null;
	}

	// -------------
	// @param Objeto contrato a guardar
	// @return Un contrato con todos los datos
	// -------------
	@PostMapping("/saveContrato")
	Contrato saveContrato(@RequestBody Contrato contrato) throws Exception {

		// ---------- Se guarda en la tabla contrato
		contrato = (Contrato) getBaseService().saveUpdate(contrato);
		System.out.println("Id Contrato: " + contrato.getId());

		for (RegistroObservacion obCon : contrato.getObservaciones()) {
			obCon.setContrato_id(contrato.getId());
			getBaseService().saveUpdate(obCon);
		}
		// getBaseService().saveUpdateAll(contrato.getObservaciones());

		for (Municipio munCon : contrato.getMunicipios()) {
			ContratoMunicipio conMun = new ContratoMunicipio();
			conMun.setContrato_id(contrato.getId());
			conMun.setMunicipio_id(munCon.getId());
			getBaseService().saveUpdate(conMun);
		}

		for (Concepto con : contrato.getConceptos()) {
			ContratoConcepto conCon = new ContratoConcepto();
			conCon.setContrato_id(contrato.getId());
			conCon.setConcepto_id(con.getId());
			getBaseService().saveUpdate(conCon);
		}

		for (TarifaContrato tafCon : contrato.getTarifas()) {
			tafCon.setContrato_id(contrato.getId());
			getBaseService().saveUpdate(tafCon);
		}

		return contrato;
	}

	// ------- Devuelve todos los activos fijos
	@GetMapping("/getActivosFijos")
	public ArrayList<ActivoFijo> getActivosFijos() {

		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();

		ArrayList<ActivoFijo> datos = (ArrayList<ActivoFijo>) getBaseService().getByPropertiesNamed(ActivoFijo.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			return datos;
		}

		return null;
	}

	// ------- Devuelve un activo Fijo
	@GetMapping("/getActivoFijo/{id}")
	public ActivoFijo getActivoFijo(@PathVariable("id") int id) {

		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		properties.add("activofijo_id = :id");
		propertyValues.put("id", id);

		ArrayList<ActivoFijo> datos = (ArrayList<ActivoFijo>) getBaseService().getByPropertiesNamed(ActivoFijo.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			return datos.get(0);
		}

		return null;
	}

	// -------------
	// @param Objeto Activo Fijo a guardar
	// @return Un Activo Fijo con todos los datos
	// -------------
	@PostMapping("/saveActivoFijo")
	ActivoFijo saveActivoFijo(@RequestBody ActivoFijo activoFijo) throws Exception {

		// ---------- Se guarda en la tabla ActivoFijo
		activoFijo = (ActivoFijo) getBaseService().saveUpdate(activoFijo);

		return activoFijo;
	}

	// ------- Devuelve todos los Radicados
	@GetMapping("/getRadicados")
	public ArrayList<Radicado> getRadicados() {

		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();

		ArrayList<Radicado> datos = (ArrayList<Radicado>) getBaseService().getByPropertiesNamed(Radicado.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			return datos;
		}
		return null;
	}

	// ------- Devuelve un Radicado
	@GetMapping("/getRadicado/{id}")
	public Radicado getRadicado(@PathVariable("id") int id) {

		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		properties.add("radicado_id = :id");
		propertyValues.put("id", id);

		ArrayList<Radicado> datos = (ArrayList<Radicado>) getBaseService().getByPropertiesNamed(Radicado.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			return datos.get(0);
		}
		return null;
	}

	// ---------- Retorna una persona buscada por id ------
	@GetMapping("/getPersonaEmail/{email}")
	public Usuario getPersonaByEmail(@PathVariable("email") String email) {
		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();
		properties.add("email = :email");
		propertyValues.put("email", email);

		ArrayList<Usuario> datos = (ArrayList<Usuario>) getBaseService().getByPropertiesNamed(Usuario.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			Usuario retorno = datos.get(0);
			return retorno;
		}

		return null;
	}

	// ---------- Retorna una persona buscada por id ------
	@GetMapping("/login/{user}/{pass}")
	public Usuario login(@PathVariable String user, @PathVariable String pass) {
		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();
		properties.add("usuario = :user");
		propertyValues.put("user", user);
		properties.add("clave = :pass");
		propertyValues.put("pass", pass);

		ArrayList<Usuario> datos = (ArrayList<Usuario>) getBaseService().getByPropertiesNamed(Usuario.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			Usuario retorno = datos.get(0);
			retorno.setRoles(getRolesByUserId(retorno.getId()));
			// TODO: buscar las opciones de los roles
			retorno.setOpciones(getOpcionesByPersonId(retorno.getId()));
			return retorno;
		}

		return null;
	}

	// -------------
	// @param Objeto Radicado a guardar
	// @return Un Radicado con todos los datos
	// -------------
	// @PostMapping("/saveRadicado")
	// Radicado saveRadicado(@RequestBody Radicado radicado) throws Exception {
	//
	// String upload_folder = "D://backups//files//adescubrirWeb//documentos//";
	// // ---------- Se guarda en la tabla Radicado
	//// radicado = (Radicado) getBaseService().saveUpdate(radicado);
	// for (MultipartFile fl: radicado.getFiles()) {
	// byte[] bytes = fl.getBytes();
	// Path path = Paths.get(upload_folder+fl.getOriginalFilename());
	// Files.write(path,);
	// }
	// return radicado;
	// }

	// -------------
	// @return Devuelve todos los conceptos
	// -------------
	@GetMapping("/getConceptos/{estado}")
	public Collection<Concepto> getConceptos(@PathVariable String estado) {
		// ArrayList<String> properties = new ArrayList<>();
		// Map<String, Object> propertyValues = new HashMap<>();
		// ArrayList<Concepto> datos = (ArrayList<Concepto>) getBaseService().getByPropertiesNamed(Concepto.class.getName(),properties, propertyValues, "");
		//
		// if (!datos.isEmpty()) {
		// for (Concepto conp: datos) {
		// conp.setTipoConcepto(getTipoConcepto(conp.getTipoConcepto_id()));
		// }
		// return datos;
		// }
		String where = ";";
		if (!estado.equals("*")) {
			where = " WHERE estado = '" + estado + "';";
		}
		ArrayList<Concepto> retorno = new ArrayList<>();
		String query =
				"SELECT concepto_id,nombre,(SELECT nombre FROM tipoconcepto WHERE tipoconcepto_id = c.tipoconcepto_id) AS tipoConcepto, estado"
						+ " FROM concepto AS c" + where;

		ArrayList<Object[]> cont = (ArrayList<Object[]>) ServiceLocator.getInstance().getBaseService().executeSQLQuery(query);
		if (!cont.isEmpty()) {
			for (Object[] objects : cont) {
				Concepto data = new Concepto();
				data.setId((int) objects[0]);
				data.setNombre((String) objects[1]);
				data.setNombreConcepto((String) objects[2]);
				data.setEstado((String) objects[3]);
				retorno.add(data);
			}
		}
		return retorno;
	}

	// -------------
	// @param id del tipo de concepto
	// @return Devuelve un tipo de concepto
	// -------------
	@GetMapping("/getTipoConcepto/{id}")
	public TipoConcepto getTipoConcepto(@PathVariable("id") int id) {

		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();
		properties.add("tipoconcepto_id = :id");
		propertyValues.put("id", id);

		ArrayList<TipoConcepto> datos =
				(ArrayList<TipoConcepto>) getBaseService().getByPropertiesNamed(TipoConcepto.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			TipoConcepto retorno = datos.get(0);
			return retorno;
		}

		return null;
	}

	// -------------
	// @param Objeto Concepto a guardar
	// @return Un Concepto con todos los datos
	// -------------
	@PostMapping("/saveConcepto")
	Concepto saveConcepto(@RequestBody Concepto concepto) throws Exception {

		// ---------- Se guarda en la tabla Concepto
		concepto = (Concepto) getBaseService().saveUpdate(concepto);

		return concepto;
	}

	// -------------
	// @return Devuelve todos los indicadores de retencion
	// -------------
	@GetMapping("/getIndicadoresRetencion")
	public Collection<IndicadorRetencion> getIndicadoresRetencion() {

		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		ArrayList<IndicadorRetencion> datos =
				(ArrayList<IndicadorRetencion>) getBaseService().getByPropertiesNamed(IndicadorRetencion.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			return datos;
		}

		return null;
	}

	// -------------
	// @return Devuelve un indicador de retencion
	// -------------
	@GetMapping("/getIndicadorRetencion/{id}")
	public IndicadorRetencion getIndicadorRetencion(@PathVariable("id") int id) {

		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		properties.add("indicador_id = :id");
		propertyValues.put("id", id);

		ArrayList<IndicadorRetencion> datos =
				(ArrayList<IndicadorRetencion>) getBaseService().getByPropertiesNamed(IndicadorRetencion.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			return datos.get(0);
		}

		return null;
	}

	// -------------
	// @param Objeto Estado a guardar
	// @return Un Estado con todos los datos
	// -------------
	@PostMapping("/saveIndicadorRetencion")
	IndicadorRetencion saveIndicadorRetencion(@RequestBody IndicadorRetencion indicador) throws Exception {

		// ---------- Se guarda en la tabla Estado
		indicador = (IndicadorRetencion) getBaseService().saveUpdate(indicador);

		return indicador;
	}

	// -------------
	// @return Devuelve todos los estados
	// -------------
	@GetMapping("/getEstados")
	public Collection<Estado> getEstados() {

		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		ArrayList<Estado> datos = (ArrayList<Estado>) getBaseService().getByPropertiesNamed(Estado.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			return datos;
		}

		return null;
	}

	// ---------- Retorna un estado buscado por id ------
	@GetMapping("/getEstado/{id}")
	public Estado getEstado(@PathVariable("id") int id) {
		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();
		properties.add("estado_id = :id");
		propertyValues.put("id", id);

		ArrayList<Estado> datos = (ArrayList<Estado>) getBaseService().getByPropertiesNamed(Estado.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			Estado retorno = datos.get(0);
			return retorno;
		}

		return null;
	}

	// -------------
	// @param Objeto Estado a guardar
	// @return Un Estado con todos los datos
	// -------------
	@PostMapping("/saveEstado")
	Estado saveEstado(@RequestBody Estado estado) throws Exception {

		// ---------- Se guarda en la tabla Estado
		estado = (Estado) getBaseService().saveUpdate(estado);

		return estado;
	}

	// -------------
	// @param Objeto Tipo Concepto a guardar
	// @return Un Tipo Concepto con todos los datos
	// -------------
	@PostMapping("/saveTipoConcepto")
	TipoConcepto saveTipoConcepto(@RequestBody TipoConcepto tipoConcepto) throws Exception {

		// ---------- Se guarda en la tabla Estado
		tipoConcepto = (TipoConcepto) getBaseService().saveUpdate(tipoConcepto);

		return tipoConcepto;
	}

	// -------------
	// @param
	// @return Todos los Tipos de Eventos
	// -------------
	@GetMapping("/getTipoEventos")
	public Collection<TipoEvento> getTipoEventos() {
		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();

		ArrayList<TipoEvento> datos = (ArrayList<TipoEvento>) getBaseService().getByPropertiesNamed(TipoEvento.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			return datos;
		}

		return null;
	}

	// -------------
	// @param id del tipo de evento
	// @return Todos los Tipos de Eventos
	// -------------
	@GetMapping("/getTipoEvento/{id}")
	public TipoEvento getTipoEventos(@PathVariable("id") int id) {
		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();
		properties.add("tipoevento_id = :id");
		propertyValues.put("id", id);

		ArrayList<TipoEvento> datos = (ArrayList<TipoEvento>) getBaseService().getByPropertiesNamed(TipoEvento.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			return datos.get(0);
		}

		return null;
	}

	// -------------
	// @param Objeto Tipo Evento a guardar
	// @return Un Tipo Evento con todos los datos
	// -------------
	@PostMapping("/saveTipoEvento")
	TipoEvento saveTipoEvento(@RequestBody TipoEvento tipoEvento) throws Exception {

		// ---------- Se guarda en la tabla Estado
		tipoEvento = (TipoEvento) getBaseService().saveUpdate(tipoEvento);

		return tipoEvento;
	}

	// -------------
	// @param
	// @return Todos los Tipos de Proveedor
	// -------------
	@GetMapping("/getTiposProveedor")
	public Collection<TipoProveedor> getTiposProveedor() {
		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();

		ArrayList<TipoProveedor> datos =
				(ArrayList<TipoProveedor>) getBaseService().getByPropertiesNamed(TipoProveedor.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			return datos;
		}

		return null;
	}

	// -------------
	// @param
	// @return Todos los Tipos de Proveedor
	// -------------
	@GetMapping("/getTiposByProveedor/{idProveedor}")
	public List<ProveedorTipo> getTiposByProveedor(@PathVariable("idProveedor") int idProveedor, boolean isFull) {
		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();
		properties.add("id_proveedor = :id");
		propertyValues.put("id", idProveedor);

		ArrayList<ProveedorTipo> datos =
				(ArrayList<ProveedorTipo>) getBaseService().getByPropertiesNamed(ProveedorTipo.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			if (isFull) {
				for (ProveedorTipo tipo: datos) {
					TipoProveedor tipoTemp = getTipoProveedor(tipo.getIdTipo());
					tipo.setObjTipo(tipoTemp);
				}
			}
			return datos;
		}

		return null;
	}

	// -------------
	// @param id del tipo de evento
	// @return Todos los Tipos de Eventos
	// -------------
	@GetMapping("/getTipoProveedor/{id}")
	public TipoProveedor getTipoProveedor(@PathVariable("id") int id) {
		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();
		properties.add("tipoproveedor_id = :id");
		propertyValues.put("id", id);

		ArrayList<TipoProveedor> datos =
				(ArrayList<TipoProveedor>) getBaseService().getByPropertiesNamed(TipoProveedor.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			return datos.get(0);
		}

		return null;
	}

	// -------------
	// @param Objeto Tipo Evento a guardar
	// @return Un Tipo Evento con todos los datos
	// -------------
	@PostMapping("/saveTipoProveedor")
	TipoProveedor saveTipoProveedor(@RequestBody TipoProveedor tipoProveedor) throws Exception {

		// ---------- Se guarda en la tabla Estado
		tipoProveedor = (TipoProveedor) getBaseService().saveUpdate(tipoProveedor);

		return tipoProveedor;
	}

	// -------------
	// @param
	// @return Todos los Documentos
	// -------------
	@GetMapping("/getDocumentos")
	public Collection<Documento> getDocumentos() {
		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();

		ArrayList<Documento> datos = (ArrayList<Documento>) getBaseService().getByPropertiesNamed(Documento.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			return datos;
		}

		return null;
	}

	// -------------
	// @param id del Documento
	// @return Todos los Documentos
	// -------------
	@GetMapping("/getDocumento/{id}")
	public Documento getDocumento(@PathVariable("id") int id) {
		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();
		properties.add("documento_id = :id");
		propertyValues.put("id", id);

		ArrayList<Documento> datos = (ArrayList<Documento>) getBaseService().getByPropertiesNamed(Documento.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			return datos.get(0);
		}

		return null;
	}

	// -------------
	// @param Objeto Documentos a guardar
	// @return Un Documentos con todos los datos
	// -------------
	@PostMapping("/saveDocumento")
	Documento saveDocumento(@RequestBody Documento documento) throws Exception {

		// ---------- Se guarda en la tabla Estado
		documento = (Documento) getBaseService().saveUpdate(documento);

		return documento;
	}

	// -------------
	// @param
	// @return Todos los Documentos
	// -------------
	@GetMapping("/getRangosNum")
	public Collection<RangoNumeracion> getRangosNum() {
		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();

		ArrayList<RangoNumeracion> datos =
				(ArrayList<RangoNumeracion>) getBaseService().getByPropertiesNamed(RangoNumeracion.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			return datos;
		}

		return null;
	}

	// -------------
	// @param id del Documento
	// @return Todos los Documentos
	// -------------
	@GetMapping("/getRangoNum/{id}")
	public RangoNumeracion getRangoNum(@PathVariable("id") int id) {
		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();
		properties.add("rango_id = :id");
		propertyValues.put("id", id);

		ArrayList<RangoNumeracion> datos =
				(ArrayList<RangoNumeracion>) getBaseService().getByPropertiesNamed(RangoNumeracion.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			return datos.get(0);
		}

		return null;
	}

	// -------------
	// @param Objeto Documentos a guardar
	// @return Un Documentos con todos los datos
	// -------------
	@PostMapping("/saveRangoNum")
	RangoNumeracion saveRangoNum(@RequestBody RangoNumeracion rangoNumeracion) throws Exception {

		// ---------- Se guarda en la tabla Estado
		rangoNumeracion = (RangoNumeracion) getBaseService().saveUpdate(rangoNumeracion);

		return rangoNumeracion;
	}

	// @Autowired
	// private ServletContext servletContext;
	// -------------
	// @param id del Documento
	// @return Todos los Documentos
	// -------------
	@GetMapping("/getFileSystemById/{id}")
	public Resource getFileSystemById(@PathVariable("id") int id, HttpServletResponse response) {
		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();
		properties.add("id = :id");
		propertyValues.put("id", id);

		ArrayList<RegistroDocumento> datos =
				(ArrayList<RegistroDocumento>) getBaseService().getByPropertiesNamed(RegistroDocumento.class.getName(), properties, propertyValues, "");

		Path path = Paths.get(datos.get(0).getRutaArchivo()).normalize();
		Resource resource = null;
		try {
			resource = new UrlResource(path.toUri());
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return resource;
	}

	// -------------
	// @param id del Documento
	// @return Todos los Documentos
	// -------------
	@GetMapping("/getFileSystemRuta/{idEvento}/{idDocumento}")
	public Resource getFileSystemRuta(@PathVariable("idEvento") int idEvento, @PathVariable("idDocumento") int idDocumento) {
		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();
		properties.add("evento_id = :idEvento");
		propertyValues.put("idEvento", idEvento);
		properties.add("documento_id = :idDocumento");
		propertyValues.put("idDocumento", idDocumento);

		ArrayList<EventoDocumentos> datos =
				(ArrayList<EventoDocumentos>) getBaseService().getByPropertiesNamed(EventoDocumentos.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			Path path = Paths.get(datos.get(0).getRutaArchivo()).normalize();
			Resource resource = null;
			try {
				resource = new UrlResource(path.toUri());
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
			return resource;
		}
		return null;
	}

	// -------------
	// @param
	// @return Todos las veredas
	// -------------
	@GetMapping("/getVeredasByMunicipio/{idMunicipio}")
	public Collection<Vereda> getVeredasByMunicipio(@PathVariable("idMunicipio") int idMunicipio) {

		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		properties.add("municipio_id = :municipio_id");
		propertyValues.put("municipio_id", idMunicipio);

		ArrayList<Vereda> datos = (ArrayList<Vereda>) getBaseService().getByPropertiesNamed(Vereda.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			for (Vereda mun : datos) {
				mun.setNombre(mun.getNombre() + " (" + mun.getDane() + ")");
			}
			return datos;
		}

		return null;
	}

	// -------------
	// @param
	// @return Todos las veredas
	// -------------
	@GetMapping("/getResguardosByMunicipio/{idMunicipio}")
	public Collection<Resguardo> getResguardosByMunicipio(@PathVariable("idMunicipio") int idMunicipio) {

		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		properties.add("municipio_id = :municipio_id");
		propertyValues.put("municipio_id", idMunicipio);

		ArrayList<Resguardo> datos = (ArrayList<Resguardo>) getBaseService().getByPropertiesNamed(Resguardo.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			for (Resguardo mun : datos) {
				mun.setNombre(mun.getNombre() + " (" + mun.getDane() + ")");
			}
			return datos;
		}

		return null;
	}

	// -------------
	// @param
	// @return Todos los resguardos
	// -------------
	@GetMapping("/getResguardos")
	public Collection<Resguardo> getResguardos() {

		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();

		ArrayList<Resguardo> datos = (ArrayList<Resguardo>) getBaseService().getByPropertiesNamed(Resguardo.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			for (Resguardo res : datos) {
				res.setNombreMunicipio(getMunicipioById(res.getIdMunicipio()).getNombre());
			}
			return datos;
		}
		return null;
	}

	// -------------
	// @param id del resguardo
	// @return Resguardo
	// -------------
	@GetMapping("/getResguardo/{id}")
	public Resguardo getResguardo(@PathVariable("id") int id) {

		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		properties.add("id = :id");
		propertyValues.put("id", id);

		ArrayList<Resguardo> datos = (ArrayList<Resguardo>) getBaseService().getByPropertiesNamed(Resguardo.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			Resguardo retorno = datos.get(0);
			retorno.setMunicipio(getMunicipioById(retorno.getIdMunicipio()));
			return retorno;
		}
		return null;
	}

	// -------------
	// @param Objeto Resguardo a guardar
	// @return Un Resguardo con todos los datos
	// -------------
	@PostMapping("/saveResguardo")
	Resguardo saveResguardo(@RequestBody Resguardo resguardo) throws Exception {

		// ---------- Se guarda en la tabla Resguardo
		resguardo = (Resguardo) getBaseService().saveUpdate(resguardo);

		return resguardo;
	}

	// -------------
	// @param
	// @return Todos las veredas
	// -------------
	@GetMapping("/getKumpanias")
	public Collection<Kumpania> getKumpanias() {

		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();

		ArrayList<Kumpania> datos = (ArrayList<Kumpania>) getBaseService().getByPropertiesNamed(Kumpania.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			for (Kumpania mun : datos) {
				mun.setNombre(mun.getNombre() + " (" + mun.getDane() + ")");
			}
			return datos;
		}
		return null;
	}

	// -------------
	// @param id de la Kumpania
	// @return Toda la kumpania
	// -------------
	@GetMapping("/getKumpania/{id}")
	public Kumpania getKumpania(@PathVariable("id") int id) {

		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		properties.add("id = :id");
		propertyValues.put("id", id);

		ArrayList<Kumpania> datos = (ArrayList<Kumpania>) getBaseService().getByPropertiesNamed(Kumpania.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			return datos.get(0);
		}
		return null;
	}

	// -------------
	// @param Objeto Kumpania a guardar
	// @return Una Kumpania con todos los datos
	// -------------
	@PostMapping("/saveKumpania")
	Kumpania saveKumpania(@RequestBody Kumpania kumpania) throws Exception {

		// ---------- Se guarda en la tabla kumpania
		kumpania = (Kumpania) getBaseService().saveUpdate(kumpania);

		return kumpania;
	}

	// -------------
	// @param
	// @return Todos los Consejo Comunitario
	// -------------
	@GetMapping("/getConsejosComByMunicipio/{idMunicipio}")
	public Collection<ConsejoComunitario> getConsejosComunitariosByMunicipio(@PathVariable("idMunicipio") int idMunicipio) {

		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		properties.add("municipio_id = :municipio_id");
		propertyValues.put("municipio_id", idMunicipio);

		ArrayList<ConsejoComunitario> datos =
				(ArrayList<ConsejoComunitario>) getBaseService().getByPropertiesNamed(ConsejoComunitario.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			for (ConsejoComunitario mun : datos) {
				mun.setNombre(mun.getNombre() + " (" + mun.getDane() + ")");
			}
			return datos;
		}
		return null;
	}

	// -------------
	// @param
	// @return Todos los Consejo Comunitario
	// -------------
	@GetMapping("/getConsejos")
	public Collection<ConsejoComunitario> getConsejos() {

		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();

		ArrayList<ConsejoComunitario> datos =
				(ArrayList<ConsejoComunitario>) getBaseService().getByPropertiesNamed(ConsejoComunitario.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			for (ConsejoComunitario res : datos) {
				res.setNombreMunicipio(getMunicipioById(res.getIdMunicipio()).getNombre());
			}
			return datos;
		}
		return null;
	}

	// -------------
	// @param id del Consejo Comunitario
	// @return Consejo Comunitario
	// -------------
	@GetMapping("/getConsejo/{id}")
	public ConsejoComunitario getConsejo(@PathVariable("id") int id) {

		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		properties.add("id = :id");
		propertyValues.put("id", id);

		ArrayList<ConsejoComunitario> datos =
				(ArrayList<ConsejoComunitario>) getBaseService().getByPropertiesNamed(ConsejoComunitario.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			ConsejoComunitario retorno = datos.get(0);
			retorno.setMunicipio(getMunicipioById(retorno.getIdMunicipio()));
			return retorno;
		}
		return null;
	}

	// -------------
	// @param Objeto Resguardo a guardar
	// @return Un Resguardo con todos los datos
	// -------------
	@PostMapping("/saveConsejo")
	ConsejoComunitario saveConsejo(@RequestBody ConsejoComunitario consejo) throws Exception {

		// ---------- Se guarda en la tabla Consejo Comunitario
		consejo = (ConsejoComunitario) getBaseService().saveUpdate(consejo);

		return consejo;
	}

	// -------------
	// @param
	// @return Todos los DocumentoContable
	// -------------
	@GetMapping("/getDocumentosContables")
	public Collection<TipoDocumentoContable> getDocumentosContables() {
		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();

		ArrayList<TipoDocumentoContable> datos =
				(ArrayList<TipoDocumentoContable>) getBaseService().getByPropertiesNamed(TipoDocumentoContable.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			return datos;
		}

		return null;
	}

	// -------------
	// @param id del DocumentoContable
	// @return Todos los DocumentoContable
	// -------------
	@GetMapping("/getTipoDocumentoContable/{id}")
	public TipoDocumentoContable getTipoDocumentoContable(@PathVariable("id") int id) {
		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();
		properties.add("tipodocumento_id = :id");
		propertyValues.put("id", id);

		ArrayList<TipoDocumentoContable> datos =
				(ArrayList<TipoDocumentoContable>) getBaseService().getByPropertiesNamed(TipoDocumentoContable.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			return datos.get(0);
		}

		return null;
	}

	// -------------
	// @param Objeto DocumentoContable a guardar
	// @return Un DocumentoContable con todos los datos
	// -------------
	@PostMapping("/saveDocumentoContable")
	TipoDocumentoContable saveDocumentoContable(@RequestBody TipoDocumentoContable documentoContable) throws Exception {

		// ---------- Se guarda en la tabla Estado
		documentoContable = (TipoDocumentoContable) getBaseService().saveUpdate(documentoContable);

		return documentoContable;
	}

	// -------------
	// @param
	// @return Todos los Medio Pago
	// -------------
	@GetMapping("/getMediosPago")
	public Collection<MedioPago> getMediosPago() {
		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();

		ArrayList<MedioPago> datos = (ArrayList<MedioPago>) getBaseService().getByPropertiesNamed(MedioPago.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			return datos;
		}

		return null;
	}

	// -------------
	// @param id del Medio Pago
	// @return Un Medio Pago
	// -------------
	@GetMapping("/getMedioPago/{id}")
	public MedioPago getMedioPago(@PathVariable("id") int id) {
		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();
		properties.add("mediopago_id = :id");
		propertyValues.put("id", id);

		ArrayList<MedioPago> datos = (ArrayList<MedioPago>) getBaseService().getByPropertiesNamed(MedioPago.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			return datos.get(0);
		}

		return null;
	}

	// -------------
	// @param Objeto Medio Pago a guardar
	// @return Un Medio Pago con todos los datos
	// -------------
	@PostMapping("/saveMedioPago")
	MedioPago saveMedioPago(@RequestBody MedioPago medioPago) throws Exception {

		// ---------- Se guarda en la tabla Estado
		medioPago = (MedioPago) getBaseService().saveUpdate(medioPago);

		return medioPago;
	}

	// ------- Devuelve todas las aerolineas
	@GetMapping("/getAerolineas")
	public ArrayList<Aerolinea> getAerolineas() {

		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();

		ArrayList<Aerolinea> datos = (ArrayList<Aerolinea>) getBaseService().getByPropertiesNamed(Aerolinea.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			return datos;
		}

		return null;
	}

	// ------- Devuelve una aerolinea
	@GetMapping("/getAerolinea/{id}")
	public Aerolinea getAerolinea(@PathVariable int id) {
		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();
		properties.add("aerolina_id = :id");
		propertyValues.put("id", id);

		ArrayList<Aerolinea> datos = (ArrayList<Aerolinea>) getBaseService().getByPropertiesNamed(Aerolinea.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			return datos.get(0);
		}

		return null;
	}

	// ------- Guarda una aerolinea
	@PostMapping("/saveAerolinea")
	Aerolinea saveAerolinea(@RequestBody Aerolinea aerolinea) throws Exception {

		// ---------- Se guarda en la tabla Estado
		aerolinea = (Aerolinea) getBaseService().saveUpdate(aerolinea);

		return aerolinea;
	}

	@PostMapping("/generateToken")
	public String generateToken(@RequestBody AuthRequest authRequest) throws Exception {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authRequest.getUserName(), authRequest.getPassword()));
		} catch (Exception ex) {
			return null;
		}
		return jwtUtil.generateToken(authRequest.getUserName());
	}

	public <T> ArrayList<Object> getObjetos(Collection<T> datos) {
		ArrayList<Object> retorno = new ArrayList<Object>();
		for (T evento : datos) {
			retorno.add(evento);
		}
		return retorno;
	}

	@PostMapping("/dinamic/{objeto}")
	public Map<String, Object> executeProcess(@RequestParam Map<String, String> params, @RequestBody Map<String, Object> request, @PathVariable String objeto)
			throws Exception {
		ObjetoDinamico objetoEjecucion = getObjetoEjecucionDinamicoVersionActiva(objeto);
		Message message = new Message();
		message.setBody(request);
		message.getParameters().put("requestParam", params);
		message.setParametro("requestUser", "");
		message.getProperties().putAll(message.getParameters());
		message = executeDynamicObject(message, objetoEjecucion.getCodigoFuente());
		return (Map<String, Object>) message.getBody();
	}

	public static Message executeDynamicObject(Message message, String source) throws Exception {

		final GroovyClassLoader classLoader = new GroovyClassLoader();
		try {
			source = "import co.com.adescubrir.model.bo.Message;\n" + source;
			Class groovy = classLoader.parseClass(source);
			GroovyObject groovyObj = (GroovyObject) groovy.newInstance();
			message = (Message) groovyObj.invokeMethod("process", new Object[] { message });
		} catch (CompilationFailedException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} finally {
			classLoader.close();
		}
		return message;
	}

	public static ObjetoDinamico getObjetoEjecucionDinamicoVersionActiva(String nombreObjeto) throws Exception {

		ArrayList<String> properties = new ArrayList<String>();
		Map<String, Object> propertyValues = new HashMap<String, Object>();
		properties.add("nombre = :nombre");
		propertyValues.put("nombre", nombreObjeto);
		properties.add("activa = :activa");
		propertyValues.put("activa", true);
		ArrayList<ObjetoDinamico> lista =
				(ArrayList<ObjetoDinamico>) ServiceLocator.getInstance().getBaseService().getByPropertiesNamed(ObjetoDinamico.class.getName(), properties,
						propertyValues, "version desc");
		if (lista.size() == 0) {
			throw new Exception("NO hay version activa para el objeto " + nombreObjeto);
		}
		if (lista.size() > 1) {
			throw new Exception("Existe mas de una versiones activa para el objeto " + nombreObjeto);
		}
		return lista.get(0);
	}

	@PostMapping("/dinamicVoidOutput/{objeto}")
	public void executeProcessServletOutput(@RequestParam Map<String, String> params, @RequestBody Map<String, Object> request, @PathVariable String objeto,
			HttpServletRequest httpRequest, HttpServletResponse httpResponse) throws Exception {
		ObjetoDinamico objetoEjecucion = getObjetoEjecucionDinamicoVersionActiva(objeto);
		Message message = new Message();
		message.setBody(request);
		message.getParameters().put("requestParam", params);
		message.setParametro("requestUser", "");
		message.getProperties().putAll(message.getParameters());
		message = executeDynamicObject(message, objetoEjecucion.getCodigoFuente());
		byte[] data = null;
		if (message.getProperty("tipoSalida") == "EXCEL") {
			ArrayList<Map<String, Object>> datos = (ArrayList<Map<String, Object>>) message.getBody();

			ByteArrayOutputStream out = null;
			try {
				out = new ByteArrayOutputStream();
				WritableWorkbook libro1 = Workbook.createWorkbook(out);
				WritableSheet hoja1 = libro1.createSheet("Datos", 0);
				WritableFont times16font = new WritableFont(WritableFont.ARIAL, 10, WritableFont.NO_BOLD, false);
				WritableFont times16fontBold = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false);

				WritableCellFormat celda = new WritableCellFormat(times16font);
				celda.setVerticalAlignment(VerticalAlignment.CENTRE);
				WritableCellFormat celdaWrap = new WritableCellFormat(times16font);
				DateFormat dateTimeFormat = new DateFormat("d/mm/yyyy h:mm:ss");
				// DateFormat dateFormat = new DateFormat("d/mm/yyyy");
				// WritableCellFormat celdaDate = new WritableCellFormat(times16font, dateFormat);
				WritableCellFormat celdaDateTime = new WritableCellFormat(times16font, dateTimeFormat);
				celdaWrap.setWrap(true);
				WritableCellFormat celdaHeader = new WritableCellFormat(times16fontBold);
				celdaHeader.setBackground(Colour.GRAY_25);
				celdaHeader.setAlignment(Alignment.CENTRE);
				celdaHeader.setBorder(Border.ALL, BorderLineStyle.THIN);
				// DateFormat dateFormat = new DateFormat("d/mm/yyyy h:mm:ss");
				// WritableCellFormat celdaDate = new
				// WritableCellFormat(times16font, dateFormat);
				int fila = 0;
				int columna = 0;

				if (!datos.isEmpty()) {
					for (Map.Entry<String, Object> header : datos.get(0).entrySet()) {
						hoja1.addCell(new Label(columna++, fila, header.getKey(), celdaHeader));
					}
				}
				fila++;

				for (Map<String, Object> obj : datos) {
					columna = 0;

					for (Map.Entry<String, Object> field : obj.entrySet()) {
						if (field.getValue() instanceof Number) {
							hoja1.addCell(new jxl.write.Number(columna++, fila, (Double) field.getValue(), celda));
						} else if (field.getValue() instanceof Date) {
							hoja1.addCell(new DateTime(columna++, fila, (Date) field.getValue(), celdaDateTime));
						} else {
							hoja1.addCell(new Label(columna++, fila, field.getValue().toString(), celda));
						}
					}
					fila++;
				}
				libro1.write();
				libro1.close();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			}
			data = out.toByteArray();
			message.setProperty("fileContentType", "application/x-download");
			message.setProperty("fileContentDisposition", "attachment");
		} else {
			data = (byte[]) message.getBody();
		}

		// Se construye la salida por ServletOutput
		// String fileName = nombreObjetoDinamico + "_" + new SimpleDateFormat("yyyMMddHHmmss").format(new Date()) + ".xls";
		ServletOutputStream op;
		try {
			op = httpResponse.getOutputStream();
			// httpResponse.setContentType("application/x-download");
			httpResponse.setContentType((String) message.getProperty("fileContentType"));
			httpResponse.setContentLength(data.length);
			httpResponse.setHeader("Expires", "0");
			httpResponse.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
			httpResponse.setHeader("Pragma", "public");
			httpResponse.setHeader("Content-disposition", "" + message.getProperty("fileContentDisposition") + ";filename=" + message.getProperty("fileName"));
			op.write(data);
			op.flush();
			op.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@PostMapping("/updatePassword")
	Map<String, String> updatePassword(@RequestBody Map<String, String> data) throws Exception {

		byte[] bytesDecodificados = Base64.getDecoder().decode(data.get("_clave_"));
		String cadenaDecodificada = new String(bytesDecodificados);

		Persona persona = getPersona(Integer.parseInt(data.get("user")),false);

		persona.setClave(cadenaDecodificada);

		getBaseService().saveUpdate(persona);
		// ---------- Se guarda en la tabla Estado
		return data;
	}
}
