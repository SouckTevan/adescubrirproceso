package co.com.adescubrir.controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.core.io.ClassPathResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import co.com.adescubrir.business.ServiceLocator;
import co.com.adescubrir.model.bo.EventoConsumoConcepto;
import co.com.adescubrir.model.bo.Persona;
import co.com.adescubrir.model.bo.contable.DetalleDocumento;
import co.com.adescubrir.model.bo.contable.DocumentoContable;
import co.com.adescubrir.model.bo.eventos.Contrato;
import co.com.adescubrir.model.bo.eventos.CotizacionEvento;
import co.com.adescubrir.model.bo.eventos.Evento;
import co.com.adescubrir.model.bo.eventos.EventoViaticos;
import co.com.adescubrir.model.bo.eventos.LegalizacionGasto;
import co.com.adescubrir.model.bo.eventos.TarifaCotizacion;
import co.com.adescubrir.model.bo.eventos.TiqueteEvento;
import co.com.adescubrir.model.bo.security.Usuario;
import co.com.adescubrir.service.ApplicationService;
import co.com.adescubrir.util.NumerosEnLetras;
import co.com.mycommons.jasper.JRHashDataSource;
import co.com.mycommons.jsf.components.JSFUtil;
import co.com.mycommons.util.Formato;
import jxl.Workbook;
import jxl.write.Formula;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

@RequestMapping("/pdfServices/")
@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST})
public class PDFServicesRest {

    private final Path root = Paths.get("/backups/files/adescubrirWeb/");
    private final AdministracionServiceRest admin = new AdministracionServiceRest();
    private final EventoServiceRest eventoServices = new EventoServiceRest();

    @PostMapping("/generarPDFCotizacion")
    public byte[] generarPDFCotizacion(@RequestBody Map<String, Object> json, HttpServletResponse httpResponse) {
        ObjectMapper mapper = new ObjectMapper();
        Evento evento = mapper.convertValue(json.get("evento"), Evento.class);
        Contrato contrato = admin.getContrato(evento.getIdContrato(), false);
        int idCotSelected = (int) json.get("idCotSelected");
        CotizacionEvento cotizacion = eventoServices.getEventoCotizacion(idCotSelected, evento.getRangoFechas());
        try
        {
            JasperReport masterReport = (JasperReport) JRLoader.loadObject(new File(root + "/formatosPDF/jasper/OrdenServicio.jasper"));
            JasperReport subreportHeaderColumnas = (JasperReport) JRLoader.loadObject(new File(root + "/formatosPDF/jasper/columnasConsumos.jasper"));
            Map<String, Object> parameters = new HashMap<>();
            Calendar fecha = Calendar.getInstance(new Locale("es", "CO"));
            parameters.put("EVENTO_ID", "" + evento.getId());
            parameters.put("NOMBRE_EMPRESA", ApplicationService.getParametro("empresa.nombre"));
            parameters.put("NIT_EMPRESA", ApplicationService.getParametro("empresa.nit"));
            if (contrato.getNitCompania() != null)
            {
                if (!"".equals(contrato.getNitCompania()))
                {
                    parameters.put("NIT_EMPRESA", contrato.getNitCompania());
                }
            }
            if (contrato.getNombreCompania() != null)
            {
                if (!"".equals(contrato.getNombreCompania()))
                {
                    parameters.put("NOMBRE_EMPRESA", contrato.getNombreCompania());
                }
            }
            parameters.put("LOGO", leerArchivo("logoNuevo398x147.png"));
            parameters.put("NOMBRE_EVENTO", evento.getNombre());
            if (CotizacionEvento.PRELIQUIDACION.equals(cotizacion.getEstado()))
            {
                parameters.put("TITULO_REPORTE", "PRE-LIQUIDACIÓN");
            }
            if (CotizacionEvento.ORDEN_SERVICIO.equals(cotizacion.getEstado()))
            {
                parameters.put("TITULO_REPORTE", "ORDEN DE SERVICIO");
            }
            parameters.put("PROGRAMA", evento.getTexto1());
            parameters.put("RESPONSABLE", admin.getPersona(evento.getIdResponsable(),false).getNombre());
            parameters.put("SEDE", evento.getSede().getNombre());
            fecha.setTime(evento.getFechaInicioEvento());
            parameters.put("FECHA_INICIAL", fecha.getTime());
            parameters.put("COORDINADOR", evento.getCoordinador().getNombre());
            parameters.put("PROVEEDOR", cotizacion.getProveedor().getRazonSocial());
            parameters.put("NUMERO_CONTRATO", contrato.getNumero());
            parameters.put("NOMBRE_CONTRATO", contrato.getNumero() + " - " + contrato.getNombre());
            parameters.put("OBSERVACIONES", cotizacion.getObservaciones());
            fecha.setTime(evento.getFechaFinEvento());
            parameters.put("FECHA_FINAL", fecha.getTime());
            // Titulo Columnas
            HashMap<String, Object> temp = new HashMap<>();

            int cont = 1;
            for (String dia : evento.getRangoFechas())
            {
                temp.put("DIA" + cont, Formato.formatoFecha(dia, "dd MMM yy"));
                cont++;
            }
            parameters.put("COLUMN_HEADER_MAP", temp);
            parameters.put("SUBREPORT_HEADER_COLUMNAS", subreportHeaderColumnas);
            ArrayList<HashMap<String, Object>> datosConceptos = new ArrayList<>();
            for (TarifaCotizacion tarifa : cotizacion.getTarifas())
            {
                HashMap<String, Object> concepto = new HashMap<>();
                concepto.put("CONCEPTO", tarifa.getConcepto().getNombre());
                for (Map.Entry<String, EventoConsumoConcepto> consumoDia : tarifa.getConsumos().entrySet())
                {
                    HashMap<String, Object> temp1 = new HashMap<>();
                    cont = 1;
                    Map<String, Integer> treeMap = new TreeMap<>(consumoDia.getValue().getCantidades());
                    for (Map.Entry<String, Integer> cantidad : treeMap.entrySet()) {
                        temp1.put("DIA" + cont, "" + cantidad.getValue());
                        cont++;
                    }
                    concepto.put("CANTIDADES", temp1);
                    datosConceptos.add(concepto);
                }
            }
            JRHashDataSource datos = new JRHashDataSource(datosConceptos);
            ServletOutputStream op;
            try {
                JasperPrint jasperPrint = JasperFillManager.fillReport(masterReport, parameters, datos);
                byte[] pdf = JasperExportManager.exportReportToPdf(jasperPrint);
                op = httpResponse.getOutputStream();
                httpResponse.setContentType("application/pdf");
                httpResponse.setContentLength(pdf.length);
                httpResponse.setHeader("Expires", "0");
                httpResponse.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
                httpResponse.setHeader("Pragma", "public");
                httpResponse.setHeader("Content-disposition", "inline;filename=Cotizacion");
                op.write(pdf);
                op.flush();
                op.close();
            }catch (JRException | IOException e) {
                e.printStackTrace();
            }
        }
        catch (JRException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @PostMapping("/generarPDFCompatarivoProveedores")
    public byte[] generarPDFCompatarivoProveedores(@RequestBody Map<String, Object> json, HttpServletResponse httpResponse) {
        ObjectMapper mapper = new ObjectMapper();
        Evento evento = eventoServices.getEvento((int) json.get("idEvento"));
        Contrato contrato = admin.getContrato(evento.getIdContrato(), false);
        ArrayList<Integer> cotizacionesSeleccionadas = mapper.convertValue(json.get("cotiSeleccionadas"), ArrayList.class);
        File reportFile = new File(root + "/formatosPDF/jasper/ComparativoProveedores.jasper");
        try
        {
            JasperReport masterReport = (JasperReport) JRLoader.loadObject(reportFile);
            JasperReport subreportCotizacion = (JasperReport) JRLoader.loadObject(new File(root + "/formatosPDF/jasper/ComparativoProveedores_Cotizacion.jasper"));
            Map<String, Object> parameters = new HashMap<>();
            Calendar fecha = Calendar.getInstance(new Locale("es", "CO"));
            parameters.put("EVENTO_ID", "" + evento.getId());
            parameters.put("NOMBRE_EMPRESA", ApplicationService.getParametro("empresa.nombre"));
            parameters.put("NIT_EMPRESA", ApplicationService.getParametro("empresa.nit"));
            if (contrato.getNitCompania() != null)
            {
                if (!"".equals(contrato.getNitCompania()))
                {
                    parameters.put("NIT_EMPRESA", contrato.getNitCompania());
                }
            }
            if (contrato.getNombreCompania() != null)
            {
                if (!"".equals(contrato.getNombreCompania()))
                {
                    parameters.put("NOMBRE_EMPRESA", contrato.getNombreCompania());
                }
            }

            parameters.put("LOGO", leerArchivo("logoNuevo398x147.png"));
            parameters.put("NOMBRE_EVENTO", evento.getNombre());
            parameters.put("TITULO_REPORTE", "RESUMEN DE EVENTO");
            parameters.put("PROGRAMA", evento.getTexto1());
            parameters.put("RESPONSABLE", admin.getPersona(evento.getIdResponsable(), false).getNombre());
            parameters.put("SEDE", evento.getSede().getNombre());
            fecha.setTime(evento.getFechaInicioEvento());
            parameters.put("FECHA_INICIAL", fecha.getTime());
            parameters.put("COORDINADOR", evento.getCoordinador().getNombre());
            parameters.put("SOLICITANTE", evento.getSolicitante());
            parameters.put("NUMERO_CONTRATO", contrato.getNumero());
            fecha.setTime(evento.getFechaFinEvento());
            parameters.put("FECHA_FINAL", fecha.getTime());
            parameters.put("RESUMEN", evento.getResumenEjecutivo());
            parameters.put("ORDEN", evento.getOrden());
            parameters.put("SUBREPORT_COTIZACION", subreportCotizacion);
            ArrayList<HashMap<String, Object>> cotizaciones = new ArrayList<>();
            double totalCotizaciones = 0;

            ArrayList<Map<String, Object>> cotizacionesTemp = eventoServices.getEventoCotizaciones(evento.getId(), evento.getRangoFechas());

            for (Map<String, Object> cotizacion : cotizacionesTemp)
            {
                if (cotizacionesSeleccionadas.contains(cotizacion.get("id")))
                {
                    CotizacionEvento cotiza = eventoServices.getEventoCotizacion((int) cotizacion.get("id"), evento.getRangoFechas());

                    HashMap<String, Object> cotizaMap = new HashMap<>();
                    cotizaMap.put("PROVEEDOR_NOMBRE", cotiza.getProveedor().getNombre());
                    cotizaMap.put("OBSERVACIONES", cotiza.getObservaciones());
                    double totalCotizacion = 0;
                    ArrayList<HashMap<String, Object>> datosConceptos = new ArrayList<>();
                    double totalBase = 0;
                    for (TarifaCotizacion tarifa : cotiza.getTarifas())
                    {
                        if (tarifa.getValor() > 0 && tarifa.getTotalCantidad() > 0)
                        {
                            HashMap<String, Object> conceptosMap = new HashMap<>();
                            conceptosMap.put("CONCEPTO", tarifa.getConcepto().getNombre());
                            conceptosMap.put("CANTIDAD", tarifa.getTotalCantidad());
                            conceptosMap.put("VALOR", tarifa.getValor());
                            if (tarifa.getConcepto().getPorcentajeIva() > 0)
                            {
                                double base = tarifa.getValor() / (1 + (tarifa.getConcepto().getPorcentajeIva() / 100));
                                totalBase += base * tarifa.getTotalCantidad();
                                conceptosMap.put("BASE", (double) Math.round(base));
                                conceptosMap.put("IVA", (double) Math.round(base * (tarifa.getConcepto().getPorcentajeIva() / 100)));
                            }
                            else
                            {
                                conceptosMap.put("BASE", tarifa.getValor());
                                totalBase += Math.round(tarifa.getValor()) * tarifa.getTotalCantidad();
                                conceptosMap.put("IVA", (double) 0);
                            }
                            totalCotizacion += tarifa.getTotalCantidad() * tarifa.getValor();
                            datosConceptos.add(conceptosMap);
                        }
                    }
                    double intermediacion = 0;
                    if (contrato.getPoIntermediacion() > 0)
                    {
                        if (ApplicationService.getParametro("eventos.cotizacion." + contrato.getId() + ".intermediacion").equals("total"))
                        {
                            intermediacion = (totalCotizacion * (contrato.getPoIntermediacion() / 100)) * 1.16;
                        }
                        else
                            intermediacion = (totalBase * (contrato.getPoIntermediacion() / 100)) * 1.16;
                        cotizaMap.put("VALOR_INTERMEDIACION", (double) Math.round(intermediacion));
                    }
                    else
                        cotizaMap.put("VALOR_INTERMEDIACION", (double) 0);
                    cotizaMap.put("TOTAL_COTIZACION", totalCotizacion);
                    cotizaMap.put("TOTAL", totalCotizacion + intermediacion);
                    totalCotizaciones += totalCotizacion + intermediacion;
                    JRHashDataSource datosCon = new JRHashDataSource(datosConceptos);
                    cotizaMap.put("COTIZACION", datosCon);
                    cotizaciones.add(cotizaMap);
                }
            }
            parameters.put("TOTAL_COTIZACIONES", totalCotizaciones);
            JRHashDataSource datos = new JRHashDataSource(cotizaciones);
            byte[] pdf = null;
            try {
                JasperPrint jasperPrint = JasperFillManager.fillReport(masterReport, parameters, datos);
                pdf = JasperExportManager.exportReportToPdf(jasperPrint);
            }catch (JRException e) {
                e.printStackTrace();
            }
            ServletOutputStream op;
            try {
                op = httpResponse.getOutputStream();
                httpResponse.setContentType("application/pdf");
                httpResponse.setContentLength(pdf.length);
                httpResponse.setHeader("Expires", "0");
                httpResponse.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
                httpResponse.setHeader("Pragma", "public");
                httpResponse.setHeader("Content-disposition", "inline;filename=Cotizacion");
                op.write(pdf);
                op.flush();
                op.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
//            fecha = null;
//            parameters = null;
//            cotizaciones = null;
//            datos = null;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @PostMapping("/generarPDFDocumentoContable")
    private byte[] generarPDFDocumentoContable(@RequestBody DocumentoContable documento, HttpServletRequest request) {
        if (documento.getTipodocumento_id() == eventoServices.getTipoDocumentoContable(1).getId())
        {
            JasperReport masterReport = null;
            JasperReport subreportHeaderColumnas = null;
            try
            {
                masterReport = (JasperReport) JRLoader.loadObject(new File(root + "/formatosPDF/jasper/FacturaDeVenta.jasper"));
                subreportHeaderColumnas = (JasperReport) JRLoader.loadObject(new File(root + "/formatosPDF/jasper/FacturaDeVenta_impuestos.jasper"));
            }
            catch (JRException e1)
            {
                e1.printStackTrace();
            }
            Map<String, Object> parameters = new HashMap<>();
            Calendar fecha = Calendar.getInstance(new Locale("es", "CO"));
            parameters.put("MOSTRAR_CABEZOTE", new Boolean(ApplicationService.getParametro("pdf.documentocontable.facturaVenta.mostrarCabezote")));
            parameters.put("MOSTRAR_TEXTO_LEGAL", new Boolean(ApplicationService.getParametro("pdf.documentocontable.facturaVenta.mostrarTextoLegal")));
            parameters.put("SUBREPORT_IMPUESTOS", subreportHeaderColumnas);
            parameters.put("NOMBRE_EMPRESA", ApplicationService.getParametro("empresa.nombre"));
            parameters.put("NIT_EMPRESA", ApplicationService.getParametro("empresa.nit"));
            parameters.put("DIRECCION_EMPRESA", ApplicationService.getParametro("empresa.direccion"));
            parameters.put("CIUDAD_EMPRESA", ApplicationService.getParametro("empresa.ciudad"));
            // TEXTOS
            parameters.put("TEXTO1", ApplicationService.getParametro("pdf.documentocontable.facturaVenta.texto1"));
            parameters.put("TEXTO2", ApplicationService.getParametro("pdf.documentocontable.facturaVenta.texto2"));
            parameters.put("TEXTO3", ApplicationService.getParametro("pdf.documentocontable.facturaVenta.texto3"));
            parameters.put("TEXTO4", ApplicationService.getParametro("pdf.documentocontable.facturaVenta.texto4"));
            if (documento.getObservaciones() != null)
                parameters.put("OBSERVACIONES", documento.getObservaciones());
            else
                parameters.put("OBSERVACIONES", "");
            if (documento.getEventoId() != 0)
            {
                Evento evento = eventoServices.getEvento(documento.getEventoId());
                Contrato contrato = admin.getContrato(evento.getIdContrato(), false);
                Map<String, String> data = new HashMap<>();
                data.put("&orden", evento.getOrden());
                data.put("&cdp", evento.getCdp());
                data.put("&id", "" + evento.getId());
                data.put("&nombre", evento.getNombre());
                data.put("&contrato", contrato.getNumero());
                data.put("&ciudad", evento.getSede().getNombre());
                data.put("&texto1", evento.getTexto1());
                data.put("&texto2", evento.getTexto2());
                data.put("&texto3", evento.getTexto3());
                data.put("&programa", evento.getPrograma());
                data.put("&solicitante", evento.getSolicitante());
                if (contrato.getNitCompania() != null)
                {
                    if (!"".equals(contrato.getNitCompania()))
                    {
                        parameters.put("NIT_EMPRESA", contrato.getNitCompania());
                    }
                }
                if (contrato.getNombreCompania() != null)
                {
                    if (!"".equals(contrato.getNombreCompania()))
                    {
                        parameters.put("NOMBRE_EMPRESA", contrato.getNombreCompania());
                    }
                }
                if (contrato.getDireccionCompania() != null)
                {
                    if (!"".equals(contrato.getDireccionCompania()))
                    {
                        parameters.put("DIRECCION_EMPRESA", contrato.getDireccionCompania());
                    }
                }
                if (contrato.getCiudadCompania() != null)
                {
                    if (!"".equals(contrato.getCiudadCompania()))
                    {
                        parameters.put("CIUDAD_EMPRESA", contrato.getCiudadCompania());
                    }
                }
                if (contrato.getCuentaCompania() != null)
                {
                    if (!"".equals(contrato.getCuentaCompania()))
                    {
                        parameters.put("TEXTO1", contrato.getCuentaCompania());
                    }
                }
                if (contrato.getObservacionesFactura() != null && !contrato.getObservacionesFactura().equals(""))
                {
                    String observ = contrato.getObservacionesFactura();
                    for (Map.Entry<String, String> map : data.entrySet())
                        if (observ.contains(map.getKey()))
                            observ = observ.replaceAll(map.getKey(), map.getValue());
                    parameters.put("OBSERVACIONES", observ);
                }
            }
            parameters.put("LOGO", new ClassPathResource("adescubrir_403x104.png"));
            if (documento.getRangoNumeracion() != null)
                parameters.put("RESOLUCION", documento.getRangoNumeracion().getTextoLegal());
            parameters.put("FECHA_FINAL", fecha.getTime());
            parameters.put("NUMERO_FACTURA", documento.getNumero() == null ? "" : documento.getNumero());
            if (documento.getFechaFactura() == null)
                parameters.put("FECHA", documento.getFecha());
            else
                parameters.put("FECHA", documento.getFechaFactura());
            parameters.put("FECHA_VENCIMIENTO", documento.getFechaVencimiento());
            Persona cliente = admin.getPersona(documento.getPersona().getId(),true);
            if (cliente.getNombres() == null || "".equals(cliente.getNombres()))
                parameters.put("NOMBRE_PERSONA", cliente.getRazonSocial());
            else
                parameters.put("NOMBRE_PERSONA", cliente.getNombre());
            parameters.put("IDENTIFICACION_PERSONA", cliente.getIdentificacion());
            if (cliente.getMunicipio() != null)
                parameters.put("CIUDAD_PERSONA", cliente.getMunicipio().getNombre());
            else
                parameters.put("CIUDAD_PERSONA", "");
            parameters.put("DIRECCION_PERSONA", cliente.getDireccion());
            parameters.put("TELEFONO_PERSONA", cliente.getTelefono());
            if (documento.getCreador() == null)
                parameters.put("NOMBRE_USUARIO", "");
            else
                parameters.put("NOMBRE_USUARIO", documento.getCreador().getNombre());
            // Titulo Columnas
            ArrayList<HashMap<String, Object>> datosConceptos = new ArrayList<>();
            datosConceptos.add(new HashMap<>());
            for (DetalleDocumento detalle : documento.getDetalle())
            {
                HashMap<String, Object> concepto = new HashMap<>();
                concepto.put("CODIGO_CONCEPTO", detalle.getConcepto().getCodigo() + documento.getMunicipio().getCodigo());
                concepto.put("NOMBRE_CONCEPTO", detalle.getConcepto().getNombre());
                concepto.put("CANTIDAD", detalle.getCantidad());
                concepto.put("VALOR", detalle.getValor());
                concepto.put("TOTAL", detalle.getTotal());
                concepto.put("IMPUESTOS", detalle.getImpuestos());
                concepto.put("PO_IMPUESTOS", detalle.getPoImpuestos());
                concepto.put("DESCUENTOS", detalle.getDescuento());
                datosConceptos.add(concepto);
            }
            int numeroDetalles = Integer.parseInt(ApplicationService.getParametro("pdf.documentocontable.detalle.lines"));
            numeroDetalles = numeroDetalles - datosConceptos.size();
            for (int i = 0; i < numeroDetalles; i++)
            {
                HashMap<String, Object> concepto = new HashMap<>();
                concepto.put("NOMBRE_CONCEPTO", null);
                concepto.put("CANTIDAD", null);
                concepto.put("VALOR", null);
                concepto.put("TOTAL", null);
                concepto.put("IMPUESTOS", null);
                concepto.put("PO_IMPUESTOS", null);
                concepto.put("DESCUENTOS", null);
                datosConceptos.add(concepto);
            }
            parameters.put("TOTAL_BASE", documento.getBase());
            parameters.put("TOTAL_DESCUENTOS", documento.getDescuentos());
            parameters.put("TOTAL_IMPUESTOS", documento.getImpuestos());
            parameters.put("TOTAL_RETENCIONES", documento.getRetenciones());
            parameters.put("TOTAL_DOCUMENTO", documento.getTotal());
            parameters.put("TOTAL_LETRAS", NumerosEnLetras.numerosEnLetras((long) documento.getTotal(), NumerosEnLetras.PROPER_CASE));
            parameters.put("TOTAL_COMISIONES", documento.getComisiones());
            parameters.put("TOTAL_IVACOMISIONES", documento.getIvaComisiones());
            // Discriminacion de Impuestos
            ArrayList<HashMap<String, Object>> datosImpuestos = new ArrayList<>();
            for (Map.Entry<Double, Double[]> impuesto : eventoServices.getDiscriminacionImpuestos(documento).entrySet())
            {
                HashMap<String, Object> concepto = new HashMap<>();
                concepto.put("NOMBRE_IMPUESTO", "IVA DEL " + impuesto.getKey() + "%");
                concepto.put("BASE_IMPUESTO", impuesto.getValue()[0].doubleValue());
                concepto.put("PO_IMPUESTO", impuesto.getKey());
                concepto.put("VALOR_IMPUESTO", impuesto.getValue()[1].doubleValue());
                datosImpuestos.add(concepto);
            }
            JRHashDataSource jrds_datosImpuestos = new JRHashDataSource(datosImpuestos);
            parameters.put("DISCRIMINACION_IMPUESTOS", jrds_datosImpuestos);
            JRHashDataSource datos = new JRHashDataSource(datosConceptos);
            try
            {
                // Preparación del reporte (reporte diseñado con ireport)
                JasperPrint jasperPrint = JasperFillManager.fillReport(masterReport, parameters, datos);
                byte[] pdf = JasperExportManager.exportReportToPdf(jasperPrint);
                request.getSession().setAttribute("bytesMEDIA", pdf);
                request.getSession().setAttribute("nombreArchivo", "facturadeventa_" + documento.getId() + ".pdf");
                return pdf;
            }
            catch (JRException e)
            {
                e.printStackTrace();
            }
//            fecha = null;
//            parameters = null;
//            datosConceptos = null;
//            datos = null;
        }
        return null;
    }

    @PostMapping("/generarPDFPresupuesto")
    public byte[] generarPDFPresupuesto(@RequestBody Map<String, Object> json) {
        try {
            Evento evento = eventoServices.getEvento((int) json.get("idEvento"));
            Contrato contrato = admin.getContrato(evento.getIdContrato(),false);
            File reportFile = new File(root + "/formatosPDF/jasper/PresupuestoEvento.jasper");
            JasperReport masterReport = (JasperReport) JRLoader.loadObject(reportFile);
            Map<String, Object> parameters = new HashMap<>();
            Calendar fecha = Calendar.getInstance(new Locale("es", "CO"));
            parameters.put("EVENTO_ID", "" + evento.getId());
            parameters.put("NOMBRE_EMPRESA", ApplicationService.getParametro("empresa.nombre"));
            parameters.put("NIT_EMPRESA", ApplicationService.getParametro("empresa.nit"));
            if (contrato.getNitCompania() != null) {
                if (!"".equals(contrato.getNitCompania())) {
                    parameters.put("NIT_EMPRESA",contrato.getNitCompania());
                }
            }
            if (contrato.getNombreCompania() != null) {
                if (!"".equals(contrato.getNombreCompania())) {
                    parameters.put("NOMBRE_EMPRESA", contrato.getNombreCompania());
                }
            }
            parameters.put("LOGO", leerArchivo("logoNuevo398x147.png"));
            parameters.put("NOMBRE_EVENTO", evento.getNombre());
            parameters.put("TITULO_REPORTE", "RESUMEN DE EVENTO");
            parameters.put("PROGRAMA", evento.getTexto1());
            parameters.put("RESPONSABLE", evento.getResponsable().getNombre());
            parameters.put("SEDE", evento.getSede().getNombre());
            fecha.setTime(evento.getFechaInicioEvento());
            parameters.put("FECHA_INICIAL", fecha.getTime());
            parameters.put("COORDINADOR", evento.getCoordinador().getNombre());
            parameters.put("SOLICITANTE", evento.getSolicitante());
            parameters.put("CON_OBSERVACIONES", evento.getObservacionesConsumos());
            parameters.put("ORDEN", evento.getOrden());
            parameters.put("NUMERO_CONTRATO", contrato.getNumero());
            parameters.put("VALOR_PRESUPUESTO", (evento.getValorPresupuesto() == null) ? 0 : Double.parseDouble(evento.getValorPresupuesto().toString()));
            fecha.setTime(evento.getFechaFinEvento());
            parameters.put("FECHA_FINAL", fecha.getTime());
            ArrayList<HashMap<String, Object>> conceptos = new ArrayList<>();
            for (Map.Entry<String, EventoConsumoConcepto> consumo : evento.getEventoConsumos().entrySet()) {
                if (consumo.getValue().getTotalTarifa() > 0) {
                    HashMap<String, Object> concepto = new HashMap<>();
                    concepto.put("CONCEPTO", consumo.getValue().getNombreConcepto());
                    concepto.put("CANTIDAD", consumo.getValue().getTotal());
                    concepto.put("VALOR", consumo.getValue().getTarifa());
                    concepto.put("TOTAL", consumo.getValue().getTotalTarifa());
                    conceptos.add(concepto);
                }
            }
            JRHashDataSource datos = new JRHashDataSource(conceptos);
            try {
                // Preparación del reporte (reporte diseñado con ireport)
                JasperPrint jasperPrint = JasperFillManager.fillReport(masterReport, parameters, datos);
                byte[] pdf = JasperExportManager.exportReportToPdf(jasperPrint);
                return pdf;
            } catch (JRException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @PostMapping("/generarPDFPresupuestoDetallado")
    public byte[] generarPDFPresupuestoDetallado(@RequestBody Map<String, Object> json) {
        JasperReport masterReport = null;
        File reportFile = null;
        try
        {
            Evento evento = eventoServices.getEvento((int) json.get("idEvento"));
            Contrato contrato = admin.getContrato(evento.getIdContrato(),false);
            reportFile = new File(root + "/formatosPDF/jasper/PresupuestoEventoDetallado.jasper");
            masterReport = (JasperReport) JRLoader.loadObject(reportFile);
            JasperReport subreportHeaderColumnas = (JasperReport) JRLoader.loadObject(new File(root + "/formatosPDF/jasper/columnasConsumos.jasper"));
            JasperReport subreportConsumos = (JasperReport) JRLoader.loadObject(new File(root + "/formatosPDF/jasper/ResumenEvento_Consumos.jasper"));
            Map<String, Object> parameters = new HashMap<String, Object>();
            Calendar fecha = Calendar.getInstance(new Locale("es", "CO"));
            parameters.put("EVENTO_ID", "" + evento.getId());
            parameters.put("NOMBRE_EMPRESA", ApplicationService.getParametro("empresa.nombre"));
            parameters.put("NIT_EMPRESA", ApplicationService.getParametro("empresa.nit"));
            if (contrato.getNitCompania() != null)
            {
                if (!"".equals(contrato.getNitCompania()))
                {
                    parameters.put("NIT_EMPRESA", contrato.getNitCompania());
                }
            }
            if (contrato.getNombreCompania() != null)
            {
                if (!"".equals(contrato.getNombreCompania()))
                {
                    parameters.put("NOMBRE_EMPRESA", contrato.getNombreCompania());
                }
            }
            parameters.put("LOGO", leerArchivo("logoNuevo398x147.png"));
            parameters.put("NOMBRE_EVENTO", evento.getNombre());
            parameters.put("TITULO_REPORTE", "PRESUPUESTO DETALLADO DEL EVENTO");
            parameters.put("PROGRAMA", evento.getPrograma());
            parameters.put("ORDEN", evento.getOrden());
            parameters.put("RESPONSABLE", evento.getResponsable().getNombre());
            parameters.put("SEDE", evento.getSede().getNombre());
            fecha.setTime(evento.getFechaInicioEvento());
            parameters.put("FECHA_INICIAL", fecha.getTime());
            parameters.put("COORDINADOR", evento.getCoordinador().getNombre());
            parameters.put("SOLICITANTE", evento.getSolicitante());
            parameters.put("NUMERO_CONTRATO", contrato.getNumero());
            parameters.put("CON_OBSERVACIONES", evento.getObservacionesConsumos());
            fecha.setTime(evento.getFechaFinEvento());
            parameters.put("FECHA_FINAL", fecha.getTime());
            parameters.put("SUBREPORT_CONSUMOS", subreportConsumos);
            parameters.put("VALOR_PRESUPUESTO", (evento.getValorPresupuesto() == null) ? 0 : Double.parseDouble(evento.getValorPresupuesto().toString()));
            parameters.put("PORCENTAJE_INTERMEDIACION", contrato.getPoIntermediacion());
            // Titulo Columnas
            HashMap<String, Object> temp = new HashMap<>();
            int cont = 1;
            for (String dia : evento.getRangoFechas())
            {
                temp.put("DIA" + cont, Formato.formatoFecha(dia, "ddMMMyy"));
                cont++;
            }
            parameters.put("COLUMN_HEADER_MAP", temp);
            parameters.put("SUBREPORT_HEADER_COLUMNAS", subreportHeaderColumnas);
            ArrayList<HashMap<String, Object>> datosConceptos = new ArrayList<>();
            for (Map.Entry<String, EventoConsumoConcepto> consumo : evento.getEventoConsumos().entrySet()) {
                if (consumo.getValue().getTotalTarifa() > 0) {
                    HashMap<String, Object> conceptos = new HashMap<>();
                    conceptos.put("CONCEPTO", consumo.getValue().getNombreConcepto());
                    conceptos.put("CANTIDAD", consumo.getValue().getTotal());
                    conceptos.put("VALOR", consumo.getValue().getTarifa());
                    conceptos.put("TOTAL", consumo.getValue().getTotalTarifa());
                    HashMap<String, Object> temp1 = new HashMap<>();
                    cont = 1;
                    for (Map.Entry<String, Integer> cantidad : consumo.getValue().getCantidades().entrySet()) {
                        temp1.put("DIA" + cont, "" + cantidad.getValue());
                        cont++;
                    }
                    conceptos.put("CANTIDADES", temp1);
                    datosConceptos.add(conceptos);
                }
            }
            JRHashDataSource datos = new JRHashDataSource(datosConceptos);
            parameters.put("DATASOURCE_CONSUMOS", datos);
            try {
                // Preparación del reporte (reporte diseñado con ireport)
                JasperPrint jasperPrint = JasperFillManager.fillReport(masterReport, parameters, datos);
                byte[] pdf = JasperExportManager.exportReportToPdf(jasperPrint);
                return pdf;
            } catch (JRException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public InputStream leerArchivo(String path) {
        InputStream is = PDFServicesRest.class.getClassLoader().getResourceAsStream(path);
        return is;
    }

    @PostMapping("/openFile")
    public byte[] openFile(@RequestBody Map<String, Object> json) throws IOException {
        String path = (String) json.get("path");
        byte[] media;
        try {
            FileInputStream file = new FileInputStream(new File(path));
            int size = file.available();
            media = new byte[size];
            file.read(media);
            file.close();
            return media;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @PostMapping("/generarExcelCotizacion")
    public byte[] exportarExcelCotizaciones(@RequestBody Map<String, Object> json) {
        Evento evento = eventoServices.getEvento((int) json.get("idEvento"));
        CotizacionEvento cotizacion = eventoServices.getEventoCotizacion((int) json.get("idCotizacion"), evento.getRangoFechas());

        ByteArrayOutputStream out = null;
        try
        {
            out = new ByteArrayOutputStream();
            WritableWorkbook libro1 = Workbook.createWorkbook(out);
            WritableSheet hoja1 = libro1.createSheet("Proveedor", 0);
            WritableFont times16font = new WritableFont(WritableFont.ARIAL, 10, WritableFont.NO_BOLD, true);
            WritableFont times16fontBold = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, true);
            WritableCellFormat celda = new WritableCellFormat(times16font);
            WritableCellFormat celdaHeader = new WritableCellFormat(times16fontBold);
            int fila = 0;
            int columna = 0;
            try
            {
                Formula formula;
                Number number;
                Label label = new Label(columna, fila, "DATOS DEL OPERADOR", celdaHeader);
                hoja1.addCell(label);
                fila++;
                label = new Label(columna, fila, "Empresa", celdaHeader);
                hoja1.addCell(label);
                columna++;
                label = new Label(columna, fila, "ADESCUBRIR TRAVEL Y ADVENTURE SAS", celda);
                hoja1.addCell(label);
                fila++;
                columna = 0;
                label = new Label(columna, fila, "NIT", celdaHeader);
                hoja1.addCell(label);
                columna++;
                label = new Label(columna, fila, "890.802.221-2", celda);
                hoja1.addCell(label);
                fila++;
                columna = 0;
                label = new Label(columna, fila, "Dirección", celdaHeader);
                hoja1.addCell(label);
                columna++;
                label = new Label(columna, fila, "Carrera 24A No. Manizales - Caldas", celda);
                hoja1.addCell(label);
                fila++;
                columna = 0;
                label = new Label(columna, fila, "Teléfono", celdaHeader);
                hoja1.addCell(label);
                columna++;
                label = new Label(columna, fila, "8811565", celda);
                hoja1.addCell(label);
                fila++;
                columna = 0;
                label = new Label(columna, fila, "Representante Legal", celdaHeader);
                hoja1.addCell(label);
                columna++;
                label = new Label(columna, fila, "Claudia Muriel", celda);
                hoja1.addCell(label);
                fila++;
                columna = 0;
                label = new Label(columna, fila, "Responsable del Evento:", celdaHeader);
                hoja1.addCell(label);
                columna++;
                label = new Label(columna, fila, evento.getTexto2(), celda);
                hoja1.addCell(label);
                fila++;
                columna = 0;
                label = new Label(columna, fila, "ID", celdaHeader);
                hoja1.addCell(label);
                columna++;
                label = new Label(columna, fila, "" + evento.getId(), celda);
                hoja1.addCell(label);
                fila++;
                columna = 0;
                label = new Label(columna, fila, "Ciudad del evento", celdaHeader);
                hoja1.addCell(label);
                columna++;
                label = new Label(columna, fila, evento.getSede().getNombre(), celda);
                hoja1.addCell(label);
                fila++;
                columna = 0;
                label = new Label(columna, fila, "Contrato", celdaHeader);
                hoja1.addCell(label);
                columna++;
                label = new Label(columna, fila, evento.getContrato().getNumero(), celda);
                hoja1.addCell(label);
                fila++;
                columna = 0;
                fila++;
                label = new Label(columna, fila, "COSTOS DIRECTOS", celdaHeader);
                hoja1.addCell(label);
                columna++;
                label = new Label(columna, fila, "CANTIDAD", celdaHeader);
                hoja1.addCell(label);
                columna++;
                label = new Label(columna, fila, "VR. UNITARIO", celdaHeader);
                hoja1.addCell(label);
                columna++;
                label = new Label(columna, fila, "VR. EJECUTADO", celdaHeader);
                hoja1.addCell(label);
                fila++;
                String formulaSubTotalCostos = "SUMA(D13:D";
                for (TarifaCotizacion tarifa : cotizacion.getTarifas())
                {
                    if (tarifa.getValor() > 0 && tarifa.getTotalCantidad() > 0)
                    {
                        label = new Label(0, fila, tarifa.getConcepto().getNombre(), celda);
                        hoja1.addCell(label);
                        number = new Number(1, fila, tarifa.getTotalCantidad(), celda);
                        hoja1.addCell(number);
                        number = new Number(2, fila, tarifa.getValor(), celda);
                        hoja1.addCell(number);
                        formula = new Formula(3, fila, "B" + (fila + 1) + "*" + "C" + (fila + 1), celda);
                        hoja1.addCell(formula);
                        fila++;
                    }
                }
                int filaSubtotal = fila;
                formulaSubTotalCostos += "" + (filaSubtotal);
                label = new Label(0, fila, "SUBTOTAL COSTOS DIRECTOS", celdaHeader);
                hoja1.addCell(label);
                formula = new Formula(3, fila, formulaSubTotalCostos, celdaHeader);
                hoja1.addCell(formula);
                fila++;
                label = new Label(0, fila, "GESTION ADMINISTRATIVA", celda);
                hoja1.addCell(label);
                formula = new Formula(3, fila, "D" + (filaSubtotal + 1) + "*" + new DecimalFormat("##0.000").format(admin.getContrato(evento.getIdContrato(), false).getPoIntermediacion() / 100).replaceAll(",", "."), celdaHeader);
                hoja1.addCell(formula);
                fila++;
                label = new Label(0, fila, "IVA GESTION ADMINISTRATIVA", celda);
                hoja1.addCell(label);
                formula = new Formula(3, fila, "D" + fila + "*" + new DecimalFormat("##0.00").format(0.16).replaceAll(",", "."), celdaHeader);
                hoja1.addCell(formula);
                fila++;
                label = new Label(0, fila, "SUBTOTAL GESTION ADMINISTRATIVA", celdaHeader);
                hoja1.addCell(label);
                formula = new Formula(3, fila, "D" + (fila - 1) + "+D" + (fila), celdaHeader);
                hoja1.addCell(formula);
                fila++;
                label = new Label(0, fila, "TOTAL", celdaHeader);
                hoja1.addCell(label);
                formula = new Formula(3, fila, "D" + (filaSubtotal + 1) + "+D" + fila, celdaHeader);
                hoja1.addCell(formula);
            }
            catch (RowsExceededException e)
            {
                e.printStackTrace();
            }
            catch (WriteException e)
            {
                e.printStackTrace();
            }
            libro1.write();
            libro1.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        catch (WriteException e)
        {
            e.printStackTrace();
        }
        catch (IllegalArgumentException e)
        {
            e.printStackTrace();
        }
        catch (SecurityException e)
        {
            e.printStackTrace();
        }
        return out.toByteArray();
    }

    @PostMapping("/generarPDFGastoCuentaCobro")
    public byte[] generarPDFGastoCuentaCobro(@RequestBody Map<String, Object> json) throws JRException {
        LegalizacionGasto gasto = eventoServices.getLegalizacion((int) json.get("idEvento"),(int) json.get("idLegalizacion"));
        String sede = (String) json.get("sede");
        int idEvento = (int) json.get("idEvento");
        String nombreEvento = (String) json.get("nombreEvento");

        SimpleDateFormat sdf = new SimpleDateFormat("dd 'de' MMMM 'de' yyyy");
        DecimalFormat df = new DecimalFormat("'$' #,##0");
        Calendar fecha = Calendar.getInstance(new Locale("es", "CO"));
        JasperReport masterReport = null;
        File reportFile = null;
        reportFile = new File(ApplicationService.getFilesDirectory() + "/formatosPDF/jasper/CuentaDeCobro.jasper");
        masterReport = (JasperReport) JRLoader.loadObject(reportFile);
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("NOMBRE", gasto.getNombre());
        parameters.put("IDENTIFICACION", gasto.getIdentificacion());
        parameters.put("TIPO_IDENTIFICACION", "C.C.");
        parameters.put("VALOR", df.format(gasto.getValor()));
        parameters.put("CIUDAD_FECHA", sede + ", " + sdf.format(fecha.getTime()));
        parameters.put("ALOJAMIENTO", gasto.getValorAlojamiento() == 0 ? "" : df.format(gasto.getValorAlojamiento()));
        parameters.put("TRANSPORTE", gasto.getValorTransporte() == 0 ? "" : df.format(gasto.getValorTransporte()));
        parameters.put("TIPO_TRANSPORTE", (gasto.isTerrestre() ? "TERRESTRE" : "") + (gasto.isTerrestre() && gasto.isFluvial() ? " y " : "") + (gasto.isFluvial() ? "FLUVIAL" : ""));
        parameters.put("ALIMENTACION", gasto.getValorAlimentacion() == 0 ? "" : df.format(gasto.getValorAlimentacion()));
        parameters.put("CONCEPTO", gasto.getConcepto());
        parameters.put("RUTA", gasto.getRuta());
        parameters.put("TOTAL_ALIALO", df.format(gasto.getValorAlimentacion() + gasto.getValorAlojamiento()));
        parameters.put("EVENTO_ID", "" + idEvento);
        parameters.put("EVENTO_NOMBRE", "" + nombreEvento);
        parameters.put("CONTRATO_NOMBRE", "" + admin.getContrato((int) json.get("idContrato")).getNombre());

        try {
            // Preparación del reporte (reporte diseñado con ireport)
            JasperPrint jasperPrint = JasperFillManager.fillReport(masterReport, parameters, new JREmptyDataSource());
            byte[] pdf = JasperExportManager.exportReportToPdf(jasperPrint);
            return pdf;
        } catch (JRException e) {
            e.printStackTrace();
        }
        return null;
    }

    @PostMapping("/generarPDFViaticoCuentaCobro")
    public byte[] generarPDFViaticoCuentaCobro(@RequestBody Map<String, Object> json) throws JRException {
        EventoViaticos viatico = eventoServices.getViatico((int) json.get("idEvento"),(int) json.get("idLegalizacion"));
        String sede = (String) json.get("sede");
        int idEvento = (int) json.get("idEvento");
        String nombreEvento = (String) json.get("nombreEvento");

        System.out.println(viatico.getValor());

        SimpleDateFormat sdf = new SimpleDateFormat("dd 'de' MMMM 'de' yyyy");
        DecimalFormat df = new DecimalFormat("'$' #,##0");
        Calendar fecha = Calendar.getInstance(new Locale("es", "CO"));
        JasperReport masterReport;
        File reportFile;
        reportFile = new File(ApplicationService.getFilesDirectory() + "/formatosPDF/jasper/CuentaDeCobro.jasper");
        masterReport = (JasperReport) JRLoader.loadObject(reportFile);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("NOMBRE", viatico.getNombre());
        parameters.put("IDENTIFICACION", viatico.getIdentificacion());
        parameters.put("TIPO_IDENTIFICACION", "C.C.");
        parameters.put("VALOR", df.format(viatico.getValor()));
        parameters.put("CIUDAD_FECHA", sede + ", " + sdf.format(fecha.getTime()));
        parameters.put("ALOJAMIENTO", viatico.getValorAlojamiento() == 0 ? "" : df.format(viatico.getValorAlojamiento()));
        parameters.put("TRANSPORTE", viatico.getValorTransporte() == 0 ? "" : df.format(viatico.getValorTransporte()));
        parameters.put("TIPO_TRANSPORTE", (viatico.isTerrestre() ? "TERRESTRE" : "") + (viatico.isTerrestre() && viatico.isFluvial() ? " y " : "") + (viatico.isFluvial() ? "FLUVIAL" : ""));
        parameters.put("ALIMENTACION", viatico.getValorAlimentacion() == 0 ? "" : df.format(viatico.getValorAlimentacion()));
        parameters.put("CONCEPTO", viatico.getConcepto());
        parameters.put("RUTA", viatico.getRuta());
        parameters.put("TOTAL_ALIALO", df.format(viatico.getValorAlimentacion() + viatico.getValorAlojamiento()));
        parameters.put("EVENTO_ID", "" + idEvento);
        parameters.put("EVENTO_NOMBRE", "" + nombreEvento);
        parameters.put("CONTRATO_NOMBRE", "" + admin.getContrato((int) json.get("idContrato")).getNombre());

        try {
            // Preparación del reporte (reporte diseñado con ireport)
            JasperPrint jasperPrint = JasperFillManager.fillReport(masterReport, parameters, new JREmptyDataSource());
            byte[] pdf = JasperExportManager.exportReportToPdf(jasperPrint);
            return pdf;
        } catch (JRException e) {
            e.printStackTrace();
        }
        return null;
    }

    @PostMapping("/generarPDFDocumentoEquivalenteGasto")
    public byte[] generarPDFDocumentoEquivalenteGasto(@RequestBody Map<String, Object> json) {
        JasperReport masterReport;
        File reportFile;
        Evento evento = eventoServices.getEvento((int) json.get("idEvento"));
        Contrato contrato = admin.getContrato((int) json.get("idContrato"), false);
        LegalizacionGasto gasto = eventoServices.getLegalizacion((int) json.get("idEvento"), (int) json.get("idLegalizacion"));
        Usuario usuario = admin.getUsuario((int) json.get("idUsuario"),false);
        ArrayList<EventoViaticos> viaticos = eventoServices.getViaticos((int) json.get("idEvento"));
        try
        {
            reportFile = new File(ApplicationService.getFilesDirectory() + "/formatosPDF/jasper/DocumentoEquivalente.jasper");
            masterReport = (JasperReport) JRLoader.loadObject(reportFile);
            Map<String, Object> parameters = new HashMap<>();
            Calendar fecha = Calendar.getInstance(new Locale("es", "CO"));
            parameters.put("EVENTO_ID", "" + evento.getId());
            parameters.put("NOMBRE_EMPRESA", ApplicationService.getParametro("empresa.nombre"));
            parameters.put("NIT_EMPRESA", ApplicationService.getParametro("empresa.nit"));
            if (contrato.getNitCompania() != null)
            {
                if (!"".equals(contrato.getNitCompania()))
                {
                    parameters.put("NIT_EMPRESA", contrato.getNitCompania());
                }
            }
            if (contrato.getNombreCompania() != null)
            {
                if (!"".equals(contrato.getNombreCompania()))
                {
                    parameters.put("NOMBRE_EMPRESA", contrato.getNombreCompania());
                }
            }
            parameters.put("LOGO", leerArchivo("logoNuevo398x147.png"));
            parameters.put("NOMBRE_EVENTO", evento.getNombre());
            parameters.put("PROGRAMA", evento.getTexto1());
            parameters.put("RESPONSABLE", admin.getPersona(evento.getIdResponsable(), false).getNombre());
            parameters.put("SEDE", evento.getSede().getNombre());
            fecha.setTime(evento.getFechaInicioEvento());
            parameters.put("FECHA_INICIAL", fecha.getTime());
            parameters.put("NUMERO_CONTRATO", contrato.getNumero());
            fecha.setTime(evento.getFechaFinEvento());
            parameters.put("FECHA_FINAL", fecha.getTime());
            parameters.put("RESUMEN", evento.getResumenEjecutivo());
            parameters.put("TOTAL_ANTICIPOS", (double) 0);
            ArrayList<HashMap<String, Object>> gastos = new ArrayList<>();
            if (gasto != null)
            {
                parameters.put("NUMERO", gasto.getNumeroDocEquivalente());
                parameters.put("DIRECCION", gasto.getDireccion());
                parameters.put("TELEFONO", gasto.getTelefono());
                parameters.put("IDENTIFICACION", gasto.getIdentificacion());
                parameters.put("USUARIO", gasto.getNombre());
                HashMap<String, Object> gastoMap = new HashMap<>();
                gastoMap.put("FECHA", gasto.getFecha());
                gastoMap.put("CONCEPTO", gasto.getConcepto());
                gastoMap.put("RUTA", gasto.getRuta());
                gastoMap.put("VALOR", gasto.getValor());
                if (gasto.getRetencionIVA() > 0)
                {
                    gastoMap.put("TARIFA_IVA", 16d);
                    gastoMap.put("IVA_TEORICO", gasto.getValor() * 0.16);
                    gastoMap.put("TARIFA_RETENCIONIVA", 15d);
                    gastoMap.put("RETEIVA_ASUMIDO", gasto.getRetencionIVA());
                }
                else
                {
                    gastoMap.put("TARIFA_IVA", (double) 0);
                    gastoMap.put("IVA_TEORICO", (double) 0);
                    gastoMap.put("TARIFA_RETENCIONIVA", (double) 0);
                    gastoMap.put("RETEIVA_ASUMIDO", (double) 0);
                }
                gastoMap.put("RETERENTA", gasto.getRetencionRenta());
                gastos.add(gastoMap);
            }
            else
            {
                parameters.put("NUMERO", evento.getNumeroDocEquivalente());
                parameters.put("DIRECCION", usuario.getDireccion());
                parameters.put("TELEFONO", usuario.getTelefono());
                parameters.put("IDENTIFICACION", usuario.getIdentificacion());
                parameters.put("USUARIO", usuario.getNombre());
                double totalItem = 0;
                for (EventoViaticos gastot : viaticos)
                {
                    if (gastot.getTipo().equals("TRANSPORTE_VIATICOS"))
                    {
                        totalItem += gastot.getValor();
                    }
                }
                HashMap<String, Object> gastoMap = new HashMap<>();
                gastoMap.put("FECHA", fecha);
                gastoMap.put("CONCEPTO", "RECONOCIMIENTO DE TRANSPORTES");
                gastoMap.put("RUTA", "");
                gastoMap.put("VALOR", totalItem);
                gastoMap.put("TARIFA_IVA", (double) 0);
                gastoMap.put("IVA_TEORICO", (double) 0);
                gastoMap.put("TARIFA_RETENCIONIVA", (double) 0);
                gastoMap.put("RETEIVA_ASUMIDO", (double) 0);
                gastos.add(gastoMap);
                totalItem = 0;
                for (EventoViaticos gastot : viaticos)
                {
                    if (gastot.getTipo().equals("ALIMENTACION_VIATICOS"))
                    {
                        totalItem += gastot.getValor();
                    }
                }
                gastoMap = new HashMap<>();
                gastoMap.put("FECHA", fecha);
                gastoMap.put("CONCEPTO", "RECONOCIMIENTO DE ALIMIENTACION");
                gastoMap.put("RUTA", "");
                gastoMap.put("VALOR", totalItem);
                gastoMap.put("TARIFA_IVA", (double) 0);
                gastoMap.put("IVA_TEORICO", (double) 0);
                gastoMap.put("TARIFA_RETENCIONIVA", (double) 0);
                gastoMap.put("RETEIVA_ASUMIDO", (double) 0);
                gastos.add(gastoMap);
                totalItem = 0;
                for (EventoViaticos gastot : viaticos)
                {
                    if (gastot.getTipo().equals("ALOJAMIENTO_VIATICOS"))
                    {
                        totalItem += gastot.getValor();
                    }
                }
                gastoMap = new HashMap<>();
                gastoMap.put("FECHA", fecha);
                gastoMap.put("CONCEPTO", "RECONOCIMIENTO DE ALOJAMIENTO");
                gastoMap.put("RUTA", "");
                gastoMap.put("VALOR", totalItem);
                gastoMap.put("TARIFA_IVA", (double) 0);
                gastoMap.put("IVA_TEORICO", (double) 0);
                gastoMap.put("TARIFA_RETENCIONIVA", (double) 0);
                gastoMap.put("RETEIVA_ASUMIDO", (double) 0);
                gastos.add(gastoMap);
            }
            parameters.put("TOTAL_REGISTROS", gastos.size());
            JRHashDataSource datos = new JRHashDataSource(gastos);
            try
            {
                JasperPrint jasperPrint = JasperFillManager.fillReport(masterReport, parameters, datos);
                byte[] pdf = JasperExportManager.exportReportToPdf(jasperPrint);
                return pdf;
            }
            catch (JRException e)
            {
                e.printStackTrace();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @PostMapping("/generarPDFDocumentoEquivalenteViatico")
    public byte[] generarPDFDocumentoEquivalenteViatico(@RequestBody Map<String, Object> json) {
        JasperReport masterReport;
        File reportFile;
        Evento evento = eventoServices.getEvento((int) json.get("idEvento"));
        Contrato contrato = admin.getContrato((int) json.get("idContrato"), false);
        EventoViaticos gasto = null;
        Usuario usuario = admin.getUsuario((int) json.get("idUsuario"),false);
        ArrayList<EventoViaticos> viaticos = eventoServices.getViaticos((int) json.get("idEvento"));
        try
        {
            reportFile = new File(ApplicationService.getFilesDirectory() + "/formatosPDF/jasper/DocumentoEquivalente.jasper");
            masterReport = (JasperReport) JRLoader.loadObject(reportFile);
            Map<String, Object> parameters = new HashMap<>();
            Calendar fecha = Calendar.getInstance(new Locale("es", "CO"));
            parameters.put("EVENTO_ID", "" + evento.getId());
            parameters.put("NOMBRE_EMPRESA", ApplicationService.getParametro("empresa.nombre"));
            parameters.put("NIT_EMPRESA", ApplicationService.getParametro("empresa.nit"));
            if (contrato.getNitCompania() != null)
            {
                if (!"".equals(contrato.getNitCompania()))
                {
                    parameters.put("NIT_EMPRESA", contrato.getNitCompania());
                }
            }
            if (contrato.getNombreCompania() != null)
            {
                if (!"".equals(contrato.getNombreCompania()))
                {
                    parameters.put("NOMBRE_EMPRESA", contrato.getNombreCompania());
                }
            }
            parameters.put("LOGO", leerArchivo("logoNuevo398x147.png"));
            parameters.put("NOMBRE_EVENTO", evento.getNombre());
            parameters.put("PROGRAMA", evento.getTexto1());
            parameters.put("RESPONSABLE", admin.getPersona(evento.getIdResponsable(), false).getNombre());
            parameters.put("SEDE", evento.getSede().getNombre());
            fecha.setTime(evento.getFechaInicioEvento());
            parameters.put("FECHA_INICIAL", fecha.getTime());
            parameters.put("NUMERO_CONTRATO", contrato.getNumero());
            fecha.setTime(evento.getFechaFinEvento());
            parameters.put("FECHA_FINAL", fecha.getTime());
            parameters.put("RESUMEN", evento.getResumenEjecutivo());
            parameters.put("TOTAL_ANTICIPOS", (double) 0);
            ArrayList<HashMap<String, Object>> gastos = new ArrayList<>();
            if (gasto != null)
            {
                parameters.put("NUMERO", gasto.getNumeroDocEquivalente());
                parameters.put("DIRECCION", gasto.getDireccion());
                parameters.put("TELEFONO", gasto.getTelefono());
                parameters.put("IDENTIFICACION", gasto.getIdentificacion());
                parameters.put("USUARIO", gasto.getNombre());
                HashMap<String, Object> gastoMap = new HashMap<>();
                gastoMap.put("FECHA", gasto.getFecha());
                gastoMap.put("CONCEPTO", gasto.getConcepto());
                gastoMap.put("RUTA", gasto.getRuta());
                gastoMap.put("VALOR", gasto.getValor());
                if (gasto.getRetencionIVA() > 0)
                {
                    gastoMap.put("TARIFA_IVA", 16d);
                    gastoMap.put("IVA_TEORICO", gasto.getValor() * 0.16);
                    gastoMap.put("TARIFA_RETENCIONIVA", 15d);
                    gastoMap.put("RETEIVA_ASUMIDO", gasto.getRetencionIVA());
                }
                else
                {
                    gastoMap.put("TARIFA_IVA", (double) 0);
                    gastoMap.put("IVA_TEORICO", (double) 0);
                    gastoMap.put("TARIFA_RETENCIONIVA", (double) 0);
                    gastoMap.put("RETEIVA_ASUMIDO", (double) 0);
                }
                gastoMap.put("RETERENTA", gasto.getRetencionRenta());
                gastos.add(gastoMap);
            }
            else
            {
                parameters.put("NUMERO", evento.getNumeroDocEquivalente());
                parameters.put("DIRECCION", usuario.getDireccion());
                parameters.put("TELEFONO", usuario.getTelefono());
                parameters.put("IDENTIFICACION", usuario.getIdentificacion());
                parameters.put("USUARIO", usuario.getNombre());
                double totalItem = 0;
                for (EventoViaticos gastot : viaticos)
                {
                    if (gastot.getTipo().equals("TRANSPORTE_VIATICOS"))
                    {
                        totalItem += gastot.getValor();
                    }
                }
                HashMap<String, Object> gastoMap = new HashMap<>();
                gastoMap.put("FECHA", fecha);
                gastoMap.put("CONCEPTO", "RECONOCIMIENTO DE TRANSPORTES");
                gastoMap.put("RUTA", "");
                gastoMap.put("VALOR", totalItem);
                gastoMap.put("TARIFA_IVA", (double) 0);
                gastoMap.put("IVA_TEORICO", (double) 0);
                gastoMap.put("TARIFA_RETENCIONIVA", (double) 0);
                gastoMap.put("RETEIVA_ASUMIDO", (double) 0);
                gastos.add(gastoMap);
                totalItem = 0;
                for (EventoViaticos gastot : viaticos)
                {
                    if (gastot.getTipo().equals("ALIMENTACION_VIATICOS"))
                    {
                        totalItem += gastot.getValor();
                    }
                }
                gastoMap = new HashMap<>();
                gastoMap.put("FECHA", fecha);
                gastoMap.put("CONCEPTO", "RECONOCIMIENTO DE ALIMIENTACION");
                gastoMap.put("RUTA", "");
                gastoMap.put("VALOR", totalItem);
                gastoMap.put("TARIFA_IVA", (double) 0);
                gastoMap.put("IVA_TEORICO", (double) 0);
                gastoMap.put("TARIFA_RETENCIONIVA", (double) 0);
                gastoMap.put("RETEIVA_ASUMIDO", (double) 0);
                gastos.add(gastoMap);
                totalItem = 0;
                for (EventoViaticos gastot : viaticos)
                {
                    if (gastot.getTipo().equals("ALOJAMIENTO_VIATICOS"))
                    {
                        totalItem += gastot.getValor();
                    }
                }
                gastoMap = new HashMap<>();
                gastoMap.put("FECHA", fecha);
                gastoMap.put("CONCEPTO", "RECONOCIMIENTO DE ALOJAMIENTO");
                gastoMap.put("RUTA", "");
                gastoMap.put("VALOR", totalItem);
                gastoMap.put("TARIFA_IVA", (double) 0);
                gastoMap.put("IVA_TEORICO", (double) 0);
                gastoMap.put("TARIFA_RETENCIONIVA", (double) 0);
                gastoMap.put("RETEIVA_ASUMIDO", (double) 0);
                gastos.add(gastoMap);
            }
            parameters.put("TOTAL_REGISTROS", gastos.size());
            JRHashDataSource datos = new JRHashDataSource(gastos);
            try
            {
                JasperPrint jasperPrint = JasperFillManager.fillReport(masterReport, parameters, datos);
                byte[] pdf = JasperExportManager.exportReportToPdf(jasperPrint);
                return pdf;
            }
            catch (JRException e)
            {
                e.printStackTrace();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @PostMapping("/generarPDFLegalizacionGastos")
    public byte[] generarPDFLegalizacionGastos(@RequestBody Map<String, Object> json) {
        JasperReport masterReport;
        File reportFile;
        Evento evento = eventoServices.getEvento((int) json.get("idEvento"));
        Contrato contrato = admin.getContrato((int) json.get("idContrato"), false);
        ArrayList<LegalizacionGasto> gastosBD = eventoServices.getLegalizaciones((int) json.get("idEvento"));
        Usuario usuario = admin.getUsuario((int) json.get("idUsuario"),false);
        ArrayList<EventoViaticos> viaticos = eventoServices.getViaticos((int) json.get("idEvento"));
        double anticipos = Double.parseDouble((String) json.get("anticipo"));
        try
        {
            reportFile = new File(ApplicationService.getFilesDirectory() + "/formatosPDF/jasper/LegalizacionGastos.jasper");
            masterReport = (JasperReport) JRLoader.loadObject(reportFile);
            Map<String, Object> parameters = new HashMap<>();
            Calendar fecha = Calendar.getInstance(new Locale("es", "CO"));
            parameters.put("EVENTO_ID", "" + evento.getId());
            parameters.put("NOMBRE_EMPRESA", ApplicationService.getParametro("empresa.nombre"));
            parameters.put("NIT_EMPRESA", ApplicationService.getParametro("empresa.nit"));
            if (contrato.getNitCompania() != null)
            {
                if (!"".equals(contrato.getNitCompania()))
                {
                    parameters.put("NIT_EMPRESA", contrato.getNitCompania());
                }
            }
            if (contrato.getNombreCompania() != null)
            {
                if (!"".equals(contrato.getNombreCompania()))
                {
                    parameters.put("NOMBRE_EMPRESA", contrato.getNombreCompania());
                }
            }
            parameters.put("LOGO", leerArchivo("logoNuevo398x147.png"));
            parameters.put("NOMBRE_EVENTO", evento.getNombre());
            parameters.put("TITULO_REPORTE", "RESUMEN DE EVENTO");
            parameters.put("PROGRAMA", evento.getTexto1());
            parameters.put("RESPONSABLE", admin.getPersona(evento.getIdResponsable(), false).getNombre());
            parameters.put("SEDE", evento.getSede().getNombre());
            fecha.setTime(evento.getFechaInicioEvento());
            parameters.put("FECHA_INICIAL", fecha.getTime());
            parameters.put("COORDINADOR", admin.getPersona(evento.getIdCoordinador(), false).getNombre());
            parameters.put("SOLICITANTE", evento.getSolicitante());
            parameters.put("USUARIO", usuario.getNombre());
            parameters.put("NUMERO_CONTRATO", contrato.getNumero());
            fecha.setTime(evento.getFechaFinEvento());
            parameters.put("FECHA_FINAL", fecha.getTime());
            parameters.put("RESUMEN", evento.getResumenEjecutivo());
            parameters.put("TOTAL_ANTICIPOS", anticipos);
            ArrayList<HashMap<String, Object>> gastos = new ArrayList<>();
            if (gastosBD != null && gastosBD.size() > 0) {
                Collections.sort(gastosBD, (o1, o2) -> o1.getFecha().compareTo(o1.getFecha()));
                for (LegalizacionGasto gasto : gastosBD)
                {
                    if (!gasto.getTipo().equals("ANTICIPO"))
                    {
                        HashMap<String, Object> gastoMap = new HashMap<>();
                        gastoMap.put("FECHA", gasto.getFecha());
                        gastoMap.put("CONCEPTO", gasto.getConcepto());
                        gastoMap.put("RUTA", gasto.getRuta());
                        gastoMap.put("VALOR", gasto.getValor());
                        gastos.add(gastoMap);
                    }
                }
            }

            if (viaticos != null && viaticos.size() > 0) {
                Collections.sort(viaticos, (o1, o2) -> o1.getFecha().compareTo(o1.getFecha()));
                for (EventoViaticos gasto : viaticos)
                {
                    HashMap<String, Object> gastoMap = new HashMap<>();
                    gastoMap.put("FECHA", gasto.getFecha());
                    gastoMap.put("CONCEPTO", gasto.getConcepto());
                    gastoMap.put("RUTA", gasto.getRuta());
                    gastoMap.put("VALOR", gasto.getValor());
                    gastos.add(gastoMap);
                }
            }
            JRHashDataSource datos = new JRHashDataSource(gastos);
            try
            {
                JasperPrint jasperPrint = JasperFillManager.fillReport(masterReport, parameters, datos);
                byte[] pdf = JasperExportManager.exportReportToPdf(jasperPrint);
                return pdf;
            }
            catch (JRException e)
            {
                e.printStackTrace();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @PostMapping("/generarExcelLegalizaciones")
    public byte[] exportarGastos(@RequestBody Map<String, Object> json) {
        String propiedades[] = new String[] { "Fecha", "Tipo", "Concepto", "Ruta", "Valor", "Identificacion", "Nombres", "Apellidos" };
        ArrayList<LegalizacionGasto> legalizaciones = eventoServices.getLegalizaciones((int) json.get("idEvento"));
        return generarExcel(admin.getObjetos(legalizaciones), propiedades);
    }

    @PostMapping("/generarExcelTiquetes")
    public byte[] exportarTiquetes(@RequestBody Map<String, Object> json) {
        String propiedades[] = new String[] { "nombrePasajero", "aerolinea.nombre", "ruta", "numero", "valor", "impuestos", "tasas",
                "administrativo", "fee", "modificacion", "total", "fechaSalida","horaSalida", "fechaRegreso", "horaRegreso",
                "observaciones", "estado", "localizador", "factura", "valorEvento" };
        ArrayList<TiqueteEvento> tiquetes = eventoServices.getTiquetes((int) json.get("idEvento"));
        System.out.println(tiquetes.size());
        return generarExcel(admin.getObjetos(tiquetes), propiedades);
    }

    public byte[] generarExcel(ArrayList<Object> datos, String properties[]) {
        if (!datos.isEmpty())
        {
            Class clase = datos.get(0).getClass();
            ByteArrayOutputStream out = null;
            try
            {
                out = new ByteArrayOutputStream();
                WritableWorkbook libro1 = Workbook.createWorkbook(out);
                WritableSheet hoja1 = libro1.createSheet("Datos", 0);
                WritableFont times16font = new WritableFont(WritableFont.ARIAL, 10, WritableFont.NO_BOLD, true);
                WritableFont times16fontBold = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, true);
                WritableCellFormat celda = new WritableCellFormat(times16font);
                WritableCellFormat celdaHeader = new WritableCellFormat(times16fontBold);
                int fila = 0;
                int columna = 0;
                Label label = new Label(columna, fila, "", celda);
                for (int i = 0; i < properties.length; i++)
                {
                    label = new Label(columna, fila, properties[i], celdaHeader);
                    try
                    {
                        hoja1.addCell(label);
                    }
                    catch (RowsExceededException e)
                    {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    catch (WriteException e)
                    {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    columna++;
                }
                fila++;
                for (Object obj : datos)
                {
                    columna = 0;
                    for (int i = 0; i < properties.length; i++)
                    {
                        Object value = null;
                        if (properties[i].indexOf(".") == -1) {
                            System.out.println(properties[i]);
                            value = clase.getMethod("get" + JSFUtil.primeraMayuscula(properties[i]), new Class[0]).invoke(obj, new Object[0]);
                        }else
                        {
                            StringTokenizer tokenizer2 = new StringTokenizer(properties[i], ".");
                            if (tokenizer2.hasMoreElements())
                            {
                                String token0 = (String) tokenizer2.nextElement();
                                String token1 = (String) tokenizer2.nextElement();
                                Object value0 = eventoServices.getPropertyValue(obj, token0);
                                if (value0 != null)
                                {
                                    Object value1 = eventoServices.getPropertyValue(value0, token1);
                                    if (value1 != null)
                                    {
                                        if (tokenizer2.hasMoreElements())
                                        {
                                            String token2 = (String) tokenizer2.nextElement();
                                            Object value2 = eventoServices.getPropertyValue(value1, token2);
                                            if (value2 != null)
                                            {
                                                value = value2;
                                            }
                                        }
                                        else
                                        {
                                            value = value1;
                                        }
                                    }
                                }
                            }
                        }
                        if (value != null)
                        {
                            if (value instanceof Double)
                            {
                                DecimalFormat df = new DecimalFormat("##0");
                                label = new Label(columna, fila, df.format(((Double) value).doubleValue()), celda);
                            }
                            else if (value instanceof Date)
                            {
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
                                label = new Label(columna, fila, sdf.format((Date) value), celda);
                            }
                            else
                            {
                                label = new Label(columna, fila, "" + (value == null ? "" : value), celda);
                            }
                        }
                        else
                        {
                            label = new Label(columna, fila, "", celda);
                        }
                        try
                        {
                            hoja1.addCell(label);
                        }
                        catch (RowsExceededException e)
                        {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        catch (WriteException e)
                        {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        columna++;
                    }
                    fila++;
                }
                libro1.write();
                libro1.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            catch (WriteException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (IllegalArgumentException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (IllegalAccessException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (InvocationTargetException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (NoSuchMethodException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return out.toByteArray();
        }
        return null;
    }

    @PostMapping("/generarExcelEventos")
    public byte[] generarExcelEventos(@RequestBody Map<String, Object> json) {
        //SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String where = "";

        if ((int)json.get("idEvento") != 0) {
            where = "WHERE evento_id = " + json.get("idEvento");
        }else if (json.get("idContrato") != "0") {
            where = "WHERE contrato_id = " + json.get("idContrato");
        }else if ((int)json.get("idEstado") != 0) {
            where = "WHERE estado_id = " + json.get("idEstado");
        }

        if (json.get("fechaInicio") != null && !json.get("fechaInicio").equals("") && json.get("fechaFin") != null && !json.get("fechaFin").equals("")) {
            //Date fechaInicio = sdf.format(json.get("fechaInicio"));
            String f_i = (String) json.get("fechaInicio");
            String f_f = (String) json.get("fechaFin");
            if (where.equals("")) {
                where = "WHERE fecha_inicio_evento >= '" + f_i + "' AND fecha_fin_evento <= '" + f_f + "'";
            }else {
                where += " AND fecha_inicio_evento >= '" + f_i + "' AND fecha_fin_evento <= '" + f_f + "'";
            }
        }

        String query = "SELECT evento_id AS id, nombre, fecha_inicio_evento, fecha_fin_evento,(SELECT nombre FROM municipio WHERE municipio_id = ev.sede_id) AS sede, (SELECT nombre FROM estado WHERE estado_id = ev.estado_id) AS estado, ";
               query += "numero_asistentes, cdp, orden, solicitante, (SELECT nombre || ' ' apellidos FROM persona WHERE persona_id = ev.responsable_id) AS responsable, ";
               query += "(SELECT nombre || ' ' apellidos FROM persona WHERE persona_id = ev.coordinador_id) AS coordinador, centrocostos, texto1, texto2, texto3, ";
               query += "tcontratado, tejecutado, tfacturado, tproveedores, tgastos, programa, observacionescarpeta FROM evento ev " + where;

        System.out.println(query);

        ArrayList<Object[]> resQuery = (ArrayList<Object[]>) ServiceLocator.getInstance().getBaseService().executeSQLQuery(query);

//        {"", "Estado", "", "CDP", "Orden", "Solicitante", "Coordinador", "Responsable", "Centro Costos", "Texto1", "Texto2", "Texto3", "Total Contratado", "Total Ejecutado", "Total Facturado", "Total Proveedor", "Total Gastos", "Programa", "Observacion Carpeta",  };

        ByteArrayOutputStream out = null;
        if (!resQuery.isEmpty()) {
            try {
                out = new ByteArrayOutputStream();
                WritableWorkbook libro1 = Workbook.createWorkbook(out);
                WritableSheet hoja1 = libro1.createSheet("Datos", 0);
                WritableFont times16font = new WritableFont(WritableFont.ARIAL, 10, WritableFont.NO_BOLD, true);
                WritableFont times16fontBold = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, true);
                WritableCellFormat celda = new WritableCellFormat(times16font);
                WritableCellFormat celdaHeader = new WritableCellFormat(times16fontBold);
                int fila = 0;
                int columna = 0;
                try {
                    Formula formula;
                    Number number;
                    Label label;
                    label = new Label(columna, fila, "Id", celdaHeader);
                    hoja1.addCell(label);
                    columna++;
                    label = new Label(columna, fila, "Nombre", celdaHeader);
                    hoja1.addCell(label);
                    columna++;
                    label = new Label(columna, fila, "Fecha Inicio Evento", celdaHeader);
                    hoja1.addCell(label);
                    columna++;
                    label = new Label(columna, fila, "Fecha Fin Evento", celdaHeader);
                    hoja1.addCell(label);
                    columna++;
                    label = new Label(columna, fila, "Sede", celdaHeader);
                    hoja1.addCell(label);
                    columna++;
                    label = new Label(columna, fila, "Estado", celdaHeader);
                    hoja1.addCell(label);
                    columna++;
                    label = new Label(columna, fila, "Número Asistentes", celdaHeader);
                    hoja1.addCell(label);
                    columna++;
                    label = new Label(columna, fila, "CDP", celdaHeader);
                    hoja1.addCell(label);
                    columna++;
                    label = new Label(columna, fila, "Orden", celdaHeader);
                    hoja1.addCell(label);
                    columna++;
                    label = new Label(columna, fila, "Solicitante", celdaHeader);
                    hoja1.addCell(label);
                    columna++;
                    label = new Label(columna, fila, "Responsable", celdaHeader);
                    hoja1.addCell(label);
                    columna++;
                    label = new Label(columna, fila, "Coordinador", celdaHeader);
                    hoja1.addCell(label);
                    columna++;
                    label = new Label(columna, fila, "Centro Costos", celdaHeader);
                    hoja1.addCell(label);
                    columna++;
                    label = new Label(columna, fila, "Texto 1", celdaHeader);
                    hoja1.addCell(label);
                    columna++;
                    label = new Label(columna, fila, "Texto 2", celdaHeader);
                    hoja1.addCell(label);
                    columna++;
                    label = new Label(columna, fila, "Texto 3", celdaHeader);
                    hoja1.addCell(label);
                    columna++;
                    label = new Label(columna, fila, "Total Contratado", celdaHeader);
                    hoja1.addCell(label);
                    columna++;
                    label = new Label(columna, fila, "Total Ejecutado", celdaHeader);
                    hoja1.addCell(label);
                    columna++;
                    label = new Label(columna, fila, "Total Facturado", celdaHeader);
                    hoja1.addCell(label);
                    columna++;
                    label = new Label(columna, fila, "Total Proveedores", celdaHeader);
                    hoja1.addCell(label);
                    columna++;
                    label = new Label(columna, fila, "Total Gastos", celdaHeader);
                    hoja1.addCell(label);
                    columna++;
                    label = new Label(columna, fila, "Programa", celdaHeader);
                    hoja1.addCell(label);
                    columna++;
                    label = new Label(columna, fila, "Observaciones Carpeta", celdaHeader);
                    hoja1.addCell(label);
                    for (Object[] objects : resQuery) {
                        fila++;
                        columna = 0;
                        label = new Label(columna, fila, "" + ((int) objects[0]), celda);
                        hoja1.addCell(label);
                        columna++;
                        label = new Label(columna, fila, ((String) objects[1]), celda);
                        hoja1.addCell(label);
                        columna++;
                        label = new Label(columna, fila, ""+(objects[2]), celda);
                        hoja1.addCell(label);
                        columna++;
                        label = new Label(columna, fila, ""+(objects[3]), celda);
                        hoja1.addCell(label);
                        columna++;
                        label = new Label(columna, fila, ((String) objects[4]), celda);
                        hoja1.addCell(label);
                        columna++;
                        label = new Label(columna, fila, ((String) objects[5]), celda);
                        hoja1.addCell(label);
                        columna++;
                        label = new Label(columna, fila, "" + ((int) objects[6]), celda);
                        hoja1.addCell(label);
                        columna++;
                        label = new Label(columna, fila, ((String) objects[7]), celda);
                        hoja1.addCell(label);
                        columna++;
                        label = new Label(columna, fila, ((String) objects[8]), celda);
                        hoja1.addCell(label);
                        columna++;
                        label = new Label(columna, fila, ((String) objects[9]), celda);
                        hoja1.addCell(label);
                        columna++;
                        label = new Label(columna, fila, ((String) objects[10]), celda);
                        hoja1.addCell(label);
                        columna++;
                        label = new Label(columna, fila, ((String) objects[11]), celda);
                        hoja1.addCell(label);
                        columna++;
                        label = new Label(columna, fila, ((String) objects[12]), celda);
                        hoja1.addCell(label);
                        columna++;
                        label = new Label(columna, fila, ((String) objects[13]), celda);
                        hoja1.addCell(label);
                        columna++;
                        label = new Label(columna, fila, ((String) objects[14]), celda);
                        hoja1.addCell(label);
                        columna++;
                        label = new Label(columna, fila, ((String) objects[15]), celda);
                        hoja1.addCell(label);
                        columna++;
                        label = new Label(columna, fila, "" + ((double) objects[16]), celda);
                        hoja1.addCell(label);
                        columna++;
                        label = new Label(columna, fila, "" + ((double) objects[17]), celda);
                        hoja1.addCell(label);
                        columna++;
                        label = new Label(columna, fila, "" + ((double) objects[18]), celda);
                        hoja1.addCell(label);
                        columna++;
                        label = new Label(columna, fila, "" + ((double) objects[19]), celda);
                        hoja1.addCell(label);
                        columna++;
                        label = new Label(columna, fila, "" + ((double) objects[20]), celda);
                        hoja1.addCell(label);
                        columna++;
                        label = new Label(columna, fila, ((String) objects[21]), celda);
                        hoja1.addCell(label);
                        columna++;
                        label = new Label(columna, fila, ((String) objects[22]), celda);
                        hoja1.addCell(label);
                    }
                } catch (WriteException e) {
                    e.printStackTrace();
                }
                libro1.write();
                libro1.close();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (WriteException e) {
                e.printStackTrace();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (SecurityException e) {
                e.printStackTrace();
            }
        }
        return out.toByteArray();
    }
}
