package co.com.adescubrir.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.com.adescubrir.business.IBaseService;
import co.com.adescubrir.business.ServiceLocator;
import co.com.adescubrir.model.bo.TipoProveedor;

@RequestMapping("${spring.data.rest.basePath}/baseService/")
@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET, RequestMethod.POST})
public class BaseServiceServiceRest {

    private IBaseService getBaseService() {
        return ServiceLocator.getInstance().getBaseService();
    }

    @GetMapping("/getTiposProveedor")
    public Collection<TipoProveedor> getTiposProveedor()
    {
        ArrayList<String> properties = new ArrayList<>();
        Map<String, Object> propertyValues = new HashMap<>();

        ArrayList<TipoProveedor> datos = (ArrayList<TipoProveedor>) getBaseService().getByPropertiesNamed(TipoProveedor.class.getName(),properties, propertyValues, "");

        return datos;
    }
}
