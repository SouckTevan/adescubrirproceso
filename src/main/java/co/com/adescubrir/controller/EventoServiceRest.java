package co.com.adescubrir.controller;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import co.com.adescubrir.model.bo.eventos.*;
import org.apache.commons.io.IOUtils;
import org.hibernate.HibernateException;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;

import co.com.adescubrir.business.IBaseService;
import co.com.adescubrir.business.ServiceLocator;
import co.com.adescubrir.model.bo.Concepto;
import co.com.adescubrir.model.bo.EventoConsumoConcepto;
import co.com.adescubrir.model.bo.Persona;
import co.com.adescubrir.model.bo.Proveedor;
import co.com.adescubrir.model.bo.RangoNumeracion;
import co.com.adescubrir.model.bo.Tarifa;
import co.com.adescubrir.model.bo.contable.Anticipo;
import co.com.adescubrir.model.bo.contable.DetalleDocumento;
import co.com.adescubrir.model.bo.contable.DocumentoContable;
import co.com.adescubrir.model.bo.contable.TipoDocumentoContable;
import co.com.adescubrir.model.bo.security.Usuario;
import co.com.adescubrir.service.ApplicationService;
import co.com.adescubrir.util.Constants;
import co.com.mycommons.jsf.components.JSFUtil;
import co.com.mycommons.util.CorreoElectronico;
import co.com.mycommons.util.Formato;

@SuppressWarnings("unchecked")
@RequestMapping("/eventos/")
@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
public class EventoServiceRest {

	private IBaseService getBaseService() {
		return ServiceLocator.getInstance().getBaseService();
	}

	private AdministracionServiceRest admin = new AdministracionServiceRest();

	private final Path root = Paths.get("/backups/files/adescubrirWeb/");

	@Autowired
	private EmailServicesRest emailServicesRest;

	@Autowired
	ServletContext context;

	// ------- Return Todos los eventos filtrados
	@PostMapping("/homeEvento")
	public ArrayList<HomeEvento> homeEvento(@RequestBody HomeParameters dataFiltro, @RequestHeader("usuario") String usuario) {

		ArrayList<HomeEvento> retorno = new ArrayList<>();
		String where = "";

		if (dataFiltro.getIdEvento() != 0) {
			where = "WHERE evento_id = " + dataFiltro.getIdEvento();
		}
		if (dataFiltro.getIdContrato() != 0) {
			if (!where.equals("")) {
				where += " AND contrato_id = " + dataFiltro.getIdContrato();
			} else {
				where = "WHERE contrato_id = " + dataFiltro.getIdContrato();
			}
		}
		if (!dataFiltro.getIdMunicipio().equals("0")) {
			if (!where.equals("")) {
				where += " AND tipomunicipio = " + dataFiltro.getIdMunicipio();
			} else {
				where = "WHERE tipomunicipio = " + dataFiltro.getIdMunicipio();
			}
		}
		if (dataFiltro.getFechaInicio() != null && !dataFiltro.getFechaInicio().equals("") && dataFiltro.getFechaFin() != null
				&& !dataFiltro.getFechaFin().equals("")) {
			if (!where.equals("")) {
				where += " AND fecha_inicio_evento BETWEEN '" + dataFiltro.getFechaInicio() + "' AND '" + dataFiltro.getFechaFin() + "'";
			} else {
				where = "WHERE fecha_inicio_evento BETWEEN '" + dataFiltro.getFechaInicio() + "' AND '" + dataFiltro.getFechaFin() + "'";
			}
		} else {
			if (dataFiltro.getFechaInicio() != null && !dataFiltro.getFechaInicio().equals("")) {
				if (!where.equals("")) {
					where += " AND fecha_inicio_evento = '" + dataFiltro.getFechaInicio() + "'";
				} else {
					where = "WHERE fecha_inicio_evento = '" + dataFiltro.getFechaInicio() + "'";
				}
			}
			if (dataFiltro.getFechaFin() != null && !dataFiltro.getFechaFin().equals("")) {
				if (!where.equals("")) {
					where += " AND fecha_fin_evento = '" + dataFiltro.getFechaFin() + "'";
				} else {
					where = "WHERE fecha_fin_evento = '" + dataFiltro.getFechaFin() + "'";
				}
			}
		}
		if (dataFiltro.getIdEstado() != 0) {
			if (!where.equals("")) {
				where += " AND estado_id = " + dataFiltro.getIdEstado();
			} else {
				where = "WHERE estado_id = " + dataFiltro.getIdEstado();
			}
		}

		String query =
				"SELECT evento_id, nombre, cdp, orden, fecha_inicio_evento, fecha_fin_evento, desdediaanterior, hastadiasiguiente, numero_asistentes,"
						+ "(SELECT nombre FROM contrato WHERE contrato_id = ev.contrato_id) as contrato, (SELECT nombre FROM tipoevento WHERE tipoevento_id = ev.tipoevento_id) as tipoEvento,"
						+ "(SELECT nombre FROM municipio WHERE municipio_id = ev.sede_id) as municipio, tcontratado, tejecutado, tfacturado, tproveedores, tgastos, texto1,"
						+ "(SELECT nombre FROM estado WHERE estado_id = ev.estado_id) as estado, (SELECT (SELECT nombre FROM departamento WHERE departamento_id = mun.departamento_id) FROM municipio as mun WHERE  mun.municipio_id = ev.sede_id) as departamento,"
						+ "(SELECT nombre FROM vereda WHERE id = ev.vereda) as vereda, (SELECT nombre FROM resguardo WHERE id = ev.resguardo) as resguardo, (SELECT nombre FROM kumpania WHERE id = ev.kumpania) as kumpania, (SELECT nombre || ' ' apellidos FROM persona WHERE persona_id = ev.responsable_id) AS responsable,"
						+ "(SELECT nombre || ' ' apellidos FROM persona WHERE persona_id = ev.coordinador_id) AS coordinador FROM evento AS ev "
						+ where;

		System.out.println(query);

		ArrayList<Object[]> cont = (ArrayList<Object[]>) ServiceLocator.getInstance().getBaseService().executeSQLQuery(query);

		if (!cont.isEmpty()) {
			for (Object[] objects : cont) {
				HomeEvento data = new HomeEvento();
				data.setId((int) objects[0]);
				data.setNombre((String) objects[1]);
				data.setCdp((String) objects[2]);
				data.setOrden((String) objects[3]);
				data.setFechaInicioEvento((Date) objects[4]);
				data.setFechaFinEvento((Date) objects[5]);
				data.setDesdeDiaAnterior((boolean) objects[6]);
				data.setHastaDiaSiguiente((boolean) objects[7]);
				data.setNroAsistentes((int) objects[8]);
				data.setNombreContrato((String) objects[9]);
				data.setNombreTipoEvento((String) objects[10]);
				data.setNombreMunicipio((String) objects[11]);
				data.setTotalContratado((double) objects[12]);
				data.setTotalEjecutado((double) objects[13]);
				data.setTotalFacturado((double) objects[14]);
				data.setTotalProveedores((double) objects[15]);
				data.setTotalGastos((double) objects[16]);
				data.setTexto1((String) objects[17]);
				data.setNombreEstado((String) objects[18]);
				data.setNombreDepartamento((String) objects[19]);
				data.setNombreVereda((String) objects[20]);
				data.setNombreResguardo((String) objects[21]);
				data.setNombreKumpania((String) objects[22]);
				data.setResponsableActual((String) objects[23]);
				data.setNombreCoordinador((String) objects[24]);
				retorno.add(data);
			}
		}

		return retorno;
	}

	// -------------
	// @param Objeto evento a guardar
	// @return Un evento con todos los datos
	// -------------
	@PostMapping("/saveEvento")
	Evento saveEvento(@RequestBody Evento evento) throws Exception {
		// ---------- Se guarda en la tabla contrato
		// SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		evento.setFechaInicioEvento(convertTimezone(evento.getFechaInicioEvento(), 1));
		evento.setFechaFinEvento(convertTimezone(evento.getFechaFinEvento(), 1));
		evento.setFechaSolicitud(convertTimezone(evento.getFechaSolicitud(), 1));

		evento = (Evento) getBaseService().saveUpdate(evento);

		// ArrayList<EventoConsumos> consumos = new ArrayList<EventoConsumos>();
		// for (Map.Entry<String, EventoConsumoConcepto> concepto : evento.getEventoConsumos().entrySet()) {
		// for (Map.Entry<String, Integer> cantidad : concepto.getValue().getCantidades().entrySet()) {
		// EventoConsumos consumo = new EventoConsumos();
		// consumo.setIdEvento(evento.getId());
		// consumo.setIdConcepto(concepto.getValue().getConceptoId());
		// consumo.setFecha(sdf.parse(cantidad.getKey()));
		// consumo.setCantidad(cantidad.getValue());
		// consumos.add(consumo);
		// }
		// }
		// getBaseService().saveUpdateAll(consumos);
		// evento.setRangoFechas();
		// evento.setEventoConsumos(consultarConsumos(evento));

		// ArrayList<EventoEjecucion> ejecuciones = new ArrayList<EventoEjecucion>();
		// for (Map.Entry<String, EventoConsumoConcepto> concepto : evento.getEventoEjecucion().entrySet()) {
		// for (Map.Entry<String, Integer> cantidad : concepto.getValue().getCantidades().entrySet()) {
		// EventoEjecucion ejecucion = new EventoEjecucion();
		// ejecucion.setIdEvento(evento.getId());
		// ejecucion.setIdConcepto(concepto.getValue().getConceptoId());
		// ejecucion.setFecha(sdf.parse(cantidad.getKey()));
		// ejecucion.setCantidad(cantidad.getValue());
		// ejecuciones.add(ejecucion);
		// }
		// }
		// getBaseService().saveUpdateAll(ejecuciones);
		// evento.setEventoEjecucion(consultarEjecucion(evento));

		double totalContratado = 0;
		for (Map.Entry<String, EventoConsumoConcepto> concepto : evento.getEventoConsumos().entrySet()) {
			for (Map.Entry<String, Integer> cantidad : concepto.getValue().getCantidades().entrySet()) {
				if (cantidad.getValue() != 0)
					totalContratado += cantidad.getValue() * concepto.getValue().getTarifa();
			}
		}
		evento.setTotalContratado(totalContratado);
		double totalEjecutado = 0;
		for (Map.Entry<String, EventoConsumoConcepto> concepto : evento.getEventoEjecucion().entrySet()) {
			for (Map.Entry<String, Integer> cantidad : concepto.getValue().getCantidades().entrySet()) {
				if (cantidad.getValue() != 0)
					totalEjecutado += cantidad.getValue() * concepto.getValue().getTarifa();
			}
		}
		evento.setTotalEjecutado(totalEjecutado);

//		if(evento.getFactura() != null)
//		{
//			evento.setTotalFacturado(evento.getFactura().getTotal());
//		}

//		double totalGastos = 0;
//		for (LegalizacionGasto gasto : evento.getGastos())
//		{
//			totalGastos += gasto.getValor();
//		}
//		ArrayList<EventoViaticos> viaticos = getViaticos(evento.getId());
//		for (EventoViaticos gasto : viaticos)
//		{
//			totalGastos += gasto.getValor();
//			System.out.println(gasto.getValor());
//		}
//		evento.setTotalGastos(totalGastos);


		evento = (Evento) getBaseService().saveUpdate(evento);
		evento.setRangoFechas();
		ArrayList<TarifaContrato> tarifasContrato = (ArrayList<TarifaContrato>) consultarTarifasContrato(evento, evento.getAnoEvento());
		evento.setEventoConsumos(consultarConsumos(evento, tarifasContrato));
		evento.setEventoEjecucion(consultarEjecucion(evento, tarifasContrato));
		evento.setTarifasDelEvento((List<TarifaEvento>) getTarifasEvento(evento.getId(), true));
		evento.setFechaInicioEvento(convertTimezone(evento.getFechaInicioEvento(), -1));
		// evento.setFechaFinEvento(convertTimezone(evento.getFechaFinEvento(), -1));
		return evento;
	}

	public Date convertTimezone(Date fecha, int cantidad) {
		Calendar hoy = Calendar.getInstance();
		hoy.setTime(fecha);
		hoy.add(Calendar.DATE, cantidad);

		return hoy.getTime();
	}

	@PostMapping("/saveConsumos")
	public Map<String, EventoConsumoConcepto> saveConsumos(@RequestBody Evento evento) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		ArrayList<EventoConsumos> consumos = new ArrayList<>();
		for (Map.Entry<String, EventoConsumoConcepto> concepto : evento.getEventoConsumos().entrySet()) {
			for (Map.Entry<String, Integer> cantidad : concepto.getValue().getCantidades().entrySet()) {
				EventoConsumos consumo = new EventoConsumos();
				consumo.setIdEvento(evento.getId());
				consumo.setIdConcepto(concepto.getValue().getConceptoId());
				consumo.setFecha(sdf.parse(cantidad.getKey()));
				consumo.setCantidad(cantidad.getValue());
				consumos.add(consumo);
			}
		}
		getBaseService().saveUpdateAll(consumos);
		return consultarConsumos(evento, null);
	}

	@PostMapping("/saveEjecuciones")
	public Map<String, EventoConsumoConcepto> saveEjecuciones(@RequestBody Evento evento) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		ArrayList<EventoEjecucion> ejecuciones = new ArrayList<>();
		for (Map.Entry<String, EventoConsumoConcepto> concepto : evento.getEventoEjecucion().entrySet()) {
			for (Map.Entry<String, Integer> cantidad : concepto.getValue().getCantidades().entrySet()) {
				EventoEjecucion ejecucion = new EventoEjecucion();
				ejecucion.setIdEvento(evento.getId());
				ejecucion.setIdConcepto(concepto.getValue().getConceptoId());
				ejecucion.setFecha(sdf.parse(cantidad.getKey()));
				ejecucion.setCantidad(cantidad.getValue());
				ejecuciones.add(ejecucion);
			}
		}
		getBaseService().saveUpdateAll(ejecuciones);
		return consultarEjecucion(evento, null);
	}

	@GetMapping("/getEventoDocumentos/{idEvento}")
	public List<EventoDocumentos> getEventoDocumentosByEventId(@PathVariable int idEvento) {
		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		properties.add("evento_id = :evento_id");
		propertyValues.put("evento_id", idEvento);

		ArrayList<EventoDocumentos> datos =
				(ArrayList<EventoDocumentos>) getBaseService().getByPropertiesNamed(EventoDocumentos.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			for (EventoDocumentos eventoDoc : datos) {
				eventoDoc.setDocumento(admin.getDocumento(eventoDoc.getIdDocumento()));

				String query = "SELECT persona_id, nombres, nombre, apellidos FROM persona WHERE persona_id =" + eventoDoc.getIdUsuario();

				ArrayList<Object[]> cont = (ArrayList<Object[]>) ServiceLocator.getInstance().getBaseService().executeSQLQuery(query);

				if (!cont.isEmpty()) {
					for (Object[] objects : cont) {
						Usuario user = new Usuario();
						user.setId((int) objects[0]);
						if ((objects[1]) == null) {
							if (objects[3] == null) {
								user.setNombres((String) objects[2]);
							} else {
								user.setNombres(objects[2] + " " + objects[3]);
							}
						} else {
							user.setNombres((String) objects[1]);
						}
						eventoDoc.setUsuario(user);
					}
				}
			}
			return datos;
		}

		return null;
	}

	// -------------
	// @return Todos los tipos evento
	// -------------
	@GetMapping("/getEvento/{id}")
	public Evento getEvento(@PathVariable("id") int id) {
		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		properties.add("evento_id = :id");
		propertyValues.put("id", id);

		ArrayList<Evento> datos = (ArrayList<Evento>) getBaseService().getByPropertiesNamed(Evento.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			Evento evento = datos.get(0);
			// * evento.setDocumentos(getEventoDocumentosByEventId(evento.getId()));
			evento.setTarifasDelEvento((List<TarifaEvento>) getTarifasEvento(evento.getId(), true));
			// evento.setContrato(admin.getContrato(evento.getIdContrato(), false));
			// * evento.setAnticipos((List<Anticipo>) getAnticipos(evento.getId()));
			// * evento.setBitacora((List<Bitacora>) getBitacora(evento.getId()));
			// * evento.setEventoObservacion((List<EventoObservaciones>) getObservacion(evento.getId()));
			// * evento.setFacturacion((List<ObservacionFacturacion>) getObsFacturacion(evento.getId()));

			evento.setSede(admin.getMunicipioById(evento.getIdSede()));
			evento.setRangoFechas();
			ArrayList<TarifaContrato> tarifasContrato = (ArrayList<TarifaContrato>) consultarTarifasContrato(evento, evento.getAnoEvento());
			evento.setEventoConsumos(consultarConsumos(evento, tarifasContrato));
			evento.setEventoEjecucion(consultarEjecucion(evento, tarifasContrato));
			// evento.setTarifasContrato(getTarifasContrato(evento));
			// * evento.setCotizaciones((List<CotizacionEvento>) getEventoCotizaciones(evento.getId(), evento.getRangoFechas()));
			// * evento.setFactura(getFactura(evento.getId(), 0));
			return evento;
		}

		return null;
	}

	@PostMapping("/consultarConsumos")
	public Map<String, EventoConsumoConcepto> consultarConsumos(@RequestBody Evento evento, ArrayList<TarifaContrato> tarifasContrato) {
		if (tarifasContrato == null)
			tarifasContrato = (ArrayList<TarifaContrato>) consultarTarifasContrato(evento, evento.getAnoEvento());
		ArrayList<TarifaEvento> tarifasEvento = (ArrayList<TarifaEvento>) getTarifasEvento(evento.getId(), true);
		Map<String, EventoConsumos> consumosEvento = consultarConsumosEvento(evento);

		ArrayList<String> rangoFechas = (ArrayList<String>) evento.getRangoFechas();
		Map<Integer, TarifaContrato> tarifas = new HashMap<>();

		if (tarifasEvento != null) {
			for (TarifaEvento tarifaE : tarifasEvento) {
				TarifaContrato tarifaC = new TarifaContrato();
				tarifaC.setConcepto_id(tarifaE.getConcepto_id());
				tarifaC.setConcepto(tarifaE.getConcepto());
				tarifaC.setValor(tarifaE.getValor());
				tarifas.put(tarifaE.getConcepto_id(), tarifaC);
			}
		}
		for (TarifaContrato tarifaContrato : tarifasContrato) {
			if (!tarifas.containsKey(tarifaContrato.getConcepto_id())) {
				tarifas.put(tarifaContrato.getConcepto_id(), tarifaContrato);
			}
		}

		ArrayList<TarifaContrato> allTarifas = new ArrayList<>(tarifas.values());

		Map<String, EventoConsumoConcepto> retorno = new LinkedHashMap<>();
		for (TarifaContrato tarifaContrato : allTarifas) {
			if (retorno.containsKey("" + tarifaContrato.getConcepto_id())) {
				for (String fecha : rangoFechas) {
					retorno.get("" + tarifaContrato.getConcepto_id()).getCantidades().put(fecha, 0);
					if (consumosEvento.containsKey("" + tarifaContrato.getConcepto_id() + "_" + fecha)) {
						retorno.get("" + tarifaContrato.getConcepto_id()).getCantidades().put(fecha,
								consumosEvento.get("" + tarifaContrato.getConcepto_id() + "_" + fecha).getCantidad());
					}
				}
			} else {
				EventoConsumoConcepto concepto = new EventoConsumoConcepto();
				concepto.setConceptoId(tarifaContrato.getConcepto_id());
				concepto.setTarifa(tarifaContrato.getValor());
				concepto.setNombreConcepto(tarifaContrato.getConcepto().getNombre());
				for (String fecha : rangoFechas) {
					concepto.getCantidades().put(fecha, 0);
					if (consumosEvento.containsKey("" + tarifaContrato.getConcepto_id() + "_" + fecha)) {
						concepto.getCantidades().put(fecha, consumosEvento.get("" + tarifaContrato.getConcepto_id() + "_" + fecha).getCantidad());
					}
					concepto.setTotalTarifa(concepto.getTotalTarifa() + (concepto.getTarifa() * concepto.getCantidades().get(fecha)));
					concepto.setTotal(concepto.getTotal() + concepto.getCantidades().get(fecha));
				}
				retorno.put("" + tarifaContrato.getConcepto_id(), concepto);
			}
		}
		return retorno;
	}

	@PostMapping("/consultarEjecucion")
	public Map<String, EventoConsumoConcepto> consultarEjecucion(@RequestBody Evento evento, ArrayList<TarifaContrato> tarifasContrato) {
		if (tarifasContrato == null)
			tarifasContrato = (ArrayList<TarifaContrato>) consultarTarifasContrato(evento, evento.getAnoEvento());
		ArrayList<TarifaEvento> tarifasEvento = (ArrayList<TarifaEvento>) getTarifasEvento(evento.getId(), true);
		Map<String, EventoEjecucion> ejecucionEvento = consultarEjecucionEvento(evento);

		ArrayList<String> rangoFechas = (ArrayList<String>) evento.getRangoFechas();
		Map<Integer, TarifaContrato> tarifas = new HashMap<>();

		if (tarifasEvento != null) {
			for (TarifaEvento tarifaE : tarifasEvento) {
				TarifaContrato tarifaC = new TarifaContrato();
				tarifaC.setConcepto_id(tarifaE.getConcepto_id());
				tarifaC.setConcepto(tarifaE.getConcepto());
				tarifaC.setValor(tarifaE.getValor());
				tarifas.put(tarifaE.getConcepto_id(), tarifaC);
			}
		}

		for (TarifaContrato tarifaContrato : tarifasContrato) {
			if (!tarifas.containsKey(tarifaContrato.getConcepto_id())) {
				tarifas.put(tarifaContrato.getConcepto_id(), tarifaContrato);
			}
		}

		ArrayList<TarifaContrato> allTarifas = new ArrayList<>(tarifas.values());

		Map<String, EventoConsumoConcepto> retorno = new LinkedHashMap<>();
		for (TarifaContrato tarifaContrato : allTarifas) {
			if (retorno.containsKey("" + tarifaContrato.getConcepto_id())) {
				for (String fecha : rangoFechas) {
					retorno.get("" + tarifaContrato.getConcepto_id()).getCantidades().put(fecha, 0);
					if (ejecucionEvento.containsKey("" + tarifaContrato.getConcepto_id() + "_" + fecha)) {
						retorno.get("" + tarifaContrato.getConcepto_id()).getCantidades().put(fecha,
								ejecucionEvento.get("" + tarifaContrato.getConcepto_id() + "_" + fecha).getCantidad());
					}
				}
			} else {
				EventoConsumoConcepto concepto = new EventoConsumoConcepto();
				concepto.setConceptoId(tarifaContrato.getConcepto_id());
				concepto.setTarifa(tarifaContrato.getValor());
				concepto.setNombreConcepto(tarifaContrato.getConcepto().getNombre());
				for (String fecha : rangoFechas) {
					concepto.getCantidades().put(fecha, 0);
					if (ejecucionEvento.containsKey("" + tarifaContrato.getConcepto_id() + "_" + fecha)) {
						concepto.getCantidades().put(fecha, ejecucionEvento.get("" + tarifaContrato.getConcepto_id() + "_" + fecha).getCantidad());
					}
					concepto.setTotalTarifa(concepto.getTotalTarifa() + (concepto.getTarifa() * concepto.getCantidades().get(fecha)));
					concepto.setTotal(concepto.getTotal() + concepto.getCantidades().get(fecha));
				}
				retorno.put("" + tarifaContrato.getConcepto_id(), concepto);
			}
		}
		return retorno;
	}

	public Map<String, EventoConsumos> consultarConsumosEvento(Evento evento) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		properties.add("idEvento = :id");
		propertyValues.put("id", evento.getId());

		ArrayList<EventoConsumos> datos =
				(ArrayList<EventoConsumos>) getBaseService().getByPropertiesNamed(EventoConsumos.class.getName(), properties, propertyValues,
						"idConcepto, fecha");
		Map<String, EventoConsumos> retorno = new HashMap<>();
		for (EventoConsumos eventoConsumos : datos) {
			retorno.put("" + eventoConsumos.getIdConcepto() + "_" + sdf.format(eventoConsumos.getFecha()), eventoConsumos);
		}
		return retorno;
	}

	public Map<String, EventoEjecucion> consultarEjecucionEvento(Evento evento) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		properties.add("idEvento = :id");
		propertyValues.put("id", evento.getId());

		ArrayList<EventoEjecucion> datos =
				(ArrayList<EventoEjecucion>) getBaseService().getByPropertiesNamed(EventoEjecucion.class.getName(), properties, propertyValues,
						"idConcepto, fecha");
		Map<String, EventoEjecucion> retorno = new HashMap<>();
		for (EventoEjecucion eventoEjecucion : datos) {
			retorno.put("" + eventoEjecucion.getIdConcepto() + "_" + sdf.format(eventoEjecucion.getFecha()), eventoEjecucion);
		}
		return retorno;
	}

	/*
	 * public Map<String, EventoConsumoConcepto> calcularConsumosNuevos(Evento evento) { ArrayList<String> properties = new ArrayList<>(); Map<String, Object> propertyValues = new HashMap<>(); properties.add("contrato_id = :idContrato"); propertyValues.put("idContrato", evento.getIdContrato());
	 * properties.add("municipio_id = :idMunicipio"); propertyValues.put("idMunicipio", evento.getIdSede()); properties.add("tipotarifa_id = :idTarifa"); propertyValues.put("idTarifa", evento.getIdTipoTarifa()); properties.add("ano = :ano"); propertyValues.put("ano", 2020); ArrayList<TarifaContrato>
	 * datos = (ArrayList<TarifaContrato>) getBaseService().getByPropertiesNamed(TarifaContrato.class.getName(), properties, propertyValues, ""); Map<String, EventoConsumoConcepto> retorno = new LinkedHashMap<String, EventoConsumoConcepto>(); for (TarifaContrato tarifaContrato : datos) {
	 * EventoConsumoConcepto concepto = new EventoConsumoConcepto(); concepto.setConceptoId(tarifaContrato.getConcepto_id()); for (String fecha : evento.getRangoFechas()) { concepto.getCantidades().put(fecha, 0); } retorno.put("" + concepto.getConceptoId(), concepto); } return retorno; }
	 */

	/*
	 * public Map<String, EventoConsumoConcepto> calcularConsumos(Evento evento) { ArrayList<String> properties = new ArrayList<>(); Map<String, Object> propertyValues = new HashMap<>(); properties.add("contrato_id = :idContrato"); propertyValues.put("idContrato", evento.getIdContrato());
	 * properties.add("municipio_id = :idMunicipio"); propertyValues.put("idMunicipio", evento.getIdSede()); properties.add("tipotarifa_id = :idTarifa"); propertyValues.put("idTarifa", evento.getIdTipoTarifa()); properties.add("ano = :ano"); propertyValues.put("ano", 2020); ArrayList<TarifaContrato>
	 * datos = (ArrayList<TarifaContrato>) getBaseService().getByPropertiesNamed(TarifaContrato.class.getName(), properties, propertyValues, ""); Map<String, EventoConsumoConcepto> retorno = new LinkedHashMap<String, EventoConsumoConcepto>(); for (TarifaContrato tarifaContrato : datos) {
	 * EventoConsumoConcepto concepto = new EventoConsumoConcepto(); concepto.setConceptoId(tarifaContrato.getConcepto_id()); for (String fecha : evento.getRangoFechas()) { concepto.getCantidades().put(fecha, 0); } retorno.put("" + concepto.getConceptoId(), concepto); } return retorno; }
	 */

	public Collection<TarifaContrato> consultarTarifasContrato(Evento evento, int ano) {
		String sMunicipios = "";
		if (evento.getTipoMunicipio().equals("") || evento.getTipoMunicipio().equals("-1")) {
			sMunicipios = "" + evento.getIdSede();
		} else {
			if (evento.getSede().getDepartamento().getId() == 17)
				sMunicipios = "" + evento.getTipoMunicipio();
			else
				sMunicipios = "" + evento.getSede().getDepartamento().getId() + "" + evento.getTipoMunicipio();
		}
		System.out.println(sMunicipios);

		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		properties.add("contrato_id = :idContrato");
		propertyValues.put("idContrato", evento.getIdContrato());
		properties.add("municipio_id = :idMunicipio");
		propertyValues.put("idMunicipio", Integer.parseInt(sMunicipios));
		// properties.add("tipotarifa_id = :idTarifa");
		// propertyValues.put("idTarifa", evento.getIdTipoTarifa());
		properties.add("ano = :ano");
		propertyValues.put("ano", ano);
		ArrayList<TarifaContrato> datos =
				(ArrayList<TarifaContrato>) getBaseService().getByPropertiesNamed(TarifaContrato.class.getName(), properties, propertyValues, "");
		ArrayList<Integer> conceptos = new ArrayList<Integer>();
		for (TarifaContrato tarifaContrato : datos) {
			conceptos.add(tarifaContrato.getConcepto_id());
		}
		ArrayList<Concepto> listaConceptos = new ArrayList<Concepto>(); 
		if (!conceptos.isEmpty()) {
			properties.clear();
			propertyValues.clear();
			properties.add("id in (:id)");
			propertyValues.put("id", conceptos);
			listaConceptos =
					(ArrayList<Concepto>) getBaseService().getByPropertiesNamed(Concepto.class.getName(), properties, propertyValues, "");			
		}


		for (TarifaContrato tarifaContrato : datos) {
			Concepto busqueda = new Concepto();
			busqueda.setId(tarifaContrato.getConcepto_id());
			int index = listaConceptos.indexOf(busqueda);
			if (index != -1) {
				tarifaContrato.setConcepto(listaConceptos.get(index));	
			}
			
		}
		return datos;
	}

	public ArrayList<Map<String, String>> getTarifasContrato(Evento evento) {
		String query =
				"SELECT valor, (SELECT nombre FROM concepto WHERE concepto_id = ct.concepto_id) AS nombreConcepto"
						+ " FROM contrato_tarifa AS ct WHERE contrato_id = " + evento.getIdContrato() + " AND municipio_id = " + evento.getIdSede()
						+ " AND tipotarifa_id = " + evento.getIdTipoTarifa() + " AND ano = " + evento.getAno();

		ArrayList<Object[]> cont = (ArrayList<Object[]>) ServiceLocator.getInstance().getBaseService().executeSQLQuery(query);
		ArrayList<Map<String, String>> retorno = new ArrayList<>();

		if (!cont.isEmpty()) {
			for (Object[] objects : cont) {
				Map<String, String> mp = new HashMap<>();

				mp.put("valor", "" + ((double) objects[0]));
				mp.put("Nombre", ((String) objects[1]));

				retorno.add(mp);
			}
		}

		return retorno;
	}

	// -------------
	// @return Todos los tipos evento
	// -------------
	@GetMapping("/getTiposEventos")
	public Collection<TipoEvento> getTiposEventos() {
		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();

		ArrayList<TipoEvento> datos = (ArrayList<TipoEvento>) getBaseService().getByPropertiesNamed(TipoEvento.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			return datos;
		}

		return null;
	}

	// -------------
	// @param Objeto evento a guardar
	// @return Un evento con todos los datos
	// -------------
	@PostMapping("/saveEventoDocumento")
	EventoDocumentos saveEventoDocumento(@RequestParam("file") MultipartFile file, @RequestParam("documento") String evenDoc) throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		EventoDocumentos doc = objectMapper.readValue(evenDoc, EventoDocumentos.class);

		// --------- Se guarda el objeto Proveedor Tarifa
		if (file.getOriginalFilename().equals(doc.getNombreArchivo())) {
			String fileName = doc.getIdEvento() + "_" + doc.getFechaHora().getTime() + "_" + file.getOriginalFilename();
			doc.setRutaArchivo("/backups/files/adescubrirWeb/eventos/" + fileName);
			try {
				Files.copy(file.getInputStream(), this.root.resolve("eventos/" + fileName));
			} catch (Exception e) {
				throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
			}
			doc = (EventoDocumentos) getBaseService().saveUpdate(doc);
		}

		return doc;
	}

	// -------------
	// @return Devuelve todos las tarifas evento
	// -------------
	@GetMapping("/getTarifasEvento/{idEvento}")
	public Collection<TarifaEvento> getTarifasEvento(@PathVariable int idEvento, boolean conConcepto) {

		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		properties.add("evento_id = :evento_id");
		propertyValues.put("evento_id", idEvento);

		ArrayList<TarifaEvento> datos =
				(ArrayList<TarifaEvento>) getBaseService().getByPropertiesNamed(TarifaEvento.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			if (conConcepto) {
				for (TarifaEvento tarifaE : datos) {
					tarifaE.setConcepto(admin.getConceptoById(tarifaE.getConcepto_id()));
				}
			}
			System.out.println(datos.size());
			return datos;
		}
		return null;
	}

	// public ArrayList<String> validarAnticipo(@PathVariable int idPersona) {
	// Persona pAnt = admin.getPersona(idPersona, true);
	//
	// ArrayList<String> retorno = new ArrayList<String>();
	//
	// if (pAnt.getEmail() == null || (pAnt.getEmail() != null && pAnt.getEmail().equals("")))
	// {
	// retorno.add("El Tercero no tiene email");
	// }
	// if (pAnt.getCuentasBancarias().isEmpty())
	// {
	// retorno.add("El Tercero no tiene cuentas bancarias");
	// }
	// return retorno;
	// }

	// -------------
	// @return Genera en Anticipo
	// -------------
	@PostMapping("/generarAnticipo")
	public Anticipo generarAnticipo(HttpServletRequest request, @RequestBody Anticipo anticipo) throws Exception {
		System.out.println("anticipo.getIdBeneficiario(): "+anticipo.getIdBeneficiario());
		Persona pAnt = admin.getPersona(anticipo.getIdBeneficiario(), true);
		System.out.println("Beneficiario: "+pAnt);
		System.out.println("Email: "+pAnt.getEmail());
		//System.out.println("pAnt.getCuentasBancarias(): "+pAnt.getCuentasBancarias().size());
		Evento evento = getEvento(anticipo.getIdEvento());
		int valido = 0;
		if (pAnt == null) {
			valido++;
		}else {
			if (pAnt.getEmail() == null || (pAnt.getEmail() != null && pAnt.getEmail().equals(""))) {
				valido++;
			}
			if (pAnt.getCuentasBancarias() == null || pAnt.getCuentasBancarias().isEmpty()) {
				valido++;
			}
		}
		anticipo.isGenerar();
		System.out.println("Valor: "+valido);
		System.out.println("Is Pagado: " + anticipo.isPagado());
		if (valido == 0 || anticipo.isPagado()) {// TODO
		System.out.println("IsGenerar: "+anticipo.isGenerar());
			if (!anticipo.isGenerar()) {
				anticipo.setEstado(Anticipo.SOLICITADO);
			}
			if (anticipo.isGenerar()) {
				anticipo.setEstado(Anticipo.AUTORIZACION);
			}
			if (anticipo.isPagado()) {
				anticipo.setEstado(Anticipo.PAGADO);
			}
			// anticipo.setFechaVencimiento(new SimpleDateFormat("yyyy-MM-dd").parse(fechaVencimiento));
			anticipo.setBeneficiario(pAnt);
			anticipo.setCreador(admin.getUsuario(anticipo.getIdCreador(), false));

			return emailAnticipo(request, evento, anticipo, false);
		} else {
			return null;
		}
	}

	public Anticipo emailAnticipo(HttpServletRequest request, Evento evento, Anticipo anticipo, boolean reenviar) throws Exception {
		if (!reenviar) {
			anticipo = saveAnticipo(anticipo);
		}
		boolean isSolicitado = anticipo.getEstado().equals(Anticipo.SOLICITADO);
		Contrato contrato = admin.getContrato(evento.getIdContrato());
		String body = "";

		if (anticipo.getEstado().equals(Anticipo.AUTORIZACION) || anticipo.getEstado().equals(Anticipo.PAGADO)
				|| anticipo.getEstado().equals(Anticipo.AUTORIZADO)) {
			String semilla = "" + Calendar.getInstance().getTimeInMillis();
			String opcion = "APROBAR_ANTICIPO";
			String clave = ApplicationService.getParametro("security.servlet.password.documentocontable");

			body =
					"</b><br>\nConcepto del pago: <b>" + anticipo.getConcepto() + "</b><br>\nPrioridad: <b>" + anticipo.getPrioridad() + "</b><br>\nCiudad: <b>"
							+ evento.getSede().getNombre() + "</b><br>\nObservaciones: <b>" + anticipo.getObservacion() + "</b><br>\nUsuario Creador: <b>"
							+ anticipo.getCreador().getUsuario() + "</b><br>\nFecha Creaci" + Constants.HTML_TILDE_o + "n: <b>"
							+ DateFormat.getDateInstance().format(new Date())
							+ (!Anticipo.PAGADO.equals(anticipo.getEstado())
									? "</b>\n\nPor favor proceder a autorizar el anticipo. Menu -> Tesoreria -> Listado Pagos. Consulte por Tipo Anticipo"
											+ "\n\nPara Aprobar de clic en el siguiente link " + "http://" + request.getServerName() + ":"
											+ request.getServerPort() + "/adescubrirWeb/DocumentoContableServlet?documentoID=" + anticipo.getId() + "&opcion="
											+ opcion + "&semilla=" + semilla + "&security="
											+ ApplicationService.MD5(semilla + clave + ("" + anticipo.getId()) + opcion)
									: "</b><br><br><b>ANTICIPO YA PAGADO</b>");
		}

		if (isSolicitado) {
			body =
					"</b><br>\nCiudad: <b>" + evento.getSede().getNombre() + "</b><br>\nObservaciones: <b>" + anticipo.getObservacion()
							+ "</b><br>\nUsuario Creador: <b>" + anticipo.getCreador().getUsuario() + "</b><br>\nFecha Creaci" + Constants.HTML_TILDE_o
							+ "n: <b>" + DateFormat.getDateInstance().format(new Date())
							+ "</b><br><br>\n\nPor favor proceder a generar el anticipo en el evento. Dar clic en el boton Genera del anticipo.";
		}

		try {
			String imageStr = base64File("logoGrande.png");

			CorreoElectronico.enviarCorreoHtml(ApplicationService.getParametro("mail.notificaciones.eventos.from"),
					ApplicationService.getParametro("mail.notificaciones.eventos.from.password"),
					ApplicationService.getParametro("mail.notificaciones.pagos.autorizacion.to"),
					"", "",
					(isSolicitado ? "Solicitud" : "Generación") + " de Anticipo. No: " + anticipo.getId() + ". Evento: " + evento.getId(),
					"Buen d" + Constants.HTML_TILDE_i + "a,<br>\nSe ha creado el anticipo No: " + anticipo.getId() + ".<br><br>\n\nA nombre de : <b>"
							+ anticipo.getBeneficiario().getNombre() + "</b><br>\nIdentificaci" + Constants.HTML_TILDE_o + "n: <b>"
							+ anticipo.getBeneficiario().getIdentificacion() + "</b><br>\nValor: <b>"
							+ Formato.convertirDoubleACadena(Constants.CURRENCY_FORMAT, anticipo.getValor()) + "</b><br>\nContrato : <b>"
							+ contrato.getNumero() + " - " + contrato.getNombre() + "</b><br>\nEmpresa del Contrato: <b>"
							+ contrato.getNitCompania() + " - " + contrato.getNombreCompania() + "</b><br>\nEvento: <b>"
							+ evento.getId() + "</b><br>\nFecha Inicio: <b>" + DateFormat.getDateInstance().format(evento.getFechaInicioEvento()) + body
							+ "<br><br><img src='data:image/png;base64," + imageStr + "'\" width=\"200\" height=\"100\"></img>",
					true);
			// "logoGrande.png", "image/png", b,
		} catch (Exception e) {
			e.printStackTrace();
		}

		return anticipo;
	}

	@PostMapping("/saveAnticipo")
	public Anticipo saveAnticipo(@RequestBody Anticipo anticipo) throws Exception {
		 if (anticipo.getId() == 0) {
			 if (TipoDocumentoContable.CXC.equals(anticipo.getTipoDocumentoTxt())) {
				anticipo.setTipoDocumento(getTipoDocumentoContable(1));
				anticipo.setTipodocumento_id(1);
			 }
		 }
		anticipo = (Anticipo) getBaseService().saveUpdate(anticipo);

		return anticipo;
	}

	public TipoDocumentoContable getTipoDocumentoContable(int idTipoDocumento) {
		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		properties.add("tipodocumento_id = :tipodocumento_id");
		propertyValues.put("tipodocumento_id", idTipoDocumento);

		ArrayList<TipoDocumentoContable> datos =
				(ArrayList<TipoDocumentoContable>) getBaseService().getByPropertiesNamed(TipoDocumentoContable.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			TipoDocumentoContable retorno = datos.get(0);
			retorno.setRangoNumeracion(getRangoNumeracion(retorno.getRango_id()));
			return retorno;
		}
		return null;
	}

	public RangoNumeracion getRangoNumeracion(int idRango) {
		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		properties.add("rango_id = :rango_id");
		propertyValues.put("rango_id", idRango);

		ArrayList<RangoNumeracion> datos =
				(ArrayList<RangoNumeracion>) getBaseService().getByPropertiesNamed(RangoNumeracion.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			return datos.get(0);
		}
		return null;
	}

	@GetMapping("/getAnticipos/{idEvento}")
	public Collection<Anticipo> getAnticipos(@PathVariable int idEvento) {
		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		properties.add("evento_id = :evento_id");
		propertyValues.put("evento_id", idEvento);

		ArrayList<Anticipo> anticipos = (ArrayList<Anticipo>) getBaseService().getByPropertiesNamed(Anticipo.class.getName(), properties, propertyValues, "");
		ArrayList<DocumentoContable> documentos =
				(ArrayList<DocumentoContable>) getBaseService().getByPropertiesNamed(DocumentoContable.class.getName(), properties, propertyValues, "");

		for (Anticipo anticipo : anticipos) {
			Proveedor prov = admin.getProveedor(anticipo.getIdBeneficiario());
			if (prov == null) {
				Persona per = admin.getPersona(anticipo.getIdBeneficiario(), false);
				if (per != null) {
					prov = new Proveedor();
					prov.setId(per.getId());
					prov.setIdentificacion(per.getIdentificacion());
					prov.setNombre(per.getNombre());
				}
			}
			anticipo.setBeneficiario(prov);
			anticipo.setCreador(admin.getUsuario(anticipo.getIdCreador(), false));
			anticipo.setViejo(false);
		}

		if (!documentos.isEmpty()) {
			for (DocumentoContable documento : documentos) {
				Anticipo anticipo = new Anticipo();
				anticipo.setId((int) documento.getId());
				Proveedor prov = admin.getProveedor(documento.getPersona_id());
				if (prov == null) {
					Persona per = admin.getPersona(documento.getPersona_id(), false);
					if (per != null) {
						prov = new Proveedor();
						prov.setId(per.getId());
						prov.setIdentificacion(per.getIdentificacion());
						prov.setNombre(per.getNombre());
					}
				}
				anticipo.setBeneficiario(prov);
				anticipo.setIdBeneficiario(documento.getPersona_id());
				anticipo.setConcepto(documento.getConceptoPago());
				anticipo.setValor((int) documento.getTotal());
				anticipo.setIdEvento(documento.getEventoId());
				anticipo.setFecha(documento.getFecha());
				anticipo.setFechaVencimiento(documento.getFechaVencimiento());
				anticipo.setObservacion(documento.getObservaciones());
				anticipo.setPrioridad(documento.getPrioridad());
				anticipo.setCreador(admin.getUsuario(documento.getCreador_id(), false));
				anticipo.setIdCreador(documento.getCreador_id());
				anticipo.setEstado(documento.getEstado());
				anticipo.setViejo(true);
				anticipos.add(anticipo);
			}
			return anticipos;
		}

		return anticipos;
	}

	@GetMapping("/getAnticipo/{id}")
	public Object getAnticipo(@PathVariable int id) {
		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		properties.add("id = :id");
		propertyValues.put("id", id);

		ArrayList<Anticipo> anticipo = (ArrayList<Anticipo>) getBaseService().getByPropertiesNamed(Anticipo.class.getName(), properties, propertyValues, "");

		if (anticipo.isEmpty()) {
			properties.clear();
			propertyValues.clear();
			properties.add("documentocontable_id = :documentocontable_id");
			propertyValues.put("documentocontable_id", id);
			ArrayList<DocumentoContable> doc =
					(ArrayList<DocumentoContable>) getBaseService().getByPropertiesNamed(DocumentoContable.class.getName(), properties, propertyValues, "");
			if (!doc.isEmpty()) {
				return doc.get(0);
			}
		} else {
			return anticipo.get(0);
		}
		return null;
	}

	@PostMapping("/enviarAnticipoAGastos")
	public LegalizacionGasto enviarAnticipoAGastos(@RequestBody Anticipo anticipo) throws Exception {
		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		properties.add("evento_id = :evento_id");
		propertyValues.put("evento_id", anticipo.getIdEvento());
		properties.add("documento_id = :documento_id");
		propertyValues.put("documento_id", anticipo.getId());

		ArrayList<LegalizacionGasto> gastos = (ArrayList<LegalizacionGasto>) getBaseService().getByPropertiesNamed(LegalizacionGasto.class.getName(), properties, propertyValues, "");

		if (gastos.isEmpty()) {
			final LegalizacionGasto gasto = new LegalizacionGasto();
			gasto.setFecha(anticipo.getFecha());
			gasto.setIdEvento(anticipo.getIdEvento());
			gasto.setTipo("ANTICIPO");
			if (anticipo.getBeneficiario().getNombres() == null)
				gasto.setNombres(anticipo.getBeneficiario().getNombre());
			else
				gasto.setNombres(anticipo.getBeneficiario().getNombres());

			if (anticipo.getBeneficiario().getApellidos() == null)
				gasto.setApellidos(anticipo.getBeneficiario().getNombre());
			else
				gasto.setApellidos(anticipo.getBeneficiario().getApellidos());

			gasto.setFechaRegistro(new Date());
			gasto.setConcepto(anticipo.getConcepto());
			gasto.setIdentificacion(anticipo.getBeneficiario().getIdentificacion());
			gasto.setUsuario(anticipo.getCreador());
			gasto.setIdUsuario(anticipo.getCreador().getId());
			gasto.setValor(anticipo.getValor());
			gasto.setValorEvento(anticipo.getValor());
			gasto.setDocumentoId(anticipo.getId());

			properties.clear();
			propertyValues.clear();
			properties.add("evento_id = :evento_id");
			propertyValues.put("evento_id", anticipo.getIdEvento());

			ArrayList<LegalizacionGasto> gastosAll = (ArrayList<LegalizacionGasto>) getBaseService().getByPropertiesNamed(LegalizacionGasto.class.getName(), properties, propertyValues, "");

			if (gastosAll.isEmpty()) {
				gasto.setId(1);
			}else{
				gasto.setId((gastosAll.get(gastosAll.size() - 1).getId())+1);
			}
			return (LegalizacionGasto) getBaseService().saveUpdate(gasto);
		}else {
			return null;
		}
	}

	// -------------
	// @return Guarda una Bitacora
	// -------------
	@PostMapping("/saveBitacora")
	public Bitacora saveBitacora(@RequestBody Bitacora bitacora) throws Exception {
		bitacora = (Bitacora) getBaseService().saveUpdate(bitacora);
		bitacora.setUsuario(admin.getUsuario(bitacora.getUsuario_id(), false));
		return bitacora;
	}

	@GetMapping("/getBitacora/{idEvento}")
	public Collection<Bitacora> getBitacora(@PathVariable int idEvento) {
		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		properties.add("evento_id = :evento_id");
		propertyValues.put("evento_id", idEvento);

		ArrayList<Bitacora> datos = (ArrayList<Bitacora>) getBaseService().getByPropertiesNamed(Bitacora.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			for (Bitacora bitacora : datos) {
				bitacora.setUsuario(admin.getUsuario(bitacora.getUsuario_id(), false));
			}
		}
		return datos;
	}

	// -------------
	// @return Guarda un Evento Observacion
	// -------------
	@PostMapping("/saveObservacion")
	public EventoObservaciones saveObservacion(@RequestBody EventoObservaciones observacion) throws Exception {
		observacion = (EventoObservaciones) getBaseService().saveUpdate(observacion);
		observacion.setUsuario(admin.getUsuario(observacion.getUsuario_id(), false));
		return observacion;
	}

	@GetMapping("/getObservacion/{idEvento}")
	public Collection<EventoObservaciones> getObservacion(@PathVariable int idEvento) {
		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		properties.add("evento_id = :evento_id");
		propertyValues.put("evento_id", idEvento);

		ArrayList<EventoObservaciones> datos =
				(ArrayList<EventoObservaciones>) getBaseService().getByPropertiesNamed(EventoObservaciones.class.getName(), properties, propertyValues, "");
		if (!datos.isEmpty()) {
			for (EventoObservaciones observacion : datos) {
				observacion.setUsuario(admin.getUsuario(observacion.getUsuario_id(), false));
			}
		}
		return datos;
	}

	// -------------
	// @return Guarda un Evento Observacion
	// -------------
	@PostMapping("/saveObsFacturacion")
	public ObservacionFacturacion saveObsFacturacion(@RequestBody ObservacionFacturacion obsFacturacion) throws Exception {
		obsFacturacion = (ObservacionFacturacion) getBaseService().saveUpdate(obsFacturacion);
		obsFacturacion.setUsuario(admin.getUsuario(obsFacturacion.getUsuario_id(), false));
		return obsFacturacion;
	}

	@GetMapping("/getObsFacturacion/{idEvento}")
	public Collection<ObservacionFacturacion> getObsFacturacion(@PathVariable int idEvento) {
		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		properties.add("evento_id = :evento_id");
		propertyValues.put("evento_id", idEvento);

		ArrayList<ObservacionFacturacion> datos =
				(ArrayList<ObservacionFacturacion>) getBaseService().getByPropertiesNamed(ObservacionFacturacion.class.getName(), properties, propertyValues,
						"");

		if (!datos.isEmpty()) {
			for (ObservacionFacturacion obsFacturacion : datos) {
				obsFacturacion.setUsuario(admin.getUsuario(obsFacturacion.getUsuario_id(), false));
			}
		}
		return datos;
	}

	// -------------
	// @return Guarda un Evento Observacion
	// -------------
	@PostMapping("/saveTarifa")
	public TarifaEvento saveTarifa(@RequestBody TarifaEvento tarifa) throws Exception {
		return (TarifaEvento) getBaseService().saveUpdate(tarifa);
	}

	@PostMapping("/saveTarifas")
	public Collection<TarifaEvento> saveTarifas(@RequestBody Collection<TarifaEvento> tarifas) throws Exception {
		for (TarifaEvento te : tarifas) {
			getBaseService().saveUpdate(te);
		}
		return tarifas;
	}

	@PostMapping("/deleteTarifa")
	public void deleteTarifa(@RequestBody TarifaEvento tarifa) throws Exception {
		getBaseService().delete(tarifa);
	}

	@PostMapping("/calcularFacturaUnConcepto")
	private DocumentoContable calcularFacturaUnConcepto(@RequestBody String dataFiltro) throws JSONException, IOException {

		JSONObject obj = new JSONObject(dataFiltro);
		ObjectMapper objectMapper = new ObjectMapper();
		boolean chckbxNoCalcularIntermediacin = obj.getBoolean("noCalcularInter");
		boolean chckbxNoCalcularDescuento = obj.getBoolean("noCalcularDesc");
		boolean isAgrupar = obj.getBoolean("isAgrupar");
		Evento evento = objectMapper.readValue(obj.getString("evento"), Evento.class);
		evento.setContrato(admin.getContrato(evento.getIdContrato(), true));

		if (evento.getFactura() == null || (evento.getFactura() != null && evento.getFactura().getId() == 0)) {
			evento.setFactura(new DocumentoContable());
			evento.getFactura().setPersona(evento.getContrato().getContratante());
			evento.getFactura().setPersona_id(evento.getContrato().getContratante_id());
			evento.getFactura().setMunicipio(evento.getSede());
			evento.getFactura().setMunicipio_id(evento.getSede().getId());
			evento.getFactura().setCreador(admin.getUsuario(evento.getIdCreador(), false));
			evento.getFactura().setCreador_id(evento.getIdCreador());
			evento.getFactura().setTipoDocumentoTxt(TipoDocumentoContable.CXC);
			evento.getFactura().setEstado(DocumentoContable.PENDIENTE);
			evento.getFactura().setEventoId(evento.getId());
			// if (!isAgrupar) {
			// evento.getFactura().setFecha(new Date());
			// evento.getFactura().setHora(new Date());
			// }
			evento.getFactura().setFechaFactura(evento.getFactura().getFecha());
		} else {
			evento.getFactura().getDetalle().clear();
		}

		evento.getFactura().setObservaciones("Evento ID: " + evento.getId() + " Contrato: " + evento.getContrato().getNumero() + " Ciudad del Evento: "
				+ evento.getSede().getNombre() + " Dependencia: " + evento.getTexto1() + " Solicitud: " + evento.getOrden() + " CDP: " + evento.getCdp());

		evento.getFactura().setFecha(new Date());
		evento.getFactura().setHora(new Date());

		HashMap<Concepto, DetalleDocumento> detalles = new HashMap<>();
		DetalleDocumento detalleAgrupado = new DetalleDocumento();
		if (isAgrupar) {
			// evento.getFactura().setFecha(new Date());
			// evento.getFactura().setHora(new Date());
			// evento.getFactura().setTipoDocumento(getApplicationMB().getTipoDocumentoFacturaVenta());
			Concepto conceptoAgrupado = new Concepto();
			conceptoAgrupado.setId(1530);
			conceptoAgrupado.setNombre("LOGISTICA");
			detalleAgrupado.setConcepto(conceptoAgrupado);
			detalleAgrupado.setConcepto_id(conceptoAgrupado.getId());
		}

		for (Map.Entry<String, EventoConsumoConcepto> consumo : evento.getEventoEjecucion().entrySet()) {
			if (detalles.containsKey(consumo.getValue().getNombreConcepto()))// consumo.getConcepto()
			{
				int cantidad = detalles.get(consumo.getValue().getNombreConcepto()).getCantidad(); // consumo.getConcepto()
				detalles.get(admin.getConceptoById(consumo.getValue().getConceptoId())).setCantidad(cantidad + consumo.getValue().getTotal()); // consumo.getConcepto() - consumo.getCantidad()
			} else {
				DetalleDocumento detalle = new DetalleDocumento();
				detalle.setConcepto(admin.getConceptoById(consumo.getValue().getConceptoId()));// consumo.getConcepto()
				detalle.setConcepto_id(detalle.getConcepto().getId());
				detalle.setCantidad(consumo.getValue().getTotal());// consumo.getCantidad()
				detalles.put(detalle.getConcepto(), detalle);// consumo.getConcepto()
			}
		}

		ArrayList<TarifaEvento> tarifasEvento = (ArrayList<TarifaEvento>) evento.getTarifasDelEvento();
		for (Iterator it = detalles.entrySet().iterator(); it.hasNext();) {
			Map.Entry<Concepto, DetalleDocumento> entry = (Map.Entry<Concepto, DetalleDocumento>) it.next();
			for (TarifaEvento tarifaEvento : tarifasEvento) {
				if (entry.getKey().equals(tarifaEvento.getConcepto())) {
					detalles.get(entry.getKey()).setValor(tarifaEvento.getValor());
					if (!isAgrupar) {
						detalles.get(entry.getKey()).setAplicarDescuento(tarifaEvento.isConDescuento());
						detalles.get(entry.getKey()).setPoDescuento(evento.getContrato().getPoDescuento());
					}
				}
			}
			if (isAgrupar) {
				detalleAgrupado.setValor(detalleAgrupado.getValor() + (entry.getValue().getCantidad() * entry.getValue().getValor()));
			} else {
				evento.getFactura().getDetalle().add(detalles.get(entry.getKey()));
			}
		}
		if (isAgrupar) {
			detalleAgrupado.setCantidad(1);
			evento.getFactura().getDetalle().add(detalleAgrupado);
		}
		evento.getFactura().setComisiones(0);
		evento.getFactura().setImpuestos(0);
		if (!isAgrupar) {
			if (chckbxNoCalcularDescuento)
				evento.getFactura().setPoDescuento(0);
			else
				evento.getFactura().setPoDescuento(evento.getContrato().getPoDescuento());
		}
		DocumentoContable.calcularValoresDetalle(evento.getFactura());
		if (!chckbxNoCalcularIntermediacin) {
			DocumentoContable.calcularComision(evento.getFactura(), evento.getContrato().getPoIntermediacion());
		}
		DocumentoContable.calcularTotal(evento.getFactura());

		return evento.getFactura();
	}

	@PostMapping("/saveFactura")
	private DocumentoContable saveFactura(@RequestBody String dataFactura) throws Exception {
		JSONObject obj = new JSONObject(dataFactura);
		ObjectMapper objectMapper = new ObjectMapper();
		boolean chckbxNoCalcularIntermediacin = obj.getBoolean("noCalcularInter");
		Evento evento = objectMapper.readValue(obj.getString("evento"), Evento.class);

		DocumentoContable.calcularValoresDetalle(evento.getFactura());
		if (!chckbxNoCalcularIntermediacin) {
			DocumentoContable.calcularComision(evento.getFactura(), evento.getContrato().getPoIntermediacion());
		}
		DocumentoContable.calcularTotal(evento.getFactura());

		if (evento.getFactura().getId() == 0) {
			if (TipoDocumentoContable.CXC.equals(evento.getFactura().getTipoDocumentoTxt())) {
				evento.getFactura().setTipoDocumento(getTipoDocumentoContable(1));
				evento.getFactura().setTipodocumento_id(evento.getFactura().getTipoDocumento().getId());
			}
		}
		evento.getFactura().setPersona(admin.getPersona(evento.getFactura().getPersona_id(), false));
		System.out.println("Antes Rango: "+evento.getFactura().getRangonumeracion());
		if (evento.getFactura().getRangonumeracion() == 0) {
			evento.getFactura().setRangonumeracion(-1);
		}
		DocumentoContable docContable = (DocumentoContable) getBaseService().saveUpdate(evento.getFactura());

		for (DetalleDocumento detail : docContable.getDetalle()) {
			detail.setDocumentocontable_id(((int) docContable.getId()));
			getBaseService().saveUpdate(detail);
		}

		evento.setIdFactura((int) docContable.getId());
		evento.setTotalFacturado(evento.getFactura().getTotal());
		getBaseService().saveUpdate(evento);
		return docContable;
	}

	public Map<Double, Double[]> getDiscriminacionImpuestos(DocumentoContable documentoContable) {
		HashMap<Double, Double[]> impuestos = new HashMap<>();
		for (DetalleDocumento detalle : documentoContable.getDetalle()) {
			if (impuestos.containsKey(detalle.getPoImpuestos())) {
				double totalBase = impuestos.get(detalle.getPoImpuestos())[0].doubleValue() + detalle.getBase();
				double totalImpuesto = impuestos.get(detalle.getPoImpuestos())[1].doubleValue() + detalle.getImpuestos();
				impuestos.put(detalle.getPoImpuestos(), new Double[] { totalBase, totalImpuesto });
			} else {
				impuestos.put(detalle.getPoImpuestos(), new Double[] { detalle.getBase(), detalle.getImpuestos() });
			}
		}
		double totalBase = (impuestos.get(16.0) != null ? impuestos.get(16.0)[0].doubleValue() : 0) + documentoContable.getComisiones();
		double totalImpuesto = (impuestos.get(16.0) != null ? impuestos.get(16.0)[1].doubleValue() : 0) + (documentoContable.getComisiones() * 0.16);
		impuestos.put(16.0, new Double[] { totalBase, totalImpuesto });
		return impuestos;
	}

	@GetMapping("/getFactura/{idEvento}/{idDocumento}")
	public DocumentoContable getFactura(@PathVariable int idEvento, @PathVariable int idDocumento) {
		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		DocumentoContable documentoC = new DocumentoContable();

		if (idEvento != 0) {
			properties.add("evento_id = :evento_id");
			propertyValues.put("evento_id", idEvento);
		} else {
			properties.add("documentocontable_id = :documentocontable_id");
			propertyValues.put("documentocontable_id", idDocumento);
		}

		ArrayList<DocumentoContable> datos =
				(ArrayList<DocumentoContable>) getBaseService().getByPropertiesNamed(DocumentoContable.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			documentoC = datos.get(0);
			documentoC.setDetalle(getDocumentoDetalle((int) documentoC.getId()));
			documentoC.setPersona(admin.getPersona(documentoC.getPersona_id(), false));
			documentoC.setTipoDocumento(admin.getTipoDocumentoContable(documentoC.getTipodocumento_id()));
			documentoC.setMunicipio(admin.getMunicipioById(documentoC.getMunicipio_id()));
		}
		return documentoC;
	}

	public ArrayList<DetalleDocumento> getDocumentoDetalle(int idDocumento) {
		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		properties.add("documentocontable_id = :documentocontable_id");
		propertyValues.put("documentocontable_id", idDocumento);

		ArrayList<DetalleDocumento> datos =
				(ArrayList<DetalleDocumento>) getBaseService().getByPropertiesNamed(DetalleDocumento.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			for (DetalleDocumento detailDoc : datos) {
				detailDoc.setConcepto(admin.getConceptoById(detailDoc.getConcepto_id()));

			}
			return datos;
		}
		return null;
	}

	@PostMapping("/calcularCotizacion")
	public CotizacionEvento calcularCotizacion(@RequestBody String data) throws JSONException, IOException {
		JSONObject obj = new JSONObject(data);
		ObjectMapper objectMapper = new ObjectMapper();
		Evento evento = objectMapper.readValue(obj.getString("evento"), Evento.class);
		CotizacionEvento cotizacion = objectMapper.readValue(obj.getString("cotizacion"), CotizacionEvento.class);

		ArrayList<TarifaCotizacion> copiaTarifas = new ArrayList<>();
		cotizacion.getConsumos().clear();
		copiaTarifas.addAll(cotizacion.getTarifas());
		cotizacion.setConsumos(new LinkedHashMap<>());
		cotizacion.setTarifas(new ArrayList<>());
		ArrayList<Tarifa> tarifasProveedor = new ArrayList<>();
		ArrayList<String> rangoFechas = (ArrayList<String>) evento.getRangoFechas();
		ArrayList<Tarifa> consultaTarifas = (ArrayList<Tarifa>) admin.getProveedorTarifa(cotizacion.getIdProveedor());

		if (consultaTarifas != null) {
			for (Tarifa tarifa : consultaTarifas) {
				if (tarifa.getSucursal().getId() == cotizacion.getIdSucursal()) {
					if (tarifa.getTipoTarifa().getId() == cotizacion.getIdTipoTarifa()) {
						tarifasProveedor.add(tarifa);
					}
				}
			}
		}

		for (Map.Entry<String, EventoConsumoConcepto> consumosConcepto : evento.getEventoConsumos().entrySet()) {
			TarifaCotizacion tarifaCotizacion = new TarifaCotizacion();
			tarifaCotizacion.setConcepto(admin.getConceptoById(consumosConcepto.getValue().getConceptoId()));
			tarifaCotizacion.setIdConcepto(tarifaCotizacion.getConcepto().getId());
			double tarifaC = getTarifaCotizacionConcepto(copiaTarifas, tarifaCotizacion.getConcepto());
			if (tarifaC != 0) {
				tarifaCotizacion.setValor(tarifaC);
			} else {
				for (Tarifa tarifa : tarifasProveedor) {
					if (tarifa.getConcepto().equals(consumosConcepto)) {
						tarifaCotizacion.setValor(tarifa.getValor());
					}
				}
			}
			// ArrayList<EventoConsumoConcepto> consumosConcepto = (ArrayList<EventoConsumoConcepto>) evento.getConsumos(conceptoConsumo);
			// cotizacion.getConsumos().addAll(consumosConcepto);
			for (String diaConsumo : rangoFechas) {
				EventoConsumoConcepto consumoEvento = new EventoConsumoConcepto();
				consumoEvento.setConceptoId(consumosConcepto.getValue().getConceptoId());
				// try
				// {
				// consumoEvento.setFecha(new SimpleDateFormat("yyyy-MM-dd").parse(diaConsumo));
				// }
				// catch (ParseException e)
				// {
				// }
				// int index = consumosConcepto.indexOf(consumoEvento);
				// if (index > -1)
				// {
				// consumoEvento.setCantidad(consumosConcepto.get(index).getCantidad());
				// // tarifaCotizacion.getConsumos().add(consumosConcepto.get(index));
			}
			tarifaCotizacion.getConsumos().put(consumosConcepto.getKey(), consumosConcepto.getValue());
			cotizacion.getConsumos().put(consumosConcepto.getKey(), consumosConcepto.getValue());
			// }
			cotizacion.getTarifas().add(tarifaCotizacion);
		}
		return cotizacion;
	}

	private double getTarifaCotizacionConcepto(ArrayList<TarifaCotizacion> tarifas, Concepto concepto) {
		for (TarifaCotizacion tarifaCotizacion : tarifas) {
			if (tarifaCotizacion.getConcepto().getId() == concepto.getId()) {
				return tarifaCotizacion.getValor();
			}
		}
		return 0;
	}

	@PostMapping("/saveCotizacionEvento/{isSearch}")
	public CotizacionEvento saveCotizacionEvento(@RequestBody CotizacionEvento cotizacion, @PathVariable boolean isSearch) throws Exception {
		if (isSearch) {
			boolean apro = cotizacion.isAprobado();
			ArrayList<String> properties = new ArrayList<>();
			Map<String, Object> propertyValues = new HashMap<>();
			properties.add("cotizacion_id = :cotizacion_id");
			propertyValues.put("cotizacion_id", cotizacion.getId());
			ArrayList<CotizacionEvento> datos =
					(ArrayList<CotizacionEvento>) getBaseService().getByPropertiesNamed(CotizacionEvento.class.getName(), properties, propertyValues, "");

			if (!datos.isEmpty()) {
				cotizacion = datos.get(0);
				cotizacion.setAprobado(apro);
				cotizacion = (CotizacionEvento) getBaseService().saveUpdate(cotizacion);
			}
		} else {
			cotizacion = (CotizacionEvento) getBaseService().saveUpdate(cotizacion);
		}
		return cotizacion;
	}

	@PostMapping("/saveCotizacion/{idEvento}")
	public CotizacionEvento saveCotizacion(@PathVariable("idEvento") int idEvento, @RequestBody CotizacionEvento cotizacion) throws Exception {
		cotizacion = saveCotizacionEvento(cotizacion, false);

		EventoCotizacion eventoCotizacion = new EventoCotizacion();
		eventoCotizacion.setIdCotizacion(cotizacion.getId());
		eventoCotizacion.setIdEvento(idEvento);
		getBaseService().saveUpdate(eventoCotizacion);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		for (TarifaCotizacion tarifa : cotizacion.getTarifas()) {
			tarifa.setIdCotizacion(cotizacion.getId());
			getBaseService().saveUpdate(tarifa);

			for (Map.Entry<String, EventoConsumoConcepto> consumo : tarifa.getConsumos().entrySet()) {
				for (Map.Entry<String, Integer> cantidad : consumo.getValue().getCantidades().entrySet()) {
					if (cantidad.getValue() > 0) {
						CotizacionConsumo cotConsumo = new CotizacionConsumo();
						cotConsumo.setIdCotizacion(cotizacion.getId());
						cotConsumo.setIdConcepto(tarifa.getIdConcepto());
						cotConsumo.setFecha(sdf.parse(cantidad.getKey()));
						cotConsumo.setCantidad(cantidad.getValue());
						getBaseService().saveUpdate(cotConsumo);
					}
				}
			}
		}

		cotizacion.setProveedor(admin.getProveedor(cotizacion.getIdProveedor()));
		cotizacion.setSucursal(admin.getSucursalByPersonId(cotizacion.getIdProveedor()));
		cotizacion.setTipoTarifa(admin.getTipoTarifaById(cotizacion.getIdTipoTarifa()));

		return cotizacion;
	}

	@GetMapping("/getEventoCotizacion/{idCotizacion}/{rangoFechas}")
	public CotizacionEvento getEventoCotizacion(@PathVariable int idCotizacion, @PathVariable List<String> rangoFechas) {
		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		properties.add("cotizacion_id = :cotizacion_id");
		propertyValues.put("cotizacion_id", idCotizacion);

		ArrayList<CotizacionEvento> datos =
				(ArrayList<CotizacionEvento>) getBaseService().getByPropertiesNamed(CotizacionEvento.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			CotizacionEvento cotizacion = datos.get(0);
			cotizacion.setProveedor(admin.getProveedor(cotizacion.getIdProveedor()));
			cotizacion.setSucursal(admin.getSucursalByPersonId(cotizacion.getIdProveedor()));
			cotizacion.setTipoTarifa(admin.getTipoTarifaById(cotizacion.getIdTipoTarifa()));
			getTarifasCotizacion(cotizacion, rangoFechas);
			for (TarifaCotizacion tarifa : cotizacion.getTarifas()) {
				tarifa.setTotalCantidad(tarifa.getConsumos().entrySet().iterator().next().getValue().getTotal());
			}
			return cotizacion;
		}
		return null;
	}

	@PostMapping("/getEventoCotizaciones/{idEvento}")
	public ArrayList<Map<String, Object>> getEventoCotizaciones(@PathVariable int idEvento, @RequestBody List<String> rangoFechas) {
		ArrayList<Map<String, Object>> retorno = new ArrayList<>();

		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		properties.add("evento_id = :evento_id");
		propertyValues.put("evento_id", idEvento);

		ArrayList<EventoCotizacion> datos =
				(ArrayList<EventoCotizacion>) getBaseService().getByPropertiesNamed(EventoCotizacion.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			for (EventoCotizacion eventoCot : datos) {

				String query =
						"SELECT ce.cotizacion_id, ce.estado, ce.aprobado, ce.documentocontable_id, (SELECT nombre FROM persona WHERE persona_id = ce.proveedor_id) AS nombreProveedor,"
								+ "(SELECT nombre FROM persona_sucursal WHERE persona_id = ce.proveedor_id AND municipio_id = ce.sucursal_id AND nombre = ce.nombresucursal) AS nombreSucursal, (SELECT nombre FROM tipotarifa WHERE tipotarifa_id = ce.tipotarifa_id) AS nombreTarifa, "
								+ "ce.proveedor_id FROM cotizacion_evento AS ce WHERE cotizacion_id = " + eventoCot.getIdCotizacion();

				ArrayList<Object[]> cont = (ArrayList<Object[]>) ServiceLocator.getInstance().getBaseService().executeSQLQuery(query);

				if (!cont.isEmpty()) {
					for (Object[] objects : cont) {
						Map<String, Object> data = new HashMap<>();
						data.put("id", objects[0]);
						data.put("estado", objects[1]);
						data.put("aprobado", objects[2]);
						data.put("idFactura", objects[3]);
						data.put("nombreProveedor", objects[4]);
						data.put("nombreSucursal", objects[5]);
						data.put("nombreTarifa", objects[6]);
						data.put("idProveedor", objects[7]);
						retorno.add(data);
					}
				}
				// CotizacionEvento cot = getEventoCotizacion(eventoCot.getIdCotizacion(), rangoFechas);
				// int total = 0;
				// for (TarifaCotizacion tar: cot.getTarifas()) {
				// total += tar.getTotalTarifa();
				// }
				// cot.setTotalCotizacion(total);
				// retorno.add(cot);
			}
			return retorno;
		}
		return null;
	}

	public Collection<TarifaCotizacion> getTarifasCotizacion(CotizacionEvento cotizacion, List<String> rangoFechas) {

		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		properties.add("cotizacion_id = :cotizacion_id");
		propertyValues.put("cotizacion_id", cotizacion.getId());

		ArrayList<TarifaCotizacion> datos =
				(ArrayList<TarifaCotizacion>) getBaseService().getByPropertiesNamed(TarifaCotizacion.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			cotizacion.setTarifas(datos);
			for (TarifaCotizacion tarifa : datos) {
				tarifa.setConcepto(admin.getConceptoById(tarifa.getIdConcepto()));
				tarifa.setConsumos(consultarCotizacionConsumos(cotizacion, tarifa.getIdConcepto()));
				for (Map.Entry<String, EventoConsumoConcepto> consumo : tarifa.getConsumos().entrySet()) {
					for (String rangoFecha : rangoFechas) {
						if (!consumo.getValue().getCantidades().containsKey(rangoFecha)) {
							consumo.getValue().getCantidades().put(rangoFecha, 0);
						}
					}
				}
			}
			return datos;
		}
		return null;
	}

	public Map<String, EventoConsumoConcepto> consultarCotizacionConsumos(CotizacionEvento cotizacion, int idConcepto) {

		Map<String, CotizacionConsumo> consumosEvento = cotizacionConsumos(cotizacion.getId(), idConcepto);

		ArrayList<String> rangoFechas = new ArrayList<>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		for (Map.Entry<String, CotizacionConsumo> fecha : consumosEvento.entrySet()) {
			int index = rangoFechas.indexOf(sdf.format(fecha.getValue().getFecha()));
			if (index == -1) {
				rangoFechas.add(sdf.format(fecha.getValue().getFecha()));
			}
		}

		Map<String, EventoConsumoConcepto> retorno = new LinkedHashMap<>();
		for (TarifaCotizacion tarifaCotizacion : cotizacion.getTarifas()) {
			if (retorno.containsKey("" + tarifaCotizacion.getIdConcepto())) {
				for (String fecha : rangoFechas) {
					retorno.get("" + tarifaCotizacion.getIdConcepto()).getCantidades().put(fecha, 0);
					if (consumosEvento.containsKey("" + tarifaCotizacion.getIdConcepto() + "_" + fecha)) {
						retorno.get("" + tarifaCotizacion.getIdConcepto()).getCantidades().put(fecha,
								consumosEvento.get("" + tarifaCotizacion.getIdConcepto() + "_" + fecha).getCantidad());
					}
				}
			} else {
				if (idConcepto == tarifaCotizacion.getIdConcepto()) {
					EventoConsumoConcepto concepto = new EventoConsumoConcepto();
					concepto.setConceptoId(tarifaCotizacion.getIdConcepto());
					concepto.setTarifa(tarifaCotizacion.getValor());
					concepto.setNombreConcepto(tarifaCotizacion.getConcepto().getNombre());
					for (String fecha : rangoFechas) {
						concepto.getCantidades().put(fecha, 0);
						if (consumosEvento.containsKey("" + tarifaCotizacion.getIdConcepto() + "_" + fecha)) {
							concepto.getCantidades().put(fecha, consumosEvento.get("" + tarifaCotizacion.getIdConcepto() + "_" + fecha).getCantidad());
						}
						concepto.setTotalTarifa((concepto.getTarifa() * concepto.getCantidades().get(fecha)));
						concepto.setTotal(concepto.getTotal() + concepto.getCantidades().get(fecha));
					}
					retorno.put("" + tarifaCotizacion.getIdConcepto(), concepto);
				}
			}
		}
		return retorno;
	}

	public Map<String, CotizacionConsumo> cotizacionConsumos(int idCotizacion, int idConcepto) {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		properties.add("cotizacion_id = :id");
		propertyValues.put("id", idCotizacion);
		properties.add("concepto_id = :concepto_id");
		propertyValues.put("concepto_id", idConcepto);

		ArrayList<CotizacionConsumo> datos =
				(ArrayList<CotizacionConsumo>) getBaseService().getByPropertiesNamed(CotizacionConsumo.class.getName(), properties, propertyValues,
						"idConcepto, fecha");
		Map<String, CotizacionConsumo> retorno = new HashMap<>();

		for (CotizacionConsumo eventoConsumos : datos) {
			retorno.put("" + eventoConsumos.getIdConcepto() + "_" + sdf.format(eventoConsumos.getFecha()), eventoConsumos);
		}
		return retorno;
	}

	@PostMapping("/deleteCotizacion")
	public void deleteCotizacion(@RequestBody int idCotizacion) throws Exception {
		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		properties.add("cotizacion_id = :cotizacion_id");
		propertyValues.put("cotizacion_id", idCotizacion);

		ArrayList<CotizacionConsumo> datos =
				(ArrayList<CotizacionConsumo>) getBaseService().getByPropertiesNamed(CotizacionConsumo.class.getName(), properties, propertyValues, "");
		if (!datos.isEmpty()) {
			for (CotizacionConsumo cc : datos) {
				getBaseService().delete(cc);
			}
		}
		properties.clear();
		propertyValues.clear();
		properties.add("cotizacion_id = :cotizacion_id");
		propertyValues.put("cotizacion_id", idCotizacion);

		ArrayList<TarifaCotizacion> datosT =
				(ArrayList<TarifaCotizacion>) getBaseService().getByPropertiesNamed(TarifaCotizacion.class.getName(), properties, propertyValues, "");
		if (!datosT.isEmpty()) {
			for (TarifaCotizacion ct : datosT) {
				getBaseService().delete(ct);
			}
		}

		properties.clear();
		propertyValues.clear();
		properties.add("cotizacion_id = :cotizacion_id");
		propertyValues.put("cotizacion_id", idCotizacion);

		ArrayList<EventoCotizacion> datosC =
				(ArrayList<EventoCotizacion>) getBaseService().getByPropertiesNamed(EventoCotizacion.class.getName(), properties, propertyValues, "");

		if (!datosC.isEmpty()) {
			getBaseService().delete(datosC.get(0));
		}

		properties.clear();
		propertyValues.clear();
		properties.add("cotizacion_id = :cotizacion_id");
		propertyValues.put("cotizacion_id", idCotizacion);

		ArrayList<CotizacionEvento> cot =
				(ArrayList<CotizacionEvento>) getBaseService().getByPropertiesNamed(CotizacionEvento.class.getName(), properties, propertyValues, "");

		if (!cot.isEmpty()) {
			getBaseService().delete(cot.get(0));
		}
	}

	@PostMapping("/deleteAnticipo")
	public void deleteAnticipo(@RequestBody int idAnticipo) throws Exception {
		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		properties.add("id = :id");
		propertyValues.put("id", idAnticipo);

		System.out.println("Delete Anticipo: " + idAnticipo);

		ArrayList<Anticipo> datos =
				(ArrayList<Anticipo>) getBaseService().getByPropertiesNamed(Anticipo.class.getName(), properties, propertyValues, "");
		
		System.out.println("Cantidad Anticipos: " + datos.size());
		System.out.println("Cantidad Empty: " + datos.isEmpty());

		if (!datos.isEmpty()) {
			getBaseService().delete(datos.get(0));
		}else {
			properties.clear();
			propertyValues.clear();
			properties.add("documentocontable_id = :documentocontable_id");
			propertyValues.put("documentocontable_id", idAnticipo);

			ArrayList<DocumentoContable> datosDoc =
					(ArrayList<DocumentoContable>) getBaseService().getByPropertiesNamed(DocumentoContable.class.getName(), properties, propertyValues, "");

			if (!datosDoc.isEmpty()) {
				getBaseService().delete(datosDoc.get(0));
			}
		}
	}

	@PostMapping("/calcularTarifasEvento")
	public ArrayList<Map<String, String>> calcularTarifasEvento(@RequestBody Map<String, Object> json) {
		ArrayList<Map<String, String>> retorno = new ArrayList<>();
		Evento evento = getEvento((int) json.get("idEvento"));
		ArrayList<Map<String, Object>> cotizaciones = getEventoCotizaciones(evento.getId(), evento.getRangoFechas());
		int idCotizacion = 0;
		String nombreProv = "";

		for (Map<String, Object> cotizacion : cotizaciones) {
			if (evento.getIdLugar() == ((int) cotizacion.get("id"))) {
				idCotizacion = ((int) cotizacion.get("id"));
				nombreProv = (String) cotizacion.get("nombreProveedor");
			}
		}

		if (idCotizacion != 0) {
			CotizacionEvento cotTemp = new CotizacionEvento();
			cotTemp.setId(idCotizacion);
			Collection<TarifaCotizacion> tarifas = getTarifasCotizacion(cotTemp, evento.getRangoFechas());
			if (!tarifas.isEmpty()) {
				for (TarifaCotizacion tarifa : tarifas) {
					Map<String, String> datos = new HashMap<>();
					datos.put("concepto", tarifa.getConcepto().getNombre());
					datos.put("idConcepto", "" + tarifa.getConcepto().getId());
					datos.put("proveedor", nombreProv);
					datos.put("valorFacturado", "" + tarifa.getValor());
					TarifaEvento te = new TarifaEvento("" + tarifa.getIdConcepto());
					int index = evento.getTarifasDelEvento().indexOf(te);
					if (index != -1) {
						datos.put("valorEvento", "" + evento.getTarifasDelEvento().get(index).getValor());
					} else
						datos.put("valorEvento", "" + tarifa.getValor());
					retorno.add(datos);
				}
			}
		}

		return retorno;
	}

	@PostMapping("/calcularFacturaProveedor")
	public DocumentoContable calcularFacturaProveedor(@RequestBody Map<String, Object> json) {
		ObjectMapper mapper = new ObjectMapper();
		CotizacionEvento cotizacion = mapper.convertValue(json.get("cotizacion"), CotizacionEvento.class);
		Evento evento = mapper.convertValue(json.get("evento"), Evento.class);

		TipoDocumentoContable tipoFacturaProveedor =
				getTipoDocumentoContable(Integer.parseInt(ApplicationService.getParametro("tipoDocumento.facturaProveedor")));
		DocumentoContable facturaProveedor = new DocumentoContable();

		facturaProveedor.setTipoDocumento(tipoFacturaProveedor);
		facturaProveedor.setTipodocumento_id(tipoFacturaProveedor.getId());
		facturaProveedor.setEstado(DocumentoContable.AUTORIZACION);
		facturaProveedor.setPersona(admin.getPersona(cotizacion.getIdProveedor(), false));
		facturaProveedor.setPersona_id(facturaProveedor.getPersona().getId());
		facturaProveedor.setMunicipio(evento.getSede());
		facturaProveedor.setMunicipio_id(evento.getSede().getId());
		facturaProveedor.setCreador(admin.getUsuario(evento.getIdCreador(), false));
		facturaProveedor.setCreador_id(facturaProveedor.getCreador().getId());
		facturaProveedor.setFecha(new Date());
		facturaProveedor.setHora(new Date());

		for (TarifaCotizacion tarifa : cotizacion.getTarifas()) {
			DetalleDocumento detalle = new DetalleDocumento();
			detalle.setConcepto(tarifa.getConcepto());
			detalle.setConcepto_id(tarifa.getIdConcepto());
			if (facturaProveedor.getPersona().isRegimenSimplificado()) {
				detalle.getConcepto().setPorcentajeIva(0);
			}
			detalle.setValor(tarifa.getValor());
			detalle.setCantidad(tarifa.getTotalCantidad());
			facturaProveedor.getDetalle().add(detalle);
		}
		facturaProveedor.calcularValoresDetalle(facturaProveedor);

		if (cotizacion.getPorcentajeDescuento() > 0) {
			facturaProveedor.setDescuentos(facturaProveedor.getBase() * (cotizacion.getPorcentajeDescuento() / 100));
		}
		if (cotizacion.getPorcentajeComision() > 0) {
			facturaProveedor.setComisiones(facturaProveedor.getBase() * (cotizacion.getPorcentajeComision() / 100));
		}
		facturaProveedor.calcularTotal(facturaProveedor);

		return facturaProveedor;
	}

	@PostMapping("/saveFacturaCotizacion")
	public CotizacionEvento saveFacturaCotizacion(@RequestBody CotizacionEvento cotizacion) throws Exception {
		double tmp = cotizacion.getFactura().getDescuentos();
		DocumentoContable.calcularValoresDetalle(cotizacion.getFactura());
		cotizacion.getFactura().setDescuentos(tmp);
		if (cotizacion.getFactura().getId() == 0) {
			if (TipoDocumentoContable.CXC.equals(cotizacion.getFactura().getTipoDocumentoTxt())) {
				cotizacion.getFactura().setTipoDocumento(getTipoDocumentoContable(1));
			}
		}
		DocumentoContable fac = (DocumentoContable) getBaseService().saveUpdate(cotizacion.getFactura());

		for (DetalleDocumento detail : fac.getDetalle()) {
			detail.setDocumentocontable_id(((int) fac.getId()));
			getBaseService().saveUpdate(detail);
		}

		cotizacion.setIdFactura((int) fac.getId());
		saveCotizacionEvento(cotizacion, false);

		return cotizacion;
	}

	@PostMapping("/agregarGastos")
	public LegalizacionGasto agregarGastos(@RequestBody Map<String, Object> json) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		Evento evento = mapper.convertValue(json.get("evento"), Evento.class);
		CotizacionEvento cotizacion = mapper.convertValue(json.get("cotizacion"), CotizacionEvento.class);
		int idUsuario = (int) json.get("idUsuario");
		String tipo = (String) json.get("tipo");

		LegalizacionGasto gasto = new LegalizacionGasto();
		gasto.setIdentificacion(cotizacion.getFactura().getPersona().getIdentificacion());
		if (cotizacion.getFactura().getPersona().getNombres() != null && !cotizacion.getFactura().getPersona().getNombres().equals(""))
			gasto.setNombres(cotizacion.getFactura().getPersona().getNombres());
		else if (cotizacion.getFactura().getPersona().getNombre() != null && !cotizacion.getFactura().getPersona().getNombre().equals(""))
			gasto.setNombres(cotizacion.getFactura().getPersona().getNombre());
		else
			gasto.setNombres(cotizacion.getFactura().getPersona().getRazonSocial());
		if (cotizacion.getFactura().getPersona().getApellidos() != null)
			gasto.setApellidos(cotizacion.getFactura().getPersona().getApellidos());
		else
			gasto.setApellidos("");
		gasto.setValor(cotizacion.getFactura().getTotal());
		gasto.setValorEvento(gasto.getValor() - cotizacion.getFactura().getDescuentos());
		gasto.setIdUsuario(idUsuario);
		gasto.setTipo("PROVEEDOR");
		gasto.setFecha(cotizacion.getFactura().getFechaFactura());
		gasto.setFechaRegistro(new Date());
		gasto.setConcepto("FACTURA DEL PROVEEDOR DE LA COTIZACION " + cotizacion.getId());
		gasto = addLegalizacion(evento,gasto,tipo);
		gasto.setDocumentoId(cotizacion.getId());
		getBaseService().saveUpdate(gasto);

		return gasto;
	}

	public LegalizacionGasto addLegalizacion(Evento evento, LegalizacionGasto gasto, String tipo) {
		Collections.sort(evento.getGastos(), Comparator.comparing(o -> new Integer(o.getId())));

		Collections.sort(evento.getViaticos(), Comparator.comparing(o -> new Integer(o.getId())));

		if (tipo.equals("gastos")) {
			if (evento.getGastos().isEmpty()) {
				gasto.setId(1);
			} else
				gasto.setId(evento.getGastos().get(evento.getGastos().size() - 1).getId() + 1);
		} else {
			if (evento.getViaticos().isEmpty()) {
				gasto.setId(1);
			} else
				gasto.setId(evento.getViaticos().get(evento.getViaticos().size() - 1).getId() + 1);
		}
		return gasto;
	}

	public String base64File(String path) throws IOException {
		InputStream finput = EventoServiceRest.class.getClassLoader().getResourceAsStream(path);
		byte[] bytes = IOUtils.toByteArray(finput);
		String imageStr = Base64.getEncoder().encodeToString(bytes);
		return imageStr;
	}

	@PostMapping("/generarFactura")
	public DocumentoContable generarFactura(@RequestBody Map<String, Object> json) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		int idEvento = (int) json.get("idEvento");
		Evento evento = getEvento(idEvento);
		evento.setFactura(getFactura(evento.getId(), 0));
		evento.setContrato(admin.getContrato(evento.getIdContrato(), false));
		DocumentoContable documentoContable = mapper.convertValue(json.get("documentoContable"), DocumentoContable.class);

		if (evento.getFactura() == null || (evento.getFactura() != null && evento.getFactura().getId() == 0)) {
			evento.getFactura().setCreador(admin.getUsuario(documentoContable.getCreador_id(), false));
			evento.getFactura().setTipoDocumento(admin.getTipoDocumentoContable(Integer.parseInt(ApplicationService.getParametro("tipoDocumento.factura"))));
			evento.getFactura().setEstado(DocumentoContable.AUTORIZADO);
			evento.getFactura().setEventoId(evento.getId());
			evento.getFactura().setObservaciones(
					"Evento ID: " + evento.getId() + " Contrato: " + evento.getContrato().getNumero() + " Ciudad del Evento: " + evento.getSede().getNombre());
			evento.getFactura().setFecha(new Date());
			evento.getFactura().setHora(new Date());
			evento.getFactura().setFechaFactura(evento.getFactura().getFecha());
		}

		if (evento.getFactura() != null && evento.getFactura().getTipoDocumento().isNumeracionManual()) {
			if (!"".equals(evento.getFactura().getNumero())) {
				documentoContable.setEventoId(evento.getId());
				documentoContable = (DocumentoContable) getBaseService().saveUpdate(documentoContable);
				evento.setFactura(documentoContable);
				evento.setIdFactura((int) documentoContable.getId());
				saveEvento(evento);
			}
		} else {
			evento.setFactura(documentoContable);
		}
		return getFactura(evento.getId(), 0);
	}

	@PostMapping("/saveLegalizacion")
	public LegalizacionGasto saveLegalizacion(@RequestBody LegalizacionGasto gasto) throws Exception {

		if (gasto.getValorEvento() == 0)
			gasto.setValorEvento(gasto.getValor());
		else
			gasto.setValorEvento(gasto.getValorEvento());

		double total = 0;
		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		properties.add("evento_id = :evento_id");
		propertyValues.put("evento_id", gasto.getIdEvento());

		ArrayList<LegalizacionGasto> datos =
				(ArrayList<LegalizacionGasto>) getBaseService().getByPropertiesNamed(LegalizacionGasto.class.getName(), properties, propertyValues, "");

		if (gasto.getId() == 0) {
			gasto.setId((datos.size() + 1));
		}

		//total = gasto.getValor();

		getBaseService().saveUpdate(gasto);

		ArrayList<LegalizacionGasto> currentGastos = getLegalizaciones(gasto.getIdEvento());

		if (!currentGastos.isEmpty()) {
			for (LegalizacionGasto g : currentGastos) {
				total += g.getValor();
			}
		}
		// else {
		//	total = gasto.getValor();
		// }

		Evento evento = getEvento(gasto.getIdEvento());
		System.out.println(evento);
		//double currentTotal = evento.getTotalGastos();
		//System.out.println("Current Total: " + currentTotal);
		//total += currentTotal;
		gasto.setTotalGastosEvento(total);
		evento.setTotalGastos(total);
		System.out.println("Total Gasto: " + evento.getTotalGastos());
		getBaseService().saveUpdate(evento);

		return gasto;
	}

	@PostMapping("/saveViatico")
	public EventoViaticos saveViatico(@RequestBody EventoViaticos viatico) throws Exception {

		if (viatico.getValorEvento() == 0)
			viatico.setValorEvento(viatico.getValor());
		else
			viatico.setValorEvento(viatico.getValorEvento());

		double total = 0;

		ArrayList<EventoViaticos> viaticos = getViaticos(viatico.getIdEvento());

		if (viatico.getId() == 0) {
			viatico.setId(((viaticos == null) ? 0 : viaticos.size()) + 1);
		}

		viatico = (EventoViaticos) getBaseService().saveUpdate(viatico);

		ArrayList<EventoViaticos> currentViant = getViaticos(viatico.getIdEvento());

		if (!currentViant.isEmpty()) {
			for (EventoViaticos g : currentViant) {
				total += g.getValor();
			}
		} else {
			total = viatico.getValor();
		}

		Evento evento = getEvento(viatico.getIdEvento());
		double currentTotal = evento.getTotalGastos();

		total += currentTotal;

		evento.setTotalGastos(total);

		getBaseService().saveUpdate(evento);

		return viatico;
	}

	@GetMapping("/getLegalizacion/{idEvento}/{id}")
	public LegalizacionGasto getLegalizacion(@PathVariable int idEvento, @PathVariable int id) {
		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		properties.add("evento_id = :evento_id");
		propertyValues.put("evento_id", idEvento);
		properties.add("id = :id");
		propertyValues.put("id", id);

		ArrayList<LegalizacionGasto> datos =
				(ArrayList<LegalizacionGasto>) getBaseService().getByPropertiesNamed(LegalizacionGasto.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			LegalizacionGasto gasto = datos.get(0);
			gasto.setNombreUsuario(admin.getPersona(gasto.getIdUsuario(), false).getNombre());
			return gasto;
		}
		return null;
	}

	@GetMapping("/getLegalizaciones/{idEvento}")
	public ArrayList<LegalizacionGasto> getLegalizaciones(@PathVariable int idEvento) {
		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		properties.add("evento_id = :evento_id");
		propertyValues.put("evento_id", idEvento);

		ArrayList<LegalizacionGasto> datos =
				(ArrayList<LegalizacionGasto>) getBaseService().getByPropertiesNamed(LegalizacionGasto.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			for (LegalizacionGasto gasto : datos) {
				gasto.setNombreUsuario(admin.getPersona(gasto.getIdUsuario(), false).getNombre());
			}
			return datos;
		}
		return null;
	}

	@GetMapping("/getViatico/{idEvento}/{id}")
	public EventoViaticos getViatico(@PathVariable int idEvento, @PathVariable int id) {
		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		properties.add("evento_id = :evento_id");
		propertyValues.put("evento_id", idEvento);
		properties.add("id = :id");
		propertyValues.put("id", id);

		ArrayList<EventoViaticos> datos =
				(ArrayList<EventoViaticos>) getBaseService().getByPropertiesNamed(EventoViaticos.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			EventoViaticos viatico = datos.get(0);
			viatico.setNombreUsuario(admin.getPersona(viatico.getIdUsuario(), false).getNombre());
			return viatico;
		}
		return null;
	}

	@GetMapping("/getViaticos/{idEvento}")
	public ArrayList<EventoViaticos> getViaticos(@PathVariable int idEvento) {
		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		properties.add("evento_id = :evento_id");
		propertyValues.put("evento_id", idEvento);

		ArrayList<EventoViaticos> datos =
				(ArrayList<EventoViaticos>) getBaseService().getByPropertiesNamed(EventoViaticos.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			for (EventoViaticos gasto : datos) {
				gasto.setNombreUsuario(admin.getPersona(gasto.getIdUsuario(), false).getNombre());
			}
			return datos;
		}
		return null;
	}

	@GetMapping("/generarDocumentoEquivalenteGastos/{idEvento}/{idLegalizacion}")
	public LegalizacionGasto generarDocumentoEquivalenteGastos(@PathVariable int idEvento, @PathVariable int idLegalizacion) throws Exception {
		LegalizacionGasto gasto = getLegalizacion(idEvento, idLegalizacion);
		if (gasto.getNumeroDocEquivalente() == null || "".equals(gasto.getNumeroDocEquivalente())) {
			RangoNumeracion rango = admin.getRangoNum(3);
			gasto.setNumeroDocEquivalente(rango.getPrefijo() + " " + rango.getNumeroActual());
			gasto = (LegalizacionGasto) getBaseService().saveUpdate(gasto);
			rango.setNumeroActual((rango.getNumeroActual() + 1));
			getBaseService().saveUpdate(rango);
		}
		return gasto;
	}

	@GetMapping("/generarDocumentoEquivalenteViaticos/{idEvento}/{idLegalizacion}")
	public EventoViaticos generarDocumentoEquivalenteViaticos(@PathVariable int idEvento, @PathVariable int idLegalizacion) throws Exception {
		Evento evento = getEvento(idEvento);
		if (evento != null && (evento.getNumeroDocEquivalente() == null || "".equals(evento.getNumeroDocEquivalente()))) {
			RangoNumeracion rango = admin.getRangoNum(3);
			evento.setNumeroDocEquivalente(rango.getPrefijo() + " " + rango.getNumeroActual());
			getBaseService().saveUpdate(evento);
			rango.setNumeroActual((rango.getNumeroActual() + 1));
			getBaseService().saveUpdate(rango);
		}
		return new EventoViaticos();
	}

	public Object getPropertyValue(Object objeto, String property) {
		Class clase = objeto.getClass();
		try {
			/*
			 * if (clase.getDeclaredField(property).getType().equals(Boolean.TYPE)) return clase.getMethod("is" + JSFUtil.primeraMayuscula(property), null).invoke(objeto, null); else
			 */
			return clase.getMethod("get" + JSFUtil.primeraMayuscula(property), new Class[0]).invoke(objeto, new Object[0]);
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@GetMapping("/getTiquetes/{idEvento}")
	public ArrayList<TiqueteEvento> getTiquetes(@PathVariable int idEvento) {
		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		properties.add("evento_id = :evento_id");
		propertyValues.put("evento_id", idEvento);

		ArrayList<TiqueteEvento> datos =
				(ArrayList<TiqueteEvento>) getBaseService().getByPropertiesNamed(TiqueteEvento.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			for (TiqueteEvento tiquete : datos) {
				tiquete.setAerolinea(admin.getAerolinea(tiquete.getIdAerolinea()));
			}
			return datos;
		}
		return null;
	}

	@GetMapping("/getTiquete/{idEvento}/{localizador}")
	public TiqueteEvento getTiquete(@PathVariable int idEvento, @PathVariable String localizador) {
		ArrayList<String> properties = new ArrayList<>();
		Map<String, Object> propertyValues = new HashMap<>();
		properties.add("evento_id = :evento_id");
		propertyValues.put("evento_id", idEvento);
		properties.add("localizador = :localizador");
		propertyValues.put("localizador", localizador);

		ArrayList<TiqueteEvento> datos =
				(ArrayList<TiqueteEvento>) getBaseService().getByPropertiesNamed(TiqueteEvento.class.getName(), properties, propertyValues, "");

		if (!datos.isEmpty()) {
			TiqueteEvento tiquete = datos.get(0);
			tiquete.setAerolinea(admin.getAerolinea(tiquete.getIdAerolinea()));
			return tiquete;
		}
		return null;
	}

	@PostMapping("/saveTiquete")
	public TiqueteEvento saveTiquete(@RequestBody Map<String, Object> json) throws Exception {
		TiqueteEvento tiquete = emailServicesRest.emailTiquete(json);

		tiquete = (TiqueteEvento) getBaseService().saveUpdate(tiquete);

		String obser = "Se crea el tiquete ";

		if (getTiquete(tiquete.getIdEvento(), tiquete.getLocalizador()) != null) {
			obser = "Se modifica el tiquete ";
		}

		EventoModificacion modificacion =
				new EventoModificacion(tiquete.getIdEvento(), new Date(), (int) json.get("idUsuario"), obser + tiquete.getKey(), EventoModificacion.TIQUETES,
						"");
		getBaseService().saveUpdate(modificacion);

		return tiquete;
	}
}
