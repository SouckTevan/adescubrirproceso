package co.com.adescubrir.model.dao;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.persistence.Query;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.springframework.orm.hibernate5.support.HibernateDaoSupport;

import co.com.mycommons.jsf.components.JSFUtil;

@SuppressWarnings("rawtypes")
public class BaseDAOHibernate extends HibernateDaoSupport implements BaseDAO {

	@Override
	public Object saveOrUpdate(Object objeto) throws Exception {
		try {
			getHibernateTemplate().saveOrUpdate(objeto);
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
		return objeto;
	}

	@Override
	public void saveOrUpdateAll(Collection objetos) throws Exception {
		try {

			for (Object object : objetos) {
				getHibernateTemplate().saveOrUpdate(object);
			}
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	public List getByProperties(String entityName, String properties[], Object propertiesValues[], String orderBy) {
		String queryString = "from " + entityName;
		if (propertiesValues.length > 0)
			queryString += " where ";
		for (int i = 0; i < propertiesValues.length; i++) {
			queryString += properties[i] + " " + (i == propertiesValues.length - 1 ? "" : "and ");
		}
		if (!"".equals(orderBy))
			queryString += " order by " + orderBy;
		if (propertiesValues.length > 0)
			return getHibernateTemplate().find(queryString, propertiesValues);
		else
			return getHibernateTemplate().find(queryString);
	}

	@Override
	public List getByPropertiesNamed(String entityName, ArrayList<String> properties, Map<String, Object> propertyValues, String orderBy) {
		String queryString = "from " + entityName;
		if (!propertyValues.isEmpty())
			queryString += " where ";
		for (int i = 0; i < properties.size(); i++) {
			queryString += properties.get(i) + " " + (i == properties.size() - 1 ? "" : "and ");
		}
		String propsNamed[] = new String[propertyValues.size()];
		Object propsValues[] = new Object[propertyValues.size()];
		int i = 0;
		for (Map.Entry<String, Object> object : propertyValues.entrySet()) {
			propsNamed[i] = object.getKey();
			propsValues[i] = object.getValue();
			i++;
		}

		if (!"".equals(orderBy))
			queryString += " order by " + orderBy;
		if (!propertyValues.isEmpty())
			return getHibernateTemplate().findByNamedParam(queryString, propsNamed, propsValues);
		else
			return getHibernateTemplate().find(queryString);
	}

	@Override
	public void delete(Object objeto) throws Exception {
		getHibernateTemplate().delete(objeto);
	}

	@Override
	public void deleteAll(Collection entities) throws Exception {
		getHibernateTemplate().deleteAll(entities);
	}

	@Override
	public List executeSQLQuery(String sqlQuery) {
		// return
		// getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(sqlQuery).list();
		return getHibernateTemplate().getSessionFactory().getCurrentSession().createNativeQuery(sqlQuery).list();
	}

	@Override
	public Object getById(String entityName, int id, String[] initializedProterties) {
		Object retorno = null;
		retorno = getHibernateTemplate().load(entityName, id);
		if (!getHibernateTemplate().getSessionFactory().isClosed()) {
			initialize(retorno, initializedProterties);
		}
		return retorno;
	}

	public void initialize(Object objeto, String[] initializedProterties) {
		Hibernate.initialize(objeto);
		for (String property : initializedProterties) {
			if (property.indexOf(".") == -1) {
				// initialize(objeto, property);
			} else {
				StringTokenizer tokenizer2 = new StringTokenizer(property, ".");
				if (tokenizer2.hasMoreElements()) {
					String token0 = (String) tokenizer2.nextElement();
					String token1 = (String) tokenizer2.nextElement();
					// initialize(objeto, token0);
					Object value0 = getPropertyValue(objeto, token0);
					if (value0 != null) {
						if (value0 instanceof Collection) {
							for (Object obj : (Collection) value0) {
								// initialize(obj, token1);
							}
						} else {
							// initialize(value0, token1);
						}
						if (tokenizer2.hasMoreElements()) {
							Object value1 = getPropertyValue(value0, token1);
							String token2 = (String) tokenizer2.nextElement();
							if (value1 != null) {
								// initialize(value1, token2);
							}
						}
					}
				}
			}
		}
	}

	public Object getPropertyValue(Object objeto, String property) {
		Class clase = objeto.getClass();
		try {
			return clase.getMethod("get" + JSFUtil.primeraMayuscula(property), new Class[0]).invoke(objeto, new Object[0]);
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public List getByProperty(String entityName, String property, Object propertyValue, String orderBy) {
		if (!"".equals(property))
			return getByProperties(entityName, new String[] { property }, new Object[] { propertyValue }, orderBy);
		else
			return getByProperties(entityName, new String[0], new Object[0], orderBy);
	}

	@Override
	public List getByProperty(String entityName, String property, Object propertyValue, String orderBy, String[] initializedProterties) {
		List retorno = getByProperty(entityName, property, propertyValue, orderBy);
		for (Object object : retorno) {
			initialize(object, initializedProterties);
		}
		return retorno;
	}

	@Override
	public List<Map<String, Object>> getByPropertiesNamedNativeQuery(String select, String strQuery, Map<String, Object> propertyValues) {
		
		String queryString = "SELECT " + select + " " + strQuery;
		String[] listaColumnas = select.split(",");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createNativeQuery(queryString);
		for (Map.Entry<String, Object> object : propertyValues.entrySet()) {
			query.setParameter(object.getKey(), object.getValue());
		}
		ArrayList<Object[]> resQuery = (ArrayList<Object[]>) query.getResultList();
		ArrayList<Map<String, Object>> resultado = new ArrayList<Map<String, Object>>();
		for (Object[] fila : resQuery) {
			Map<String, Object> datoFila = new LinkedHashMap<String, Object>();
			for (int i = 0; i < listaColumnas.length; i++) {
				String nombreColumna = listaColumnas[i];
				if (nombreColumna.toUpperCase().contains(" AS ")) {
					nombreColumna = nombreColumna.substring(nombreColumna.toUpperCase().indexOf(" AS ") + 4);
				}
				datoFila.put(nombreColumna, fila[i]);
			}
			resultado.add(datoFila);
		}
		return resultado;
	}

	@Override
	public List<Map<String, Object>> getByPropertiesNamedNativeQuery(String tabla, String select, String strQuery, Map<String, Object> propertyValues) {

		if (select == "" || select == "*") {
			ArrayList<Object[]> bdTableDefinition = (ArrayList<Object[]>)executeSQLQuery("SELECT column_name FROM information_schema.columns WHERE table_name   = '"+tabla+"' order by ordinal_position");
			for (Object[] objects : bdTableDefinition) {
				select += (select == "" ? "" : ",") + objects[0];
			}
		}
		
		String queryString = "SELECT " + select + " " + strQuery;
		String[] listaColumnas = select.split(",");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createNativeQuery(queryString);
		for (Map.Entry<String, Object> object : propertyValues.entrySet()) {
			query.setParameter(object.getKey(), object.getValue());
		}
		ArrayList<Object[]> resQuery = (ArrayList<Object[]>) query.getResultList();
		ArrayList<Map<String, Object>> resultado = new ArrayList<Map<String, Object>>();
		for (Object[] fila : resQuery) {
			Map<String, Object> datoFila = new LinkedHashMap<String, Object>();
			for (int i = 0; i < listaColumnas.length; i++) {
				String nombreColumna = listaColumnas[i];
				if (nombreColumna.toUpperCase().contains(" AS ")) {
					nombreColumna = nombreColumna.substring(nombreColumna.toUpperCase().indexOf(" AS ") + 4);
				}
				datoFila.put(nombreColumna, fila[i]);
			}
			resultado.add(datoFila);
		}
		return resultado;
	}

}
