package co.com.adescubrir.model.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@SuppressWarnings("rawtypes")
public interface BaseDAO {
	
	public List getByProperties(String entityName, String properties[], Object propertiesValues[], String orderBy);

	List getByPropertiesNamed(String entityName, ArrayList<String> properties, Map<String, Object> propertyValues, String orderBy);

	void deleteAll(Collection entities) throws Exception;

	void delete(Object objeto) throws Exception;

	List executeSQLQuery(String sqlQuery);

	public List getByProperty(String entityName, String property, Object propertyValue, String orderBy);

	public abstract List getByProperty(String entityName, String property, Object propertyValue, String orderBy, String[] initializedProterties);

	Object saveOrUpdate(Object objeto) throws Exception;

	void saveOrUpdateAll(Collection objetos) throws Exception;

	Object getById(String entityName, int id, String[] initializedProterties);

	List<Map<String, Object>> getByPropertiesNamedNativeQuery(String select, String strQuery, Map<String, Object> propertyValues);

	List<Map<String, Object>> getByPropertiesNamedNativeQuery(String tabla, String select, String strQuery, Map<String, Object> propertyValues);
}
