package co.com.adescubrir.model.bo.eventos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import co.com.adescubrir.model.bo.Municipio;

@Entity
@Table(name = "contrato_municipio")
public class ContratoMunicipio implements Serializable {

    @Id
    @Column(name = "contrato_id", nullable = false, length = 10)
    private int contrato_id;

    @Id
    @Column(name = "municipio_id", nullable = false, length = 10)
    private int municipio_id;

    @Transient
    private Municipio municipio = new Municipio();

    public int getContrato_id() {
        return contrato_id;
    }

    public void setContrato_id(int contrato_id) {
        this.contrato_id = contrato_id;
    }

    public int getMunicipio_id() {
        return municipio_id;
    }

    public void setMunicipio_id(int municipio_id) {
        this.municipio_id = municipio_id;
    }

    public Municipio getMunicipio() {
        return municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }
}
