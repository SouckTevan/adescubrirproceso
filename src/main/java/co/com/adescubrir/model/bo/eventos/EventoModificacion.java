package co.com.adescubrir.model.bo.eventos;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import co.com.adescubrir.model.bo.security.Usuario;
import lombok.Data;

@Entity
@Data
@Table(name = "evento_modificaciones")
public class EventoModificacion implements Serializable {

    @Transient
    public static final String ESTADO = "ESTADO";
    @Transient
    public static final String DATOS = "DATOS";
    @Transient
    public static final String CONSUMOS = "CONSUMOS";
    @Transient
    public static final String COTIZACIONES = "COTIZACIONES";
    @Transient
    public static final String DOCUMENTOS = "DOCUMENTOS";
    @Transient
    public static final String TIQUETES = "TIQUETES";
    @Transient
    public static final String MODIFICACIONES = "MODIFICACIONES";
    @Transient
    public static final String FACTURACION = "FACTURACION";

    @Id
    @Column(name = "evento_id")
    private int idEvento;

    @Column(name = "fechahora")
    private Date fechaHora;

    @Id
    @Column(name = "usuario_id")
    private int idUsuario;

    @Transient
    private Usuario usuario = new Usuario();

    @Column(name = "observacion")
    private String observacion;

    @Column(name = "rutaarchivo")
    private String rutaArchivo;

    @Column(name = "tipo_modificacion")
    private String tipoModificacion;

    public EventoModificacion(int idEvento, Date fechaHora, int idUsuario, String observacion, String tipoModificacion, String rutaArchivo)
    {
        this.idEvento = idEvento;
        this.fechaHora = fechaHora;
        this.idUsuario = idUsuario;
        this.observacion = observacion;
        this.tipoModificacion = tipoModificacion;
        this.rutaArchivo = rutaArchivo;
    }
}
