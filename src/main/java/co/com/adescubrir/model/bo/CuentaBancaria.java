package co.com.adescubrir.model.bo;

import java.io.Serializable;
import java.util.Date;
import java.util.StringTokenizer;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import co.com.mycommons.util.Formato;

@Entity
@Table(name = "persona_cuentasbancarias")
@EntityListeners(AuditingEntityListener.class)
public class CuentaBancaria implements Serializable {

    @Id
    @Column(name = "persona_id", nullable = false)
    private int id;

    @Transient
    private Banco banco = new Banco();

    @Id
    @Column(name = "banco_id", nullable = false)
    private int banco_id;

    @Column(name = "tipocuenta", nullable = false, length = 10)
    private String tipoCuenta;

    @Column(name = "cuenta", nullable = false, length = 20)
    private String cuenta = "";

    @Column(name = "fechavencimiento")
    private Date fechaVencimiento;

    @Column(name = "franquicia", length = 10)
    private String franquicia;

    @Column(name = "ccv", length = 10)
    private String ccv;

    @Column(name = "titular", length = 50)
    private String titular;

    @Column(name = "nombre_titular", length = 100)
    private String nombreTitular;

    @Transient
    private String key;

    public CuentaBancaria() {
    }

    public CuentaBancaria(String key) {
        StringTokenizer tokenizer = new StringTokenizer(key, "|");
        if (tokenizer.hasMoreElements()) {
            String token = (String) tokenizer.nextElement();
            Banco banco = new Banco();
            banco.setId((new Integer(token)).intValue());
            setBanco(banco);
            if (tokenizer.hasMoreElements()) {
                token = (String) tokenizer.nextElement();
                setCuenta(token);
            }
            if (tokenizer.hasMoreElements()) {
                token = (String) tokenizer.nextElement();
                setTipoCuenta(token);
            }
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof CuentaBancaria) {
            return (getKey()).equals(((CuentaBancaria) obj).getKey());
        }
        return false;
    }

    @Override
    public String toString() {
		return (getBanco() == null ? "0" : getBanco().toString()) + "|" + Formato.cambiarSiEsNull(getCuenta(), "") + "|" + Formato.cambiarSiEsNull(getTipoCuenta(), "");
    }

    public String getKey() {
        return toString();
    }

    public Banco getBanco() {
        return banco;
    }

    public void setBanco(Banco banco) {
        this.banco = banco;
    }

    public String getTipoCuenta() {
        return tipoCuenta;
    }

    public void setTipoCuenta(String tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public String getFranquicia() {
        return franquicia;
    }

    public void setFranquicia(String franquicia) {
        this.franquicia = franquicia;
    }

    public String getCcv() {
        return ccv;
    }

    public void setCcv(String ccv) {
        this.ccv = ccv;
    }

    public String getTitular() {
        return titular;
    }

    public void setTitular(String titular) {
        this.titular = titular;
    }

    public String getNombreTitular() {
        return nombreTitular;
    }

    public void setNombreTitular(String nombreTitular) {
        this.nombreTitular = nombreTitular;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBanco_id() { return banco_id; }

    public void setBanco_id(int banco_id) { this.banco_id = banco_id; }
}
