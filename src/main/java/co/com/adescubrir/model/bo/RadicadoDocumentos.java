package co.com.adescubrir.model.bo;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "radicado_documentos")
public class RadicadoDocumentos {

    @Id
    @Column(name = "radicado_id", nullable = false)
    private int radicadoId;

    @Column(name = "documento_id", nullable = false)
    private int documentoId;

    @Column(name = "fechahora", nullable = false)
    private Timestamp fechaHora;

    @Column(name = "observacion", length = 200)
    private String observacion;

    @Column(name = "rutaarchivo", length = 200)
    private String rutaArchivo;

    @Column(name = "nombrearchivo", length = 200)
    private String nombreArchivo;

    @Column(name = "usuario_id", nullable = false)
    private int usuarioId;

    public int getRadicadoId() {
        return radicadoId;
    }

    public void setRadicadoId(int radicadoId) {
        this.radicadoId = radicadoId;
    }

    public int getDocumentoId() {
        return documentoId;
    }

    public void setDocumentoId(int documentoId) {
        this.documentoId = documentoId;
    }

    public Timestamp getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Timestamp fechaHora) {
        this.fechaHora = fechaHora;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getRutaArchivo() {
        return rutaArchivo;
    }

    public void setRutaArchivo(String rutaArchivo) {
        this.rutaArchivo = rutaArchivo;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }
}
