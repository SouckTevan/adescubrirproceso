package co.com.adescubrir.model.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import co.com.adescubrir.model.bo.security.Usuario;

@Entity
@Table(name = "persona_documentos")
@EntityListeners(AuditingEntityListener.class)
public class RegistroDocumento implements Serializable {

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "_id")
    @SequenceGenerator(name="_id", sequenceName = "persona_documentos_seq", allocationSize = 1)
    private int id;

    @Id
    @Column(name = "persona_id", nullable = false, unique = true)
    private int persona_id;

    @Id
    @Column(name = "documento_id", nullable = false)
    private int documento_id;

    @Column(name = "fechahora", nullable = false)
    private Date fechaHora;

    @Transient
    private Documento documento;

    @Id
    @Column(name = "usuario_id", nullable = false)
    private int usuario_id;

    @Transient
    private Usuario usuario;

    @Column(name = "filetype", length = 150)
    private String fileType;

    @Column(name = "rutaarchivo", length = 200)
    private String rutaArchivo;

    @Column(name = "nombrearchivo", length = 200)
    private String nombreArchivo;

    @Column(name = "observacion", length = 200)
    private String observacion;

    @Override
    public boolean equals(Object obj)
    {
        if (obj instanceof RegistroDocumento)
        {
            RegistroDocumento temp = (RegistroDocumento) obj;
            return getPersona_id() == temp.getPersona_id() && getDocumento_id() == temp.getDocumento_id();
        }
        return false;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Documento getDocumento() {
        return documento;
    }

    public void setDocumento(Documento documento) {
        this.documento = documento;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getRutaArchivo() {
        return rutaArchivo;
    }

    public void setRutaArchivo(String rutaArchivo) {
        this.rutaArchivo = rutaArchivo;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    public int getPersona_id() { return persona_id; }

    public void setPersona_id(int persona_id) { this.persona_id = persona_id; }

    public int getDocumento_id() { return documento_id; }

    public void setDocumento_id(int documento_id) { this.documento_id = documento_id; }

    public int getUsuario_id() { return usuario_id; }

    public void setUsuario_id(int usuario_id) { this.usuario_id = usuario_id; }
}
