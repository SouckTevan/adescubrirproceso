package co.com.adescubrir.model.bo.eventos;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "evento_ejecucion")
public class EventoEjecucion implements Serializable {

    @Id
    @Column(name = "evento_id")
    private int idEvento;

    @Id
    @Column(name = "concepto_id")
    private int idConcepto;

    @Id
    @Column(name = "fecha")
    private Date fecha;

    @Column(name = "cantidad")
    private int cantidad;

    @Column(name = "valor")
    private double valor = 0.0;
}
