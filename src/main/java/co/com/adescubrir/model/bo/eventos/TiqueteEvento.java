package co.com.adescubrir.model.bo.eventos;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import co.com.adescubrir.model.bo.Aerolinea;
import co.com.adescubrir.model.bo.Concepto;
import co.com.adescubrir.model.bo.Persona;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper=false)
@Table(name = "evento_tiquete")
public class TiqueteEvento implements Serializable {

    @Transient
    public static final String SOLICITADO = "SOLICITADO";

    @Transient
    public static final String EMITIDO = "EMITIDO";

    @Id
    @Column(name = "evento_id")
    private int idEvento;

    @Column(name = "numero")
    private String numero;

    @Transient
    private Concepto concepto = new Concepto();

    @Column(name = "ruta")
    private String ruta = "";

    @Transient
    private Persona pasajero;

    @Column(name = "persona_id")
    private int idPersona;

    @Column(name = "dniPasajero")
    private String dniPasajero = "";

    @Id
    @Column(name = "nombrePasajero")
    private String nombrePasajero = "";

    @Transient
    private Aerolinea aerolinea = new Aerolinea();

    @Id
    @Column(name = "aerolinea_id")
    private int idAerolinea;

    @Column(name = "total")
    private double total;

    @Column(name = "impuestos")
    private double impuestos;

    @Column(name = "valor")
    private double valor;

    @Column(name = "tasas")
    private double tasas;

    @Column(name = "administrativo")
    private double administrativo;

    @Column(name = "valorevento")
    private double valorEvento;

    @Column(name = "fee")
    private double fee;

    @Column(name = "modificacion")
    private double modificacion;

    @Column(name = "motivomodificacion")
    private String motivoModificacion;

    @Column(name = "factura")
    private String factura = "";

    @Column(name = "localizador")
    private String localizador;

    @Column(name = "estado")
    private String estado = SOLICITADO;

    @Column(name = "fechasalida")
    private Date fechaSalida;

    @Column(name = "horasalida")
    private Date horaSalida;

    @Column(name = "horaregreso")
    private Date horaRegreso;

    @Column(name = "fecharegreso")
    private Date fechaRegreso;

    @Column(name = "itinerariocompleto")
    private String itinerarioCompleto;

    @Column(name = "observaciones")
    private String observaciones;

    @Column(name = "valorrevisado")
    private double valorRevisado;

    @Column(name = "revisado")
    private boolean revisado;

    @Transient
    public String getKey()
    {
        return toString();
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj instanceof TiqueteEvento)
        {
            return (getKey()).equals(((TiqueteEvento) obj).getKey());
        }
        return false;
    }

    @Override
    public String toString()
    {
        return (getNombrePasajero() == null ? "0" : getNombrePasajero()) + "|" + getRuta() + "|" + getNumero() + "|" + getEstado();
    }
}
