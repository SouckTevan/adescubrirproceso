package co.com.adescubrir.model.bo.security;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import co.com.adescubrir.model.bo.Parametro;

@Entity
@Table(name = "rol")
@EntityListeners(AuditingEntityListener.class)
public class Rol implements Serializable {

    @Transient
    public static String LABEL_NAME = "Roles";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "_rol")
    @SequenceGenerator(name="_rol", sequenceName = "rol_seq", allocationSize = 1)
    @Column(name = "rol_id", nullable = false, unique = true)
    private int id;

    @Column(name = "nombre", nullable = false)
    private String nombre;

    @Column(name = "estado", nullable = false)
    private String estado = Parametro.ESTADO_ACTIVO;

    @Column(name = "contrato_id", nullable = false)
    private int contratoId;

    @Transient
    private List<RolOpcion> opciones = new ArrayList<RolOpcion>();

    @Transient
    private List<Integer> contratos = new ArrayList<Integer>();

    @Transient
    private List<Integer> estados = new ArrayList<Integer>();


    @Override
    public boolean equals(Object obj)
    {
        if (obj instanceof Rol)
        {
            return getId() == ((Rol) obj).getId();
        }
        return false;
    }

    @Override
    public String toString()
    {
        return "" + getId();
    }

    @Override
    public int hashCode()
    {
        return getId();
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getEstado()
    {
        return estado;
    }

    public void setEstado(String estado)
    {
        this.estado = estado;
    }

    public List<RolOpcion> getOpciones()
    {
        return opciones;
    }

    public void setOpciones(List<RolOpcion> opciones)
    {
        this.opciones = opciones;
    }

    public List<Integer> getContratos()
    {
        return contratos;
    }

    public void setContratos(List<Integer> contratos)
    {
        this.contratos = contratos;
    }

    public int getContratoId()
    {
        return contratoId;
    }

    public void setContratoId(int contratoId)
    {
        this.contratoId = contratoId;
    }

    public List<Integer> getEstados()
    {
        return estados;
    }

    public void setEstados(List<Integer> estados)
    {
        this.estados = estados;
    }
}
