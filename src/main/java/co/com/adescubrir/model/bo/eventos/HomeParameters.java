package co.com.adescubrir.model.bo.eventos;

import lombok.Data;

@Data
public class HomeParameters {
    private int idEvento;
    private int idContrato;
    private String idMunicipio;
    private String fechaInicio;
    private String fechaFin;
    private int idEstado;
}
