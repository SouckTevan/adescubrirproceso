package co.com.adescubrir.model.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "estado")
@Inheritance(strategy = InheritanceType.JOINED)
public class Estado implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "_estado")
    @SequenceGenerator(name="_estado", sequenceName = "estado_seq", allocationSize = 1)
    @Column(name = "estado_id", nullable = false, unique = true)
    private int id;

    @Column(name= "estado", nullable = false, length = 5)
    private String estado;

    @Column(name= "nombre", nullable = false, length = 50)
    private String nombre;

    @Column(name= "modulo", nullable = false, length = 255)
    private String modulo;

    @Column(name= "es_inicial", nullable = false)
    private boolean esInicial;

    @Column(name= "es_final", nullable = false)
    private boolean esFinal;

    @Column(name= "nuevo_responsable")
    private int nuevo_responsable = -1;

    @Column(name= "responsable_evento", nullable = false)
    private boolean responsableEvento;

    @Column(name= "coordinador_evento", nullable = false)
    private boolean coordinadorEvento;

    @Column(name= "activo", nullable = false)
    private boolean activo;

    @Column(name= "sumar_totales", nullable = false)
    private boolean sumarTotales;

    @Column(name= "tipo", length = 30)
    private String tipo;

    @Column(name= "color", length = 10)
    private String color;

    @Column(name= "colortext", length = 10)
    private String colorText;

    @Transient
    private Persona nuevoResponsable = new Persona();

    @Override
    public String toString()
    {
        return "" + getId();
    }

    @Override
    public int hashCode()
    {
        return getId();
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getModulo()
    {
        return modulo;
    }

    public void setModulo(String modulo)
    {
        this.modulo = modulo;
    }

    public boolean isEsInicial()
    {
        return esInicial;
    }

    public void setEsInicial(boolean esInicial)
    {
        this.esInicial = esInicial;
    }

    public boolean isEsFinal()
    {
        return esFinal;
    }

    public void setEsFinal(boolean esFinal)
    {
        this.esFinal = esFinal;
    }

    public String getEstado()
    {
        return estado;
    }

    public void setEstado(String estado)
    {
        this.estado = estado;
    }

    public boolean isResponsableEvento()
    {
        return responsableEvento;
    }

    public void setResponsableEvento(boolean responsableEvento)
    {
        this.responsableEvento = responsableEvento;
    }

    public Persona getNuevoResponsable()
    {
        return nuevoResponsable;
    }

    public void setNuevoResponsable(Persona nuevoResponsable)
    {
        this.nuevoResponsable = nuevoResponsable;
    }

    public boolean isCoordinadorEvento()
    {
        return coordinadorEvento;
    }

    public void setCoordinadorEvento(boolean coordinadorEvento)
    {
        this.coordinadorEvento = coordinadorEvento;
    }

    public boolean isActivo()
    {
        return activo;
    }

    public void setActivo(boolean activo)
    {
        this.activo = activo;
    }

    public boolean isSumarTotales()
    {
        return sumarTotales;
    }

    public void setSumarTotales(boolean sumarTotales)
    {
        this.sumarTotales = sumarTotales;
    }

    public String getTipo()
    {
        return tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    public String getColor()
    {
        return color;
    }

    public void setColor(String color)
    {
        this.color = color;
    }

    public String getColorText()
    {
        return colorText;
    }

    public void setColorText(String colorText)
    {
        this.colorText = colorText;
    }

    public int getNuevo_responsable() {
        return nuevo_responsable;
    }

    public void setNuevo_responsable(int nuevo_responsable) {
        this.nuevo_responsable = nuevo_responsable;
    }
}