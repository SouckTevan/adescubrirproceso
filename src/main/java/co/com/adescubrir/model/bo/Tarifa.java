package co.com.adescubrir.model.bo;

import java.io.Serializable;
import java.util.StringTokenizer;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "proveedor_tarifa")
@EntityListeners(AuditingEntityListener.class)
public class Tarifa implements Serializable {

    @Id
    @Column(name = "proveedor_id", nullable = false)
    private int proveedor_id;

    @Id
    @Column(name = "municipio_id")
    private int municipio_id;

    @Column(name = "nombresucursal", nullable = false, length = 150)
    private String nombresucursal;

    @Id
    @Column(name = "concepto_id")
    private int concepto_id;

    @Id
    @Column(name = "tipotarifa_id")
    private int tipotarifa_id;

    @Column(name = "valor", nullable = false)
    private double valor;

    @Column(name = "descripcion", length = 255)
    private String descripcion;

    @Transient
    private Sucursal sucursal = new Sucursal();

    @Transient
    private Concepto concepto = new Concepto();

    @Transient
    private TipoTarifa tipoTarifa = new TipoTarifa();

    @Transient
    private String key;

    public Tarifa() {
    }

    public Tarifa(String key) {
        StringTokenizer tokenizer = new StringTokenizer(key, "|");
        if (tokenizer.hasMoreElements())
        {
            String token = (String) tokenizer.nextElement();
            Sucursal sucursal = new Sucursal(token + "|" + (String) tokenizer.nextElement());
            setSucursal(sucursal);
            token = (String) tokenizer.nextElement();
            Concepto concepto = new Concepto();
            concepto.setId((new Integer(token)).intValue());
            setConcepto(concepto);
            token = (String) tokenizer.nextElement();
            TipoTarifa tipoTarifa = new TipoTarifa();
            tipoTarifa.setId((new Integer(token)).intValue());
            setTipoTarifa(tipoTarifa);
        }
    }

    public String getKey() {
        return toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Tarifa)
        {
            return (getKey()).equals(((Tarifa) obj).getKey());
        }
        return false;
    }

    @Override
    public String toString() {
        return (getSucursal() == null ? "0" : getSucursal().toString()) + "|" + (getConcepto() == null ? "0" : getConcepto().toString()) + "|"
                + (getTipoTarifa() == null ? "0" : getTipoTarifa().toString());
    }

    public Concepto getConcepto()
    {
        return concepto;
    }

    public void setConcepto(Concepto concepto)
    {
        this.concepto = concepto;
    }

    public TipoTarifa getTipoTarifa()
    {
        return tipoTarifa;
    }

    public void setTipoTarifa(TipoTarifa tipoTarifa)
    {
        this.tipoTarifa = tipoTarifa;
    }

    public double getValor()
    {
        return valor;
    }

    public void setValor(double valor)
    {
        this.valor = valor;
    }

    public Sucursal getSucursal()
    {
        return sucursal;
    }

    public void setSucursal(Sucursal sucursal)
    {
        this.sucursal = sucursal;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public int getProveedor_id() {
        return proveedor_id;
    }

    public void setProveedor_id(int proveedor_id) {
        this.proveedor_id = proveedor_id;
    }

    public int getMunicipio_id() {
        return municipio_id;
    }

    public void setMunicipio_id(int municipio_id) {
        this.municipio_id = municipio_id;
    }

    public String getNombresucursal() {
        return nombresucursal;
    }

    public void setNombresucursal(String nombresucursal) {
        this.nombresucursal = nombresucursal;
    }

    public int getConcepto_id() {
        return concepto_id;
    }

    public void setConcepto_id(int concepto_id) {
        this.concepto_id = concepto_id;
    }

    public int getTipotarifa_id() {
        return tipotarifa_id;
    }

    public void setTipotarifa_id(int tipotarifa_id) {
        this.tipotarifa_id = tipotarifa_id;
    }
}
