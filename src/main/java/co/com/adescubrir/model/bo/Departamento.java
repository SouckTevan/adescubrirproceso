package co.com.adescubrir.model.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "departamento")
@EntityListeners(AuditingEntityListener.class)
public class Departamento implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "_departamento")
    @SequenceGenerator(name="_departamento", sequenceName = "departamento_seq", allocationSize = 1)
    @Column(name = "departamento_id", nullable = false, unique = true)
    private int id;

    @Column(name = "nombre", nullable = false, length = 50)
    private String nombre;

    @Column(name = "estado", nullable = false, length = 5)
    private String estado = Parametro.ESTADO_ACTIVO;

    @Column(name = "dane", length = 20)
    private String dane;

    @Column(name = "pais", nullable = false)
    private int pais_id;

    @Transient
    private Pais pais = new Pais();

    @Override
    public boolean equals(Object obj)
    {
        if (obj instanceof Departamento)
        {
            Departamento temp = (Departamento) obj;
            return getId() == temp.getId();
        }
        return false;
    }

    @Override
    public String toString()
    {
        return "" + getId();
    }

    @Override
    public int hashCode()
    {
        return getId();
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getEstado()
    {
        return estado;
    }

    public void setEstado(String estado)
    {
        this.estado = estado;
    }

    public String getDane()
    {
        return dane;
    }

    public void setDane(String dane)
    {
        this.dane = dane;
    }

    public Pais getPais()
    {
        return pais;
    }

    public void setPais(Pais pais)
    {
        this.pais = pais;
    }

    public int getPais_id() { return pais_id; }

    public void setPais_id(int pais_id) { this.pais_id = pais_id; }
}
