package co.com.adescubrir.model.bo.eventos;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import co.com.adescubrir.model.bo.security.Usuario;

@Entity
@Table(name = "contrato_documentos")
public class ContratoDocumentos {

    @Id
    @Column(name = "contrato_id", nullable = false, length = 10)
    private int contrato_id = 0;

    @Column(name = "documento_id", nullable = false, length = 10)
    private int documento_id = 0;

    @Column(name = "fechahora", nullable = false, length = 29)
    private Date fechahora = null;

    @Column(name = "observacion", length = 200)
    private String observacion = "";

    @Column(name = "rutaarchivo", length = 200)
    private String rutaarchivo = "";

    @Column(name = "nombrearchivo", length = 200)
    private String nombrearchivo = "";

    @Column(name = "usuario_id", nullable = false, length = 10)
    private int usuario_id = 0;

    @Transient
    Usuario usuario = new Usuario();

    public int getContrato_id() {
        return contrato_id;
    }

    public void setContrato_id(int contrato_id) {
        this.contrato_id = contrato_id;
    }

    public int getDocumento_id() {
        return documento_id;
    }

    public void setDocumento_id(int documento_id) {
        this.documento_id = documento_id;
    }

    public Date getFechahora() {
        return fechahora;
    }

    public void setFechahora(Date fechahora) {
        this.fechahora = fechahora;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getRutaarchivo() {
        return rutaarchivo;
    }

    public void setRutaarchivo(String rutaarchivo) {
        this.rutaarchivo = rutaarchivo;
    }

    public String getNombrearchivo() {
        return nombrearchivo;
    }

    public void setNombrearchivo(String nombrearchivo) {
        this.nombrearchivo = nombrearchivo;
    }

    public int getUsuario_id() {
        return usuario_id;
    }

    public void setUsuario_id(int usuario_id) {
        this.usuario_id = usuario_id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
