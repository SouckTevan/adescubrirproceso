package co.com.adescubrir.model.bo.eventos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import co.com.adescubrir.model.bo.Parametro;

@Entity
@Table(name = "tipoevento")
public class TipoEvento {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "_tipoevento")
    @SequenceGenerator(name = "_tipoevento",sequenceName = "tipoevento_seq",allocationSize = 1, initialValue = 1)
    @Column(name = "tipoevento_id", updatable = false, nullable = false)
    private int id;

    @Column(name = "nombre", length = 200)
    private String nombre;

    @Column(name = "estado", nullable = false, length = 5)
    private String estado = Parametro.ESTADO_ACTIVO;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
