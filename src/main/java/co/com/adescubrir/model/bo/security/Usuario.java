package co.com.adescubrir.model.bo.security;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import co.com.adescubrir.model.bo.Persona;
import lombok.Data;

@Entity
@Table(name = "usuario")
@Data
@PrimaryKeyJoinColumn(name = "id")
public class Usuario extends Persona {

    @Column(name = "tipousuario")
    private String tipoUsuario;

    @Transient
    private String socialUser;

    @Transient
    public static String LABEL_NAME = "Usuarios";

    @Transient
    private List<Rol> roles = new ArrayList<Rol>();

    @Transient
    private List<UsuarioOpcion> opciones = new ArrayList<UsuarioOpcion>();

    @Transient
    private List<Integer> contratos = new ArrayList<Integer>();

    @Transient
    private List<Integer> estados = new ArrayList<Integer>();

//    public List<UsuarioOpcion> getOpciones()
//    {
//        return opciones;
//    }
//
//    public void setOpciones(List<UsuarioOpcion> opciones)
//    {
//        this.opciones = opciones;
//    }
//
//    public List<Rol> getRoles()
//    {
//        return roles;
//    }
//
//    public void setRoles(List<Rol> roles)
//    {
//        this.roles = roles;
//    }
//
//    public String getTipoUsuario()
//    {
//        return tipoUsuario;
//    }
//
//    public void setTipoUsuario(String tipoUsuario)
//    {
//        this.tipoUsuario = tipoUsuario;
//    }
//
//    public List<Integer> getContratos()
//    {
//        return contratos;
//    }
//
//    public void setContratos(List<Integer> contratos)
//    {
//        this.contratos = contratos;
//    }
//
//    public List<Integer> getEstados()
//    {
//        return estados;
//    }
//
//    public void setEstados(List<Integer> estados)
//    {
//        this.estados = estados;
//    }
}
