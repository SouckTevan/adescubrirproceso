package co.com.adescubrir.model.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "tipoproveedor")
@EntityListeners(AuditingEntityListener.class)
public class TipoProveedor implements Serializable {

    @Transient
    public static String LABEL_NAME = "Tipos de Proveedor";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "_tipoproveedor")
    @SequenceGenerator(name = "_tipoproveedor",sequenceName = "tipoproveedor_seq", allocationSize = 1, initialValue = 1)
    @Column(name = "tipoproveedor_id", updatable = false, nullable = false)
    private int id;

    @Column(name = "nombre", nullable = false, length = 50)
    private String nombre;

    @Column(name = "estado", nullable = false, length = 5)
    private String estado = Parametro.ESTADO_ACTIVO;

    @Override
    public boolean equals(Object obj)
    {
        if (obj instanceof TipoProveedor)
        {
            return getId() == ((TipoProveedor) obj).getId();
        }
        return false;
    }

    @Override
    public String toString()
    {
        return "" + getId();
    }

    @Override
    public int hashCode()
    {
        return getId();
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getEstado()
    {
        return estado;
    }

    public void setEstado(String estado)
    {
        this.estado = estado;
    }
}
