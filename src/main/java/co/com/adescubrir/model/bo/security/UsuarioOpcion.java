package co.com.adescubrir.model.bo.security;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;


@Entity
@Table(name = "usuario_opcion")
@EntityListeners(AuditingEntityListener.class)
public class UsuarioOpcion implements Serializable {

    @Id
    @Column(name = "usuario_id", nullable = false)
    private Integer usuarioId;

    @Transient
    private Usuario usuario;

    @Id
    @Column(name = "opcion", nullable = false)
    private String opcionId;

    @Transient
    private Opcion opcion;

    public Integer getUsuarioId() {
        return usuarioId;
    }
    public void setUsuarioId(Integer usuarioId) {
        this.usuarioId = usuarioId;
    }
    public String getOpcionId() {
        return opcionId;
    }
    public void setOpcionId(String opcionId) {
        this.opcionId = opcionId;
    }
    public Usuario getUsuario() {
        return usuario;
    }
    public  Opcion getOpcion(){ return opcion;}
}
