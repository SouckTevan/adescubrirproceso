package co.com.adescubrir.model.bo.eventos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import co.com.adescubrir.model.bo.Concepto;
import co.com.adescubrir.model.bo.Municipio;
import co.com.adescubrir.model.bo.Parametro;
import co.com.adescubrir.model.bo.Persona;
import co.com.adescubrir.model.bo.RegistroDocumento;
import co.com.adescubrir.model.bo.RegistroObservacion;

@Entity
@Table(name = "contrato")
public class Contrato implements Serializable {

    @Id
    @Column(name = "contrato_id", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "_contrato")
    @SequenceGenerator(name="_contrato", sequenceName = "contrato_seq", allocationSize = 1)
    private int id;

    @Column(name = "estado")
    private String estado = Parametro.ESTADO_ACTIVO;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "numero")
    private String numero;

    @Column(name = "fecha_firma")
    private Date fechaFirma;

    @Transient
    private Persona contratante = new Persona();

    @Column(name = "contratante_id", nullable = false)
    private int contratante_id;

    @Column(name = "rutacontrato")
    private String rutaContrato;

    @Column(name = "valortotal")
    private double valorTotal;

    @Column(name = "fecha_inicial")
    private Date fechaInicial;

    @Column(name = "fecha_fin")
    private Date fechaFin;

    @Column(name = "posobreproveedor")
    private double poSobreProveedor;

    @Column(name = "nit_cia")
    private String nitCompania = "";

    @Column(name = "nombre_cia")
    private String nombreCompania = "";

    @Column(name = "direccion_cia")
    private String direccionCompania = "";

    @Column(name = "ciudad_cia")
    private String ciudadCompania = "";

    @Column(name = "cuenta_cia")
    private String cuentaCompania = "";

    @Column(name = "pointermediacion")
    private double poIntermediacion = 0;

    @Column(name = "val_presu_evento")
    private boolean validarPresupuestoEvento = false;

    @Column(name = "podescuento")
    private double poDescuento;

    @Column(name = "obs_factura")
    private String observacionesFactura = "";

    @Transient
    private List<RegistroObservacion> observaciones = new ArrayList<RegistroObservacion>();

    @Transient
    private List<Municipio> municipios = new ArrayList<Municipio>();

    @Transient
    private List<TarifaContrato> tarifas = new ArrayList<TarifaContrato>();

    @Transient
    private List<Concepto> conceptos = new ArrayList<Concepto>();

    @Transient
    private List<RegistroDocumento> documentos = new ArrayList<RegistroDocumento>();

    @Transient
    private List<String> parametros;

    @Override
    public boolean equals(Object obj)
    {
        if (obj instanceof Contrato)
        {
            return getId() == ((Contrato) obj).getId();
        }
        return false;
    }

    @Override
    public String toString()
    {
        return "" + getId();
    }

    @Override
    public int hashCode()
    {
        return getId();
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getNumero()
    {
        return numero;
    }

    public void setNumero(String numero)
    {
        this.numero = numero;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public Persona getContratante()
    {
        return contratante;
    }

    public void setContratante(Persona contratante)
    {
        this.contratante = contratante;
    }

    public String getRutaContrato()
    {
        return rutaContrato;
    }

    public void setRutaContrato(String rutaContrato)
    {
        this.rutaContrato = rutaContrato;
    }

    public String getEstado()
    {
        return estado;
    }

    public void setEstado(String estado)
    {
        this.estado = estado;
    }

    public Date getFechaFirma()
    {
        return fechaFirma;
    }

    public void setFechaFirma(Date fechaFirma)
    {
        this.fechaFirma = fechaFirma;
    }

    public int getContratante_id() {
        return contratante_id;
    }

    public void setContratante_id(int contratante_id) {
        this.contratante_id = contratante_id;
    }

    public List<TarifaContrato> getTarifas()
    {
        return tarifas;
    }

    public void setTarifas(List<TarifaContrato> tarifas)
    {
        this.tarifas = tarifas;
    }

    public List<Municipio> getMunicipios()
    {
        return municipios;
    }

    public void setMunicipios(List<Municipio> municipios)
    {
        this.municipios = municipios;
    }

    public List<Concepto> getConceptos()
    {
        return conceptos;
    }

    public void setConceptos(List<Concepto> conceptos)
    {
        this.conceptos = conceptos;
    }

    public double getValorTotal()
    {
        return valorTotal;
    }

    public void setValorTotal(double valorTotal)
    {
        this.valorTotal = valorTotal;
    }

    public Date getFechaInicial()
    {
        return fechaInicial;
    }

    public void setFechaInicial(Date fechaInicial)
    {
        this.fechaInicial = fechaInicial;
    }

    public Date getFechaFin()
    {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin)
    {
        this.fechaFin = fechaFin;
    }

    public List<RegistroObservacion> getObservaciones()
    {
        return observaciones;
    }

    public void setObservaciones(List<RegistroObservacion> observaciones)
    {
        this.observaciones = observaciones;
    }

    public double getPoSobreProveedor()
    {
        return poSobreProveedor;
    }

    public void setPoSobreProveedor(double poSobreProveedor)
    {
        this.poSobreProveedor = poSobreProveedor;
    }

    public String getNitCompania()
    {
        return nitCompania;
    }

    public void setNitCompania(String nitCompania)
    {
        this.nitCompania = nitCompania;
    }

    public String getNombreCompania()
    {
        return nombreCompania;
    }

    public void setNombreCompania(String nombreCompania)
    {
        this.nombreCompania = nombreCompania;
    }

    public String getDireccionCompania()
    {
        return direccionCompania;
    }

    public void setDireccionCompania(String direccionCompania)
    {
        this.direccionCompania = direccionCompania;
    }

    public String getCiudadCompania()
    {
        return ciudadCompania;
    }

    public void setCiudadCompania(String ciudadCompania)
    {
        this.ciudadCompania = ciudadCompania;
    }

    public String getCuentaCompania()
    {
        return cuentaCompania;
    }

    public void setCuentaCompania(String cuentaCompania)
    {
        this.cuentaCompania = cuentaCompania;
    }

    public double getPoIntermediacion()
    {
        return poIntermediacion;
    }

    public void setPoIntermediacion(double poIntermediacion)
    {
        this.poIntermediacion = poIntermediacion;
    }

    public List<RegistroDocumento> getDocumentos()
    {
        return documentos;
    }

    public void setDocumentos(List<RegistroDocumento> documentos)
    {
        this.documentos = documentos;
    }

    public boolean isValidarPresupuestoEvento()
    {
        return validarPresupuestoEvento;
    }

    public void setValidarPresupuestoEvento(boolean validarPresupuestoEvento)
    {
        this.validarPresupuestoEvento = validarPresupuestoEvento;
    }

    public double getPoDescuento()
    {
        return poDescuento;
    }

    public void setPoDescuento(double poDescuento)
    {
        this.poDescuento = poDescuento;
    }

    public String getObservacionesFactura()
    {
        return observacionesFactura;
    }

    public void setObservacionesFactura(String observacionesFactura)
    {
        this.observacionesFactura = observacionesFactura;
    }
}
