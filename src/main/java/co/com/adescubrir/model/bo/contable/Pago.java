package co.com.adescubrir.model.bo.contable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import co.com.adescubrir.model.bo.Banco;
import lombok.Data;

@Entity
@Table(name = "documentocontable_pago")
@Data
public class Pago implements Serializable {

    @Transient
    public static final String ENTRADA = "ENTRADA";

    @Transient
    public static final String SALIDA = "SALIDA";

    @Id
//    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "_documentocontable_pago")
//    @SequenceGenerator(name="_documentocontable_pago", sequenceName = "documentocontable_pago_seq", allocationSize = 1)
    @Column(name = "id", nullable = false, unique = true)
    private int id;

    @Column(name = "documentocontable_id")
    private int idDocumentoContable;

//    @Transient
//    private String tipoPago;

    @Column(name = "estado")
    private String estado;

    @Transient
    private MedioPago medioPago;

    @Column(name = "mediopago_id")
    private int idMedioPago;

    @Transient
    private Banco banco;

    @Column(name = "banco_id")
    private int idBanco;

    @Column(name = "cheque")
    private String cheque;

    @Column(name = "cuenta")
    private String cuenta;

    @Column(name = "franquicia")
    private String franquicia;

    @Column(name = "nrotransaccion")
    private String nroTransaccion;

    @Transient
    private double valor;

    @Column(name = "impuestos")
    private double impuestos;

    @Column(name = "total")
    private double total;

    @Column(name = "texto1")
    private String texto1;

    @Column(name = "texto2")
    private String texto2;

    @Column(name = "observaciones")
    private String observaciones;

    @Column(name = "soporte")
    private String soporte = "";
}
