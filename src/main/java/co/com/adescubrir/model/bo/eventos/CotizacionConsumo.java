package co.com.adescubrir.model.bo.eventos;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "cotizacion_consumos")
public class CotizacionConsumo implements Serializable {

    @Id
    @Column(name = "cotizacion_id")
    private int idCotizacion;

    @Id
    @Column(name = "concepto_id")
    private int idConcepto;

    @Id
    @Column(name = "fecha")
    private Date fecha;

    @Id
    @Column(name = "cantidad")
    private int cantidad;

    @Column(name = "valor")
    private double valor;

}
