package co.com.adescubrir.model.bo.contable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import co.com.adescubrir.model.bo.Parametro;
import co.com.adescubrir.model.bo.RangoNumeracion;
import lombok.Data;

@Entity
@Table(name = "tipodocumento_contable")
@Data
@Inheritance(strategy = InheritanceType.JOINED)
public class TipoDocumentoContable implements Serializable {

    @Transient
    public static String LABEL_NAME = "Tipos de Documento Contable";
    @Transient
    public static final String CXP = "CXP";
    @Transient
    public static final String CXC = "CXC";
    @Transient
    public static final String ESP = "ESP";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "_tipodocumento_contable")
    @SequenceGenerator(name="_tipodocumento_contable", sequenceName = "tipodocumento_contable_seq", allocationSize = 1)
    @Column(name = "tipodocumento_id", nullable = false, unique = true)
    private int id;

    @Column(name = "nombre", length = 50, nullable = false)
    private String nombre;

    @Column(name = "estado", length = 5, nullable = false)
    private String estado = Parametro.ESTADO_ACTIVO;

    @Transient
    private RangoNumeracion rangoNumeracion;

    @Column(name = "rango_id")
    private int rango_id;

    @Transient
    private String especial;

    @Column(name = "tipocuenta", length = 5, nullable = false)
    private String tipoCuenta;

    @Column(name = "numeracionmanual")
    private boolean numeracionManual = false;

    @Column(name = "numeroobligatorio")
    private boolean numeroObligatorio = false;

    @Override
    public boolean equals(Object obj)
    {
        if (obj instanceof TipoDocumentoContable)
        {
            return getId() == ((TipoDocumentoContable) obj).getId();
        }
        return false;
    }

    @Override
    public String toString()
    {
        return "" + getId();
    }

    @Override
    public int hashCode()
    {
        return getId();
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getEstado()
    {
        return estado;
    }

    public void setEstado(String estado)
    {
        this.estado = estado;
    }

    public RangoNumeracion getRangoNumeracion()
    {
        return rangoNumeracion;
    }

    public void setRangoNumeracion(RangoNumeracion rangoNumeracion)
    {
        this.rangoNumeracion = rangoNumeracion;
    }

    public String getEspecial()
    {
        return especial;
    }

    public void setEspecial(String especial)
    {
        this.especial = especial;
    }

    public String getTipoCuenta()
    {
        return tipoCuenta;
    }

    public void setTipoCuenta(String tipoCuenta)
    {
        this.tipoCuenta = tipoCuenta;
    }

    public boolean isNumeracionManual()
    {
        return numeracionManual;
    }

    public void setNumeracionManual(boolean numeracionManual)
    {
        this.numeracionManual = numeracionManual;
    }

    public boolean isNumeroObligatorio()
    {
        return numeroObligatorio;
    }

    public void setNumeroObligatorio(boolean numeroObligatorio)
    {
        this.numeroObligatorio = numeroObligatorio;
    }

}
