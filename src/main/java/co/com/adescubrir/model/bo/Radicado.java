package co.com.adescubrir.model.bo;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.web.multipart.MultipartFile;

import co.com.adescubrir.model.bo.security.Usuario;

@Entity
@Table(name = "radicado")
public class Radicado implements Serializable {

    @Id
    @Column(name = "radicado_id", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "_radicado")
    @SequenceGenerator(name="_radicado", sequenceName = "radicado_seq", allocationSize = 1)
    private long id;

    @Column(name = "fechahora", length = 29)
    private Timestamp fechaHora;

    @Column(name = "fecharecepcion", length = 29)
    private Timestamp fechaRecepcion;

    @Column(name = "recibe_id", length = 10)
    private int recibeId;

    @Column(name = "observaciones", nullable = false, length = 100)
    private String observaciones;

    @Column(name = "destino", length = 50)
    private String destino;

    @Column(name = "ubicacion", length = 50)
    private String ubicacion;

    @Column(name = "tipodocumental", length = 100)
    private String tipoDocumental;

    @Column(name = "personaresponsable_id", length = 10)
    private int personaResponsableId;

    @Column(name = "clave", length = 50)
    private String clave;

    @Column(name = "clave1", length = 50)
    private String clave1;

    @Column(name = "clave2", length = 50)
    private String clave2;

    @Transient
    private MultipartFile file;

    @Transient
    private Usuario recibe;

    @Transient
    private Persona personaResponsable;

    @Transient
    private List<RadicadoDocumentos> anexos = new ArrayList<RadicadoDocumentos>();

    @Transient
    private ArrayList<MultipartFile> files = new ArrayList<>();

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Timestamp getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Timestamp fechaHora) {
        this.fechaHora = fechaHora;
    }

    public Timestamp getFechaRecepcion() {
        return fechaRecepcion;
    }

    public void setFechaRecepcion(Timestamp fechaRecepcion) {
        this.fechaRecepcion = fechaRecepcion;
    }

    public int getRecibeId() {
        return recibeId;
    }

    public void setRecibeId(int recibeId) {
        this.recibeId = recibeId;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public String getTipoDocumental() {
        return tipoDocumental;
    }

    public void setTipoDocumental(String tipoDocumental) {
        this.tipoDocumental = tipoDocumental;
    }

    public int getPersonaResponsableId() {
        return personaResponsableId;
    }

    public void setPersonaResponsableId(int personaResponsableId) {
        this.personaResponsableId = personaResponsableId;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getClave1() {
        return clave1;
    }

    public void setClave1(String clave1) {
        this.clave1 = clave1;
    }

    public String getClave2() {
        return clave2;
    }

    public void setClave2(String clave2) {
        this.clave2 = clave2;
    }

    public Usuario getRecibe() {
        return recibe;
    }

    public void setRecibe(Usuario recibe) {
        this.recibe = recibe;
    }

    public Persona getPersonaResponsable() {
        return personaResponsable;
    }

    public void setPersonaResponsable(Persona personaResponsable) {
        this.personaResponsable = personaResponsable;
    }

    public List<RadicadoDocumentos> getAnexos() {
        return anexos;
    }

    public void setAnexos(List<RadicadoDocumentos> anexos) {
        this.anexos = anexos;
    }

    public ArrayList<MultipartFile> getFiles() {
        return files;
    }

    public void setFiles(ArrayList<MultipartFile> files) {
        this.files = files;
    }
}
