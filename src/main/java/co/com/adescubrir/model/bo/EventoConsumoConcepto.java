package co.com.adescubrir.model.bo;

import java.util.LinkedHashMap;
import java.util.Map;

public class EventoConsumoConcepto {

	private int conceptoId;
	private String nombreConcepto;
	private Map<String, Integer> cantidades = new LinkedHashMap<>();
	private int total;
	private double tarifa;
	private double totalTarifa;

	public int getConceptoId() {
		return conceptoId;
	}

	public void setConceptoId(int conceptoId) {
		this.conceptoId = conceptoId;
	}

	public String getNombreConcepto() {
		return nombreConcepto;
	}

	public void setNombreConcepto(String nombreConcepto) {
		this.nombreConcepto = nombreConcepto;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public Map<String, Integer> getCantidades() {
		return cantidades;
	}

	public void setCantidades(Map<String, Integer> cantidades) {
		this.cantidades = cantidades;
	}

	public double getTarifa() {
		return tarifa;
	}

	public void setTarifa(double tarifa) {
		this.tarifa = tarifa;
	}

	public double getTotalTarifa() {
		return totalTarifa;
	}

	public void setTotalTarifa(double totalTarifa) {
		this.totalTarifa = totalTarifa;
	}

}
