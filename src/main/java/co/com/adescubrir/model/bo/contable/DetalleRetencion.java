package co.com.adescubrir.model.bo.contable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Data;

@Entity
@Table(name = "documentocontable_retencion")
@Data
public class DetalleRetencion implements Serializable {

    @Id
    @Column(name = "documentocontable_id")
    private int documentocontable_id;

    @Transient
    private IndicadorRetencion indicador = new IndicadorRetencion();

    @Id
    @Column(name = "indicador_id")
    private int indicador_id;

    @Column(name = "base")
    private double base;

    @Column(name = "valor")
    private double valor;

    @Column(name = "porcentaje")
    private double porcentaje;

    public DetalleRetencion()
    {
    }

    public DetalleRetencion(String key)
    {
        IndicadorRetencion indicador = new IndicadorRetencion();
        indicador.setId((new Integer(key)).intValue());
        setIndicador(indicador);
    }

    public String getKey()
    {
        return toString();
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj instanceof DetalleRetencion)
        {
            return (getKey()).equals(((DetalleRetencion) obj).getKey());
        }
        return false;
    }

    @Override
    public String toString()
    {
        return (getIndicador() != null ? getIndicador().toString() : "0");
    }
}
