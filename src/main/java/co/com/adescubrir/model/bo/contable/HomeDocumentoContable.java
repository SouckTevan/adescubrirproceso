package co.com.adescubrir.model.bo.contable;

import co.com.adescubrir.model.bo.Persona;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class HomeDocumentoContable implements Serializable {
    private int id;
    private int idEvento;
    private String estadoEvento;
    private Date fechaInicio;
    private int idPersona;
    private Persona persona;
    private int total;
    private int valor;
    private int saldo;
    private Date fechaC;
    private int idCreador;
    private String usuario;
    private String estado;
    private boolean isAnticipoNuevo;
    private String numeroContrato;
    private String nombreContrato;
    private String nombreConcepto;
}
