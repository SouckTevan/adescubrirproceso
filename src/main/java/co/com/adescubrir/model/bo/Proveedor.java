package co.com.adescubrir.model.bo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "proveedor")
@PrimaryKeyJoinColumn(name = "id")
public class Proveedor extends Persona implements Serializable {

    @Column(name = "tipoproveedor_id")
    private int tipoProveedor_id;

    @Transient
    private TipoProveedor tipoProveedor = new TipoProveedor();

    @Transient
    private Collection<Tarifa> tarifas = new ArrayList<Tarifa>();

    @Transient
    private Collection<ProveedorTipo> tipos = new ArrayList<>();

    @Override
    public boolean equals(Object obj)
    {
        if (obj instanceof Proveedor)
        {
            Proveedor temp = (Proveedor) obj;
            return getId() == temp.getId();
        }
        return false;
    }

    @Override
    public String toString()
    {
        return "" + getId();
    }

    @Override
    public int hashCode()
    {
        return getId();
    }

    public Collection<Tarifa> getTarifas()
    {
        return tarifas;
    }

    public void setTarifas(Collection<Tarifa> tarifas)
    {
        this.tarifas = tarifas;
    }

    public TipoProveedor getTipoProveedor()
    {
        return tipoProveedor;
    }

    public void setTipoProveedor(TipoProveedor tipoProveedor)
    {
        this.tipoProveedor = tipoProveedor;
    }

    public int getTipoProveedor_id() {
        return tipoProveedor_id;
    }

    public void setTipoProveedor_id(int tipoProveedor_id) {
        this.tipoProveedor_id = tipoProveedor_id;
    }

    public Collection<ProveedorTipo> getTipos() {
        return tipos;
    }

    public void setTipos(Collection<ProveedorTipo> tipos) {
        this.tipos = tipos;
    }
}
