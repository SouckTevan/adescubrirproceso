package co.com.adescubrir.model.bo.contable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import co.com.adescubrir.model.bo.Persona;
import co.com.adescubrir.model.bo.Proveedor;
import co.com.adescubrir.model.bo.security.Usuario;
import lombok.Data;

@Entity
@Data
@Table(name = "anticipo")
public class Anticipo {

    @Transient
    public static final String PENDIENTE = "PENDIENTE";
    @Transient
    public static final String SOLICITADO = "SOLICITADO";
    @Transient
    public static final String PAGADO = "PAGADO";
    @Transient
    public static final String AUTORIZACION = "AUTORIZACION";
    @Transient
    public static final String AUTORIZADO = "AUTORIZADO";
    @Transient
    public static final String APROBADA = "APROBADA";
    @Transient
    public static final String AUDITORIA = "AUDITORIA";
    @Transient
    public static final String RECHAZADO = "RECHAZADO";
    @Transient
    public static final String CANCELADO = "CANCELADO";

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "_anticipo")
    @SequenceGenerator(name = "_anticipo", sequenceName = "anticipo_seq", allocationSize = 1)
    private int id;

    @Column(name = "beneficiario_id")
    private int idBeneficiario;

    @Transient
    private Persona beneficiario;

    @Column(name = "concepto")
    private String concepto;

    @Column(name = "valor")
    private int valor;

    @Column(name = "evento_id")
    private int idEvento;

    @Column(name = "fecha")
    private Date fecha;

    @Column(name = "fechavencimiento")
    private Date fechaVencimiento;

    @Column(name = "observacion")
    private String observacion;

    @Column(name = "prioridad")
    private String prioridad;

    @Transient
    private Usuario creador;

    @Column(name = "creador_id")
    private int idCreador;

    @Column(name = "estado")
    private String estado;

    @Column(name = "autorizador_id")
    private int idAutorizador;

    @Transient
    private TipoDocumentoContable tipoDocumento;

    @Transient
    private String tipoDocumentoTxt = "";

    @Column(name = "tipodocumento_id")
    private int tipodocumento_id;

    @Column(name = "fechaautorizado")
    private Date fechaAutorizado;

    @Transient
    private boolean isGenerar;

    @Transient
    private boolean isPagado;

    @Transient
    private boolean isViejo;

    @Transient
    private List<AnticipoPagos> pagos = new ArrayList<>();

    // ----------------------------------

    @Transient
    private int totalPagos;

    @Transient
    private double saldo;

    @Transient
    public double getSaldo() {
        double retorno = getValor() - getTotalPagos();
        return retorno;
    }

    @Transient
    public double getTotalPagos() {
        double retorno = 0;
        if (getPagos() != null && getPagos().size() > 0) {
            for (AnticipoPagos pago : getPagos()) {
                retorno += pago.getTotal();
            }
        }
        return retorno;
    }
}
