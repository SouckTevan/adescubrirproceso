package co.com.adescubrir.model.bo.contable;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import co.com.adescubrir.model.bo.Concepto;
import co.com.adescubrir.model.bo.eventos.ConsumoEjecucion;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "documentocontable_detalle")
@Data
@EqualsAndHashCode(callSuper=false)
public class DetalleDocumento implements Serializable {

    @Id
    @Column(name = "documentocontable_id", nullable = false, unique = true)
    private int documentocontable_id;

    @Id
    @Column(name = "concepto_id")
    private int concepto_id;

    @Transient
    private Concepto concepto = new Concepto();

    @Column(name = "cantidad")
    private int cantidad;

    @Column(name = "valor")
    private double valor;

    @Column(name = "base")
    private double base;

    @Column(name = "impuestos")
    private double impuestos;

    @Column(name = "poimpuestos")
    private double poImpuestos;

    @Column(name = "descuento")
    private double descuento;

    @Column(name = "total")
    private double total;

    @Column(name = "retencion")
    private double retencion;

    @Column(name = "poretencion")
    private double poRetencion;

    @Transient
    private Date fecha;

    @Transient
    private Date hora;

    @Transient
    private String key;

    @Transient
    private boolean aplicarDescuento;

    @Transient
    private double poDescuento;

    public DetalleDocumento()
    {
        // TODO Auto-generated constructor stub
    }

    public DetalleDocumento(String key)
    {
        Concepto concepto = new Concepto();
        concepto.setId((new Integer(key)).intValue());
        setConcepto(concepto);
    }

    @Transient
    public String getKey()
    {
        return toString();
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj instanceof DetalleDocumento)
        {
            return (getKey()).equals(((DetalleDocumento) obj).getKey());
        }
        return false;
    }

    @Override
    public String toString()
    {
        return (getConcepto() != null ? getConcepto().toString() : "0");
    }

    public Concepto getConcepto()
    {
        return concepto;
    }

    public void setConcepto(Concepto concepto)
    {
        this.concepto = concepto;
    }

    public int getCantidad()
    {
        return cantidad;
    }

    public void setCantidad(int cantidad)
    {
        this.cantidad = cantidad;
    }

    public double getValor()
    {
        return valor;
    }

    public void setValor(double valor)
    {
        this.valor = valor;
    }

    public double getBase()
    {
        return base;
    }

    public void setBase(double base)
    {
        this.base = base;
    }

    public double getDescuento()
    {
        return descuento;
    }

    public void setDescuento(double descuento)
    {
        this.descuento = descuento;
    }

    public double getTotal()
    {
        return total;
    }

    public void setTotal(double total)
    {
        this.total = total;
    }

    public double getImpuestos()
    {
        return impuestos;
    }

    public void setImpuestos(double impuestos)
    {
        this.impuestos = impuestos;
    }

    public double getRetencion()
    {
        return retencion;
    }

    public void setRetencion(double retencion)
    {
        this.retencion = retencion;
    }

    public double getPoImpuestos()
    {
        return poImpuestos;
    }

    public void setPoImpuestos(double poImpuestos)
    {
        this.poImpuestos = poImpuestos;
    }

    public double getPoRetencion()
    {
        return poRetencion;
    }

    public void setPoRetencion(double poRetencion)
    {
        this.poRetencion = poRetencion;
    }

    public Date getFecha()
    {
        return fecha;
    }

    public void setFecha(Date fecha)
    {
        this.fecha = fecha;
    }

    public Date getHora()
    {
        return hora;
    }

    public void setHora(Date hora)
    {
        this.hora = hora;
    }

}
