package co.com.adescubrir.model.bo;

import java.io.Serializable;
import java.util.StringTokenizer;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "persona_contacto")
@EntityListeners(AuditingEntityListener.class)
public class Contacto implements Serializable {

    @Id
    @Column(name = "persona_id", nullable = false)
    private int id;

    @Transient
    private TipoContacto tipoContacto = new TipoContacto();

    @Id
    @Column(name = "tipocontacto_id", nullable = false)
    private int tipoContacto_id;

    @Column(name = "nombre", length = 100)
    private String nombre;

    @Column(name = "direccion", length = 100)
    private String direccion;

    @Transient
    private Municipio municipio = new Municipio();

    @Id
    @Column(name = "municipio_id")
    private int municipio_id;

    @Column(name = "email", length = 100)
    private String email;

    @Column(name = "telefono", length = 20)
    private String telefono;

    @Column(name = "fax", length = 20)
    private String fax;

    @Column(name = "celular", length = 20)
    private String celular;

    @Transient
    private String key;

    public Contacto() {

    }

    public Contacto(String key) {
        StringTokenizer tokenizer = new StringTokenizer(key, "|");
        if (tokenizer.hasMoreElements()) {
            String token = (String) tokenizer.nextElement();
            TipoContacto tipoContacto = new TipoContacto();
            tipoContacto.setId((new Integer(token)).intValue());
            setTipoContacto(tipoContacto);
            token = (String) tokenizer.nextElement();
            Municipio municipio = new Municipio();
            municipio.setId((new Integer(token)).intValue());
            setMunicipio(municipio);
            token = (String) tokenizer.nextElement();
            setNombre(token);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Contacto) {
            return (getKey()).equals(((Contacto) obj).getKey());
        }
        return false;
    }

    @Override
    public String toString() {
        return getTipoContacto().toString() + "|" + getMunicipio().toString() + "|" + getNombre();
    }

    public String getKey() {
        return toString();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public Municipio getMunicipio() {
        return municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    public TipoContacto getTipoContacto() {
        return tipoContacto;
    }

    public void setTipoContacto(TipoContacto tipoContacto) {
        this.tipoContacto = tipoContacto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTipoContacto_id() { return tipoContacto_id; }

    public void setTipoContacto_id(int tipoContacto_id) { this.tipoContacto_id = tipoContacto_id; }

    public int getMunicipio_id() { return municipio_id; }

    public void setMunicipio_id(int municipio_id) { this.municipio_id = municipio_id; }
}
