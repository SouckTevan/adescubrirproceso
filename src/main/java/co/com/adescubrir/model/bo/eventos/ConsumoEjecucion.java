package co.com.adescubrir.model.bo.eventos;

import java.util.ArrayList;

import lombok.Data;
import lombok.EqualsAndHashCode;

@SuppressWarnings("serial")
@Data
@EqualsAndHashCode(callSuper=false)
public class ConsumoEjecucion extends TarifaEvento {

    private ArrayList<ConsumoEvento> consumos = new ArrayList<ConsumoEvento>();
    private ArrayList<ConsumoEvento> ejecucion = new ArrayList<ConsumoEvento>();
    private ArrayList<ConsumoEvento> diferencia = new ArrayList<ConsumoEvento>();
    private int totalConsumos;
    private int totalEjecucion;
    private int totalDiferencia;


    public void calcularTotalConsumos()
    {
        totalConsumos = 0;
        for (ConsumoEvento consumo : getConsumos())
        {
            totalConsumos += consumo.getCantidad();
        }
    }

    public void calcularTotalEjecucion()
    {
        totalEjecucion = 0;
        for (ConsumoEvento consumo : getEjecucion())
        {
            totalEjecucion += consumo.getCantidad();
        }
    }

    public void calcularTotalDiferencia()
    {
        totalDiferencia = getTotalConsumos() - getTotalEjecucion();
    }
}
