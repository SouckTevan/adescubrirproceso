package co.com.adescubrir.model.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "accion")
@Inheritance(strategy = InheritanceType.JOINED)
public class Accion implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "_accion")
    @SequenceGenerator(name="_accion", sequenceName = "accion_seq", allocationSize = 1)
    @Column(name = "accion_id", nullable = false, unique = true)
    private int id;

    @Column(name = "nombre", nullable = false, length = 50)
    private String nombre;

    @Column(name = "estado", nullable = false, length = 5)
    private String estado;

    @Transient
    private Estado estadoSiguiente;

    @Column(name = "estadosiguiente_id", nullable = false)
    private int estadosiguienteid;

    public int getId()
    {
        return id;
    }
    public void setId(int id)
    {
        this.id = id;
    }
    public String getNombre()
    {
        return nombre;
    }
    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }
    public String getEstado()
    {
        return estado;
    }
    public void setEstado(String estado)
    {
        this.estado = estado;
    }
    public Estado getEstadoSiguiente()
    {
        return estadoSiguiente;
    }
    public void setEstadoSiguiente(Estado estadoSiguiente)
    {
        this.estadoSiguiente = estadoSiguiente;
    }
}
