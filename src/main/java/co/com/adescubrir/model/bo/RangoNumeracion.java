package co.com.adescubrir.model.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "rangonumeracion")
@Data
@Inheritance(strategy = InheritanceType.JOINED)
public class RangoNumeracion implements Serializable {

    public static String LABEL_NAME = "Rangos de Numeración";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "_rangonumeracion")
    @SequenceGenerator(name="_rangonumeracion", sequenceName = "rangonumeracion_seq", allocationSize = 1)
    @Column(name = "rango_id", nullable = false, unique = true)
    private int id;

    @Column(name = "nombre", nullable = false, length = 50)
    private String nombre;

    @Column(name = "estado", length = 5, nullable = false)
    private String estado = Parametro.ESTADO_ACTIVO;

    @Column(name = "prefijo", length = 20)
    private String prefijo;

    @Column(name = "numeroinicial", nullable = false)
    private long numeroInicial;

    @Column(name = "numerofinal", nullable = false)
    private long numeroFinal;

    @Column(name = "numeroactual", nullable = false)
    private long numeroActual;

    @Column(name = "fechainicial", nullable = false)
    private Date fechaInicial;

    @Column(name = "fechafinal", nullable = false)
    private Date fechaFinal;

    @Column(name = "descripcion", length = 200)
    private String descripcion;

    @Column(name = "textolegal", length = 500)
    private String textoLegal;

    @Column(name = "mailVencimientos", length = 100)
    private String mailVencimientos;

    @Override
    public boolean equals(Object obj)
    {
        if (obj instanceof RangoNumeracion)
        {
            return getId() == ((RangoNumeracion) obj).getId();
        }
        return false;
    }

    @Override
    public String toString()
    {
        return "" + getId();
    }

    @Override
    public int hashCode()
    {
        return getId();
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getEstado()
    {
        return estado;
    }

    public void setEstado(String estado)
    {
        this.estado = estado;
    }

    public String getPrefijo()
    {
        return prefijo;
    }

    public void setPrefijo(String prefijo)
    {
        this.prefijo = prefijo;
    }

    public long getNumeroInicial()
    {
        return numeroInicial;
    }

    public void setNumeroInicial(long numeroInicial)
    {
        this.numeroInicial = numeroInicial;
    }

    public long getNumeroFinal()
    {
        return numeroFinal;
    }

    public void setNumeroFinal(long numeroFinal)
    {
        this.numeroFinal = numeroFinal;
    }

    public long getNumeroActual()
    {
        return numeroActual;
    }

    public void setNumeroActual(long numeroActual)
    {
        this.numeroActual = numeroActual;
    }

    public Date getFechaInicial()
    {
        return fechaInicial;
    }

    public void setFechaInicial(Date fechaInicial)
    {
        this.fechaInicial = fechaInicial;
    }

    public Date getFechaFinal()
    {
        return fechaFinal;
    }

    public void setFechaFinal(Date fechaFinal)
    {
        this.fechaFinal = fechaFinal;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public String getTextoLegal()
    {
        return textoLegal;
    }

    public void setTextoLegal(String textoLegal)
    {
        this.textoLegal = textoLegal;
    }

    public String getMailVencimientos()
    {
        return mailVencimientos;
    }

    public void setMailVencimientos(String mailVencimientos)
    {
        this.mailVencimientos = mailVencimientos;
    }
}
