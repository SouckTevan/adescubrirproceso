package co.com.adescubrir.model.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "activofijo")
@Inheritance(strategy = InheritanceType.JOINED)
public class ActivoFijo implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "_activofijo")
    @SequenceGenerator(name="_activofijo", sequenceName = "activofijo_seq", allocationSize = 1)
    @Column(name = "activofijo_id", nullable = false, unique = true)
    private int id;

    @Column(name = "codigo", nullable = false, length = 20)
    private String codigo = "";

    @Column(name = "nombre", nullable = false, length = 50)
    private String nombre = "";

    @Column(name = "tipoactivo", length = 30)
    private String tipoActivo = "";

    @Column(name = "descripcion", length = 500)
    private String descripcion = "";

    @Column(name = "datostecnicos", length = 500)
    private String datosTecnicos = "";

    @Column(name = "serial", length = 30)
    private String serial = "";

    @Column(name = "marca", length = 30)
    private String marca = "";

    @Column(name = "estado", nullable = false, length = 5)
    private String estado = "A";

    @Column(name = "usuarioactual", length = 80)
    private String usuarioActual = "";

    @Column(name = "deeventos")
    private boolean deEventos;

    @Override
    public boolean equals(Object obj)
    {
        if (obj instanceof ActivoFijo)
        {
            ActivoFijo temp = (ActivoFijo) obj;
            return temp.getId() == getId();
        }
        return false;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public String getDatosTecnicos()
    {
        return datosTecnicos;
    }

    public void setDatosTecnicos(String datosTecnicos)
    {
        this.datosTecnicos = datosTecnicos;
    }

    public String getSerial()
    {
        return serial;
    }

    public void setSerial(String serial)
    {
        this.serial = serial;
    }

    public String getMarca()
    {
        return marca;
    }

    public void setMarca(String marca)
    {
        this.marca = marca;
    }

    public String getCodigo()
    {
        return codigo;
    }

    public void setCodigo(String codigo)
    {
        this.codigo = codigo;
    }

    public void setTipoActivo(String tipoActivo)
    {
        this.tipoActivo = tipoActivo;
    }

    public String getTipoActivo()
    {
        return tipoActivo;
    }

    public String getEstado()
    {
        return estado;
    }

    public void setEstado(String estado)
    {
        this.estado = estado;
    }

    public String getUsuarioActual()
    {
        return usuarioActual;
    }

    public void setUsuarioActual(String usuarioActual)
    {
        this.usuarioActual = usuarioActual;
    }

    public boolean isDeEventos()
    {
        return deEventos;
    }

    public void setDeEventos(boolean deEventos)
    {
        this.deEventos = deEventos;
    }




}
