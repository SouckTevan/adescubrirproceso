package co.com.adescubrir.model.bo.eventos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import co.com.adescubrir.model.bo.Concepto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@SuppressWarnings("serial")
@Entity
@Data
@EqualsAndHashCode(callSuper=false)
@AllArgsConstructor
@Table(name = "evento_tarifas")
public class TarifaEvento implements Serializable {

    @Transient
    private Concepto concepto = new Concepto();

    @Id
    @Column(name = "evento_id")
    private int evento_id;

    @Id
    @Column(name = "concepto_id")
    private int concepto_id;

    @Column(name = "valor")
    private double valor;

    @Transient
    private String key;

    @Transient
    private int cantidad;

    @Column(name = "condescuento")
    private boolean conDescuento;

    public TarifaEvento()
    {
    }

    public TarifaEvento(String key)
    {
        Concepto concepto = new Concepto();
        concepto.setId((new Integer(key)).intValue());
        setConcepto(concepto);
    }

    public String getKey()
    {
        return toString();
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj instanceof TarifaEvento)
        {
//            return evento_id == ((TarifaEvento) obj).evento_id && concepto_id == ((TarifaEvento) obj).concepto_id;
            return (getKey()).equals(((TarifaEvento) obj).getKey());
        }
        return false;
    }

    @Override
    public String toString()
    {
        return (getConcepto() == null ? "0" : getConcepto().toString());
    }
}
