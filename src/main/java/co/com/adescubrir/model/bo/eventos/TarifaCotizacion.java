package co.com.adescubrir.model.bo.eventos;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import co.com.adescubrir.model.bo.Concepto;
import co.com.adescubrir.model.bo.EventoConsumoConcepto;
import lombok.Data;

@Data
@Table(name = "cotizacion_tarifas")
@Entity
public class TarifaCotizacion implements Serializable {

    @Transient
    private Concepto concepto = new Concepto();

    @Transient
    private Map<String, EventoConsumoConcepto> consumos = new LinkedHashMap<>();

    @Id
    @Column(name = "cotizacion_id")
    private int idCotizacion;

    @Id
    @Column(name = "concepto_id")
    private int idConcepto;

    @Column(name = "valor")
    private double valor;

    @Transient
    private double totalTarifa;

    @Transient
    private int totalCantidad;

    public double getTotalTarifa() {
        double retorno = 0;
        for (Map.Entry<String, EventoConsumoConcepto> consumo : getConsumos().entrySet())
        {
            retorno += consumo.getValue().getTotal();
        }
        retorno = retorno * getValor();
        return retorno;
    }

    public int getTotalCantidad()
    {
//        int retorno = 0;
//        for (Map.Entry<String, EventoConsumoConcepto> consumo : getConsumos().entrySet())
//        {
//            retorno += consumo.getValue().getTotal();
//        }
        return totalCantidad;
    }

//    @Transient
//    public List<Integer> getDiasConsumos()
//    {
//        ArrayList<Integer> retorno = new ArrayList<Integer>();
//        for (int i = 0; i < getConsumos().size(); i++)
//        {
//            retorno.add(new Integer(i));
//        }
//        return retorno;
//    }

//    public void llenarConsumos(ArrayList<EventoConsumoConcepto> consumos)
//    {
//        for (EventoConsumoConcepto consumoEvento : consumos)
//        {
//            if (getConcepto().getId() == consumoEvento.getConceptoId())
//            {
//                getConsumos().add(consumoEvento);
//            }
//        }
//    }

    public Map<String, EventoConsumoConcepto> getConsumos() {
        return consumos != null ? consumos : new LinkedHashMap<>();
    }

    public void setConsumos(Map<String, EventoConsumoConcepto> consumos) {
        this.consumos = consumos;
    }
}
