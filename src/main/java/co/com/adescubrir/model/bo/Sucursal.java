package co.com.adescubrir.model.bo;

import java.io.Serializable;
import java.util.StringTokenizer;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@SuppressWarnings("serial")
@Entity
@Table(name = "persona_sucursal")
@EntityListeners(AuditingEntityListener.class)
public class Sucursal implements Serializable {

	@Id
	@Column(name = "persona_id", nullable = false)
	private int id;

	@Transient
	private Municipio municipio = new Municipio();

	@Id
	@Column(name = "municipio_id", nullable = false)
	private int municipio_id;

	@Column(name = "nombre", nullable = false, length = 150)
	private String nombre;

	@Column(name = "direccion", length = 100)
	private String direccion;

	@Column(name = "telefono", length = 20)
	private String telefono;

	@Column(name = "fax", length = 20)
	private String fax;

	@Column(name = "email", length = 100)
	private String email;

	@Transient
	private CuentaBancaria cuentaBancaria = new CuentaBancaria();

	@Id
	@Column(name = "banco_id")
	private Integer banco_id;

	@Transient
	private String key;

	public Sucursal() {
	}

	public Sucursal(String key) {
		StringTokenizer tokenizer = new StringTokenizer(key, "|");
		if (tokenizer.hasMoreElements()) {
			String token = (String) tokenizer.nextElement();
			Municipio municipio = new Municipio();
			municipio.setId((new Integer(token)).intValue());
			setMunicipio(municipio);
			token = (String) tokenizer.nextElement();
			setNombre(token);
		}
	}

	public String getKey() {
		return toString();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Sucursal) {
			return (getKey()).equals(((Sucursal) obj).getKey());
		}
		return false;
	}

	@Override
	public String toString() {
		return (getMunicipio() != null ? getMunicipio().toString() : "0") + "|" + getNombre();
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public CuentaBancaria getCuentaBancaria() {
		return cuentaBancaria;
	}

	public void setCuentaBancaria(CuentaBancaria cuentaBancaria) {
		this.cuentaBancaria = cuentaBancaria;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getMunicipio_id() {
		return municipio_id;
	}

	public void setMunicipio_id(int municipio_id) {
		this.municipio_id = municipio_id;
	}

	public int getBanco_id() {
		if (banco_id == null) {
			return 0;
		}
		return banco_id;
	}

	public void setBanco_id(int banco_id) {
		this.banco_id = banco_id;
	}
}
