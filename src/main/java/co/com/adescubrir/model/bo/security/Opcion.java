package co.com.adescubrir.model.bo.security;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "opcion")
@EntityListeners(AuditingEntityListener.class)
public class Opcion implements Serializable {

    @Transient
    public static String LABEL_NAME = "Opciones";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "_opcion")
    @SequenceGenerator(name="_opcion", sequenceName = "opcion_seq", allocationSize = 1)
    @Column(name = "opcion_id", nullable = false, unique = true)
    private int id;

    @Column(name = "nombre")
    private String nombre;

    @Override
    public boolean equals(Object obj)
    {
        if (obj instanceof Opcion)
        {
            return getId() == ((Opcion) obj).getId();
        }
        return false;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
