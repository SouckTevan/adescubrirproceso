package co.com.adescubrir.model.bo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(value = { "usuarioModifica", "keyString" })
public abstract class ObjetoBD {

	private String usuarioModifica = "";

	public abstract String getKeyString();

	public String getUsuarioModifica() {
		return usuarioModifica;
	}

	public void setUsuarioModifica(String usuarioModifica) {
		this.usuarioModifica = usuarioModifica;
	}
}
