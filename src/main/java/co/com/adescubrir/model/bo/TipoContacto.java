package co.com.adescubrir.model.bo;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "tipocontacto")
@EntityListeners(AuditingEntityListener.class)
public class TipoContacto implements Serializable {

    @Transient
    public static String LABEL_NAME = "Tipos de Contacto";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "_tipoContacto")
    @SequenceGenerator(name="_tipoContacto", sequenceName = "tipocontacto_seq", allocationSize = 1)
    @Column(name = "tipocontacto_id", nullable = false, unique = true)
    private int id;

    @Column(name = "nombre", nullable = false, length = 50)
    private String nombre;

    @Column(name = "estado", nullable = false, length = 5)
    private String estado = Parametro.ESTADO_ACTIVO;

    @Override
    public boolean equals(Object obj)
    {
        if (obj instanceof TipoContacto)
        {
            return getId() == ((TipoContacto) obj).getId();
        }
        return false;
    }

    @Override
    public String toString()
    {
        return "" + getId();
    }

    @Override
    public int hashCode()
    {
        return getId();
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getEstado()
    {
        return estado;
    }

    public void setEstado(String estado)
    {
        this.estado = estado;
    }
}
