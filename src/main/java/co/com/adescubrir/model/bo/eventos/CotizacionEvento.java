package co.com.adescubrir.model.bo.eventos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import co.com.adescubrir.model.bo.EventoConsumoConcepto;
import co.com.adescubrir.model.bo.Proveedor;
import co.com.adescubrir.model.bo.Sucursal;
import co.com.adescubrir.model.bo.TipoTarifa;
import co.com.adescubrir.model.bo.contable.DocumentoContable;
import co.com.adescubrir.model.bo.security.Usuario;
import lombok.Data;

@Data
@Entity
@Table(name = "cotizacion_evento")
public class CotizacionEvento implements Serializable {
    @Transient
    public static final String INICIAL = "INICIAL";
    @Transient
    public static final String PRELIQUIDACION = "PRELIQUIDACION";
    @Transient
    public static final String ORDEN_SERVICIO = "ORDEN SERVICIO";
    @Transient
    public static final String FACTURADA = "FACTURADA";
    @Transient
    public static final String CANCELADA = "CANCELADA";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "_cotizacion")
    @SequenceGenerator(name = "_cotizacion", sequenceName = "cotizacion_seq", allocationSize = 1)
    @Column(name = "cotizacion_id")
    private int id;

    @Transient
    private Proveedor proveedor = new Proveedor();

    @Column(name = "proveedor_id")
    private int idProveedor;

    @Transient
    private Sucursal sucursal = new Sucursal();

    @Column(name = "sucursal_id")
    private int idSucursal;

    @Column(name = "nombresucursal")
    private String nombreSucursal;

    @Transient
    private TipoTarifa tipoTarifa = new TipoTarifa();

    @Column(name = "tipotarifa_id")
    private int idTipoTarifa;

    @Transient
    private double porcentajeIncremento;

    @Transient
    private List<TarifaCotizacion> tarifas = new ArrayList<>();

    @Transient
    private Map<String, EventoConsumoConcepto> consumos = new LinkedHashMap<>();

    @Column(name = "observaciones")
    private String observaciones;

    @Column(name = "estado")
    private String estado = INICIAL;

    @Transient
    private Usuario creador;

    @Column(name = "creador_id")
    private int idCreador;

    @Transient
    private DocumentoContable factura = new DocumentoContable();

    @Column(name = "documentocontable_id")
    private int idFactura;

    @Column(name = "fechaaprobacion")
    private Date fechaAprobacion;

    @Column(name = "pocomision")
    private double porcentajeComision;

    @Transient
    private double totalCotizacion;

    @Transient
    private double totalFactura;

    @Column(name = "podescuento")
    private double porcentajeDescuento;

    @Column(name = "tipo")
    private String tipo;

    @Column(name = "aprobado")
    private boolean aprobado = false;

    @Column(name = "fefacturaproveedor")
    private Date fechaFacturaProveedor;

//    public CotizacionEvento(int id) {
//        this.id = id;
//    }

    @Transient
    public double getTotalFactura()
    {
        if (factura != null)
        {
            return factura.getTotal();
        }
        return 0;
    }

//    @Transient
//    public void setConsumosTarifas(ArrayList<String> rangoFechas) throws ParseException {
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//
//        for (int i = 0; i < getTarifas().size(); i++)
//        {
//            TarifaCotizacion tarifa = getTarifas().get(i);
//
//            for (String diaConsumo : rangoFechas)
//            {
//                EventoConsumoConcepto consumoEvento = new EventoConsumoConcepto();
//                consumoEvento.setConceptoId(tarifa.getConcepto().getId());
//                consumoEvento.setFecha(sdf.parse(diaConsumo));
//                boolean index = getConsumos().containsKey(consumoEvento.getConcepto().getId());
//                if (index)
//                    consumoEvento.setCantidad(getConsumos().get(consumoEvento.getConcepto().getId()).getTotal();
//                getTarifas().get(i).getConsumos().add(consumoEvento);
//            }
//        }
//    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj instanceof CotizacionEvento)
        {
            CotizacionEvento temp = (CotizacionEvento) obj;
            return getId() == temp.getId();
        }
        return false;
    }

    @Override
    public String toString()
    {
        return "" + getId();
    }

    @Override
    public int hashCode()
    {
        return getId();
    }
}
