package co.com.adescubrir.model.bo.eventos;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import co.com.adescubrir.model.bo.Documento;
import co.com.adescubrir.model.bo.security.Usuario;

@Entity
@Table(name = "evento_documentos")
public class EventoDocumentos implements Serializable {

    @Id
    @Column(name = "evento_id", nullable = false, unique = true)
    private int idEvento;

    @Id
    @Column(name = "documento_id", nullable = false)
    private int idDocumento;

    @Id
    @Column(name = "fechahora", nullable = false)
    private Date fechaHora;

    @Transient
    private Documento documento;

    @Column(name = "usuario_id", nullable = false)
    private int idUsuario;

    @Transient
    private Usuario usuario;

    @Column(name = "filetype", length = 150)
    private String fileType;

    @Column(name = "rutaarchivo", length = 200)
    private String rutaArchivo;

    @Column(name = "nombrearchivo", length = 200)
    private String nombreArchivo;

    @Column(name = "observacion", length = 200)
    private String observacion;

    @Override
    public boolean equals(Object obj)
    {
        if (obj instanceof EventoDocumentos)
        {
            EventoDocumentos temp = (EventoDocumentos) obj;
            return getIdEvento() == temp.getIdEvento() && getIdDocumento() == temp.getIdDocumento();
        }
        return false;
    }

    public int getIdEvento() {
        return idEvento;
    }

    public void setIdEvento(int idEvento) {
        this.idEvento = idEvento;
    }

    public int getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(int idDocumento) {
        this.idDocumento = idDocumento;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    public Documento getDocumento() {
        return documento;
    }

    public void setDocumento(Documento documento) {
        this.documento = documento;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getRutaArchivo() {
        return rutaArchivo;
    }

    public void setRutaArchivo(String rutaArchivo) {
        this.rutaArchivo = rutaArchivo;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }
}
