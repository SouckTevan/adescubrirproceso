package co.com.adescubrir.model.bo.eventos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "evento_cotizacion")
public class EventoCotizacion implements Serializable {

    @Id
    @Column(name = "evento_id")
    private int idEvento;

    @Id
    @Column(name = "cotizacion_id")
    private int idCotizacion;
}
