package co.com.adescubrir.model.bo.contable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import co.com.adescubrir.model.bo.Parametro;

@Entity
@Table(name = "indicador_retencion")
@EntityListeners(AuditingEntityListener.class)
public class IndicadorRetencion implements Serializable {

    @Transient
    public static final String LABEL_NAME = "Indicadores de Retencion";

    @Transient
    public static final String RENTA = "RENTA";

    @Transient
    public static final String IVA = "IVA";

    @Transient
    public static final String ICA = "ICA";

    @Transient
    public static final String TIMBRE = "TIMBRE";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "_indicadorretencion")
    @SequenceGenerator(name = "_indicadorretencion",sequenceName = "indicadorretencion_seq",allocationSize = 1, initialValue = 1)
    @Column(name = "indicador_id", updatable = false, nullable = false)
    private int id;

    @Column(name = "nombre", nullable = false)
    private String nombre;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "estado", nullable = false)
    private String estado = Parametro.ESTADO_ACTIVO;

    @Column(name = "tiporetencion", nullable = false)
    private String tipoRetencion;

    @Column(name = "porcentaje", nullable = false)
    private double porcentaje;

    @Override
    public boolean equals(Object obj)
    {
        if (obj instanceof IndicadorRetencion)
        {
            return getId() == ((IndicadorRetencion) obj).getId();
        }
        return false;
    }

    @Override
    public String toString()
    {
        return "" + getId();
    }

    @Override
    public int hashCode()
    {
        return getId();
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public String getEstado()
    {
        return estado;
    }

    public void setEstado(String estado)
    {
        this.estado = estado;
    }

    public String getTipoRetencion()
    {
        return tipoRetencion;
    }

    public void setTipoRetencion(String tipoRetencion)
    {
        this.tipoRetencion = tipoRetencion;
    }

    public double getPorcentaje()
    {
        return porcentaje;
    }

    public void setPorcentaje(double porcentaje)
    {
        this.porcentaje = porcentaje;
    }
}
