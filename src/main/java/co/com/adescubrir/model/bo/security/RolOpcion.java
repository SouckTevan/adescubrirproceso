package co.com.adescubrir.model.bo.security;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "rol_opcion")
public class RolOpcion implements Serializable {

    @Id
    @Column(name = "rol_id", nullable = false, unique = true)
    private int rol_id;

    @Id
    @Column(name = "opcion")
    private String opcion;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RolOpcion rolOpcion = (RolOpcion) o;
        return rol_id == rolOpcion.rol_id &&
                Objects.equals(opcion, rolOpcion.opcion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(rol_id, opcion);
    }

    public int getRol_id() {
        return rol_id;
    }

    public void setRol_id(int rol_id) {
        this.rol_id = rol_id;
    }

    public String getOpcion() {
        return opcion;
    }

    public void setOpcion(String opcion) {
        this.opcion = opcion;
    }
}
