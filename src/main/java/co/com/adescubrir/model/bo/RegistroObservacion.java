package co.com.adescubrir.model.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import co.com.adescubrir.model.bo.security.Usuario;

@Entity
@Table(name = "contrato_observaciones")
public class RegistroObservacion implements Serializable {

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "_observaciones")
    @SequenceGenerator(name="_observaciones", sequenceName = "contrato_observaciones_seq", allocationSize = 1)
    private int id;

//    @Id
    @Column(name = "contrato_id", nullable = false, length = 10)
    private int contrato_id;

    @Column(name = "fechahora", nullable = false, length = 29)
    private Date fechaHora = new Date();

    @Transient
    private Usuario usuario = new Usuario();

    @Column(name = "observacion", nullable = false)
    private String observacion;

    @Column(name = "usuario_id", nullable = false, length = 10)
    private int usuario_id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getContrato_id() {
        return contrato_id;
    }

    public void setContrato_id(int contrato_id) {
        this.contrato_id = contrato_id;
    }

    public int getUsuario_id() {
        return usuario_id;
    }

    public void setUsuario_id(int usuario_id) {
        this.usuario_id = usuario_id;
    }

    public Date getFechaHora()
    {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora)
    {
        this.fechaHora = fechaHora;
    }

    public Usuario getUsuario()
    {
        return usuario;
    }

    public void setUsuario(Usuario usuario)
    {
        this.usuario = usuario;
    }

    public String getObservacion()
    {
        return observacion;
    }

    public void setObservacion(String observacion)
    {
        this.observacion = observacion;
    }

}
