package co.com.adescubrir.model.bo.eventos;

import java.io.Serializable;
import java.util.Date;
import java.util.StringTokenizer;

import co.com.adescubrir.model.bo.Concepto;
import co.com.mycommons.util.Formato;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@SuppressWarnings("serial")
public class ConsumoEvento implements Serializable {

    private Concepto concepto = new Concepto();
    private Date fecha;
    private int cantidad;
    private double valor;

    public ConsumoEvento(String key)
    {
        StringTokenizer tokenizer = new StringTokenizer(key, "|");
        if (tokenizer.hasMoreElements())
        {
            String token = (String) tokenizer.nextElement();
            Concepto concepto = new Concepto();
            concepto.setId((new Integer(token)).intValue());
            setConcepto(concepto);
            token = (String) tokenizer.nextElement();
            Date fecha = Formato.convetirFecha(token).getTime();
            setFecha(fecha);
        }
    }
}
