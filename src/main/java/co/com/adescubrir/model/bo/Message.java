package co.com.adescubrir.model.bo;

import java.util.HashMap;
import java.util.Map;

public class Message {
	private Map<String, Object> properties = new HashMap<String, Object>();
	private Map<String, String> headers = new HashMap<String, String>();
	private Map<String, Object> parameters = new HashMap<String, Object>();
	private Object body = "";

	public void setProperty(String key, Object value) {
		properties.put(key, value);
	}

	public Object getProperty(String key) {
		return properties.get(key);
	}

	public void setHeader(String key, String value) {
		headers.put(key, value);
	}

	public String getHeader(String key) {
		return headers.get(key);
	}

	public void setParametro(String key, Object value) {
		parameters.put(key, value);
	}

	public Object getParametro(String key) {
		return parameters.get(key);
	}

	public Map<String, Object> getProperties() {
		return properties;
	}

	public void setProperties(Map<String, Object> properties) {
		this.properties = properties;
	}

	public Map<String, String> getHeaders() {
		return headers;
	}

	public void setHeaders(Map<String, String> headers) {
		this.headers = headers;
	}

	public Map<String, Object> getParameters() {
		return parameters;
	}

	public void setParameters(Map<String, Object> parameters) {
		this.parameters = parameters;
	}

	public Object getBody() {
		return body;
	}

	public void setBody(Object body) {
		this.body = body;
	}

}
