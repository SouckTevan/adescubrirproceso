package co.com.adescubrir.model.bo.eventos;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import co.com.adescubrir.model.bo.security.Usuario;
import lombok.Data;

@Entity
@Data
@Table(name = "evento_facturacion")
public class ObservacionFacturacion {

    @Column(name = "evento_id", nullable = false, length = 10)
    private int evento_id;

    @Id
    @Column(name = "fechahora", nullable = false, length = 29)
    private Date fechahora = new Date();;

    @Column(name = "observacion", nullable = false)
    private String observacion;

    @Column(name = "usuario_id", nullable = false, length = 10)
    private int usuario_id;

    @Transient
    private Usuario usuario = new Usuario();
}
