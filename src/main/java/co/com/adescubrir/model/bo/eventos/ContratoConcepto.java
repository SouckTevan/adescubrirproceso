package co.com.adescubrir.model.bo.eventos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import co.com.adescubrir.model.bo.Concepto;

@Entity
@Table(name = "contrato_concepto")
public class ContratoConcepto implements Serializable {

    @Id
    @Column(name = "contrato_id", nullable = false, length = 10)
    private int contrato_id = 0;

    @Id
    @Column(name = "concepto_id", nullable = false, length = 10)
    private int concepto_id = 0;

    @Transient
    Concepto concepto = new Concepto();

    public int getContrato_id() {
        return contrato_id;
    }

    public void setContrato_id(int contrato_id) {
        this.contrato_id = contrato_id;
    }

    public int getConcepto_id() {
        return concepto_id;
    }

    public void setConcepto_id(int concepto_id) {
        this.concepto_id = concepto_id;
    }

    public Concepto getConcepto() {
        return concepto;
    }

    public void setConcepto(Concepto concepto) {
        this.concepto = concepto;
    }
}
