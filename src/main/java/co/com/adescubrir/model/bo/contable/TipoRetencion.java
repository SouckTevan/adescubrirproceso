package co.com.adescubrir.model.bo.contable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import co.com.adescubrir.model.bo.Parametro;
import lombok.Data;

@Data
public class TipoRetencion implements Serializable {

    @Transient
    public static String LABEL_NAME = "Tipos de Retención";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "_tipoRetencion")
    @SequenceGenerator(name="_tipoRetencion", sequenceName = "tipo_retencion_seq", allocationSize = 1)
    @Column(name = "id", nullable = false, unique = true)
    private int id;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "estado")
    private String estado = Parametro.ESTADO_ACTIVO;
}
