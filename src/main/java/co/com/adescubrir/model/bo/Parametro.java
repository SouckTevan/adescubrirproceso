package co.com.adescubrir.model.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "parametros")
@EntityListeners(AuditingEntityListener.class)
public class Parametro implements Serializable {

    @Transient
    public static final String TIPO_AUTENTICACION = "TIPO_AUTENTICACION";

    @Transient
    public static final String SKIN_THEME = "SKIN_THEME";

    @Transient
    public static final String LOGO_GRANDE_EMPRESA = "LOGO_GRANDE_EMPRESA";

    @Transient
    public static final String ESTADO_ACTIVO = "A";

    @Transient
    public static final String ESTADO_INACTIVO = "I";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "_parametro")
    @SequenceGenerator(name="_parametro", sequenceName = "parametro_seq", allocationSize = 1)
    @Column(name = "parametro_id", nullable = false, unique = true)
    private int id;

    @Column(name = "nombre", nullable = false, length = 100)
    private String nombre;

    @Transient
    private String descripcion;

    @Column(name = "estado", nullable = false, length = 5)
    private String estado = Parametro.ESTADO_ACTIVO;

    @Column(name = "valor", nullable = false, length = 255)
    private String valor;

    @Column(name = "fecha_inicio")
    private Date fechaInicio;

    @Column(name = "fecha_fin")
    private Date fechaFin;

    public Parametro()
    {

    }

    public Parametro(int id)
    {
        this.id = id;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj instanceof Parametro)
        {
            Parametro temp = (Parametro) obj;
            return getId() == temp.getId();
        }
        return false;
    }

    @Override
    public String toString()
    {
        return "" + getId();
    }

    @Override
    public int hashCode()
    {
        return getId();
    }

    public int getId()
    {
        return this.id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return this.nombre == null ? "" : this.nombre.trim();
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getValor()
    {
        return this.valor == null ? "" : this.valor.trim();
    }

    public void setValor(String valor)
    {
        this.valor = valor;
    }

    public Date getFechaInicio()
    {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio)
    {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin()
    {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin)
    {
        this.fechaFin = fechaFin;
    }

    public String getEstado()
    {
        return estado;
    }

    public void setEstado(String estado)
    {
        this.estado = estado;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }
}
