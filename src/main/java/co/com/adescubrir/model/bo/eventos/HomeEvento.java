package co.com.adescubrir.model.bo.eventos;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import co.com.mycommons.util.Formato;

public class HomeEvento {
    private int id;
    private int idContrato;
    private String nombreContrato;
    private String nombre;
    private String cdp;
    private String orden;
    private Date fechaInicioEvento;
    private Date fechaFinEvento;
    private boolean desdeDiaAnterior = false;
    private boolean hastaDiaSiguiente = false;
    private int idTipoEvento;
    private String nombreTipoEvento;
    private String tipoMunicipio;
    private String nombreMunicipio;
    private int nroAsistentes;
    private double totalContratado = 0;
    private double totalEjecutado = 0;
    private double totalFacturado = 0;
    private double totalProveedores = 0;
    private double totalGastos = 0;
    private String nombreEstado;
    private String nombreDepartamento;
    private String nombreCoordinador;
    private String nombreVereda;
    private String nombreResguardo;
    private String nombreComunidad;
    private String nombreKumpania;
    private String texto1;
    private String responsableActual;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdContrato() {
        return idContrato;
    }

    public void setIdContrato(int idContrato) {
        this.idContrato = idContrato;
    }

    public String getNombreContrato() {
        return nombreContrato;
    }

    public void setNombreContrato(String nombreContrato) {
        this.nombreContrato = nombreContrato;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCdp() {
        return cdp;
    }

    public void setCdp(String cdp) {
        this.cdp = cdp;
    }

    public String getOrden() {
        return orden;
    }

    public void setOrden(String orden) {
        this.orden = orden;
    }

    public Date getFechaInicioEvento() {
        return fechaInicioEvento;
    }

    public void setFechaInicioEvento(Date fechaInicioEvento) {
        this.fechaInicioEvento = fechaInicioEvento;
    }

    public Date getFechaFinEvento() {
        return fechaFinEvento;
    }

    public void setFechaFinEvento(Date fechaFinEvento) {
        this.fechaFinEvento = fechaFinEvento;
    }

    public boolean isDesdeDiaAnterior() {
        return desdeDiaAnterior;
    }

    public void setDesdeDiaAnterior(boolean desdeDiaAnterior) {
        this.desdeDiaAnterior = desdeDiaAnterior;
    }

    public boolean isHastaDiaSiguiente() {
        return hastaDiaSiguiente;
    }

    public void setHastaDiaSiguiente(boolean hastaDiaSiguiente) {
        this.hastaDiaSiguiente = hastaDiaSiguiente;
    }

    public int getIdTipoEvento() {
        return idTipoEvento;
    }

    public void setIdTipoEvento(int idTipoEvento) {
        this.idTipoEvento = idTipoEvento;
    }

    public String getNombreTipoEvento() {
        return nombreTipoEvento;
    }

    public void setNombreTipoEvento(String nombreTipoEvento) {
        this.nombreTipoEvento = nombreTipoEvento;
    }

    public String getTipoMunicipio() {
        return tipoMunicipio;
    }

    public void setTipoMunicipio(String tipoMunicipio) {
        this.tipoMunicipio = tipoMunicipio;
    }

    public String getNombreMunicipio() {
        return nombreMunicipio;
    }

    public void setNombreMunicipio(String nombreMunicipio) {
        this.nombreMunicipio = nombreMunicipio;
    }

    public int getNroAsistentes() {
        return nroAsistentes;
    }

    public void setNroAsistentes(int nroAsistentes) {
        this.nroAsistentes = nroAsistentes;
    }

    public double getTotalContratado() {
        return totalContratado;
    }

    public void setTotalContratado(double totalContratado) {
        this.totalContratado = totalContratado;
    }

    public double getTotalEjecutado() {
        return totalEjecutado;
    }

    public void setTotalEjecutado(double totalEjecutado) {
        this.totalEjecutado = totalEjecutado;
    }

    public double getTotalFacturado() {
        return totalFacturado;
    }

    public void setTotalFacturado(double totalFacturado) {
        this.totalFacturado = totalFacturado;
    }

    public double getTotalProveedores() {
        return totalProveedores;
    }

    public void setTotalProveedores(double totalProveedores) {
        this.totalProveedores = totalProveedores;
    }

    public double getTotalGastos() {
        return totalGastos;
    }

    public void setTotalGastos(double totalGastos) {
        this.totalGastos = totalGastos;
    }

    public String getNombreEstado() {
        return nombreEstado;
    }

    public void setNombreEstado(String nombreEstado) {
        this.nombreEstado = nombreEstado;
    }

    public String getNombreDepartamento() {
        return nombreDepartamento;
    }

    public void setNombreDepartamento(String nombreDepartamento) {
        this.nombreDepartamento = nombreDepartamento;
    }

    public String getNombreCoordinador() {
        return nombreCoordinador;
    }

    public void setNombreCoordinador(String nombreCoordinador) {
        this.nombreCoordinador = nombreCoordinador;
    }

    public String getTexto1() {
        return texto1;
    }

    public void setTexto1(String texto1) {
        this.texto1 = texto1;
    }

    public String getNombreVereda() {
        return nombreVereda;
    }

    public void setNombreVereda(String nombreVereda) {
        this.nombreVereda = nombreVereda;
    }

    public String getNombreResguardo() {
        return nombreResguardo;
    }

    public void setNombreResguardo(String nombreResguardo) {
        this.nombreResguardo = nombreResguardo;
    }

    public String getNombreComunidad() {
        return nombreComunidad;
    }

    public void setNombreComunidad(String nombreComunidad) {
        this.nombreComunidad = nombreComunidad;
    }

    public String getNombreKumpania() {
        return nombreKumpania;
    }

    public void setNombreKumpania(String nombreKumpania) {
        this.nombreKumpania = nombreKumpania;
    }

    public String getResponsableActual() {
        return responsableActual;
    }

    public void setResponsableActual(String responsableActual) {
        this.responsableActual = responsableActual;
    }

    public List<String> getRangoFechas()
    {
        if (getFechaInicioEvento() != null && getFechaFinEvento() != null)
        {
            Calendar inicio = Calendar.getInstance();
            inicio.setTime(getFechaInicioEvento());
            if (isDesdeDiaAnterior())
                inicio.add(Calendar.DATE, -1);
            Calendar fin = Calendar.getInstance();
            fin.setTime(getFechaFinEvento());
            if (isHastaDiaSiguiente())
                fin.add(Calendar.DATE, 1);
            return Formato.numeroDeFechas(inicio.getTime(), fin.getTime());
        }
        return new ArrayList<String>();
    }
}
