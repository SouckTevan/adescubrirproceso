package co.com.adescubrir.model.bo.eventos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;

import co.com.adescubrir.model.bo.Concepto;
import co.com.adescubrir.model.bo.EventoConsumoConcepto;
import co.com.adescubrir.model.bo.Municipio;
import co.com.adescubrir.model.bo.Persona;
import co.com.adescubrir.model.bo.TipoTarifa;
import co.com.adescubrir.model.bo.contable.Anticipo;
import co.com.adescubrir.model.bo.contable.DocumentoContable;
import co.com.adescubrir.model.bo.security.Usuario;
import co.com.mycommons.util.Formato;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

@SuppressWarnings("serial")
@Entity
@Data
@Table(name = "evento")
public class Evento implements Serializable {

	@Id
	@Column(name = "evento_id", nullable = false, unique = true)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "_evento")
	@SequenceGenerator(name = "_evento", sequenceName = "evento_seq", allocationSize = 1)
//	@GenericGenerator(name="evento_increment" , strategy="increment")
//	@GeneratedValue(generator="evento_increment")
	private int id;

	@Transient
	private Contrato contrato = new Contrato();

	@Column(name = "contrato_id")
	private int idContrato;

	@Column(name = "tipoevento_id")
	private int idTipoEvento;

	@Column(name = "nombre")
	private String nombre;

	@Column(name = "sede_id")
	private int idSede;

	@Transient
	private Municipio sede = new Municipio();

	@JsonFormat(pattern = "yyyy-MM-dd")
	@Column(name = "fecha_inicio_evento")
	private Date fechaInicioEvento;

	@Column(name = "hora_inicio_evento")
	private Date horaInicioEvento;

	@Column(name = "fecha_fin_evento")
	private Date fechaFinEvento;

	@Column(name = "hora_fin_evento")
	private Date horaFinEvento;

	@Column(name = "responsable_id")
	private int idResponsable;

	@Transient
	private int ano;

	@Transient
	private int anoEvento;

	@Transient
	private Usuario responsable = new Usuario();

	@Transient
	private Persona coordinador = new Persona();

	@Column(name = "coordinador_id")
	private int idCoordinador;

	@Column(name = "solicitante")
	private String solicitante;

	@Column(name = "numero_asistentes")
	private int numeroAsistentes;

	@Column(name = "desdediaanterior")
	private boolean desdeDiaAnterior;

	@Column(name = "hastadiasiguiente")
	private boolean hastaDiaSiguiente;

	@Column(name = "tipotarifa_id")
	private int idTipoTarifa = 1;

	@Transient
	private TipoTarifa tipoTarifa = new TipoTarifa();

	@Transient
	private List<CotizacionEvento> cotizaciones = new ArrayList<>();

	@Column(name = "formato_escarapela")
	private String formatoEscarapela;

	@Column(name = "resumenejecutivo")
	private String resumenEjecutivo = "";

	@Column(name = "tiporegistroingreso")
	private String tipoRegistroIngreso;

	@Column(name = "orden")
	private String orden;

	@Column(name = "fechasolicitud")
	private Date fechaSolicitud;

	@Column(name = "texto1")
	private String texto1;

	@Column(name = "texto2")
	private String texto2;

	@Column(name = "texto3")
	private String texto3;

	@Column(name = "programa")
	private String programa;

	@Column(name = "cdp")
	private String cdp;

	@Column(name = "centrocostos")
	private String centroCostos;

	@Column(name = "rubropresupuestal")
	private String rubroPresupuestal;

	@Column(name = "valorpresupuesto")
	private Float valorPresupuesto;

	@Column(name = "contactocliente")
	private String contactoCliente;

	@Column(name = "apoyologistico")
	private String apoyoLogistico;

	// @Column(name = "responsableactual_id")
	@Transient
	private int idResponsableActual;

	@Column(name = "lugardelevento")
	private String lugarDelEvento;

	@Column(name = "observacionescarpeta")
	private String observacionesCarpeta;

	@Column(name = "estado_id")
	private int idEstado = 1;

	@Column(name = "factura_id")
	private Integer idFactura;

	@Transient
	private DocumentoContable factura;

	@Column(name = "creador_id")
	private int idCreador;

	@Column(name = "lugar_id")
	private Integer idLugar;

	@Transient
	private CotizacionEvento lugar;

	// @Column(name = "sucursal_id")
	@Transient
	private int idSucursal;

	@Column(name = "fechacreacion")
	private Date fechaCreacion;

	@Column(name = "alertas")
	private String alertas = "";

	@Column(name = "conalerta")
	private boolean conAlerta = false;

	@Column(name = "tipomunicipio")
	private String tipoMunicipio = "";

	@Column(name = "tcontratado")
	private double totalContratado = 0;

	@Column(name = "tejecutado")
	private double totalEjecutado = 0;

	@Column(name = "tfacturado")
	private double totalFacturado = 0;

	@Column(name = "tproveedores")
	private double totalProveedores = 0;

	@Column(name = "tgastos")
	private double totalGastos = 0;

	@Column(name = "docequivalente")
	private String numeroDocEquivalente = "";

	@Column(name = "resumenconsumos")
	private String resumenConsumos = "";

	@Column(name = "observacionesconsumos")
	private String observacionesConsumos = "";

	@Transient
	private Municipio municipio;

	// ------ Campos Nuevos -----------
	@Column(name = "terpirmeromber")
	private String terPirmerNomber;

	@Column(name = "tersegundonomber")
	private String terSegundoNomber;

	@Column(name = "terpirmerapellido")
	private String terPirmerApellido;

	@Column(name = "tersegundoapellido")
	private String terSegundoApellido;

	@Column(name = "tertipoidentificacion")
	private String terTipoIdentificacion;

	@Column(name = "ternumeroidentificacion")
	private String terNumeroIdentificacion;

	@Column(name = "tercelularcontacto")
	private String terCelularContacto;

	@Column(name = "tercorreoinstitucional")
	private String terCorreoInstitucional;

	@Column(name = "nacpirmernomber")
	private String nacPirmerNomber;

	@Column(name = "nacsegundonomber")
	private String nacSegundoNomber;

	@Column(name = "nacpirmerapellido")
	private String nacPirmerApellido;

	@Column(name = "nacsegundoapellido")
	private String nacSegundoApellido;

	@Column(name = "nactipoidentificacion")
	private String nacTipoIdentificacion;

	@Column(name = "nacnumeroidentificacion")
	private String nacNumeroIdentificacion;

	@Column(name = "naccelularcontacto")
	private String nacCelularContacto;

	@Column(name = "naccorreoinstitucional")
	private String nacCorreoInstitucional;

	@Column(name = "tipocomunidad")
	private String tipoComunidad;

	@Column(name = "vereda")
	private Integer vereda;

	@Column(name = "resguardo")
	private Integer resguardo;

	@Column(name = "consejocom")
	private Integer consejoCom;

	@Column(name = "kumpania")
	private Integer kumpania;

	@Column(name = "etnicacom")
	private String etnicaCom;

	@Column(name = "marcopolitico")
	private String marcoPolitico;

	@Column(name = "observacionesmp")
	private String observacionesMP;

	@Column(name = "descripcionevento")
	private String descripcionEvento;

	@Column(name = "soporteanticipo")
	private String soporteAnticipo;

	@Column(name = "soporteexcedentes")
	private String soporteExcedentes;

	@Transient
	private List<EventoDocumentos> documentos = new ArrayList<>();

	@Transient
	private Map<String, EventoConsumoConcepto> eventoConsumos = new LinkedHashMap<>();

	@Transient
	private List<ConsumoEvento> consumos = new ArrayList<>();

	@Transient
	private List<String> rangoFechas = new ArrayList<>();

	@Transient
	private List<TarifaEvento> tarifasDelEvento = new ArrayList<>();

	@Transient
	private ArrayList<Map<String, String>> tarifasContrato = new ArrayList<>();

	@Transient
	private Map<String, EventoConsumoConcepto> eventoEjecucion = new LinkedHashMap<>();

	// @Transient
	// private List<EventoEjecucion> ejecucion = new ArrayList<EventoEjecucion>();

	@Transient
	private List<LegalizacionGasto> gastos = new ArrayList<>();

	@Transient
	private List<LegalizacionGasto> viaticos = new ArrayList<LegalizacionGasto>();

	@Transient
	private List<Anticipo> anticipos = new ArrayList<>();

	@Transient
	private List<EventoObservaciones> eventoObservacion = new ArrayList<>();

	@Transient
	private List<Bitacora> bitacora = new ArrayList<>();

	@Transient
	private List<ObservacionFacturacion> facturacion = new ArrayList<>();

	public void setRangoFechas() {
		if (getFechaInicioEvento() != null && getFechaFinEvento() != null) {
			Calendar inicio = Calendar.getInstance();
			inicio.setTime(getFechaInicioEvento());
			if (isDesdeDiaAnterior())
				inicio.add(Calendar.DATE, -1);
			Calendar fin = Calendar.getInstance();
			fin.setTime(getFechaFinEvento());
			if (isHastaDiaSiguiente())
				fin.add(Calendar.DATE, 1);
			rangoFechas = Formato.numeroDeFechas(inicio.getTime(), fin.getTime());
		} else {
			rangoFechas = new ArrayList<>();
		}
	}

	public List<String> getRangoFechas() {
		return rangoFechas;
	}

	@Transient
	public int getAnoEvento() {
		Calendar hoy = Calendar.getInstance();
		hoy.setTime(fechaInicioEvento);
		return hoy.get(Calendar.YEAR);
	}

	@Transient
	public int getAno() {
		Calendar time = Calendar.getInstance();
		time.setTime(getFechaInicioEvento());
		return time.get(Calendar.YEAR);
	}

	public int getIdFactura() {
		if (idFactura == null) {
			return 0;
		}
		return idFactura;
	}

	// public Collection<ConsumoEjecucion> getConsumoEjecucion()
	// {
	// ArrayList<ConsumoEjecucion> retorno = new ArrayList<ConsumoEjecucion>();
	// for (Concepto concepto : getConceptos())
	// {
	// ConsumoEjecucion consumo = new ConsumoEjecucion();
	// consumo.setConcepto(concepto);
	// List<String> fechas = getRangoFechas();
	// for (String diaConsumo : fechas)
	// {
	// ConsumoEvento consumoDia = new ConsumoEvento(concepto.getId() + "|" + diaConsumo);
	// consumoDia.setConcepto(concepto);
	// consumoDia.setFecha(Formato.convetirFecha(diaConsumo).getTime());
	// int indexConsumo = getConsumos().indexOf(consumoDia);
	//
	// if (indexConsumo > -1)
	// {
	// consumo.getConsumos().add(getConsumos().get(indexConsumo));
	// }
	// else
	// {
	// consumo.getConsumos().add(consumoDia);
	// }
	// consumoDia = new ConsumoEvento(concepto.getId() + "|" + diaConsumo);
	// consumoDia.setConcepto(concepto);
	// consumoDia.setFecha(Formato.convetirFecha(diaConsumo).getTime());
	// int indexEjecucion = getEjecucion().indexOf(consumoDia);
	// if (indexEjecucion > -1)
	// {
	// consumo.getEjecucion().add(getEjecucion().get(indexEjecucion));
	// }
	// else
	// {
	// consumo.getEjecucion().add(consumoDia);
	// }
	// consumo.setValor(getTarifaConcepto(concepto));
	// }
	// consumo.calcularTotalConsumos();
	// consumo.calcularTotalEjecucion();
	// retorno.add(consumo);
	// }
	// return retorno;
	// }

	// public Collection<Concepto> getConceptos()
	// {
	// ArrayList<Concepto> retorno = new ArrayList<Concepto>();
	// retorno.addAll(getConceptosDeConsumos());
	// for (TarifaEvento tarifaEvento : getTarifasEvento())
	// {
	// if (!retorno.contains(tarifaEvento.getConcepto()))
	// {
	// retorno.add(tarifaEvento.getConcepto());
	// }
	// }
	// Collections.sort(retorno);
	// return retorno;
	// }

	// public List<TarifaEvento> getTarifasEvento()
	// {
	// // TODO: falta agregar los conceptos tarifas si el evento las cambia
	// ArrayList<TarifaEvento> retorno = (ArrayList<TarifaEvento>) getTarifasContrato();
	// retorno.removeAll(getTarifasDelEvento());
	// retorno.addAll(getTarifasDelEvento());
	// return retorno;
	// }

	// public List<TarifaEvento> getTarifasContrato()
	// {
	// ArrayList<TarifaEvento> retorno = new ArrayList<TarifaEvento>();
	// for (TarifaContrato tarifa : getContrato().getTarifas())
	// {
	// if (tarifa.getAno() == getAno())
	// if (tarifa.getTipoTarifa().equals(getTipoTarifa()))
	// {
	// TarifaEvento tarifaE = new TarifaEvento();
	// tarifaE.setConcepto(tarifa.getConcepto());
	// tarifaE.setValor(tarifa.getValor());
	// if (!getTarifasDelEvento().contains(tarifaE))
	// retorno.add(tarifaE);
	// }
	// }
	// return retorno;
	// }

	// public double getTarifaConcepto(Concepto concepto)
	// {
	// for (TarifaEvento tarifa : getTarifasEvento())
	// {
	// if (tarifa.getConcepto().equals(concepto))
	// return tarifa.getValor();
	// }
	// return 0;
	// }
	public List<EventoConsumoConcepto> getConsumos(Concepto concepto) {
		ArrayList<EventoConsumoConcepto> retorno = new ArrayList<>();
		for (Map.Entry<String, EventoConsumoConcepto> consumo : getEventoConsumos().entrySet()) {
			if (consumo.getValue().getConceptoId() == concepto.getId())
				retorno.add(consumo.getValue());
		}
		return retorno;
	}

	// public List<Concepto> getConceptosDeConsumos()
	// {
	// ArrayList<Concepto> retorno = new ArrayList<Concepto>();
	// for (ConsumoEvento consumo : getConsumos())
	// {
	// if (!retorno.contains(consumo.getConcepto()))
	// retorno.add(consumo.getConcepto());
	// }
	// Collections.sort(retorno);
	// return retorno;
	// }

	public int getVereda() {
		if (vereda == null)
			return 0;
		return vereda;
	}

	public int getResguardo() {
		if (resguardo == null)
			return 0;
		return resguardo;
	}

	public int getConsejoCom() {
		if (consejoCom == null)
			return 0;
		return consejoCom;
	}

	public int getKumpania() {
		if (kumpania == null)
			return 0;
		return kumpania;
	}
	
	public int getIdLugar() {
		if (idLugar == null)
			return 0;
		return idLugar;
	}
	
}
