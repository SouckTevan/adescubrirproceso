package co.com.adescubrir.model.bo;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "proveedor_tipo")
public class ProveedorTipo implements Serializable {

    @Id
    @Column(name = "id_proveedor", nullable = false)
    private int idProveedor;

    @Id
    @Column(name = "id_tipo", nullable = false)
    private int idTipo;

    @Transient
    private TipoProveedor objTipo = null;
}
