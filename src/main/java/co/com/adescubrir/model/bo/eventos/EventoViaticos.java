package co.com.adescubrir.model.bo.eventos;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import co.com.adescubrir.model.bo.Persona;
import co.com.adescubrir.model.bo.security.Usuario;
import lombok.Data;
import lombok.EqualsAndHashCode;

@SuppressWarnings("serial")
@Entity
@Table(name = "evento_viaticos")
@Data
@EqualsAndHashCode(callSuper = false)
public class EventoViaticos implements Serializable {

    @Id
    @Column(name = "id")
    private int id;

    @Id
    @Column(name = "evento_id")
    private int idEvento;

    @Column(name = "tipo")
    private String tipo = "";

    @Transient
    private Persona persona = new Persona();

    @Column(name = "valor")
    private double valor = 0;

    @Column(name = "valor_evento")
    private double valorEvento = 0;

    @Column(name = "concepto")
    private String concepto = "";

    @Column(name = "identificacion")
    private String identificacion = "";

    @Column(name = "nombres")
    private String nombres = "";

    @Column(name = "apellidos")
    private String apellidos = "";

    @Transient
    private Usuario usuario;

    @Column(name = "usuario_id")
    private int idUsuario;

    @Column(name = "valor_transporte")
    private double valorTransporte;

    @Column(name = "valor_alojamiento")
    private double valorAlojamiento;

    @Column(name = "valor_alimentacion")
    private double valorAlimentacion;

    @Column(name = "terrestre")
    private boolean terrestre;

    @Column(name = "fluvial")
    private boolean fluvial;

    @Column(name = "ruta")
    private String ruta = "";

    @Column(name = "fecha")
    private Date fecha;

    @Column(name = "fecharegistro")
    private Date fechaRegistro;

    @Column(name = "documento_id")
    private long documentoId = 0;

    @Column(name = "docequivalente")
    private String numeroDocEquivalente = "";

    @Column(name = "direccion")
    private String direccion;

    @Column(name = "telefono")
    private String telefono;

    @Column(name = "retencionrenta")
    private double retencionRenta;

    @Column(name = "retencioniva")
    private double retencionIVA;

    @Transient
    private String nombreUsuario;

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof EventoViaticos)
        {
            EventoViaticos temp = (EventoViaticos) obj;
            return temp.id == id;
        }
        return false;
    }

    @Transient
    public String getNombre()
    {
        return nombres + " " + apellidos;
    }
}
