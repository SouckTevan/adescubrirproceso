package co.com.adescubrir.model.bo.contable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import co.com.adescubrir.model.bo.Municipio;
import co.com.adescubrir.model.bo.Persona;
import co.com.adescubrir.model.bo.Proveedor;
import co.com.adescubrir.model.bo.RangoNumeracion;
import co.com.adescubrir.model.bo.security.Usuario;
import co.com.mycommons.util.Formato;
import lombok.Data;

@SuppressWarnings("serial")
@Entity
@Table(name = "documentocontable")
@Data
//@Inheritance(strategy = InheritanceType.JOINED)
public class DocumentoContable implements Serializable {

	@Transient
	public static final String PENDIENTE = "PENDIENTE";
	@Transient
	public static final String SOLICITADO = "SOLICITADO";
	@Transient
	public static final String PAGADO = "PAGADO";
	@Transient
	public static final String AUTORIZACION = "AUTORIZACION";
	@Transient
	public static final String AUTORIZADO = "AUTORIZADO";
	@Transient
	public static final String APROBADA = "APROBADA";
	@Transient
	public static final String AUDITORIA = "AUDITORIA";
	@Transient
	public static final String RECHAZADO = "RECHAZADO";
	@Transient
	public static final String CANCELADO = "CANCELADO";
	@Transient
	public static final String CON_SALDO = "CON_SALDO";

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "_documentocontable")
	@SequenceGenerator(name = "_documentocontable", sequenceName = "documentocontable_seq", allocationSize = 1)
	@Column(name = "documentocontable_id", nullable = false, unique = true)
	private long id;

	@Column(name = "numero", length = 20)
	private String numero = "";

	@Transient
	private TipoDocumentoContable tipoDocumento = new TipoDocumentoContable();

	@Transient
	private String tipoDocumentoTxt = "";

	@Column(name = "tipodocumento_id")
	private int tipodocumento_id;

	@Transient
	private RangoNumeracion rangoNumeracion;

	@Column(name = "rangonumeracion")
	private Integer rangonumeracion;

	@Transient
	private int totalPagos;

	@Transient
	private double saldo;

	@Transient
	private Persona persona = new Persona();

	@Column(name = "persona_id")
	private Integer persona_id;

	@Column(name = "fecha")
	private Date fecha;

	@Column(name = "hora")
	private Date hora;

	@Transient
	private Usuario creador = new Usuario();

	@Column(name = "creador_id")
	private int creador_id;

	@Transient
	private Municipio municipio = new Municipio();

	@Column(name = "municipio_id")
	private Integer municipio_id;

	@Transient
	private String codigoMunicipio;

	@Column(name = "estado", length = 20)
	private String estado;

	@Column(name = "observaciones", length = 500)
	private String observaciones;

	@Transient
	private List<DetalleDocumento> detalle = new ArrayList<>();

	@Transient
	private List<Pago> pagos = new ArrayList<>();

	// Retenciones
	@Transient
	private List<DetalleRetencion> detalleRetencion = new ArrayList<>();
	// valores

	@Column(name = "base", nullable = false)
	private double base;

	@Column(name = "impuestos", nullable = false)
	private double impuestos;

	@Column(name = "total", nullable = false)
	private double total;

	@Column(name = "retenciones", nullable = false)
	private double retenciones;

	@Column(name = "descuentos", nullable = false)
	private double descuentos;

	@Column(name = "comisiones")
	private double comisiones;

	@Column(name = "fechavencimiento")
	private Date fechaVencimiento;

	@Column(name = "fechafactura")
	private Date fechaFactura;

	// Campos para eventos
	@Column(name = "evento_id")
	private Integer eventoId = 0;

	@Column(name = "conceptopago", length = 50)
	private String conceptoPago = "";

	@Column(name = "prioridad", length = 20)
	private String prioridad = "";

	@Transient
	private double poDescuento;

	@Column(name = "iva_comisiones")
	private double ivaComisiones;

	public DocumentoContable() {
	}

	public DocumentoContable(TipoDocumentoContable tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public void reiniciarTotales() {
		base = 0;
		impuestos = 0;
		total = 0;
		retenciones = 0;
		descuentos = 0;
	}

	public void reiniciarRetenciones() {
		retenciones = 0;
	}

	@Transient
	public void reiniciarDetalles() {
		base = 0;
		impuestos = 0;
		descuentos = 0;
	}

	@Transient
	public double getSaldo() {
		double retorno = getTotal() - getTotalPagos();
		return retorno;
	}

	@Transient
	public double getTotalPagos() {
		double retorno = 0;
		if (getPagos() != null && getPagos().size() > 0) {
			for (Pago pago : getPagos()) {
				retorno += pago.getTotal();
			}
		}
		return retorno;
	}

	public static void calcularTotal(DocumentoContable documento) {
		if (TipoDocumentoContable.CXP.equals(documento.getTipoDocumento().getTipoCuenta())) {
			documento.setTotal(documento.getBase() + documento.getImpuestos() - documento.getDescuentos() - documento.getRetenciones());
		}
		if (TipoDocumentoContable.CXC.equals(documento.getTipoDocumento().getTipoCuenta())) {
			documento.setTotal(documento.getBase() + documento.getComisiones() + documento.getImpuestos());
		}
	}

	public static void calcularValoresDetalle(DocumentoContable documento) {
		documento.reiniciarDetalles();
		for (DetalleDocumento detalle : documento.getDetalle()) {
			calcularValores(detalle, documento.getPoDescuento());
			documento.setBase(documento.getBase() + detalle.getBase());
			documento.setImpuestos(documento.getImpuestos() + detalle.getImpuestos());
			documento.setDescuentos(documento.getDescuentos() + detalle.getDescuento());
			documento.setTotal(documento.getTotal() + detalle.getTotal());
		}
		documento.setBase(Math.round(documento.getBase()));
		documento.setImpuestos(Math.round(documento.getImpuestos()));
		documento.setDescuentos(Math.round(documento.getDescuentos()));
		documento.setTotal(Math.round(documento.getTotal()));
		calcularTotal(documento);
	}

	public static DetalleDocumento calcularValores(DetalleDocumento detalle, double poDescuento) {
		detalle.setPoImpuestos(detalle.getConcepto().getPorcentajeIva());
		if (detalle.isAplicarDescuento()) {
			poDescuento = detalle.getPoDescuento();
		}
		if (detalle.getPoImpuestos() > 0) {
			detalle.setBase((detalle.getValor() * detalle.getCantidad()) / (1 + (detalle.getPoImpuestos() / 100)));
			if (detalle.getConcepto().isNoAplicaDescuento())
				detalle.setDescuento(0);
			else
				detalle.setDescuento(detalle.getBase() * (poDescuento / 100));
			detalle.setBase(detalle.getBase() - detalle.getDescuento());
			detalle.setImpuestos(detalle.getBase() * (detalle.getPoImpuestos() / 100));
			detalle.setTotal((detalle.getBase() + detalle.getImpuestos()));
		} else {
			detalle.setBase(detalle.getCantidad() * detalle.getValor());
			if (detalle.getConcepto().isNoAplicaDescuento())
				detalle.setDescuento(0);
			else
				detalle.setDescuento(detalle.getBase() * (poDescuento / 100));
			detalle.setBase(detalle.getBase() - detalle.getDescuento());
			detalle.setTotal(detalle.getBase());
		}
		// Calcular retenciones
		// Calcular Descuentos
		// Calcular Total
		detalle.setBase(Math.round(detalle.getBase()));
		detalle.setImpuestos(Math.round(detalle.getImpuestos()));
		detalle.setTotal(Math.round(detalle.getTotal()));
		detalle.setRetencion(Math.round(detalle.getRetencion()));
		detalle.setDescuento(Math.round(detalle.getDescuento()));
		return detalle;
	}

	@Transient
	public static void calcularComision(DocumentoContable documento, double poComision) {
		double intermediacion = (documento.getBase() + documento.getDescuentos()) * (poComision / 100);
		double ivaIntermediacion = intermediacion * 0.16;
		documento.setComisiones(Math.round(intermediacion));
		documento.setIvaComisiones(Math.round(ivaIntermediacion));
		documento.setImpuestos(documento.getImpuestos() + documento.getIvaComisiones());
	}

	@Transient
	public String getCodigoMunicipio() {
		return Formato.completar("" + municipio.getId(), "0", 5, false);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public TipoDocumentoContable getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(TipoDocumentoContable tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Date getHora() {
		return hora;
	}

	public void setHora(Date hora) {
		this.hora = hora;
	}

	public Usuario getCreador() {
		return creador;
	}

	public void setCreador(Usuario creador) {
		this.creador = creador;
	}

	public List<DetalleDocumento> getDetalle() {
		return detalle;
	}

	public void setDetalle(List<DetalleDocumento> detalle) {
		this.detalle = detalle;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public double getBase() {
		return base;
	}

	public void setBase(double base) {
		this.base = base;
	}

	public double getImpuestos() {
		return impuestos;
	}

	public void setImpuestos(double impuestos) {
		this.impuestos = impuestos;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public double getRetenciones() {
		return retenciones;
	}

	public void setRetenciones(double retenciones) {
		this.retenciones = retenciones;
	}

	public double getDescuentos() {
		return descuentos;
	}

	public void setDescuentos(double descuentos) {
		this.descuentos = descuentos;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public RangoNumeracion getRangoNumeracion() {
		return rangoNumeracion;
	}

	public void setRangoNumeracion(RangoNumeracion rangoNumeracion) {
		this.rangoNumeracion = rangoNumeracion;
	}

	public List<Pago> getPagos() {
		return pagos;
	}

	public void setPagos(List<Pago> pagos) {
		this.pagos = pagos;
	}

	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFechaFactura() {
		return fechaFactura;
	}

	public void setFechaFactura(Date fechaFactura) {
		this.fechaFactura = fechaFactura;
	}

	public void setEventoId(int eventoId) {
		this.eventoId = eventoId;
	}

	public List<DetalleRetencion> getDetalleRetencion() {
		return detalleRetencion;
	}

	public void setDetalleRetencion(List<DetalleRetencion> detalleRetencion) {
		this.detalleRetencion = detalleRetencion;
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public double getComisiones() {
		return comisiones;
	}

	public void setComisiones(double comisiones) {
		this.comisiones = comisiones;
	}

	public String getConceptoPago() {
		return conceptoPago;
	}

	public void setConceptoPago(String conceptoPago) {
		this.conceptoPago = conceptoPago;
	}

	public String getPrioridad() {
		return prioridad;
	}

	public void setPrioridad(String prioridad) {
		this.prioridad = prioridad;
	}

	public double getIvaComisiones() {
		return ivaComisiones;
	}

	public void setIvaComisiones(double ivaComisiones) {
		this.ivaComisiones = ivaComisiones;
	}

	@Transient
	public int getRangonumeracion() {
		if (rangonumeracion == null || rangonumeracion == 0)
			return -1;
		return rangonumeracion;
	}

	public void setRangonumeracion(int rangonumeracion) {
		this.rangonumeracion = rangonumeracion;
	}

	public int getPersona_id() {
		if (persona_id == null)
			return 0;
		return persona_id;
	}

	public int getMunicipio_id() {
		if (municipio_id == null)
			return 0;
		return municipio_id;
	}

	public int getEventoId() {
		if (eventoId == null)
			return 0;
		return eventoId;
	}
}
