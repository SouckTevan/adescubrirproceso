package co.com.adescubrir.model.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import co.com.mycommons.util.Formato;

@Entity
@Table(name = "municipio")
@EntityListeners(AuditingEntityListener.class)
public class Municipio implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "_municipio")
    @SequenceGenerator(name="_municipio", sequenceName = "municipio_seq", allocationSize = 1)
    @Column(name = "municipio_id", nullable = false, unique = true)
    private int id;

    @Column(name = "nombre", nullable = false, length = 50)
    private String nombre;

    @Column(name = "estado", nullable = false, length = 5)
    private String estado = Parametro.ESTADO_ACTIVO;

    @Transient
    private String codigo;

    @Column(name = "dane", length = 20)
    private String dane;

    @Transient
    private Departamento departamento = new Departamento();

    @Column(name = "departamento_id", nullable = false)
    private int departamento_id;

    @Override
    public boolean equals(Object obj)
    {
        if (obj instanceof Municipio)
        {
            Municipio temp = (Municipio) obj;
            return getId() == temp.getId();
        }
        return false;
    }

    @Override
    public String toString()
    {
        return "" + getId();
    }

    @Override
    public int hashCode()
    {
        return getId();
    }

    @Transient
    public String getCodigo()
    {
        return Formato.completar("" + getId(), "0", 5, false);
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getEstado()
    {
        return estado;
    }

    public void setEstado(String estado)
    {
        this.estado = estado;
    }

    public String getDane()
    {
        return dane;
    }

    public void setDane(String dane)
    {
        this.dane = dane;
    }

    public Departamento getDepartamento()
    {
        return departamento;
    }

    public void setDepartamento(Departamento departamento)
    {
        this.departamento = departamento;
    }

    public int getDepartamento_id() {
        return departamento_id;
    }

    public void setDepartamento_id(int departamento_id) {
        this.departamento_id = departamento_id;
    }
}
