package co.com.adescubrir.model.bo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

@Entity(name = "persona")
// @Table(name = "persona", schema = "public")
@Inheritance(strategy = InheritanceType.JOINED)
public class Persona implements Serializable {

	@Id
	@Column(name = "persona_id", nullable = false, unique = true)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "_persona")
	@SequenceGenerator(name = "_persona", sequenceName = "persona_seq", allocationSize = 1)
	private int id;

	@Column(name = "nombre", nullable = false, length = 200)
	private String nombre;

	@Column(name = "nombres", length = 100)
	private String nombres;

	@Column(name = "apellidos", length = 100)
	private String apellidos;

	@Column(name = "razonsocial", length = 150)
	private String razonSocial;

	@Column(name = "nombrecomercial", length = 150)
	private String nombreComercial = "";

	@Column(name = "estado", nullable = false, length = 5)
	private String estado = Parametro.ESTADO_ACTIVO;

	@Column(name = "tipo_identificacion", length = 5)
	private String tipoIdentificacion = "";

	@Column(name = "identificacion", length = 20)
	private String identificacion = "";

	// @Column(name = "expedicion_identificacion", length = 255)
	@Transient
	private String expedicionIdentificacion = "";

	// @Column(name = "fecha_nacimiento")
	@Transient
	private Date fechaNacimiento;

	@Column(name = "direccion", length = 100)
	private String direccion = "";

	@Transient
	private Municipio municipio = new Municipio();

	@Column(name = "municipio_id", nullable = true)
	private Integer municipio_id;

	@Column(name = "telefono", length = 20)
	private String telefono = "";

	@Column(name = "fax", length = 20)
	private String fax = "";

	@Column(name = "celular", length = 20)
	private String celular = "";

	@Column(name = "email", length = 100)
	private String email = "";

	// @Column(name = "foto", length = 255)
	@Transient
	private String foto = "";

	@Transient
	private List<Contacto> contactos = new ArrayList<Contacto>();

	@Transient
	private List<CuentaBancaria> cuentasBancarias = new ArrayList<CuentaBancaria>();

	@Transient
	private List<Sucursal> sucursales = new ArrayList<Sucursal>();

	// @Column(name = "tipo_persona", length = 255)
	@Transient
	private String tipoPersona;

	@Column(name = "grancontribuyente", nullable = false)
	private boolean granContribuyente;

	@Column(name = "regimencomun", nullable = false)
	private boolean regimenComun;

	@Column(name = "regimenespecial", nullable = false)
	private boolean regimenEspecial;

	@Column(name = "regimensimplificado", nullable = false)
	private boolean regimenSimplificado;

	@Column(name = "autoretenedorrenta", nullable = false)
	private boolean autoRetenedorRenta;

	@Column(name = "autoretenedoriva", nullable = false)
	private boolean autoRetenedorIVA;

	@Column(name = "autoretenedorica", nullable = false)
	private boolean autoRetenedorICA;

	/* Datos para la autenticacion */
	@Column(name = "usuario", length = 20)
	private String usuario;

	@Column(name = "clave", length = 20)
	private String clave;

	@Column(name = "apoyologistico")
	private boolean apoyoLogistico = false;

	@Transient
	private List<RegistroDocumento> documentos = new ArrayList<RegistroDocumento>();

	@Transient
	public Sucursal getSucursal(Sucursal sucursalKey) {
		int index = sucursales.indexOf(sucursalKey);
		if (index > -1) {
			return sucursales.get(index);
		}
		return null;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Persona) {
			Persona temp = (Persona) obj;
			return getId() == temp.getId();
		}
		return false;
	}

	@Override
	public String toString() {
		return "" + getId();
	}

	@Override
	public int hashCode() {
		return getId();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getNombre() {
		if (tipoIdentificacion != null) {
			if (tipoIdentificacion.equals("NIT"))
				return razonSocial;
			else if (nombres != null) {
				return nombres + " " + apellidos;
			}
		}
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTipoIdentificacion() {
		return tipoIdentificacion;
	}

	public void setTipoIdentificacion(String tipoIdentificacion) {
		this.tipoIdentificacion = tipoIdentificacion;
	}

	public String getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	public String getExpedicionIdentificacion() {
		return expedicionIdentificacion;
	}

	public void setExpedicionIdentificacion(String expedicionIdentificacion) {
		this.expedicionIdentificacion = expedicionIdentificacion;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public List<Contacto> getContactos() {
		return contactos;
	}

	public void setContactos(List<Contacto> contactos) {
		this.contactos = contactos;
	}

	public List<CuentaBancaria> getCuentasBancarias() {
		return cuentasBancarias;
	}

	public void setCuentasBancarias(List<CuentaBancaria> cuentasBancarias) {
		this.cuentasBancarias = cuentasBancarias;
	}

	public boolean isGranContribuyente() {
		return granContribuyente;
	}

	public void setGranContribuyente(boolean granContribuyente) {
		this.granContribuyente = granContribuyente;
	}

	public boolean isRegimenComun() {
		return regimenComun;
	}

	public void setRegimenComun(boolean regimenComun) {
		this.regimenComun = regimenComun;
	}

	public boolean isRegimenEspecial() {
		return regimenEspecial;
	}

	public void setRegimenEspecial(boolean regimenEspecial) {
		this.regimenEspecial = regimenEspecial;
	}

	public boolean isRegimenSimplificado() {
		return regimenSimplificado;
	}

	public void setRegimenSimplificado(boolean regimenSimplificado) {
		this.regimenSimplificado = regimenSimplificado;
	}

	public boolean isAutoRetenedorRenta() {
		return autoRetenedorRenta;
	}

	public void setAutoRetenedorRenta(boolean autoRetenedorRenta) {
		this.autoRetenedorRenta = autoRetenedorRenta;
	}

	public boolean isAutoRetenedorIVA() {
		return autoRetenedorIVA;
	}

	public void setAutoRetenedorIVA(boolean autoRetenedorIVA) {
		this.autoRetenedorIVA = autoRetenedorIVA;
	}

	public boolean isAutoRetenedorICA() {
		return autoRetenedorICA;
	}

	public void setAutoRetenedorICA(boolean autoRetenedorICA) {
		this.autoRetenedorICA = autoRetenedorICA;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getNombreComercial() {
		return nombreComercial;
	}

	public void setNombreComercial(String nombreComercial) {
		this.nombreComercial = nombreComercial;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public List<Sucursal> getSucursales() {
		return sucursales;
	}

	public void setSucursales(List<Sucursal> sucursales) {
		this.sucursales = sucursales;
	}

	public String getTipoPersona() {
		return tipoPersona;
	}

	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public List<RegistroDocumento> getDocumentos() {
		return documentos;
	}

	public void setDocumentos(List<RegistroDocumento> documentos) {
		this.documentos = documentos;
	}

	public boolean isApoyoLogistico() {
		return apoyoLogistico;
	}

	public void setApoyoLogistico(boolean apoyoLogistico) {
		this.apoyoLogistico = apoyoLogistico;
	}

	public int getMunicipio_id() {
		if (municipio_id == null)
			return 0;
		return municipio_id;
	}

	public void setMunicipio_id(int municipio_id) {
		this.municipio_id = municipio_id;
	}
}
