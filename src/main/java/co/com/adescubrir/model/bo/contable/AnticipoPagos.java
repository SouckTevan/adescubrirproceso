package co.com.adescubrir.model.bo.contable;

import co.com.adescubrir.model.bo.Banco;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "anticipo_pagos")
public class AnticipoPagos implements Serializable {

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "_anticipo_p")
    @SequenceGenerator(name = "_anticipo_p", sequenceName = "anticipo_pagos_seq", allocationSize = 1)
    private int id;

    @Column(name = "id_anticipo")
    private int idAnticipo;

    @Transient
    private Banco banco;

    @Column(name = "id_banco")
    private int idBanco;

    @Transient
    private MedioPago medioPago;

    @Column(name = "id_mediopago")
    private int idMedioPago;

    @Column(name = "estado", length = 20)
    private String estado;

    @Column(name = "observaciones", length = 500)
    private String observaciones;

    @Column(name = "cheque")
    private String cheque;

    @Column(name = "cuenta")
    private String cuenta;

    @Column(name = "franquicia")
    private String franquicia;

    @Column(name = "nrotransaccion")
    private String nroTransaccion;

    @Column(name = "texto1")
    private String texto1;

    @Column(name = "texto2")
    private String texto2;

    @Column(name = "impuestos")
    private int impuestos;

    @Column(name = "total")
    private int total;

    @Transient
    private double valor;

    @Column(name = "soporte")
    private String soporte;
}
