package co.com.adescubrir.model.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import co.com.adescubrir.model.bo.contable.IndicadorRetencion;
import co.com.mycommons.util.Formato;
import lombok.Data;

@Entity
@Data
@Table(name = "concepto")
@EntityListeners(AuditingEntityListener.class)
public class Concepto implements Serializable, Comparable<Concepto> {

    @Transient
    public static String LABEL_NAME = "Conceptos";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "_concepto")
    @SequenceGenerator(name = "_concepto",sequenceName = "concepto_seq",allocationSize = 1, initialValue = 1)
    @Column(name = "concepto_id", updatable = false, nullable = false)
    private int id;

    @Column(name = "nombre", nullable = false, length = 50)
    private String nombre;

    @Column(name = "descripcion", length = 200)
    private String descripcion;

    @Column(name = "estado", nullable = false, length = 5)
    private String estado = Parametro.ESTADO_ACTIVO;

    @Transient
    private TipoConcepto tipoConcepto;

    @Transient
    private String codigo;

    @Transient
    private String nombreConcepto;

    @Column(name = "tipoconcepto_id", nullable = false)
    private int tipoConcepto_id;

    @Column(name = "segurohotelero", nullable = false)
    private boolean seguroHotelero = false;

    @Column(name = "porcentajeiva")
    private double porcentajeIva = 0;

    @Transient
    private IndicadorRetencion indicador;

    @Column(name = "noaplicadescuento")
    private boolean noAplicaDescuento;

    @Column(name = "indicador_id")
    private Integer indicador_id;

    @Override
    public boolean equals(Object obj)
    {
        if (obj instanceof Concepto)
        {
            return getId() == ((Concepto) obj).getId();
        }
        return false;
    }

    @Override
    public String toString()
    {
        return "" + getId();
    }

    @Override
    public int hashCode()
    {
        return getId();
    }

    @Override
    public int compareTo(Concepto o)
    {
        return getNombre().compareTo(o.getNombre());
    }

    @Transient
    public String getCodigo()
    {
        return Formato.completar("" + getId(), "0", 3, false);
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public String getEstado()
    {
        return estado;
    }

    public void setEstado(String estado)
    {
        this.estado = estado;
    }

    public TipoConcepto getTipoConcepto()
    {
        return tipoConcepto;
    }

    public void setTipoConcepto(TipoConcepto tipoConcepto)
    {
        this.tipoConcepto = tipoConcepto;
    }

    public int getTipoConcepto_id() {
        return tipoConcepto_id;
    }

    public void setTipoConcepto_id(int tipoConcepto_id) {
        this.tipoConcepto_id = tipoConcepto_id;
    }

    public int getIndicador_id() {
		if (indicador_id == null)
			return 0;
        return indicador_id;
    }

    public void setIndicador_id(int indicador_id) {
        this.indicador_id = indicador_id;
    }

    public double getPorcentajeIva()
    {
        return porcentajeIva;
    }

    public void setPorcentajeIva(double porcentajeIva)
    {
        this.porcentajeIva = porcentajeIva;
    }

    public boolean isSeguroHotelero()
    {
        return seguroHotelero;
    }

    public void setSeguroHotelero(boolean seguroHotelero)
    {
        this.seguroHotelero = seguroHotelero;
    }

    public IndicadorRetencion getIndicador()
    {
        return indicador;
    }

    public void setIndicador(IndicadorRetencion indicador)
    {
        this.indicador = indicador;
    }

    public boolean isNoAplicaDescuento()
    {
        return noAplicaDescuento;
    }

    public void setNoAplicaDescuento(boolean noAplicaDescuento)
    {
        this.noAplicaDescuento = noAplicaDescuento;
    }    
}
