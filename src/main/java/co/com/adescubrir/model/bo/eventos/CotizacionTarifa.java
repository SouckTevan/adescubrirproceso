package co.com.adescubrir.model.bo.eventos;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Transient;

import co.com.adescubrir.model.bo.Proveedor;
import co.com.adescubrir.model.bo.Sucursal;
import co.com.adescubrir.model.bo.TipoTarifa;
import lombok.Data;

@Data
public class CotizacionTarifa {
    @Transient
    public static final String INICIAL = "INICIAL";
    @Transient
    public static final String ACEPTADA = "ACEPTADA";
    @Transient
    public static final String CANCELADA = "CANCELADA";

    private int id;

    @Transient
    private Proveedor proveedor;

    @Transient
    private Sucursal sucursal;

    @Transient
    private TipoTarifa tipoTarifa;

    @Transient
    private List<TarifaEvento> tarifas = new ArrayList<TarifaEvento>();

    @Transient
    private String estado = INICIAL;
}
