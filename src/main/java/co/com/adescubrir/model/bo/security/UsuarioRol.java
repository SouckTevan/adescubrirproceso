package co.com.adescubrir.model.bo.security;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "usuario_rol")
@EntityListeners(AuditingEntityListener.class)

public class UsuarioRol implements Serializable {

    @Id
    @Column(name = "usuario_id")
    private Integer usuarioId;

    @Transient
    private Usuario usuario;

    @Id
    @Column(name = "rol_id")
    private Integer rolId;

    @Transient
    private Rol rol;

    public Integer getUsuarioId() { return usuarioId;}
    public void setUsuarioId(Integer usuarioId) {this.usuarioId = usuarioId;}
    public Integer getRolId() {return rolId;}
    public void setRolId(Integer rolId) {this.rolId = rolId;}
    public Usuario getUsuario() {
        return usuario;
    }
    public Rol getRol(){return  rol;}

}
