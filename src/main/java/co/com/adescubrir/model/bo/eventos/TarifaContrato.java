package co.com.adescubrir.model.bo.eventos;

import java.io.Serializable;
import java.util.Calendar;
import java.util.StringTokenizer;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import co.com.adescubrir.model.bo.Concepto;
import co.com.adescubrir.model.bo.Municipio;
import co.com.adescubrir.model.bo.TipoTarifa;

@Entity
@Table(name = "contrato_tarifa")
public class TarifaContrato implements Serializable {

    @Transient
    private Municipio municipio = new Municipio();

    @Transient
    private TipoTarifa tipoTarifa = new TipoTarifa();

    @Transient
    private Concepto concepto = new Concepto();

//    @Id
//    @Column(name = "id", nullable = false, unique = true)
//    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "_id")
//    @SequenceGenerator(name="_id", sequenceName = "contrato_tarifa_seq", allocationSize = 1)
//    private int id;

    @Id
    @Column(name = "contrato_id", nullable = false, length = 10)
    private int contrato_id = 0;

    @Id
    @Column(name = "municipio_id", nullable = false, length = 10)
    private int municipio_id = 0;

    @Id
    @Column(name = "tipotarifa_id", nullable = false, length = 10)
    private int tipotarifa_id = 0;

    @Id
    @Column(name = "concepto_id", nullable = false, length = 10)
    private int concepto_id = 0;

    @Column(name = "valor", nullable = false, length = 17)
    private double valor = 0;

    @Column(name = "ano", nullable = false, length = 10)
    private int ano = Calendar.getInstance().get(Calendar.YEAR);

    public TarifaContrato(){ }

    public TarifaContrato(String key)
    {
        StringTokenizer tokenizer = new StringTokenizer(key, "|");
        if (tokenizer.hasMoreElements())
        {
            String token = (String) tokenizer.nextElement();
            Municipio municipio = new Municipio();
            municipio.setId((new Integer(token)).intValue());
            setMunicipio(municipio);
            token = (String) tokenizer.nextElement();
            TipoTarifa tipoTarifa = new TipoTarifa();
            tipoTarifa.setId((new Integer(token)).intValue());
            setTipoTarifa(tipoTarifa);
            token = (String) tokenizer.nextElement();
            Concepto concepto = new Concepto();
            concepto.setId((new Integer(token)).intValue());
            setConcepto(concepto);
            token = (String) tokenizer.nextElement();
            setAno(Integer.parseInt(token));
        }
    }

    public String getKey()
    {
        return toString();
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj instanceof TarifaContrato)
        {
            return (getKey()).equals(((TarifaContrato) obj).getKey());
        }
        return false;
    }

    @Override
    public String toString()
    {
        return (getMunicipio() == null ? "0" : getMunicipio().toString()) + "|" + (getTipoTarifa() == null ? "0" : getTipoTarifa().toString()) + "|"
                + (getConcepto() == null ? "0" : getConcepto().toString()) + "|" + getAno();
    }

//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }

    public Municipio getMunicipio()
    {
        return municipio;
    }

    public void setMunicipio(Municipio municipio)
    {
        this.municipio = municipio;
    }

    public Concepto getConcepto()
    {
        return concepto;
    }

    public void setConcepto(Concepto concepto)
    {
        this.concepto = concepto;
    }

    public int getContrato_id() {
        return contrato_id;
    }

    public void setContrato_id(int contrato_id) {
        this.contrato_id = contrato_id;
    }

    public int getMunicipio_id() {
        return municipio_id;
    }

    public void setMunicipio_id(int municipio_id) {
        this.municipio_id = municipio_id;
    }

    public int getTipotarifa_id() {
        return tipotarifa_id;
    }

    public void setTipotarifa_id(int tipotarifa_id) {
        this.tipotarifa_id = tipotarifa_id;
    }

    public int getConcepto_id() {
        return concepto_id;
    }

    public void setConcepto_id(int concepto_id) {
        this.concepto_id = concepto_id;
    }

    public double getValor()
    {
        return valor;
    }

    public void setValor(double valor)
    {
        this.valor = valor;
    }

    public TipoTarifa getTipoTarifa()
    {
        return tipoTarifa;
    }

    public void setTipoTarifa(TipoTarifa tipoTarifa)
    {
        this.tipoTarifa = tipoTarifa;
    }

    public int getAno()
    {
        return ano;
    }

    public void setAno(int ano)
    {
        this.ano = ano;
    }
}
