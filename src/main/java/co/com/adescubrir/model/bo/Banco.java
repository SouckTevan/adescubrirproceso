package co.com.adescubrir.model.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "banco")
@EntityListeners(AuditingEntityListener.class)
public class Banco implements Serializable {

    @Transient
    public static String LABEL_NAME = "Bancos";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "_banco")
    @SequenceGenerator(name="_banco", sequenceName = "banco_seq", allocationSize = 1)
    @Column(name = "banco_id", nullable = false, unique = true)
    private int id;

    @Column(name = "nombre", nullable = false, length = 50)
    private String nombre;

    @Column(name = "codigobancario", length = 5)
    private String codigoBancario;

    @Column(name = "estado", nullable = false, length = 5)
    private String estado = Parametro.ESTADO_ACTIVO;

    @Override
    public boolean equals(Object obj)
    {
        if (obj instanceof Banco)
        {
            return getId() == ((Banco) obj).getId();
        }
        return false;
    }

    @Override
    public String toString()
    {
        return "" + getId();
    }

    @Override
    public int hashCode()
    {
        return getId();
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getCodigoBancario()
    {
        return codigoBancario;
    }

    public void setCodigoBancario(String codigoBancario)
    {
        this.codigoBancario = codigoBancario;
    }

    public String getEstado()
    {
        return estado;
    }

    public void setEstado(String estado)
    {
        this.estado = estado;
    }
}
