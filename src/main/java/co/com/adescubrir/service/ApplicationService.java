package co.com.adescubrir.service;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import co.com.adescubrir.business.ServiceLocator;
import co.com.adescubrir.model.bo.Parametro;

public class ApplicationService {
    private static ArrayList<Parametro> parametros = new ArrayList<Parametro>();
    static
    {
        iniciarParametros();
    }

    public static void iniciarParametros()
    {
        ArrayList<String> properties = new ArrayList<>();
        Map<String, Object> propertyValues = new HashMap<>();
        properties.add("estado = :estado");
        propertyValues.put("estado", Parametro.ESTADO_ACTIVO);
        parametros = (ArrayList<Parametro>) ServiceLocator.getInstance().getCacheService().getByPropertiesNamed(Parametro.class.getName(), properties, propertyValues, "nombre");
    }

    public static String getFilesDirectory()
    {
        return getParametro("filesDirectory");
    }

    public static String getParametro(String key)
    {
        for (Parametro parametro : parametros)
        {
            if (parametro.getNombre().equals(key))
            {
                return parametro.getValor();
            }
        }
        return "";
    }

    public static String MD5(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException
    {
        MessageDigest md;
        md = MessageDigest.getInstance("MD5");
        byte[] md5hash = new byte[32];
        md.update(text.getBytes("iso-8859-1"), 0, text.length());
        md5hash = md.digest();
        return convertToHex(md5hash);
    }

    private static String convertToHex(byte[] data)
    {
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < data.length; i++)
        {
            int halfbyte = (data[i] >>> 4) & 0x0F;
            int two_halfs = 0;
            do
            {
                if ((0 <= halfbyte) && (halfbyte <= 9))
                    buf.append((char) ('0' + halfbyte));
                else
                    buf.append((char) ('a' + (halfbyte - 10)));
                halfbyte = data[i] & 0x0F;
            }
            while (two_halfs++ < 1);
        }
        return buf.toString();
    }
}
