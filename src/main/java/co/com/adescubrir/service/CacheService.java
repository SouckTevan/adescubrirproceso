package co.com.adescubrir.service;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import co.com.adescubrir.business.BaseService;
import co.com.adescubrir.business.SpringUtil;
import co.com.adescubrir.model.bo.Departamento;
import co.com.adescubrir.model.bo.Municipio;
import co.com.adescubrir.model.bo.Pais;
import co.com.adescubrir.model.bo.Parametro;
import co.com.adescubrir.model.bo.TipoProveedor;
import co.com.adescubrir.model.bo.eventos.Contrato;
import co.com.adescubrir.model.bo.eventos.TipoEvento;
import co.com.adescubrir.model.dao.BaseDAO;
import co.com.mycommons.jsf.components.JSFUtil;

public class CacheService extends BaseService {
    public static final String SERVICE_NAME = "cacheService";
    public static final String PAISES = Pais.class.getName();
    public static final String DEPARTAMENTOS = Departamento.class.getName();
    public static final String MUNICIPIOS = Municipio.class.getName();
    public static final String TIPOS_EVENTO = TipoEvento.class.getName();
//    public static final String TIPOS_INSCRIPCION = TipoInscripcion.class.getName();
    public static final String TIPOS_PROVEEDOR = TipoProveedor.class.getName();
    private static CacheService instance;
    private Map<String, Object> cache = new HashMap<String, Object>();
    private static Map<String, String[]> initializedProperties = new HashMap<String, String[]>();
    static
    {
        instance = getCacheServiceInstance();
        instance.setBaseDAO((BaseDAO) SpringUtil.getBean("baseDAO"));
        initializedProperties.put(Contrato.class.getName(), new String[] { "conceptos", "tarifas", "municipios" });
        initializedProperties.put(Municipio.class.getName(), new String[] { "departamento" });
        initializedProperties.put(Parametro.class.getName(), new String[] { "estado", "nombre", "valor" });
    }

    private static CacheService getCacheServiceInstance()
    {
        return new CacheService();
    }

    public static CacheService getInstance()
    {
        return instance;
    }

    public Collection<String> getKeys()
    {
        return cache.keySet();
    }

    public void refrescarCache()
    {
        cache.clear();
    }

    public void refrescarObjeto(String key)
    {
        cache.remove(key);
    }

    public Collection getObjetos(String key)
    {
        if (cache.containsKey(key))
        {
            return (Collection) cache.get(key);
        }
        if (initializedProperties.containsKey(key))
            cache.put(key, getDatos(key, initializedProperties.get(key)));
        else
            cache.put(key, getDatos(key));
        return (Collection) cache.get(key);
    }

    public Object getValue(String key, Object value)
    {
        int index = ((ArrayList) getObjetos(key)).indexOf(value);
        if (index != -1)
        {
            return ((ArrayList) getObjetos(key)).get(index);
        }
        else
        {
            return null;
        }
    }

    public Collection getDatos(String key)
    {
        try
        {
            Class clase = Class.forName(key);
            if (clase.getDeclaredField("nombre") != null)
            {
                return getByProperty(key, "", "", "nombre");
            }
        }
        catch (ClassNotFoundException e)
        {
        }
        catch (SecurityException e)
        {
        }
        catch (NoSuchFieldException e)
        {
        }
        return getByProperty(key, "", "", "");
    }

    public Collection getDatos(String key, String[] initializedProperties)
    {
        try
        {
            Class clase = Class.forName(key);
            if (clase.getDeclaredField("nombre") != null)
            {
                return getByProperty(key, "", "", "nombre", initializedProperties);
            }
        }
        catch (ClassNotFoundException e)
        {
        }
        catch (SecurityException e)
        {
        }
        catch (NoSuchFieldException e)
        {
        }
        return getByProperty(key, "", "", "", initializedProperties);
    }

    public Collection getDatosFilteredByProperty(Object objeto, String property, Object propertyValue)
    {
        return getDatosFilteredByProperty(objeto.getClass().getName(), objeto.getClass(), property, propertyValue);
    }

    public Collection getDatosFilteredByProperty(String key, Class claseObjeto, String property, Object propertyValue)
    {
        ArrayList retorno = new ArrayList();
        for (Object item : getObjetos(key))
        {
            Object itemPropertyValue;
            try
            {
                itemPropertyValue = claseObjeto.getMethod("get" + JSFUtil.primeraMayuscula(property), new Class[0]).invoke(item, new Object[0]);
                if (itemPropertyValue.equals(propertyValue))
                {
                    retorno.add(item);
                }
            }
            catch (IllegalArgumentException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (IllegalAccessException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (InvocationTargetException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (NoSuchMethodException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return retorno;
    }
}
