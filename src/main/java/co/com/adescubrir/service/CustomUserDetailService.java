package co.com.adescubrir.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import co.com.adescubrir.controller.AdministracionServiceRest;
import co.com.adescubrir.model.bo.Persona;

@Service
public class CustomUserDetailService implements UserDetailsService {

    @Autowired
    private AdministracionServiceRest admin;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Persona user = admin.getPersonaByUsername(username);
        return  new org.springframework.security.core.userdetails.User(user.getUsuario(),user.getClave(),new ArrayList<>());
    }
}
