package co.com.adescubrir.business;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;


@SuppressWarnings("rawtypes")
public interface IBaseService {
	public static final String SERVICE_NAME = "baseService";

	public abstract Collection getByProperties(String entityName, String properties[], Object propertiesValues[],
                                               String orderBy);

	public abstract Object saveUpdate(Object objeto) throws Exception;

	void saveUpdateAll(Collection objetos) throws Exception;

	void deleteAll(Collection entities) throws Exception;

//	public abstract Collection getByProperty(String entityName, String property, Object propertyValue, String orderBy);

	public abstract Collection getByProperty(String entityName, String property, Object propertyValue, String orderBy, String[] initializedProterties);

	void delete(Object objeto) throws Exception;
	
	List executeSQLQuery(String sqlQuery);

	Collection getByPropertiesNamed(String entityName, ArrayList<String> properties, Map<String, Object> propertyValues, String orderBy);

	public abstract Collection getByProperty(String entityName, String property, Object propertyValue, String orderBy);

	Object getById(String entityName, int id, String[] initializedProterties);

	List<Map<String, Object>> getByPropertiesNamedNativeQuery(String select, String strQuery, Map<String, Object> propertyValues);

	List<Map<String, Object>> getByPropertiesNamedNativeQuery(String tabla, String select, String strQuery, Map<String, Object> propertyValues);
}