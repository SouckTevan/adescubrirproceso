package co.com.adescubrir.business;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import co.com.adescubrir.model.dao.BaseDAO;

@SuppressWarnings({ "rawtypes" })
public class BaseService implements IBaseService {
	
	@Autowired
	protected BaseDAO baseDAO;

	public BaseDAO getBaseDAO() {
		return baseDAO;
	}

	public void setBaseDAO(BaseDAO baseDAO) {
		this.baseDAO = baseDAO;
	}

	@Override
	public Object saveUpdate(Object objeto) throws Exception {
		return baseDAO.saveOrUpdate(objeto);
	}

	@Override
	public void saveUpdateAll(Collection objetos) throws Exception {
		if (!objetos.isEmpty()) {
			baseDAO.saveOrUpdateAll(objetos);
		}
	}

	@Override
	public Collection getByProperty(String entityName, String property, Object propertyValue, String orderBy)
	{
		return (Collection) baseDAO.getByProperty(entityName, property, propertyValue, orderBy);
	}

	@Override
	public Collection getByProperty(String entityName, String property, Object propertyValue, String orderBy, String[] initializedProterties)
	{
		return (Collection) baseDAO.getByProperty(entityName, property, propertyValue, orderBy, initializedProterties);
	}

	@Override
	public Collection getByProperties(String entityName, String[] properties, Object[] propertiesValues,
									  String orderBy) {
		return (Collection) baseDAO.getByProperties(entityName, properties, propertiesValues, orderBy);
	}

	@Override
	public Collection getByPropertiesNamed(String entityName, ArrayList<String> properties,
										   Map<String, Object> propertyValues, String orderBy) {
		return (Collection) baseDAO.getByPropertiesNamed(entityName, properties, propertyValues, orderBy);
	}

	@Override
	public Object getById(String entityName, int id, String[] initializedProterties) {
		return baseDAO.getById(entityName, id, initializedProterties);
	}

	@Override
	public void deleteAll(Collection entities) throws Exception {
		baseDAO.deleteAll(entities);
	}

	@Override
	public void delete(Object objeto) throws Exception {
		baseDAO.delete(objeto);
	}

	@Override
	public List executeSQLQuery(String sqlQuery) {
		return baseDAO.executeSQLQuery(sqlQuery);
	}
	
	@Override
	public List<Map<String, Object>> getByPropertiesNamedNativeQuery(String select, String strQuery, Map<String, Object> propertyValues) {
		return baseDAO.getByPropertiesNamedNativeQuery(select, strQuery, propertyValues);
	}
	
	@Override
	public List<Map<String, Object>> getByPropertiesNamedNativeQuery(String tabla, String select, String strQuery, Map<String, Object> propertyValues) {
		return baseDAO.getByPropertiesNamedNativeQuery(tabla, select, strQuery, propertyValues);
	}

}
