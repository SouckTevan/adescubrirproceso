package co.com.adescubrir.business;

import co.com.adescubrir.controller.AdministracionServiceRest;
import co.com.adescubrir.service.CacheService;

public class ServiceLocatorSpring extends ServiceLocator
{
    @Override
    public IBaseService getBaseService()
    {
        return (IBaseService) SpringUtil.getBean(IBaseService.SERVICE_NAME);
    }

    @Override
    public AdministracionServiceRest getAdministracionService() {
        return null;
    }

    @Override
    public CacheService getCacheService() {
        return CacheService.getInstance();
    }
}
