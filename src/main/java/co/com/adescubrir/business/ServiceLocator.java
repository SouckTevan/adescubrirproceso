package co.com.adescubrir.business;

import co.com.adescubrir.controller.AdministracionServiceRest;
import co.com.adescubrir.service.CacheService;

public abstract class ServiceLocator {

	private static ServiceLocator instance;
	static {
		instance = getServiceLocator();
	}

	private static ServiceLocator getServiceLocator() {
		 return (ServiceLocator) SpringUtil.getBean("serviceLocator");
	}

	public static ServiceLocator getInstance() {
		return instance;
	}

	public abstract IBaseService getBaseService();

	public abstract AdministracionServiceRest getAdministracionService();

	public abstract CacheService getCacheService();
}