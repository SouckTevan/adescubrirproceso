package co.com.adescubrir.business;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringUtil {

	static ApplicationContext context = null;
	static {
		context = new ClassPathXmlApplicationContext("applicationContext.xml");
	}

	public static Object getBean(String beanName) {
		Object retorno = null;
		retorno = context.getBean(beanName);
		return retorno;
	}
}
